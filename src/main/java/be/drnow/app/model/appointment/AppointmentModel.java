package be.drnow.app.model.appointment;

import be.drnow.app.model.BaseModel;
import be.drnow.app.model.doctor.DoctorModel;
import be.drnow.app.model.patient.PatientDocumentModel;
import be.drnow.app.model.patient.PatientModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class AppointmentModel extends BaseModel {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type = "uuid-char")
    private UUID pk;

    private LocalDateTime startAt;

    private int duration;

    private boolean paid;

    private double price;

    private boolean repayable;

    private String reason;

    private String cancellationReason;

    @Enumerated(EnumType.STRING)
    private AppointmentStatusEnum appointmentStatusEnum;

    private String sessionToken;

    @ManyToOne
    private DoctorModel doctor;

    @ManyToOne
    private PatientModel patient;

    @ManyToOne
    private PatientModel beneficiary;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private PatientDocumentModel patientDocument;

    @PrePersist
    private void generateSessionToken(){
        this.sessionToken = UUID.randomUUID().toString().replace("-", "");
    }

}
