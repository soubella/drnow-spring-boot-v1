package be.drnow.app.model.appointment;

public enum AppointmentStatusEnum {
    NOT_STARTED_YET,
    IN_PROGRESS,
    ENDED,
    CANCELED_BY_PATIENT,
    CANCELED_BY_DOCTOR,
    PATIENT_ABSENT,
    DOCTOR_ABSENT,
    PATIENT_AND_DOCTOR_ARE_ABSENT,
}
