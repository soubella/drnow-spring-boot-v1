package be.drnow.app.model.patient;

import be.drnow.app.model.appointment.AppointmentModel;
import be.drnow.app.model.user.UserModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class PatientModel extends UserModel {

    @Enumerated(EnumType.STRING)
    private PatientRelationshipEnum patientRelationshipEnum;

    @OneToOne(cascade = CascadeType.ALL)
    private MedicalRecordModel medicalRecord;

    private String profession;

    @JsonIgnore
    @OneToMany
    private Set<AppointmentModel> appointment;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL)
    private List<PatientModel> familyMembers;

}
