package be.drnow.app.model.patient;

import be.drnow.app.model.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class MedicalRecordModel extends BaseModel {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type = "uuid-char")
    private UUID pk;

    private float height;

    private float weight;

    private String treatingDoctorFirstName;

    private String treatingDoctorLastName;

    private String treatingDoctorPhone;

    private String treatingDoctorEmail;

    /**
     * Antécédents médicaux
     * char(1) : Y, N, D
     */

    private String cvDiseases; // Avez-vous une ou plusieurs maladies cardiovasculaires (infarctus, AVC) ?

    // Avez-vous des antécédents cardiovasculaires familiaux (infarctus, AVC) ?
    private String cvDiseasesFamily;

    private String cvDiseasesRisk; // Avez-vous un ou plusieurs facteur(s) de risque personnel(s) cardio-vasculaire(s) parmi les suivants ?

    // Êtes-vous asthmatique ?
    private String asthma;

    private String stress; // Vous sentez-vous stressé(e) ?

    private String moral; // Votre moral est-il bon ?

    private String otherPathology; // Informez-nous de toute autre pathologie dont vous êtes, ou avez été porteur.

    private String surgical; // Antécédents chirurgicaux : Veuillez mentionner vos éventuelles interventions chirurgicales

    private String longTermTreatment; // Traitement de longue durée : Si oui, laquelle ou lesquelles ?

    private String drugAllergies; // Allergies et intolérances : Si oui, laquelle ou lesquelles ?

    private String vaccineStatus; // Statut vaccinal (BCG;Coronavirus;...)

    private String insuranceProvider; // Organisme assureur ou mutuelle

}
