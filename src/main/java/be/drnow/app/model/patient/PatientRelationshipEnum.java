package be.drnow.app.model.patient;

public enum PatientRelationshipEnum {
    MAIN_ACCOUNT,
    SON,
    DAUGHTER,
    HUSBAND,
    WIFE,
    OTHER,
}
