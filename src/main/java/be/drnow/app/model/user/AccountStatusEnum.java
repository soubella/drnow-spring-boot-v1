package be.drnow.app.model.user;

public enum AccountStatusEnum {
    MISSING_INFO,
    ACCOUNT_OKAY,
}
