package be.drnow.app.model.user;

import be.drnow.app.model.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

//@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@EqualsAndHashCode(callSuper = false)
public class UserModel extends BaseModel {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type = "uuid-char")
    private UUID pk;

    private String firstName;

    private String lastName;

    @Column(unique = true)
    private String email;

    @JsonIgnore
    private String password;

    private String phoneNumber;

    private String socialNumber; // Numéro de registre national NISS (CIN)

    private Date birthday;

    @Enumerated(EnumType.STRING)
    private GenderEnum gender;

    private String deviceLanguage;

    @Column(columnDefinition = "boolean default false")
    private boolean enabled;

    @Column(columnDefinition = "boolean default false")
    private boolean canceled;

    @Column(columnDefinition = "boolean default false")
    private boolean emailVerified;

    @Enumerated(EnumType.STRING)
    private AccountStatusEnum accountStatus;

    @ManyToOne
    private RoleModel role;

    @OneToOne(cascade = CascadeType.ALL)
    private AddressModel address;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL)
    private Set<NotificationModel> notifications;

    public String getFullName(){
        return firstName + " " + lastName;
    }

}
