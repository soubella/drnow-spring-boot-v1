package be.drnow.app.model.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class AddressModel {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type = "uuid-char")
    private UUID pk;

    private String postalCode;

    private String city;

    private String line1;

    private String line2;

    @ManyToOne(fetch = FetchType.LAZY)
    private CountryModel country;

    public AddressModel(String postalCode, String city) {
        this.postalCode = postalCode;
        this.city = city;
    }
}
