package be.drnow.app.model.user;

public enum GenderEnum {
    MALE,
    FEMALE,
    OTHER
}
