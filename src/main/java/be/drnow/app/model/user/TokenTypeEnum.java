package be.drnow.app.model.user;

public enum TokenTypeEnum {
    FOR_EMAIL_REGISTRATION,
    FOR_SMS_REGISTRATION,
    FOR_EMAIL_FORGET_PASSWORD,
    FOR_SMS_FORGET_PASSWORD,
    FOR_EMAIL_CHANGE_EMAIL
}
