package be.drnow.app.model.user;

import be.drnow.app.model.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class VerificationCodeModel extends BaseModel {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type = "uuid-char")
    private UUID pk;

    private String verificationCode;

    @ManyToOne
    private UserModel userModel;

    @Enumerated(EnumType.STRING)
    private TokenTypeEnum tokenTypemodel;

    private boolean isUsed;

    private Date expirationDate;

    public VerificationCodeModel(UserModel userModel) {
        this.userModel = userModel;
        int number = new Random().nextInt(999999);
        this.verificationCode = String.format("%06d", number);
        this.expirationDate = new Date(System.currentTimeMillis() + 1800000); // + 30 min
    }
}
