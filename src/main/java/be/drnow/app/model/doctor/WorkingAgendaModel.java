package be.drnow.app.model.doctor;

import be.drnow.app.model.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class WorkingAgendaModel extends BaseModel {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type = "uuid-char")
    private UUID pk;

    private LocalDateTime fromDate;

    private LocalDateTime toDate;

    private String fromTime;

    private String toTime;

    private boolean availability;

    @JsonIgnore
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "doctormodel_pk")
    private DoctorModel doctorModel;
}
