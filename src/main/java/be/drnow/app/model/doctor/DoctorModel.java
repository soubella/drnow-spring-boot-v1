package be.drnow.app.model.doctor;


import be.drnow.app.model.user.UserModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = false)
//@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class DoctorModel extends UserModel {

    private String qualificationCode;

    //@Column(unique = true)
    private String INAMI;

    private String bio;

    private float teleconsultationPrice;

    @Column(columnDefinition = "integer default 15")
    private int appointmentDuration;

    private boolean repayable;

    @JsonIgnore
    @Lob
    private byte[] image;

    @Column(columnDefinition = "boolean default false")
    private boolean approved;

    @OneToMany(mappedBy = "doctorModel")
    private List<WorkingAgendaModel> workingAgendas;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<DoctorDocumentModel> doctorDocuments;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<LanguageModel> languages;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<DiplomaModel> diplomas;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<ExperienceModel> experiences;

    @ManyToOne(fetch = FetchType.EAGER)
    private SpecialityModel speciality;

    @ManyToOne(cascade = CascadeType.ALL)
    private ProfessionModel profession;

    @Enumerated(EnumType.STRING)
    private AgreementStatusEnum agreementStatus;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<JobPriceModel> jobPrices;

}
