package be.drnow.app.model.doctor;

public enum AgreementStatusEnum {

    CONVENTIONED,
    PARTIALLY_CONVENTIONED,
    NOT_CONVENTIONED

}
