package be.drnow.app.model.doctor;

import be.drnow.app.model.BaseModel;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@EqualsAndHashCode(callSuper = false)
public class SpecialityModel extends BaseModel {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type = "uuid-char")
    private UUID pk;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Long rank;

    @ManyToOne
    private ProfessionModel professionModel;
}
