package be.drnow.app.config;

import be.drnow.app.core.constant.DoctorylAttributeConstants;
import be.drnow.app.security.AuthEntryPointJwt;
import be.drnow.app.security.AuthTokenFilter;
import be.drnow.app.service.user.impl.DefaultUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
@PropertySource(value = "classpath:application.properties", encoding = DoctorylAttributeConstants.Config.UTF_8_EN_CODE)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final Logger logger = LoggerFactory.getLogger(WebSecurityConfig.class);

    @Autowired
    private DefaultUserService userService;

    @Autowired
    private AuthEntryPointJwt unauthorizedHandler;

    @Autowired
    private Environment environment;

    private static final String ENDPOINTS_CORS_ALLOWED_ORIGINS = "endpoints.cors.allowed-origins";
    private static final String ENDPOINTS_CORS_ALLOWED_METHODS = "endpoints.cors.allowed-methods";
    private static final String ENDPOINTS_CORS_ALLOWED_HEADERS = "endpoints.cors.allowed-headers";

    @Bean
    public AuthTokenFilter authenticationJwtTokenFilter() {
        return new AuthTokenFilter();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests().antMatchers("/api/v1/doctor/auth**").permitAll()
                .antMatchers("/**").permitAll()
                .anyRequest().authenticated();

        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public CorsFilter corsFilter() {
        String path = "/**";
        logger.info("CrossFilter loading..........");
        List<String> origins = Arrays.asList(Objects.requireNonNull(environment
                .getProperty(ENDPOINTS_CORS_ALLOWED_ORIGINS)).split(","));
        List<String> headers = Arrays.asList(Objects.requireNonNull(environment.
                getProperty(ENDPOINTS_CORS_ALLOWED_HEADERS)).split(","));
        List<String> methods = Arrays.asList(Objects.requireNonNull(environment
                .getProperty(ENDPOINTS_CORS_ALLOWED_METHODS)).split(","));

        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        // Don't do this in production, use a proper list  of allowed origins
        config.setAllowedOrigins(origins);
        config.setAllowedHeaders(headers);
        config.setAllowedMethods(methods);
        config.setMaxAge(3600L);
        source.registerCorsConfiguration(path, config);
        logger.info(String.format("Cross start Filter : %s", path));
        return new CorsFilter(source);
    }
}

