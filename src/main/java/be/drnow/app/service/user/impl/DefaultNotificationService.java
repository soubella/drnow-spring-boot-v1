package be.drnow.app.service.user.impl;

import be.drnow.app.exception.RessourceNotFoundException;
import be.drnow.app.model.user.NotificationModel;
import be.drnow.app.repository.user.NotificationRepository;
import be.drnow.app.service.user.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DefaultNotificationService implements NotificationService {

    private NotificationRepository notificationRepository;

    @Override
    public void addOrUpdateNotification(NotificationModel notificationModel) {
        notificationRepository.save(notificationModel);
    }

    @Override
    public List<NotificationModel> findNotificationsByUserPk(UUID userPk) {
        return notificationRepository.findByUserPk(userPk);
    }

    @Override
    public NotificationModel findNotificationByPk(UUID pk) {
        return notificationRepository.findById(pk).orElseThrow(() -> new RessourceNotFoundException("Notification"));
    }

    @Autowired
    public void setNotificationRepository(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }
}
