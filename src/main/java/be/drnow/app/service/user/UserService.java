package be.drnow.app.service.user;

import be.drnow.app.dto.user.*;
import be.drnow.app.model.user.TokenTypeEnum;
import be.drnow.app.model.user.UserModel;
import be.drnow.app.model.user.VerificationCodeModel;

import java.util.UUID;

public interface UserService {
    JwtResponseDTO authenticateUser(LoginRequestDTO loginRequest);

    UserModel findUserByEmail(String email);

    UserModel findUserByPk(UUID pk);

    boolean checkUserByEmail(String email);

    void saveVerificationCode(VerificationCodeModel verificationCode, TokenTypeEnum tokenType);

    void updateUser(UserModel userModel);

    VerificationCodeModel checkVerificationCode(ConfirmationCodeRequestDTO confirmationCodeRequestDTO);

    void resetPassword(NewPasswordRequestDTO newPasswordRequestDTO);

    void changePassword(NewPasswordRequestDTO newPasswordRequestDTO);

    void resetEmail(ChangeEmailDTO changeEmailDTO);

    UserModel getCurrentUser();

    UUID getCurrentUserPk();
}
