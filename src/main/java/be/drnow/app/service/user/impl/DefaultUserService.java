package be.drnow.app.service.user.impl;

import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.user.*;
import be.drnow.app.exception.*;
import be.drnow.app.exception.doctor.DoctorNotApprovedException;
import be.drnow.app.model.user.TokenTypeEnum;
import be.drnow.app.model.user.UserModel;
import be.drnow.app.model.user.VerificationCodeModel;
import be.drnow.app.repository.doctor.DoctorRepository;
import be.drnow.app.repository.user.UserRepository;
import be.drnow.app.repository.user.VerificationCodeRepository;
import be.drnow.app.security.JwtUtils;
import be.drnow.app.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DefaultUserService implements UserDetailsService, UserService {

    @Value("${doctoryl.enable.doctor.freetrial}")
    private boolean freeTrialEnabled;
    @Value("${doctoryl.enable.doctor.email}")
    private boolean nonVerifiedEmailEnabled;

    private static final String DOCTOR_ROLE = "ROLE_DOCTOR";
    private static final String UNKNOWN_ROLE = "ROLE_UNKNOWN";
    private static final String CONFIRMATION_CODE = "Confirmation Code";
    private static final String USER_NOT_FOUND_WITH_EMAIL = "User Not Found with this email";

    private UserRepository userRepository;
    private DoctorRepository doctorRepository;
    private VerificationCodeRepository verificationCodeRepository;
    private AuthenticationManager authenticationManager;
    private JwtUtils jwtUtils;
    private PasswordEncoder encoder;

    @Override
    public UserDetails loadUserByUsername(String email) {
        UserModel user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(USER_NOT_FOUND_WITH_EMAIL + email);
        }
        return UserDetailsDTO.build(user);
    }

    @Override
    public JwtResponseDTO authenticateUser(LoginRequestDTO loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsDTO userDetails = (UserDetailsDTO) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        String role = UNKNOWN_ROLE;

        if (!roles.isEmpty()) {
            role = roles.get(0);
        }

        if (role.equals(DOCTOR_ROLE)) {
            checkDoctorPermissions(userDetails.getId());
        }

        return new JwtResponseDTO(jwt,
                userDetails.getId(),
                userDetails.getEmail(),
                role);
    }

    @Override
    public UserModel findUserByEmail(String email) {
        UserModel userModel = userRepository.findByEmail(email);
        if (userModel == null) {
            throw new UsernameNotFoundException(USER_NOT_FOUND_WITH_EMAIL);
        }
        return userModel;
    }

    @Override
    public UserModel findUserByPk(UUID pk) {
        return userRepository.findById(pk).orElseThrow(() -> new RessourceNotFoundException("USER"));
    }

    @Override
    public boolean checkUserByEmail(String email) {
        return userRepository.findByEmail(email) != null;
    }

    private void checkDoctorPermissions(UUID doctorId) {
        if (!nonVerifiedEmailEnabled && !userRepository.existsByPkAndEnabledTrue(doctorId)) {
            throw new EmailNotConfirmedException();
        } else if (!freeTrialEnabled && !doctorRepository.existsByPkAndApprovedTrue(doctorId)) {
            throw new DoctorNotApprovedException();
        }
    }

    @Override
    public void saveVerificationCode(VerificationCodeModel verificationCode, TokenTypeEnum tokenType) {
        Objects.requireNonNull(verificationCode, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + verificationCode.getClass().getName());
        verificationCode.setTokenTypemodel(tokenType);
        verificationCodeRepository.save(verificationCode);
    }

    @Override
    public VerificationCodeModel checkVerificationCode(ConfirmationCodeRequestDTO confirmationCodeRequestDTO) {
        VerificationCodeModel verificationCodeModel = verificationCodeRepository.findByVerificationCode(confirmationCodeRequestDTO.getConfirmationCode());
        if (verificationCodeModel == null
                || !verificationCodeModel.getUserModel().getEmail().equals(confirmationCodeRequestDTO.getEmail())
                || !verificationCodeModel.getTokenTypemodel().toString().equals(confirmationCodeRequestDTO.getTokenType())
        ) {
            throw new InputNotValidException(CONFIRMATION_CODE);
        } else if (verificationCodeModel.getExpirationDate().before(new Date(System.currentTimeMillis()))) {
            throw new VerificationCodeExpiredException();
        }
        return verificationCodeModel;
    }

    @Override
    public void resetPassword(NewPasswordRequestDTO newPasswordRequestDTO) {
        VerificationCodeModel verificationCodeModel = verificationCodeRepository.findByVerificationCode(newPasswordRequestDTO.getConfirmationCode());
        if (verificationCodeModel == null || !verificationCodeModel.getUserModel().getEmail().equals(newPasswordRequestDTO.getEmail())) {
            throw new InputNotValidException(CONFIRMATION_CODE);
        } else if (verificationCodeModel.isUsed()) {
            throw new ResourceAlreadyInUseException(CONFIRMATION_CODE);
        }
        UserModel userModel = findUserByEmail(newPasswordRequestDTO.getEmail());
        userModel.setPassword(encoder.encode(newPasswordRequestDTO.getNewPassword()));
        userRepository.save(userModel);
        verificationCodeModel.setUsed(true);
        verificationCodeRepository.save(verificationCodeModel);
    }

    @Override
    public void changePassword(NewPasswordRequestDTO newPasswordRequestDTO) {
        UserModel userModel = findUserByEmail(newPasswordRequestDTO.getEmail());
        if (!encoder.matches(newPasswordRequestDTO.getCurrentPassword(), userModel.getPassword())) {
            throw new InputNotValidException("Password");
        }
        userModel.setPassword(encoder.encode(newPasswordRequestDTO.getNewPassword()));
        userRepository.save(userModel);
    }

    @Override
    public void resetEmail(ChangeEmailDTO changeEmailDTO) {
        VerificationCodeModel verificationCodeModel = verificationCodeRepository.findByVerificationCode(changeEmailDTO.getConfirmationCode());
        if (verificationCodeModel == null || !verificationCodeModel.getUserModel().getEmail().equals(changeEmailDTO.getOldEmail())) {
            throw new InputNotValidException(CONFIRMATION_CODE);
        } else if (verificationCodeModel.isUsed()) {
            throw new ResourceAlreadyInUseException(CONFIRMATION_CODE);
        }
        if (userRepository.existsByEmail(changeEmailDTO.getNewEmail())) {
            throw new ResourceAlreadyInUseException("Email");
        }
        UserModel userModel = findUserByEmail(changeEmailDTO.getOldEmail());
        userModel.setEmail(changeEmailDTO.getNewEmail());
        userRepository.save(userModel);
        verificationCodeModel.setUsed(true);
        verificationCodeRepository.save(verificationCodeModel);
    }

    @Override
    public void updateUser(UserModel userModel) {
        userRepository.save(userModel);
    }

    @Override
    public UserModel getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth.getPrincipal().toString().equals("anonymousUser")) return null;
        return findUserByPk(((UserDetailsDTO) auth.getPrincipal()).getId());
    }

    @Override
    public UUID getCurrentUserPk() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getPrincipal().toString().equals("anonymousUser")) return UUID.randomUUID();
        return ((UserDetailsDTO) auth.getPrincipal()).getId();
    }

    @Autowired
    public void setVerificationCodeRepository(VerificationCodeRepository verificationCodeRepository) {
        this.verificationCodeRepository = verificationCodeRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setDoctorRepository(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Autowired
    public void setJwtUtils(JwtUtils jwtUtils) {
        this.jwtUtils = jwtUtils;
    }

    @Autowired
    public void setEncoder(PasswordEncoder encoder) {
        this.encoder = encoder;
    }
}
