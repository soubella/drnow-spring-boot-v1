package be.drnow.app.service.user;

import be.drnow.app.model.user.NotificationModel;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface NotificationService {
    List<NotificationModel> findNotificationsByUserPk(UUID userPk);

    void addOrUpdateNotification(NotificationModel notificationModel);

    NotificationModel findNotificationByPk(UUID pk);

}
