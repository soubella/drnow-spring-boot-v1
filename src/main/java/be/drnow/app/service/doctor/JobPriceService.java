package be.drnow.app.service.doctor;

import be.drnow.app.model.doctor.JobPriceModel;

import java.util.List;
import java.util.UUID;

public interface JobPriceService {
    void addOrUpdateJobPrice(JobPriceModel jobPriceModel);

    List<JobPriceModel> getAllJobPricesByDoctorPk(UUID doctorPk);

    void deleteJobPriceByPk(UUID jobPricePk);
}
