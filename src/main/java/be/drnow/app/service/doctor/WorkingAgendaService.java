package be.drnow.app.service.doctor;

import be.drnow.app.model.doctor.WorkingAgendaModel;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

public interface WorkingAgendaService {

    void addOrUpdateWorkingAgenda(WorkingAgendaModel workingAgendaModel);

    void deleteWorkingAgendaByPk(UUID pk);

    Set<WorkingAgendaModel> findAvailableWorkingAgendas(UUID doctorPk);

    Set<WorkingAgendaModel> findUnAvailableWorkingAgendas(UUID doctorPk);

    boolean isAvailable(LocalDateTime time, UUID doctorPk);
}
