package be.drnow.app.service.doctor.impl;

import be.drnow.app.model.doctor.WorkingAgendaModel;
import be.drnow.app.repository.doctor.WorkingAgendaRepository;
import be.drnow.app.service.doctor.WorkingAgendaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.UUID;

@Service
public class DefaultWorkingAgendaService implements WorkingAgendaService {

    private WorkingAgendaRepository workingAgendaRepository;

    @Override
    public void addOrUpdateWorkingAgenda(WorkingAgendaModel workingAgendaModel) {
        workingAgendaRepository.save(workingAgendaModel);
    }

    @Override
    public void deleteWorkingAgendaByPk(UUID pk) {
        workingAgendaRepository.deleteById(pk);
    }

    @Override
    public Set<WorkingAgendaModel> findAvailableWorkingAgendas(UUID doctorPk) {
        return workingAgendaRepository.findByAvailabilityTrueAndDoctorModelPk(doctorPk);
    }

    @Override
    public Set<WorkingAgendaModel> findUnAvailableWorkingAgendas(UUID doctorPk) {
        return workingAgendaRepository.findByAvailabilityFalseAndDoctorModelPk(doctorPk);
    }

    @Override
    public boolean isAvailable(LocalDateTime date, UUID doctorPk) {

//        String time = date.toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm"));
//        date = date.minusMinutes(date.getMinute());
//        date = date.minusHours(date.getHour());

//        boolean result1 = workingAgendaRepository.checkAvailability(date, true, doctorPk) > 0;
//        boolean result2 = workingAgendaRepository.checkAvailability(date, false, doctorPk) == 0;

        return workingAgendaRepository.checkAvailability(date, true, doctorPk) > 0
                && workingAgendaRepository.checkAvailability(date, false, doctorPk) == 0;
    }

    @Autowired
    public void setWorkingAgendaRepository(WorkingAgendaRepository workingAgendaRepository) {
        this.workingAgendaRepository = workingAgendaRepository;
    }
}
