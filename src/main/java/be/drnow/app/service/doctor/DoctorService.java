package be.drnow.app.service.doctor;

import be.drnow.app.dto.doctor.form.DoctorRegisterForm;
import be.drnow.app.dto.doctor.response.DoctorSearchResponseDTO;
import be.drnow.app.model.doctor.DoctorModel;
import be.drnow.app.model.user.VerificationCodeModel;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface DoctorService {
    void register(DoctorRegisterForm doctorRegisterForm);

    void confirmAccount(VerificationCodeModel verificationCode);

    Set<DoctorSearchResponseDTO> search(final String search, final String address);

    DoctorModel findByPk(UUID pk);

    void updateProfile(DoctorModel doctorModel);

    void updateImage(UUID pk, MultipartFile file);

    List<DoctorModel> searchForDoctors(String keyword, Pageable pageable);

    byte[] downloadImage(UUID pk);

    List<DoctorModel> findAllDoctors(Pageable pageable);
}
