package be.drnow.app.service.doctor.impl;

import be.drnow.app.converter.Converter;
import be.drnow.app.dto.doctor.form.DoctorRegisterForm;
import be.drnow.app.dto.doctor.response.DoctorSearchResponseDTO;
import be.drnow.app.exception.FileNotFoundException;
import be.drnow.app.exception.ResourceAlreadyInUseException;
import be.drnow.app.exception.RessourceNotFoundException;
import be.drnow.app.helper.ImageHelper;
import be.drnow.app.model.doctor.DoctorModel;
import be.drnow.app.model.user.AccountStatusEnum;
import be.drnow.app.model.user.VerificationCodeModel;
import be.drnow.app.repository.doctor.DoctorRepository;
import be.drnow.app.repository.user.VerificationCodeRepository;
import be.drnow.app.service.doctor.DoctorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class DefaultDoctorService implements DoctorService {

    private static final Logger logger = LoggerFactory.getLogger(DefaultDoctorService.class);
    private static final String DOCTOR = "Doctor";

    private DoctorRepository doctorRepository;
    private VerificationCodeRepository verificationCodeRepository;

    private Converter<DoctorRegisterForm, DoctorModel> doctorRegisterDtoToModel;
    private Converter<Set<DoctorModel>, Set<DoctorSearchResponseDTO>> doctorModelConvertAll;

    @Override
    public void register(DoctorRegisterForm doctorRegisterForm) {
        if (doctorRepository.existsByEmail(doctorRegisterForm.getEmail())) {
            throw new ResourceAlreadyInUseException("Email");
        }
//        if (doctorRepository.existsByINAMI(doctorRegisterForm.getINAMI())) {
//            throw new ResourceAlreadyInUseException("INAMI");
//        }
//        if (doctorRepository.existsByQualificationCode(doctorRegisterForm.getQualificationCode())) {
//            throw new ResourceAlreadyInUseException("Qualification Code");
//        }
        DoctorModel doctorModel = new DoctorModel();
        doctorModel.setAccountStatus(AccountStatusEnum.MISSING_INFO);
        doctorRegisterDtoToModel.convert(doctorRegisterForm, doctorModel);
        doctorRepository.save(doctorModel);
    }

    @Override
    public void confirmAccount(VerificationCodeModel verificationCode) {
        DoctorModel doctorModel = doctorRepository.findByEmail(verificationCode.getUserModel().getEmail());
        doctorModel.setEmailVerified(true);
        doctorModel.setEnabled(true);
        doctorRepository.save(doctorModel);
        verificationCode.setUsed(true);
        verificationCodeRepository.save(verificationCode);
    }

    @Override
    @Transactional
    public Set<DoctorSearchResponseDTO> search(final String value, final String address) {
        Set<DoctorSearchResponseDTO> response = new HashSet<>();
        Set<DoctorModel> doctorModels;
        if (!StringUtils.isEmpty(value)) {
            doctorModels = new HashSet<>(doctorRepository.
                    findDoctorModelByFirstNameContainsOrLastNameContainsOrProfessionTitleContainsOrSpecialityNameContains(value, value, value, value));
        } else {
            doctorModels = new HashSet<>(doctorRepository.findDoctorModelByAddressCityContains(address));
        }
        doctorModelConvertAll.convert(doctorModels, response);
        return response;
    }

    @Override
    public DoctorModel findByPk(UUID pk) {
        return doctorRepository.findById(pk).orElseThrow(() -> new RessourceNotFoundException(DOCTOR));
    }

    @Override
    public void updateProfile(DoctorModel doctorModel) {
        doctorRepository.save(doctorModel);
    }

    @Override
    public void updateImage(UUID pk, MultipartFile file) {
        DoctorModel doctorModel = doctorRepository.findById(pk).orElseThrow(() -> new RessourceNotFoundException(DOCTOR));
        try {
            doctorModel.setImage(ImageHelper.compressBytes(file.getBytes()));
            doctorRepository.save(doctorModel);
        } catch (IOException e) {
            logger.info(e.getMessage());
        }
    }

    @Override
    public byte[] downloadImage(UUID pk) {
        DoctorModel doctorModel = doctorRepository.findById(pk).orElseThrow(() -> new RessourceNotFoundException(DOCTOR));
        if (doctorModel.getImage() == null) throw new FileNotFoundException("Image");
        return ImageHelper.decompressBytes(doctorModel.getImage());
    }

    @Override
    public List<DoctorModel> searchForDoctors(String keyword, Pageable pageable) {
        return doctorRepository.searchForDoctors(keyword, pageable);
    }

    @Override
    public List<DoctorModel> findAllDoctors(Pageable pageable) {
        return doctorRepository.findAllDoctors(pageable).getContent();
    }

    @Autowired
    public void setDoctorRepository(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    @Autowired
    public void setDoctorRegisterDtoToModel(Converter<DoctorRegisterForm, DoctorModel> doctorRegisterDtoToModel) {
        this.doctorRegisterDtoToModel = doctorRegisterDtoToModel;
    }

    @Qualifier("DoctorSearchResponseConverterList")
    @Autowired
    public void setDoctorModelConvertAll(Converter<Set<DoctorModel>, Set<DoctorSearchResponseDTO>> doctorModelConvertAll) {
        this.doctorModelConvertAll = doctorModelConvertAll;
    }

    @Autowired
    public void setVerificationCodeRepository(VerificationCodeRepository verificationCodeRepository) {
        this.verificationCodeRepository = verificationCodeRepository;
    }
}

