package be.drnow.app.service.doctor.impl;

import be.drnow.app.model.doctor.JobPriceModel;
import be.drnow.app.repository.doctor.JobPriceRepository;
import be.drnow.app.service.doctor.JobPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DefaultJobPriceService implements JobPriceService {

    private JobPriceRepository jobPriceRepository;

    @Override
    public void addOrUpdateJobPrice(JobPriceModel jobPriceModel) {
        jobPriceRepository.save(jobPriceModel);
    }

    @Override
    public void deleteJobPriceByPk(UUID pk) {
        jobPriceRepository.deleteById(pk);
    }

    @Override
    public List<JobPriceModel> getAllJobPricesByDoctorPk(UUID doctorPk) {
        return jobPriceRepository.findAllByDoctorPk(doctorPk);
    }

    @Autowired
    public void setJobPriceRepository(JobPriceRepository jobPriceRepository) {
        this.jobPriceRepository = jobPriceRepository;
    }
}
