package be.drnow.app.service.shared;

import be.drnow.app.dto.shared.MailDTO;

import javax.mail.MessagingException;

public interface EmailService {
    void sendEmail(MailDTO mail, String template) throws MessagingException;
}
