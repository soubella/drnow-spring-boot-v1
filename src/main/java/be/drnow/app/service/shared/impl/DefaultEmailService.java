package be.drnow.app.service.shared.impl;

import be.drnow.app.dto.shared.MailDTO;
import be.drnow.app.service.shared.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Locale;

@Service
public class DefaultEmailService implements EmailService {

    private final Logger logger = LoggerFactory.getLogger(DefaultEmailService.class);

    @Value("${spring.mail.username}")
    private String fromEmail;

    private JavaMailSender emailSender;
    private SpringTemplateEngine templateEngine;

    @Async
    @Override
    public void sendEmail(MailDTO mailDTO, String template) throws MessagingException {

        logger.info("START... Sending email");

        MimeMessage message = emailSender.createMimeMessage();
        //MimeMessageHelper helper = new MimeMessageHelper(message, false, StandardCharsets.UTF_8.name());

        Context context = new Context();
        context.setLocale(Locale.FRENCH);
        context.setVariables(mailDTO.getProps());
        String html = templateEngine.process(template, context);

        //helper.setTo(mailDTO.getMailTo());
        //helper.setText(html, true);
        //helper.setSubject(mailDTO.getSubject());
        //helper.setFrom(fromEmail);
        message.setSubject(mailDTO.getSubject(), "utf-8");
        message.setHeader("Content-Type", "text/html; charset=utf-8");
        message.setContent(html, "text/html; charset=utf-8");
        message.setFrom(fromEmail);
        message.setRecipients(Message.RecipientType.TO, mailDTO.getMailTo());
        emailSender.send(message);
;
        logger.info("END... Email sent success");
    }

    @Autowired
    public void setEmailSender(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Autowired
    public void setTemplateEngine(SpringTemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }
}
