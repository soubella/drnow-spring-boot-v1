package be.drnow.app.service.appointment;

import be.drnow.app.model.appointment.AppointmentModel;
import be.drnow.app.model.appointment.AppointmentStatusEnum;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface AppointmentService {
    void takeOrUpdateAppointment(AppointmentModel appointmentModel);

    Set<AppointmentModel> findAppointmentsByDoctorPk(UUID doctorPk, List<AppointmentStatusEnum> appointmentStatusEnums);

    AppointmentModel findAppointmentByPk(UUID pk);

    boolean isAvailable(UUID doctorPk, LocalDateTime time);

    Set<AppointmentModel> findAppointmentsByPatientPk(UUID patientPk);
}
