package be.drnow.app.service.appointment.impl;

import be.drnow.app.exception.RessourceNotFoundException;
import be.drnow.app.model.appointment.AppointmentModel;
import be.drnow.app.model.appointment.AppointmentStatusEnum;
import be.drnow.app.repository.appointment.AppointmentRepository;
import be.drnow.app.service.appointment.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class DefaultAppointmentService implements AppointmentService {

    private AppointmentRepository appointmentRepository;

    @Override
    public void takeOrUpdateAppointment(AppointmentModel appointmentModel) {
        appointmentRepository.save(appointmentModel);
    }

    @Override
    public AppointmentModel findAppointmentByPk(UUID pk) {
        return appointmentRepository.findById(pk).orElseThrow(() -> new RessourceNotFoundException("Appointment"));
    }

    @Override
    public Set<AppointmentModel> findAppointmentsByDoctorPk(UUID doctorPk, List<AppointmentStatusEnum> appointmentStatusEnums) {
        if (appointmentStatusEnums != null)
            return appointmentRepository.findByDoctorPkAndAppointmentStatusEnumNotIn(doctorPk, appointmentStatusEnums);
        return appointmentRepository.findByDoctorPk(doctorPk);
    }

    @Override
    public boolean isAvailable(UUID doctorPk, LocalDateTime time) {
        return !(time.getMinute() % 5 != 0 || appointmentRepository.existsByStartAtAndDoctorPk(time, doctorPk));
    }

    @Override
    public Set<AppointmentModel> findAppointmentsByPatientPk(UUID patientPk) {
        return appointmentRepository.findByPatientPk(patientPk);
    }

    @Autowired
    public void setAppointmentRepository(AppointmentRepository appointmentRepository) {
        this.appointmentRepository = appointmentRepository;
    }
}
