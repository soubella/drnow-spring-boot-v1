package be.drnow.app.service.patient.impl;

import be.drnow.app.exception.ResourceAlreadyInUseException;
import be.drnow.app.exception.RessourceNotFoundException;
import be.drnow.app.model.patient.MedicalRecordModel;
import be.drnow.app.model.patient.PatientModel;
import be.drnow.app.model.user.RoleModel;
import be.drnow.app.repository.appointment.AppointmentRepository;
import be.drnow.app.repository.patient.PatientRepository;
import be.drnow.app.repository.user.RoleRepository;
import be.drnow.app.service.patient.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DefaultPatientService implements PatientService {

    private PatientRepository patientRepository;
    private RoleRepository roleRepository;

    @Override
    public PatientModel registerPatient(PatientModel patientModel) {
        if (patientRepository.existsByEmail(patientModel.getEmail())) {
            throw new ResourceAlreadyInUseException("Email");
        }

        if(patientModel.getRole() == null){
            patientModel.setRole(roleRepository.findByName("ROLE_PATIENT"));
        }

        return patientRepository.save(patientModel);
    }

    @Override
    public PatientModel findPatientByPk(UUID patientPk) {
        return patientRepository.findById(patientPk).orElseThrow(() -> new RessourceNotFoundException("Patient"));
    }

    @Override
    public void addMedicalRecordToPatient(MedicalRecordModel medicalRecordModel, PatientModel patientModel) {
        patientModel.setMedicalRecord(medicalRecordModel);
        patientRepository.save(patientModel);
    }

    @Override
    public PatientModel addBeneficiaryToPatient(PatientModel patientModel, PatientModel beneficiaryModel) {
        PatientModel savedBeneficiary = patientRepository.save(beneficiaryModel);
        patientModel.getFamilyMembers().add(savedBeneficiary);
        patientRepository.save(patientModel);
        return savedBeneficiary;
    }

    @Override
    public PatientModel findPatientByEmail(String email) {
        return patientRepository.findByEmail(email).orElseThrow(() -> new RessourceNotFoundException("Patient"));
    }

    @Override
    public void updatePatientProfile(PatientModel patientModel) {
        patientRepository.save(patientModel);
    }

    @Override
    public List<PatientModel> findPatientsByDoctorPk(UUID pk) {
        return patientRepository.findPatientsByDoctorPk(pk);
    }

    @Autowired
    public void setPatientRepository(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    @Autowired
    public void setRoleRepository(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }
}
