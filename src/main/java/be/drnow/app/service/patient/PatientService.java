package be.drnow.app.service.patient;

import be.drnow.app.model.patient.MedicalRecordModel;
import be.drnow.app.model.patient.PatientModel;

import java.util.List;
import java.util.UUID;

public interface PatientService {
    PatientModel registerPatient(PatientModel patientModel);

    PatientModel findPatientByPk(UUID patientPk);

    void addMedicalRecordToPatient(MedicalRecordModel medicalRecordModel, PatientModel patientModel);

    PatientModel findPatientByEmail(String email);

    void updatePatientProfile(PatientModel patientModel);

    PatientModel addBeneficiaryToPatient(PatientModel patientModel, PatientModel beneficiaryModel);

    List<PatientModel> findPatientsByDoctorPk(UUID pk);
}
