package be.drnow.app.core.constant;

/**
 * @author : medalaoui
 * @since : 13/05/2020, mer.
 **/
public final class DoctorylMessagesConstants {

    public interface Error { // NOSONAR
        String OBJECT_NOT_NULL = "Object cannot be null : ";
        String OBJECT_NOT_EMPTY = "Object cannot be empty : ";
    }

    public interface Logger { // NOSONAR
        String START_LOAD = "load start";
        String END_LOAD = "load end successfully";
        String DATA_IS_LOADED = "data is already loaded";
    }

    public interface Exception { // NOSONAR - These codes (_XX) are used in angular translat service to identify the correspondent message
        String RESOURCE_IS_ALREADY_IN_USE = "%s is already in use";
        String INPUT_IS_NOT_VALID = "%s is not valid";
        String ACCOUNT_NOT_APPROVED = "Your account is not Approved yet";
        String EMAIL_NOT_CONFIRMED = "Your email is not confirmed";
        String ACCOUNT_DOES_NOT_EXIST_WITH_THIS_EMAIL = "Account does not exist with this email";
        String RESSOURCE_NOT_FOUND = " %s not found";
        String VERIFICATION_CODE_EXPIRED = "your verification code expired";
    }

    public interface Succes { // NOSONAR
        String USER_REGISTERED_SUCCESSFULLY = "User registered successfully!";
        String ACCOUNT_CONFIRMED_SUCCESSFULLY = "Account confirmed successfully!";
        String ACCOUNT_HAS_BEEN_VERIFIED_SUCCESSFULLY = "Account has been verified successfully";
        String JOB_HAS_BEEN_ADDED_SUCCESSFULLY = "Job has been added successfully";
        String JOB_HAS_BEEN_UPDATED_SUCCESSFULLY = "Job has been updated successfully";
        String JOB_HAS_BEEN_DELETED_SUCCESSFULLY = "Job has been deleted successfully";
        String OBJECT_HAS_BEEN_ADDED_SUCCESSFULLY = "%s has been added successfully";
        String OBJECT_HAS_BEEN_UPDATED_SUCCESSFULLY = "%s has been updated successfully";
        String OBJECT_HAS_BEEN_DELETED_SUCCESSFULLY = "%s has been deleted successfully";
        String OBJECT_HAS_BEEN_CANCELED_SUCCESSFULLY = "%s has been canceled successfully";
        String CONFIRMATION_CODE_HAS_BEEN_SENT_TO_YOUR_EMAIL = "Confirmation Code has been sent to your email";
        String CONFIRMATION_OK = "Confirmation Code has been sent verified successfully";
        String PASSWORD_HAS_BEEN_CHANGED = "Your password has been changed successfully";
        String APPOINTMENT_HAS_BEEN_TAKEN_SUCCESSFULLY = "Appointment has been taken successfully";
        String MEDICAL_RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY = "Medical record has been updated successfully";
        String YOUR_EMAIL_HAS_BEEN_CHANGED_SUCCESSFULLY = "Your email has been changed successfully";
        String YOUR_SECURITY_NUMBER_HAS_BEEN_ADDED_SUCCESSFULLY = "Your security number has been added successfully";
    }

    public interface Email { // NOSONAR
        String SUBJECT_FOR_REGISTRATION_VERIFICATION_MAIL = "Doctoryl - Confirmation Account";
        String SUBJECT_FOR_FORGET_PASSWORD_VERIFICATION_MAIL = "Doctoryl - Change your password";
        String SUBJECT_FOR_CHANGE_EMAIL_VERIFICATION_MAIL = "Doctoryl - Change your Email";
        String HEADER_EMAIL_VERIFICATION = "Bonjour : %s ";
        String BODY_EMAIL_VERIFICATION = " Votre code d'identification est: ";
        String REGISTRATION_VERIFICATION_MAIL_TEMPLATE = "verification-email-template";
        String DOCTOR_WELCOME_MAIL_TEMPLATE = "doctor-welcome-template";
        String NOTIFICATION_MAIL_TEMPLATE = "notification-email-template";
    }
}
