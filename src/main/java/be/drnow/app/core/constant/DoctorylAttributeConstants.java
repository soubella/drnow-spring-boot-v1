package be.drnow.app.core.constant;

/**
 * @author : medalaoui
 * @since : 15/05/2020, ven.
 **/
public class DoctorylAttributeConstants { // NOSONAR

    public static final class Config { // NOSONAR
        public static final String API_VERSION_1 = "DOCTORYL-API-VERSION=1";
        public static final String API_VERSION_2 = "DOCTORYL-API-VERSION=2";
        public static final String UTF_8_EN_CODE = "UTF-8";
    }

    public static final class Regex { // NOSONAR
        public static final String REGEX_INAMI_NUMBERS = "^[0-9]{8}$";
    }
}
