package be.drnow.app.core.validator.doctor;


import be.drnow.app.core.constant.DoctorylAttributeConstants;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.exception.WSException;
import be.drnow.app.exception.doctor.INAMIFormatNotCorrectException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author : medalaoui
 * @since : 29/05/2020, ven.
 **/

@Component
public class DoctorRegisterValidator {

    static Logger logger = LoggerFactory.getLogger(DoctorRegisterValidator.class);
    static Environment environment;
    private static final String GOV_URL_PROPERTIES = "web.site.ondpanon.riziv.fgov.be";
    private static final String DOM_ELEMENT = "div";
    private static final char ONE = '1';

    public static boolean INAMIValidator(String INAMI) throws INAMIFormatNotCorrectException, IOException {
        boolean validate = false;
        Objects.requireNonNull(INAMI, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + "INAMI");

        if (!Pattern.compile(DoctorylAttributeConstants.Regex.REGEX_INAMI_NUMBERS).matcher(INAMI).matches()) {
            logger.error(INAMIFormatNotCorrectException.class.getName().concat(INAMI));
            throw new INAMIFormatNotCorrectException(INAMI); // dont match regex
        }

        String URL = Objects.requireNonNull(environment.getProperty(GOV_URL_PROPERTIES)).concat(INAMI);
        Connection.Response response = Jsoup.connect(URL).
                method(Connection.Method.GET).timeout(30 * 1000).execute();

        if (response.statusCode() == HttpStatus.OK.value()) {
            validate = response.parse().select(DOM_ELEMENT).first().text().charAt(0) == ONE;
        } else {
            throw new WSException(URL);
        }
        return validate;
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        DoctorRegisterValidator.environment = environment;
    }
}
