package be.drnow.app.core.annotation;

import be.drnow.app.core.constant.DoctorylAttributeConstants;
import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.*;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author : medalaoui
 * @since : 23/05/2020, sam.
 **/

@Target(TYPE)
@Retention(RUNTIME)
@RequestMapping
@RestController
public @interface DoctorylController {

    @AliasFor(annotation = RequestMapping.class, attribute = "name")
    String name() default "";

    @AliasFor(annotation = RequestMapping.class, attribute = "value")
    String[] value() default {};

    @AliasFor(annotation = RequestMapping.class, attribute = "path")
    String[] path() default {};

    @AliasFor(annotation = RequestMapping.class, attribute = "method")
    RequestMethod[] method() default {};

    @AliasFor(annotation = RequestMapping.class, attribute = "params")
    String[] params() default {};

    //@AliasFor(annotation = RequestMapping.class, value = "headers")
    //String[] headers() default {DoctorylAttributeConstants.Config.API_VERSION_1};

    @AliasFor(annotation = RequestMapping.class, attribute = "consumes")
    String[] consumes() default {};

    @AliasFor(annotation = RequestMapping.class, attribute = "produces")
    String[] produces() default {};

}
