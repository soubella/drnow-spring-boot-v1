package be.drnow.app.core.initialdata;


import be.drnow.app.core.constant.DoctorylAttributeConstants;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.model.doctor.ProfessionModel;
import be.drnow.app.model.doctor.SpecialityModel;
import be.drnow.app.repository.doctor.ProfessionRepository;
import be.drnow.app.repository.doctor.SpecialityRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author : medalaoui
 * @since : 15/05/2020, ven.
 **/

@PropertySource(value = "classpath:dataSource.properties", encoding = DoctorylAttributeConstants.Config.UTF_8_EN_CODE)
//@Configuration
//@DependsOn("professionDataBaseInitializer")
public class DoctorSpecialityLoader {

    private Environment environment;

    Logger logger = LoggerFactory.getLogger(DoctorSpecialityLoader.class);
    private static final String DOCTOR_SPECIALITY_LOADER = DoctorProfessionLoader.class.getName();
    private static final String SPECIALITY_DATA_PROPERTIES = "initializer.database.data.speciality";


    @Bean
    public ApplicationRunner specialityDataBaseInitializer(SpecialityRepository specialityRepository,
                                                           ProfessionRepository professionRepository) throws JsonProcessingException {

        List<ProfessionModel> professionModels = professionRepository.findAll();
        String specialityObjectJsonArray = this.environment.getProperty(SPECIALITY_DATA_PROPERTIES);

        if (specialityRepository.count() == 0) {
            logger.info(DoctorylMessagesConstants.Logger.START_LOAD);
            Assert.notNull(specialityObjectJsonArray, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL
                    + DOCTOR_SPECIALITY_LOADER);


            List<SpecialityModel> specialityModels = new ArrayList<>();
            ObjectMapper objectMapper = new ObjectMapper();

            SpecialtyData[] specialtyDataList = objectMapper.readValue(specialityObjectJsonArray,
                    SpecialtyData[].class);

            professionSpecialityAttachment(professionModels, specialityModels, specialtyDataList);

            return args -> {
                specialityRepository.saveAll(specialityModels);
                logger.info(DoctorylMessagesConstants.Logger.END_LOAD);
            };
        } else {
            return args -> logger.info(DoctorylMessagesConstants.Logger.DATA_IS_LOADED);
        }
    }

    private void professionSpecialityAttachment(List<ProfessionModel> professionModels,
                                                List<SpecialityModel> specialityModels,
                                                SpecialtyData[] specialtyDataList) {
        Arrays.stream(specialtyDataList).forEach(specialtyData -> {
            professionModels.forEach(professionModel -> {
                if (specialtyData.profession.equalsIgnoreCase(professionModel.getTitle())) {
                    specialityModels.add(SpecialityModel.builder()
                            .name(specialtyData.speciality)
                            .professionModel(professionModel).build());
                }
            });
        });
    }

    private static class SpecialtyData {
        private String speciality;
        private String profession;

        public String getSpeciality() {
            return speciality;
        }

        public String getProfession() {
            return profession;
        }

        public void setSpeciality(String speciality) {
            this.speciality = speciality;
        }

        public void setProfession(String profession) {
            this.profession = profession;
        }
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
