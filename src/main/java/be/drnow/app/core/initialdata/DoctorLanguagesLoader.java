package be.drnow.app.core.initialdata;

import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.model.doctor.LanguageModel;
import be.drnow.app.repository.doctor.DoctorLanguagesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * @author : medalaoui
 * @since : 17/05/2020, dim.
 **/

//@Configuration
public class DoctorLanguagesLoader {

    Logger logger = LoggerFactory.getLogger(DoctorLanguagesLoader.class);


    @Bean
    public ApplicationRunner languagesDataBaseInitializer(DoctorLanguagesRepository doctorLanguagesRepository) {
        if (doctorLanguagesRepository.findAll().isEmpty()) {
            logger.info(DoctorylMessagesConstants.Logger.START_LOAD);

            List<LanguageModel> languageModels = new ArrayList<>();

            Arrays.stream(Locale.getAvailableLocales()).forEach(locale -> {
                languageModels.add(LanguageModel.builder()
                        .name(locale.getDisplayName())
                        .code(locale.getISO3Language())
                        .build());
            });
            return args -> {
                doctorLanguagesRepository.saveAll(languageModels);
                logger.info(DoctorylMessagesConstants.Logger.END_LOAD);
            };
        } else {
            return args -> logger.info(DoctorylMessagesConstants.Logger.DATA_IS_LOADED);
        }
    }


}
