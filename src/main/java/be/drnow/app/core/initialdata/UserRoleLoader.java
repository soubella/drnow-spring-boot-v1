package be.drnow.app.core.initialdata;


import be.drnow.app.core.constant.DoctorylAttributeConstants;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.model.user.RoleModel;
import be.drnow.app.repository.user.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author : medalaoui
 * @since : 15/05/2020, ven.
 **/
@Configuration
@PropertySource(value = "classpath:dataSource.properties", encoding = DoctorylAttributeConstants.Config.UTF_8_EN_CODE)
public class UserRoleLoader {

    private Environment environment;

    Logger logger = LoggerFactory.getLogger(UserRoleLoader.class);
    private static final String ROLES_DATA_PROPERTIES = "initializer.database.data.roles";
    private static final String USER_ROLE_LOADER = UserRoleLoader.class.getName();

    @Bean
    public ApplicationRunner rolesDataBaseInitializer(RoleRepository roleRepository) {
        String specialityData = this.environment.getProperty(ROLES_DATA_PROPERTIES);

        if (roleRepository.count() == 0) {
            logger.info(DoctorylMessagesConstants.Logger.START_LOAD);
            Assert.notNull(specialityData, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + USER_ROLE_LOADER);
            List<RoleModel> roleModels = new ArrayList<>();
            Arrays.stream(specialityData.split(",")).forEach(role ->
                    roleModels.add(RoleModel.builder().name(role).build())
            );
            return args -> {
                roleRepository.saveAll(roleModels);
                logger.info(DoctorylMessagesConstants.Logger.END_LOAD);
            };
        } else {
            return args -> logger.info(DoctorylMessagesConstants.Logger.DATA_IS_LOADED);
        }
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
