package be.drnow.app.core.initialdata;

import be.drnow.app.core.constant.DoctorylAttributeConstants;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.model.doctor.ProfessionModel;
import be.drnow.app.repository.doctor.ProfessionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author : medalaoui
 * @since : 15/05/2020, ven.
 **/

//@PropertySource(value = "classpath:dataSource.properties", encoding = DoctorylAttributeConstants.Config.UTF_8_EN_CODE)
//@Configuration
public class DoctorProfessionLoader {

    private Logger logger = LoggerFactory.getLogger(DoctorProfessionLoader.class);

    private static final String PROFESSION_DATA_PROPERTIES = "initializer.database.data.profession";
    private static final String DOCTOR_PROFESSION_LOADER = DoctorProfessionLoader.class.getName();

    private Environment environment;

    @Bean(value = "professionDataBaseInitializer")
    public ApplicationRunner professionDataBaseInitializer(ProfessionRepository professionRepository) {
        if (professionRepository.findAll().isEmpty()) {
            logger.info(DoctorylMessagesConstants.Logger.START_LOAD);

            String dataProfession = this.environment.getProperty(PROFESSION_DATA_PROPERTIES);

            Assert.notNull(dataProfession, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL +
                    DOCTOR_PROFESSION_LOADER);

            String[] professions = dataProfession.split(",");
            List<ProfessionModel> professionModels = new ArrayList<>();

            Arrays.stream(professions).forEach(s ->
                    professionModels.add(ProfessionModel.builder().title(s).build()));
            professionRepository.saveAll(professionModels);

            return args -> logger.info(DoctorylMessagesConstants.Logger.END_LOAD);
        } else {
            return args -> logger.info(DoctorylMessagesConstants.Logger.DATA_IS_LOADED);
        }
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
