package be.drnow.app.dto.doctor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class WorkingAgendaDTO {

    private UUID pk;

    private LocalDateTime fromDate;

    private LocalDateTime toDate;

    private String fromTime;

    private String toTime;

    private boolean availability;

    private UUID doctorPk;

}
