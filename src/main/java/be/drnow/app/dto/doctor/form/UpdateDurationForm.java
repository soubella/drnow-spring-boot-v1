package be.drnow.app.dto.doctor.form;

import lombok.Data;

import java.util.UUID;

@Data
public class UpdateDurationForm {
    private UUID doctorPk;

    private int duration;
}
