package be.drnow.app.dto.doctor;

public enum AgreementStatusDTO {

    CONVENTIONED,
    PARTIALLY_CONVENTIONED,
    NOT_CONVENTIONED

}
