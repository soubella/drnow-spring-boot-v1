package be.drnow.app.dto.doctor.response;

import be.drnow.app.dto.doctor.*;
import be.drnow.app.dto.user.AddressDTO;
import be.drnow.app.model.doctor.AgreementStatusEnum;
import be.drnow.app.model.user.GenderEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Set;
import java.util.UUID;

/**
 * @author : medalaoui
 * @since : 24/05/2020, dim.
 **/
@EqualsAndHashCode(callSuper = false)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DoctorSearchResponseDTO {

    private UUID pk;

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private GenderEnum gender;

    private AddressDTO address;
    /*d*/
    private String qualificationCode;

    private String INAMI;

    private String profilPicUrl;

    private byte[] image;

    private Set<WorkingAgendaDTO> workingAgendas;

    private Set<LanguageDTO> languages;

    private Set<DoctorSpecialityDTO> specialities;

    private ProfessionDTO profession;

    private AgreementStatusEnum agreementStatus;

    private Set<JobPriceDTO> jobPrices;
}
