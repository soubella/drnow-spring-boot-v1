package be.drnow.app.dto.doctor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DiplomaDTO {

    private UUID pk;

    private String title;

    private LocalDate dateDiploma;

    private String university;

    private String description;

}
