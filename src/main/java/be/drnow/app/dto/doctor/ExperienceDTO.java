package be.drnow.app.dto.doctor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ExperienceDTO {

    private UUID pk;

    private String title;

    private String description;

    private Date startAt;

    private Date endAt;

}
