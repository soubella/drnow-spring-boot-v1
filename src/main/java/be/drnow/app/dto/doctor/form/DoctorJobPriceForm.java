package be.drnow.app.dto.doctor.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class DoctorJobPriceForm {
    private UUID pk;

    private String name;

    private String description;

    private double price;

    private UUID doctorPk;
}
