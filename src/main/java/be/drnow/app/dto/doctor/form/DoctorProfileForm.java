package be.drnow.app.dto.doctor.form;

import be.drnow.app.dto.doctor.LanguageDTO;
import be.drnow.app.model.doctor.AgreementStatusEnum;
import be.drnow.app.model.user.GenderEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class DoctorProfileForm {
    private UUID pk;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private GenderEnum gender;

    private String postalCode;

    private String city;

    private String bio;

    private float teleconsultationPrice;

    private AgreementStatusEnum agreementStatus;

    private Set<LanguageDTO> languages;
}
