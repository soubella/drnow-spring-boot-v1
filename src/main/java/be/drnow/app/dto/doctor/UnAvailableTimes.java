package be.drnow.app.dto.doctor;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class UnAvailableTimes {
    private LocalDateTime startAt;
}
