package be.drnow.app.dto.doctor;

import lombok.Data;

import java.util.UUID;

@Data
public class DoctorAutoCompleteDTO {
    private UUID pk;

    private String fullName;

    //private byte[] image;
}
