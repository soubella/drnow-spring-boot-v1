package be.drnow.app.dto.doctor;

import be.drnow.app.dto.user.UserDTO;
import be.drnow.app.model.doctor.AgreementStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@EqualsAndHashCode(callSuper = false)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DoctorDTO extends UserDTO {

    private String qualificationCode;

    private String INAMI;

    private String bio;

    private float teleconsultationPrice;

    private boolean repayable;

    @JsonIgnore
    private byte[] image;

    private boolean approved;

    private Set<WorkingAgendaDTO> workingAgendas;

    private Set<DoctorDocumentDTO> doctorDocuments;

    private Set<LanguageDTO> languages;

    private Set<DiplomaDTO> diplomas;

    private Set<ExperienceDTO> experiences;

    private DoctorSpecialityDTO speciality;

    private ProfessionDTO profession;

    private AgreementStatusEnum agreementStatus;

    private Set<JobPriceDTO> jobPrices;

}
