package be.drnow.app.dto.doctor;

import be.drnow.app.dto.user.AddressDTO;
import be.drnow.app.model.doctor.*;
import be.drnow.app.model.user.GenderEnum;
import lombok.Data;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Data
public class DoctorSearchResultDTO {

    private UUID pk;

    private String firstName;

    private String lastName;

    private String bio;

    private float teleconsultationPrice;

    private int appointmentDuration;

    private boolean repayable;

    private GenderEnum gender;

    private AddressDTO address;

    private byte[] image;

    private SpecialityModel speciality;

    private ProfessionModel profession;

    private List<WorkingAgendaModel> workingAgendas;

    private AgreementStatusEnum agreementStatus;

    private List<UnAvailableTimes> unAvailableTimes;

    private Set<LanguageModel> languages;

}
