package be.drnow.app.dto.doctor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class JobPriceDTO {

    private UUID pk;

    private String name;

    private String description;

    private double price;

}
