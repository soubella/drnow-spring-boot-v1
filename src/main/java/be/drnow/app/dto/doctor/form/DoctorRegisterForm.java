package be.drnow.app.dto.doctor.form;

import be.drnow.app.dto.user.SignupRequestDTO;

public class DoctorRegisterForm extends SignupRequestDTO {
    private String qualificationCode;

    private String INAMI;

    private String profession;

    private String speciality;

    public DoctorRegisterForm() {
        super();
    }

    public DoctorRegisterForm(String INAMI, String qualificationCode, String email, String password) {
        super(email, password);
        this.INAMI = INAMI;
        this.qualificationCode = qualificationCode;
    }

    public String getProfession() {
        return profession;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getQualificationCode() {
        return qualificationCode;
    }

    public String getINAMI() {
        return INAMI;
    }

    public void setQualificationCode(String qualificationCode) {
        this.qualificationCode = qualificationCode;
    }

    public void setINAMI(String INAMI) {
        this.INAMI = INAMI;
    }
}
