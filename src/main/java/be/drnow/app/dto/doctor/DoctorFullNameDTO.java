package be.drnow.app.dto.doctor;

import java.util.UUID;

public interface DoctorFullNameDTO {
    UUID getPk();

    String getFullName();
}
