package be.drnow.app.dto.admin;

import be.drnow.app.dto.user.UserDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
public class AdminDTO extends UserDTO {

    private UUID pk;

}
