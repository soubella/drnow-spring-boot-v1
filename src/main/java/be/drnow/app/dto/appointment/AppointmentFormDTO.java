package be.drnow.app.dto.appointment;

import be.drnow.app.model.appointment.AppointmentStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AppointmentFormDTO {
    private LocalDateTime startAt;

    private boolean paid;

    private double price;

    private boolean repayable;

    private String reason;

    private UUID doctorPk;

    private UUID patientPk;

    private UUID beneficiaryPk;

    private AppointmentStatusEnum appointmentStatusEnum;
}
