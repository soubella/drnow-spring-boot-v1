package be.drnow.app.dto.appointment;

import be.drnow.app.model.appointment.AppointmentStatusEnum;
import lombok.Data;

@Data
public class CancelAppointmentForm {

    private String cancellationReason;

    private AppointmentStatusEnum appointmentStatusEnum;

}
