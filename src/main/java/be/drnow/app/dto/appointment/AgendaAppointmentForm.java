package be.drnow.app.dto.appointment;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class AgendaAppointmentForm {

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private String reason;

    private LocalDateTime startAt;

    private UUID doctorPk;

}
