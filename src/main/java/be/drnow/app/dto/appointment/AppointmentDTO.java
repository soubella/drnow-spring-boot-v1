package be.drnow.app.dto.appointment;

import be.drnow.app.dto.doctor.DoctorDTO;
import be.drnow.app.dto.patient.PatientDTO;
import be.drnow.app.dto.patient.PatientDocumentDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AppointmentDTO {

    private UUID pk;

    private LocalDateTime startAt;

    private int duration;

    private boolean paid;

    private double price;

    private boolean repayment;

    private DoctorDTO doctor;

    private PatientDTO patient;

    private PatientDocumentDTO patientDocument;

}
