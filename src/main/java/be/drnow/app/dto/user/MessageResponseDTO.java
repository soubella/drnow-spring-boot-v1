package be.drnow.app.dto.user;

public class MessageResponseDTO {
    private String message;
    private String code;

    public MessageResponseDTO(String message) {
        this.message = message;
    }

    public MessageResponseDTO(String message, String code) {
        this.message = message;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
