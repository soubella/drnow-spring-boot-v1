package be.drnow.app.dto.user.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserSocialNumberForm {
    private String socialNumber;

    private UUID userPk;
}
