package be.drnow.app.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class NewPasswordRequestDTO {
    private String newPassword;

    private String email;

    private String confirmationCode;

    private String currentPassword;
}
