package be.drnow.app.dto.user;

import be.drnow.app.model.user.GenderEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserDTO {

    private UUID pk;

    private String firstName;

    private String lastName;

    private String email;

    @JsonIgnore
    private String password;

    private String phoneNumber;

    private Date birthday;

    private GenderEnum gender;

    private boolean enabled;

    private boolean canceled;

    private RoleDTO role;

    private AddressDTO address;

    private Set<NotificationDTO> notifications;

}
