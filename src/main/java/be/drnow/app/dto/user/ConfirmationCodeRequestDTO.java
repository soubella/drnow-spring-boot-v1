package be.drnow.app.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ConfirmationCodeRequestDTO {
    private String confirmationCode;

    private String email;

    private String tokenType;
}
