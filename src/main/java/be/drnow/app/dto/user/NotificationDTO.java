package be.drnow.app.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class NotificationDTO {

    private UUID pk;

    private String title;

    private String message;

    private boolean seen;

    private UUID userPk;

    private UserDTO user;

}
