package be.drnow.app.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AddressDTO {

    private UUID pk;

    private String postalCode;

    private String city;

    private String line1;

    private String line2;

    private CountryDTO country;

}
