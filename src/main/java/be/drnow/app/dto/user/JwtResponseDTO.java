package be.drnow.app.dto.user;

import java.util.UUID;

public class JwtResponseDTO {

    private UUID pk;

    private String jwt;

    private String email;

    private String role;

    public JwtResponseDTO() {
    }

    public JwtResponseDTO(String jwt, UUID pk, String email, String role) {
        this.pk = pk;
        this.jwt = jwt;
        this.email = email;
        this.role = role;
    }

    public UUID getPk() {
        return pk;
    }

    public void setPk(UUID pk) {
        this.pk = pk;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
