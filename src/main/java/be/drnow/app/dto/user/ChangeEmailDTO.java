package be.drnow.app.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ChangeEmailDTO {
    private String newEmail;
    private String oldEmail;
    private String confirmationCode;
}
