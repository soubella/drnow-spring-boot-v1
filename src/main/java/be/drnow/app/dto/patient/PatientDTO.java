package be.drnow.app.dto.patient;


import be.drnow.app.dto.appointment.AppointmentDTO;
import be.drnow.app.dto.user.UserDTO;
import be.drnow.app.model.patient.MedicalRecordModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = false)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PatientDTO extends UserDTO {

    private String profession;

    private boolean mainAccount;

    private String relationType;

    private MedicalRecordModel medicalRecord;

    private Set<AppointmentDTO> appointment;

    private List<PatientDTO> familyMembers;

}
