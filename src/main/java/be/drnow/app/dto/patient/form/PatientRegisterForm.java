package be.drnow.app.dto.patient.form;

import be.drnow.app.dto.user.SignupRequestDTO;
import be.drnow.app.model.patient.PatientRelationshipEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PatientRegisterForm extends SignupRequestDTO {
    private PatientRelationshipEnum patientRelationshipEnum;
}
