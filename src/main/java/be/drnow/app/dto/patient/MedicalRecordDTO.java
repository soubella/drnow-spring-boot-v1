package be.drnow.app.dto.patient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class MedicalRecordDTO {

    private UUID pk;

    private float height;

    private float weight;

    private String regularDoctor;

    private boolean cvDiseases;

    private boolean cvDiseasesFamily;

    private boolean stress;

    private boolean moral;

    private boolean regularDentalFollow;

    private boolean asthma;

    private String pathology;

    private String surgical;

    private String longTermTreatment;

    private String drugAllergies;

    private String lifeStyle;

}
