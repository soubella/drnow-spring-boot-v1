package be.drnow.app.dto.patient;

import be.drnow.app.dto.appointment.AppointmentDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PatientDocumentDTO {

    private UUID pk;

    private String url;

    private String name;

    private PatientDocumentTypeDTO type;

    private AppointmentDTO appointment;

}
