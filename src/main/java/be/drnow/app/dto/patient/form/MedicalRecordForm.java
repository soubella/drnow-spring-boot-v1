package be.drnow.app.dto.patient.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MedicalRecordForm {

    private UUID pk;

    private float height;

    private float weight;

    private UUID patientPk;

    private String treatingDoctorFirstName;

    private String treatingDoctorLastName;

    private String treatingDoctorPhone;

    private String treatingDoctorEmail;

    /**
     * Antécédents médicaux
     * char(1) : Y, N, D
     */

    @Column(columnDefinition = "char(1)")
    private String cvDiseases; // Avez-vous une ou plusieurs maladies cardiovasculaires (infarctus, AVC) ?

    @Column(columnDefinition = "char(1)") // Avez-vous des antécédents cardiovasculaires familiaux (infarctus, AVC) ?
    private String cvDiseasesFamily;

    private String cvDiseasesRisk; // Avez-vous un ou plusieurs facteur(s) de risque personnel(s) cardio-vasculaire(s) parmi les suivants ?

    @Column(columnDefinition = "char(1)") // Êtes-vous asthmatique ?
    private String asthma;

    @Column(columnDefinition = "char(1)")
    private String stress; // Vous sentez-vous stressé(e) ?

    @Column(columnDefinition = "char(1)")
    private String moral; // Votre moral est-il bon ?

    private String otherPathology; // Informez-nous de toute autre pathologie dont vous êtes, ou avez été porteur.

    private String surgical; // Antécédents chirurgicaux : Veuillez mentionner vos éventuelles interventions chirurgicales

    private String longTermTreatment; // Traitement de longue durée : Si oui, laquelle ou lesquelles ?

    private String drugAllergies; // Allergies et intolérances : Si oui, laquelle ou lesquelles ?

    private String vaccineStatus; // Statut vaccinal (BCG;Coronavirus;...)

    private String insuranceProvider; // Organisme assureur ou mutuelle
}
