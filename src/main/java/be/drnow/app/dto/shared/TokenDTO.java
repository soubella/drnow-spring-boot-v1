package be.drnow.app.dto.shared;

import lombok.Data;

@Data
public class TokenDTO {

    private String token;

}
