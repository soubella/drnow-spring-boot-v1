package be.drnow.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class DrnowSpringBootV1Application {

    public static void main(String[] args) {
        SpringApplication.run(DrnowSpringBootV1Application.class, args);
    }

}
