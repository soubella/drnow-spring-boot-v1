package be.drnow.app.repository.patient;

import be.drnow.app.model.patient.PatientModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Repository
public interface PatientRepository extends JpaRepository<PatientModel, UUID> {
    boolean existsByEmail(String email);

    Optional<PatientModel> findByEmail(String email);

    @Query(value = "SELECT DISTINCT a.patient FROM AppointmentModel a WHERE a.doctor.pk = :doctorPk")
    List<PatientModel> findPatientsByDoctorPk(@Param("doctorPk") UUID doctorPk);
}
