package be.drnow.app.repository.appointment;

import be.drnow.app.dto.doctor.UnAvailableTimes;
import be.drnow.app.model.appointment.AppointmentModel;
import be.drnow.app.model.appointment.AppointmentStatusEnum;
import be.drnow.app.model.patient.PatientModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Repository
public interface AppointmentRepository extends JpaRepository<AppointmentModel, UUID> {
    Set<AppointmentModel> findByDoctorPk(UUID pk);

    Set<AppointmentModel> findByDoctorPkAndAppointmentStatusEnumNotIn(UUID pk, List<AppointmentStatusEnum> appointmentStatusEnums);

    @Query("SELECT a.beneficiary FROM AppointmentModel a WHERE a.doctor.pk = :doctorPk")
    Set<PatientModel> findPatientsByDoctorPk(@Param("doctorPk") UUID doctorPk);

    @Query("SELECT new be.drnow.app.dto.doctor.UnAvailableTimes(a.startAt) FROM AppointmentModel a WHERE a.doctor.pk = :doctorPk AND a.startAt >= :currentDateTime")
    List<UnAvailableTimes> findAppointmentsDatesByDoctorPk(@Param("doctorPk") UUID doctorPk, @Param("currentDateTime") LocalDateTime currentDateTime);

    boolean existsByStartAtAndDoctorPk(LocalDateTime startAt, UUID doctorPk);

    Set<AppointmentModel> findByPatientPk(UUID patientPk);
}
