package be.drnow.app.repository.doctor;

import be.drnow.app.model.doctor.LanguageModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface LanguageRepository extends JpaRepository<LanguageModel, UUID> {
    LanguageModel findByCode(String code);
}
