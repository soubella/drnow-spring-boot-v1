package be.drnow.app.repository.doctor;

import be.drnow.app.model.doctor.JobPriceModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface JobPriceRepository extends JpaRepository<JobPriceModel, UUID> {
    List<JobPriceModel> findAllByDoctorPk(UUID doctorPk);
}
