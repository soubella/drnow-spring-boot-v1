package be.drnow.app.repository.doctor;

import be.drnow.app.dto.doctor.DoctorFullNameDTO;
import be.drnow.app.model.doctor.DoctorModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.UUID;


@Repository
public interface DoctorRepository extends PagingAndSortingRepository<DoctorModel, UUID> {

    DoctorModel findByEmail(String email);

    boolean existsByEmail(String email);

    boolean existsByINAMI(String inami);

    boolean existsByQualificationCode(String qualificationCode);

    boolean existsByPkAndApprovedTrue(UUID pk);

    @Transactional
    @Modifying
    @Query("UPDATE DoctorModel d SET d.appointmentDuration = :duration WHERE d.pk = :pk")
    void updateAppointmentDuration(@Param("duration") int duration, @Param("pk") UUID pk);

    @Query("SELECT d.appointmentDuration FROM DoctorModel d WHERE d.pk = :pk")
    int findAppointmentDurationByDoctorPk(@Param("pk") UUID pk);

    @Query("SELECT d.pk as pk, CONCAT(CONCAT(d.firstName,' '), d.lastName) as fullName FROM DoctorModel" +
            " d WHERE CONCAT(CONCAT(d.firstName,' '), d.lastName) LIKE CONCAT('%',:keyword,'%') OR" +
            " CONCAT(CONCAT(d.lastName,' '), d.firstName) LIKE CONCAT('%',:keyword,'%')" +
            " AND d.accountStatus = 'ACCOUNT_OKAY'" +
            " ORDER BY d.profession.rank")
    List<DoctorFullNameDTO> findDoctorByFullName(@Param("keyword") String keyword);

    @Query("SELECT d FROM DoctorModel" +
            " d WHERE CONCAT(CONCAT(d.firstName,' '), d.lastName) LIKE CONCAT('%',:keyword,'%') OR" +
            " CONCAT(CONCAT(d.lastName,' '), d.firstName) LIKE CONCAT('%',:keyword,'%') OR" +
            " CONCAT(CONCAT(d.profession.title,' '), d.speciality.name) LIKE CONCAT('%',:keyword,'%')" +
            " AND d.accountStatus = be.drnow.app.model.user.AccountStatusEnum.ACCOUNT_OKAY" +
//            " AND d.workingAgendas.size > 0" +
            " ORDER BY size(d.workingAgendas) DESC")
    List<DoctorModel> searchForDoctors(@Param("keyword") String keyword, Pageable pageable);

    //@Query(value = "SELECT * FROM Doctor_Model D INNER JOIN User_Model U ON D.pk = U.pk" +
//        " INNER JOIN Working_Agenda_Model W ON D.pk = W.doctormodel_pk" +
//        " INNER JOIN speciality_model S ON D.speciality_pk = S.pk" +
//        " INNER JOIN Profession_model P ON D.profession_pk = P.pk ORDER BY P.rank ASC, S.rank ASC",
//        nativeQuery = true)
    @Query("SELECT d FROM DoctorModel d WHERE d.accountStatus = 'ACCOUNT_OKAY'")
    Page<DoctorModel> findAllDoctors(Pageable pageable);

    // Search Doctors by first name, last name, profession, specialities, address
    Set<DoctorModel>
    findDoctorModelByFirstNameContainsOrLastNameContainsOrProfessionTitleContainsOrSpecialityNameContains(
            String firstName, String lastName, String profession, String speciality);
    // end search Doctors

    // Search Doctors by Address
    Set<DoctorModel> findDoctorModelByAddressCityContains(String address);
    // end search Doctors
}


