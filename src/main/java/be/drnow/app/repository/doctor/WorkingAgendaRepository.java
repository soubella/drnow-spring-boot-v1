package be.drnow.app.repository.doctor;

import be.drnow.app.model.doctor.WorkingAgendaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

public interface WorkingAgendaRepository extends JpaRepository<WorkingAgendaModel, UUID> {
    Set<WorkingAgendaModel> findByAvailabilityTrueAndDoctorModelPk(UUID pk);

    Set<WorkingAgendaModel> findByAvailabilityFalseAndDoctorModelPk(UUID pk);

    @Query("SELECT COUNT(w) FROM WorkingAgendaModel w WHERE w.fromDate <= :date AND w.toDate >= :date" +
            " AND w.availability = :availability" +
            " AND w.doctorModel.pk = :doctorPk")
    int checkAvailability(@Param("date") LocalDateTime date, @Param("availability") boolean availability, @Param("doctorPk") UUID doctorPk);
}
