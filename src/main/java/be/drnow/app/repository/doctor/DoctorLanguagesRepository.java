package be.drnow.app.repository.doctor;

import be.drnow.app.model.doctor.LanguageModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * @author : medalaoui
 * @since : 17/05/2020, dim.
 **/
@Repository
public interface DoctorLanguagesRepository extends JpaRepository<LanguageModel, UUID> {
}
