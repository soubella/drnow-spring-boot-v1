package be.drnow.app.repository.doctor;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import be.drnow.app.model.doctor.SpecialityModel;

/**
 * @author : medalaoui
 * @since : 14/05/2020, jeu.
 **/
@Repository
public interface SpecialityRepository extends JpaRepository<SpecialityModel, UUID> {
    SpecialityModel findByNameOrderByName(String name);
}
