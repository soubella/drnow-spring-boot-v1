package be.drnow.app.repository.user;

import be.drnow.app.model.user.VerificationCodeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface VerificationCodeRepository extends JpaRepository<VerificationCodeModel, UUID> {
    VerificationCodeModel findByVerificationCode(String confirmationCode);
}
