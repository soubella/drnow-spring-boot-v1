package be.drnow.app.repository.user;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import be.drnow.app.model.user.UserModel;


@Repository
public interface UserRepository extends JpaRepository<UserModel, UUID> {
    UserModel findByEmail(String email);

    boolean existsByPkAndEnabledTrue(UUID pk);

    boolean existsByEmail(String email);
}
