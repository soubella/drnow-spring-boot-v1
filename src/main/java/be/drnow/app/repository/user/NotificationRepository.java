package be.drnow.app.repository.user;

import be.drnow.app.model.user.NotificationModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface NotificationRepository extends JpaRepository<NotificationModel, UUID> {
    List<NotificationModel> findByUserPk(UUID pk);
}
