package be.drnow.app.repository.user;

import java.util.UUID;

import be.drnow.app.model.user.RoleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RoleRepository extends JpaRepository<RoleModel, UUID> {
    RoleModel findByName(String name);
}
