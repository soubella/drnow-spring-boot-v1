package be.drnow.app.converter;

import org.springframework.stereotype.Component;

/**
 * @author : medalaoui
 * @since : 13/05/2020, mer.
 **/
@Component
public interface Converter<S, T> {

    void convert(S source, T target);

}
