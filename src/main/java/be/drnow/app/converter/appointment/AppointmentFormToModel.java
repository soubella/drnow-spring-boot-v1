package be.drnow.app.converter.appointment;

import be.drnow.app.converter.Converter;
import be.drnow.app.dto.appointment.AppointmentFormDTO;
import be.drnow.app.exception.RessourceNotFoundException;
import be.drnow.app.model.appointment.AppointmentModel;
import be.drnow.app.model.doctor.DoctorModel;
import be.drnow.app.repository.doctor.DoctorRepository;
import be.drnow.app.repository.patient.PatientRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component("appointmentFormToModel")
public class AppointmentFormToModel implements Converter<AppointmentFormDTO, AppointmentModel> {

    private Logger logger = LoggerFactory.getLogger(AppointmentFormToModel.class);

    private final PatientRepository patientRepository;
    private final DoctorRepository doctorRepository;

    @Override
    public void convert(AppointmentFormDTO source, AppointmentModel target) {

        DoctorModel doctorModel = doctorRepository.findById(source.getDoctorPk()).orElseThrow(() -> new RessourceNotFoundException("Doctor"));

        target.setDoctor(doctorModel);
        target.setPatient(patientRepository.findById(source.getPatientPk()).orElseThrow(() -> new RessourceNotFoundException("Patient")));
        target.setBeneficiary(patientRepository.findById(source.getBeneficiaryPk()).orElseThrow(() -> new RessourceNotFoundException("Beneficiary")));

        target.setReason(source.getReason());
        target.setPrice(source.getPrice());
        target.setRepayable(source.isRepayable());
        target.setPaid(source.isPaid());
        target.setAppointmentStatusEnum(source.getAppointmentStatusEnum());
        target.setStartAt(source.getStartAt());
        target.setDuration(doctorModel.getAppointmentDuration());
    }

}
