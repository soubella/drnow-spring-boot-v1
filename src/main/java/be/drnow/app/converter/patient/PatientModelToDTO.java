package be.drnow.app.converter.patient;

import be.drnow.app.converter.Converter;
import be.drnow.app.dto.patient.PatientDTO;
import be.drnow.app.dto.user.AddressDTO;
import be.drnow.app.model.patient.MedicalRecordModel;
import be.drnow.app.model.patient.PatientModel;
import be.drnow.app.model.user.AddressModel;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class PatientModelToDTO implements Converter<PatientModel, PatientDTO> {

    @Override
    public void convert(PatientModel source, PatientDTO target) {
        target.setPk(source.getPk());
        target.setFirstName(source.getFirstName());
        target.setLastName(source.getLastName());
        target.setEmail(source.getEmail());
        target.setPhoneNumber(source.getPhoneNumber());
        target.setGender(source.getGender());
        target.setMedicalRecord(source.getMedicalRecord());
        target.setBirthday(source.getBirthday());
        if (Objects.nonNull(source.getAddress())) {
            target.setAddress(new AddressDTO());
            convertAddress(source.getAddress(), target.getAddress());
        }
        target.setRelationType(source.getPatientRelationshipEnum().toString());
    }

    private void convertMedicalRecord(MedicalRecordModel medicalRecordModel) {

    }

    private void convertAddress(AddressModel source, AddressDTO target) {
        target.setCity(source.getCity());
        target.setPostalCode(source.getPostalCode());
    }

}
