package be.drnow.app.converter.patient;

import be.drnow.app.converter.Converter;
import be.drnow.app.dto.patient.form.PatientRegisterForm;
import be.drnow.app.model.patient.PatientModel;
import be.drnow.app.model.user.AddressModel;
import be.drnow.app.repository.user.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Objects;

@RequiredArgsConstructor
@Component("patientRegisterFormToModel")
public class PatientRegisterFormToModel implements Converter<PatientRegisterForm, PatientModel> {

    private Logger logger = LoggerFactory.getLogger(PatientRegisterFormToModel.class);

    private static final String ROLE_PATIENT = "ROLE_PATIENT";

    private final RoleRepository roleRepository;
    private final PasswordEncoder encoder;

    @Override
    public void convert(PatientRegisterForm source, PatientModel target) {
        // personal info
        if (Objects.nonNull(source.getFirstName()))
            target.setFirstName(source.getFirstName());

        if (Objects.nonNull(source.getLastName()))
            target.setLastName(source.getLastName());

        if (Objects.nonNull(source.getGenderEnum()))
            target.setGender(source.getGenderEnum());

        if (Objects.nonNull(source.getPhoneNumber()))
            target.setPhoneNumber(source.getPhoneNumber());

        if (Objects.nonNull(source.getPatientRelationshipEnum()))
            target.setPatientRelationshipEnum(source.getPatientRelationshipEnum());

        if (Objects.nonNull(source.getBirthday())) {
            try {
                convertBirthDay(source, target);
            } catch (ParseException e) {
                logger.error(e.getMessage());
            }
        }
        // login
        if (Objects.nonNull(source.getEmail()))
            target.setEmail(source.getEmail());

        if (Objects.nonNull(source.getPassword()))
            target.setPassword(encoder.encode(source.getPassword()));

        if (Objects.nonNull(source.getCity()) && Objects.nonNull(source.getPostalCode())) {
            if (Objects.nonNull(target.getAddress())) {
                target.getAddress().setCity(source.getCity());
                target.getAddress().setPostalCode(source.getPostalCode());
            } else {
                target.setAddress(new AddressModel());
                target.getAddress().setCity(source.getCity());
                target.getAddress().setPostalCode(source.getPostalCode());
            }
        }

        if (!Objects.nonNull(target.getPk())) {
            target.setRole(roleRepository.findByName(ROLE_PATIENT));
        }

    }

    private void convertBirthDay(PatientRegisterForm source, PatientModel target) throws ParseException {
        target.setBirthday(new SimpleDateFormat("ddMMyyyy").parse(source.getBirthday()));
    }

}
