package be.drnow.app.converter.patient;

import be.drnow.app.converter.Converter;
import be.drnow.app.dto.patient.form.MedicalRecordForm;
import be.drnow.app.model.patient.MedicalRecordModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Objects;

@RequiredArgsConstructor
@Component("medicalRecordFormToModel")
public class MedicalRecordFormToModel implements Converter<MedicalRecordForm, MedicalRecordModel> {

    @Override
    public void convert(MedicalRecordForm source, MedicalRecordModel target) {

        if (!Objects.isNull(source.getPk())) {
            target.setPk(source.getPk());
        }

        target.setHeight(source.getHeight());
        target.setWeight(source.getWeight());

        target.setAsthma(source.getAsthma());

        target.setCvDiseases(source.getCvDiseases());
        target.setCvDiseasesFamily(source.getCvDiseasesFamily());
        target.setCvDiseasesRisk(source.getCvDiseasesRisk());

        target.setDrugAllergies(source.getDrugAllergies());
        target.setInsuranceProvider(source.getInsuranceProvider());
        target.setLongTermTreatment(source.getLongTermTreatment());
        target.setMoral(source.getMoral());
        target.setOtherPathology(source.getOtherPathology());
        target.setSurgical(source.getSurgical());
        target.setStress(source.getStress());
        target.setVaccineStatus(source.getVaccineStatus());

        target.setTreatingDoctorFirstName(source.getTreatingDoctorFirstName());
        target.setTreatingDoctorLastName(source.getTreatingDoctorLastName());
        target.setTreatingDoctorEmail(source.getTreatingDoctorEmail());
        target.setTreatingDoctorPhone(source.getTreatingDoctorPhone());
    }

}
