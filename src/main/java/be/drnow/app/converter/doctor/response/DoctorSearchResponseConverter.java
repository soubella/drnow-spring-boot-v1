package be.drnow.app.converter.doctor.response;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.JobPriceDTO;
import be.drnow.app.dto.doctor.LanguageDTO;
import be.drnow.app.dto.doctor.ProfessionDTO;
import be.drnow.app.dto.doctor.WorkingAgendaDTO;
import be.drnow.app.dto.doctor.response.DoctorSearchResponseDTO;
import be.drnow.app.model.doctor.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author : medalaoui
 * @since : 24/05/2020, dim.
 **/
@Component("DoctorSearchResponseConverter")
public class DoctorSearchResponseConverter implements Converter<DoctorModel, DoctorSearchResponseDTO> {

    private Converter<Set<WorkingAgendaModel>, Set<WorkingAgendaDTO>> workingAgendaConverterAll;
    private Converter<Set<LanguageModel>, Set<LanguageDTO>> languageConvertAll;
    private Converter<ProfessionModel, ProfessionDTO> professionConverter;
    private Converter<Set<JobPriceModel>, Set<JobPriceDTO>> jobPriceConvertAll;

    @Override
    public void convert(DoctorModel source, DoctorSearchResponseDTO target) {
        Objects.requireNonNull(source.getPk(), DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + source.getClass().getName());

        target.setPk(source.getPk());
        target.setFirstName(source.getFirstName());
        target.setLastName(source.getLastName());
        target.setEmail(source.getEmail());
        target.setPhoneNumber(source.getPhoneNumber());
        target.setGender(source.getGender());

        target.setQualificationCode(source.getQualificationCode());
        target.setINAMI(source.getINAMI());
        target.setImage(source.getImage());
        target.setAgreementStatus(source.getAgreementStatus());

        if (!CollectionUtils.isEmpty(source.getWorkingAgendas())) { // not
            Set<WorkingAgendaDTO> workingAgendaDTOSet = new HashSet<>();
            workingAgendaConverterAll.convert(new HashSet<>(source.getWorkingAgendas()), workingAgendaDTOSet);
            target.setWorkingAgendas(workingAgendaDTOSet);
        }

        if (!CollectionUtils.isEmpty(source.getLanguages())) {
            Set<LanguageDTO> languageDTOSet = new HashSet<>();
            languageConvertAll.convert(source.getLanguages(), languageDTOSet);
            target.setLanguages(languageDTOSet);

        }

//        if (!CollectionUtils.isEmpty(source.getSpecialities())) {
//            Set<DoctorSpecialityDTO> doctorSpecialityDTOSet = new HashSet<>();
//            specialityConvertAll.convert(source.getSpecialities(), doctorSpecialityDTOSet);
//            target.setSpecialities(doctorSpecialityDTOSet);
//        }

        if (Objects.nonNull(source.getProfession())) {  // not null
            ProfessionDTO professionDTO = new ProfessionDTO();
            professionConverter.convert(source.getProfession(), professionDTO);
            target.setProfession(professionDTO);
        }

        if (!CollectionUtils.isEmpty(source.getJobPrices())) {
            Set<JobPriceDTO> jobPriceDTOS = new HashSet<>();
            jobPriceConvertAll.convert(source.getJobPrices(), jobPriceDTOS);
            target.setJobPrices(jobPriceDTOS);
        }
    }


    @Autowired
    public void setWorkingAgendaConverterAll(@Qualifier("DoctorWorkingModelToDTOList") Converter<Set<WorkingAgendaModel>,
            Set<WorkingAgendaDTO>> workingAgendaConverterAll) {
        this.workingAgendaConverterAll = workingAgendaConverterAll;
    }

    @Autowired
    public void setLanguageConvertAll(@Qualifier("DoctorLanguagesModelToDTOList") Converter<Set<LanguageModel>,
            Set<LanguageDTO>> languageConvertAll) {
        this.languageConvertAll = languageConvertAll;
    }

    @Autowired
    public void setProfessionConverter(@Qualifier("DoctorProfessionModelToDTO") Converter<ProfessionModel,
            ProfessionDTO> professionConverter) {
        this.professionConverter = professionConverter;
    }

    @Autowired
    public void setJobPriceConvertAll(@Qualifier("DoctorJobPriceModelToDTOList") Converter<Set<JobPriceModel>, Set<JobPriceDTO>> jobPriceConvertAll) {
        this.jobPriceConvertAll = jobPriceConvertAll;
    }
}
