package be.drnow.app.converter.doctor;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.LanguageDTO;
import be.drnow.app.model.doctor.LanguageModel;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author : medalaoui
 * @since : 23/05/2020, sam.
 **/
@Component("DoctorLanguagesModelToDTO")
public class DoctorLanguagesModelToDTO implements Converter<LanguageModel, LanguageDTO> {

    @Override
    public void convert(LanguageModel source, LanguageDTO target) {
        Objects.requireNonNull(source, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + source.getClass().getName());

        target.setCode(source.getCode());
        target.setName(source.getName());
        target.setPk(source.getPk());
    }
}
