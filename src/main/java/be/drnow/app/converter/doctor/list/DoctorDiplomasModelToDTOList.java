package be.drnow.app.converter.doctor.list;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.DiplomaDTO;
import be.drnow.app.model.doctor.DiplomaModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Set;

/**
 * @author : medalaoui
 * @since : 24/05/2020, dim.
 **/
@Component("DoctorDiplomasModelToDTOList")
public class DoctorDiplomasModelToDTOList implements Converter<Set<DiplomaModel>, Set<DiplomaDTO>> {

    Converter<DiplomaModel, DiplomaDTO> converter;

    @Override
    public void convert(Set<DiplomaModel> sources, Set<DiplomaDTO> targets) {
        Objects.requireNonNull(sources, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + sources.getClass().getName());

        sources.forEach(source -> {
            DiplomaDTO target = new DiplomaDTO();
            converter.convert(source, target);
            targets.add(target);
        });
    }

    @Autowired
    public void setConverter(Converter<DiplomaModel, DiplomaDTO> converter) {
        this.converter = converter;
    }
}
