package be.drnow.app.converter.doctor.list;

import be.drnow.app.converter.Converter;
import be.drnow.app.dto.doctor.DoctorSearchResultDTO;
import be.drnow.app.model.doctor.DoctorModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Set;

@RequiredArgsConstructor
@Component
public class DoctorModelToSearchDTOList implements Converter<Set<DoctorModel>, Set<DoctorSearchResultDTO>> {

    private final Converter<DoctorModel, DoctorSearchResultDTO> converter;

    @Override
    public void convert(Set<DoctorModel> sources, Set<DoctorSearchResultDTO> targets) {
        sources.forEach(source -> {
            DoctorSearchResultDTO target = new DoctorSearchResultDTO();
            converter.convert(source, target);
            targets.add(target);
        });
    }
}
