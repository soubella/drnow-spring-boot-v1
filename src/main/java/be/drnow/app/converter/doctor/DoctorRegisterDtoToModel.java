package be.drnow.app.converter.doctor;

import be.drnow.app.converter.Converter;
import be.drnow.app.dto.doctor.form.DoctorRegisterForm;
import be.drnow.app.model.doctor.DoctorModel;
import be.drnow.app.model.doctor.ProfessionModel;
import be.drnow.app.model.doctor.SpecialityModel;
import be.drnow.app.model.user.AddressModel;
import be.drnow.app.model.user.RoleModel;
import be.drnow.app.repository.doctor.ProfessionRepository;
import be.drnow.app.repository.doctor.SpecialityRepository;
import be.drnow.app.repository.user.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Objects;

@RequiredArgsConstructor
@Component
public class DoctorRegisterDtoToModel implements Converter<DoctorRegisterForm, DoctorModel> {

    private final RoleRepository roleRepository;
    private final ProfessionRepository professionRepository;
    private final SpecialityRepository specialityRepository;
    private final PasswordEncoder encoder;

    private static final int DEFAULT_DURATION = 15;

    @Override
    public void convert(DoctorRegisterForm doctorRegisterForm, DoctorModel doctorModel) {
        AddressModel addressModel = new AddressModel();
        doctorModel.setFirstName(doctorRegisterForm.getFirstName());
        doctorModel.setLastName(doctorRegisterForm.getLastName());
        doctorModel.setPhoneNumber(doctorRegisterForm.getPhoneNumber());
        doctorModel.setProfession(getProfessionModelByDoctorSignupRequest(doctorRegisterForm));
        doctorModel.setEmail(doctorRegisterForm.getEmail());
        doctorModel.setPassword(encoder.encode(doctorRegisterForm.getPassword()));
        doctorModel.setINAMI(doctorRegisterForm.getINAMI());
        doctorModel.setQualificationCode(doctorRegisterForm.getQualificationCode());
        doctorModel.setRole(getRoleModelByDoctorSignupRequest(doctorRegisterForm));
        if (Objects.nonNull(doctorRegisterForm.getSpeciality()) && !doctorRegisterForm.getSpeciality().isEmpty()) {
            doctorModel.setSpeciality(getSpecialityModelByDoctorSignupRequest(doctorRegisterForm));
        }
        addressModel.setCity(doctorRegisterForm.getCity());
        doctorModel.setAddress(addressModel);
        doctorModel.setAppointmentDuration(DEFAULT_DURATION);
    }

    private RoleModel getRoleModelByDoctorSignupRequest(DoctorRegisterForm doctorRegisterForm) {
        return roleRepository.findByName(doctorRegisterForm.getRole());
    }

    private ProfessionModel getProfessionModelByDoctorSignupRequest(DoctorRegisterForm doctorRegisterForm) {
        return professionRepository.findByTitle(doctorRegisterForm.getProfession());
    }

    private SpecialityModel getSpecialityModelByDoctorSignupRequest(DoctorRegisterForm doctorRegisterForm) {
        return specialityRepository.findByNameOrderByName(doctorRegisterForm.getSpeciality());
    }
}
