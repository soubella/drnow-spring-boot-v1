package be.drnow.app.converter.doctor;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.DoctorSearchResultDTO;
import be.drnow.app.dto.user.AddressDTO;
import be.drnow.app.helper.ImageHelper;
import be.drnow.app.model.doctor.DoctorModel;
import be.drnow.app.repository.appointment.AppointmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

@Component
public class DoctorModelToSearchDTO implements Converter<DoctorModel, DoctorSearchResultDTO> {

    private AppointmentRepository appointmentRepository;

    @Override
    public void convert(DoctorModel source, DoctorSearchResultDTO target) {
        Objects.requireNonNull(source.getPk(), DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + source.getClass().getName());

        target.setPk(source.getPk());
        target.setFirstName(source.getFirstName());
        target.setLastName(source.getLastName());
        target.setAgreementStatus(source.getAgreementStatus());
        target.setBio(source.getBio());
        target.setGender(source.getGender());
        target.setRepayable(source.isRepayable());
        target.setSpeciality(source.getSpeciality());
        target.setProfession(source.getProfession());
        target.setLanguages(source.getLanguages());
        target.setTeleconsultationPrice(source.getTeleconsultationPrice());
        target.setWorkingAgendas(source.getWorkingAgendas());
        target.setAppointmentDuration(source.getAppointmentDuration());
        convertAddress(source, target);
        target.setUnAvailableTimes(appointmentRepository.
                findAppointmentsDatesByDoctorPk(source.getPk(), LocalDateTime.now(ZoneOffset.UTC)));
        if (source.getImage() != null)
            target.setImage(ImageHelper.decompressBytes(source.getImage()));
    }

    private void convertAddress(DoctorModel source, DoctorSearchResultDTO target) {
        target.setAddress(new AddressDTO());
        target.getAddress().setCity(source.getAddress().getCity());
    }

    @Autowired
    public void setAppointmentRepository(AppointmentRepository appointmentRepository) {
        this.appointmentRepository = appointmentRepository;
    }

}
