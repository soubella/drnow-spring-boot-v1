package be.drnow.app.converter.doctor.list;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.JobPriceDTO;
import be.drnow.app.model.doctor.JobPriceModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Set;

/**
 * @author : medalaoui
 * @since : 24/05/2020, dim.
 **/
@Component("DoctorJobPriceModelToDTOList")
public class DoctorJobPriceModelToDTOList implements Converter<Set<JobPriceModel>, Set<JobPriceDTO>> {

    Converter<JobPriceModel, JobPriceDTO> converter;

    @Override
    public void convert(Set<JobPriceModel> sources, Set<JobPriceDTO> targets) {
        Objects.requireNonNull(sources, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + sources.getClass().getName());

        sources.forEach(source -> {
            JobPriceDTO target =new JobPriceDTO();
            converter.convert(source, target);
            targets.add(target);
        });
    }

    @Autowired
    public void setConverter(Converter<JobPriceModel, JobPriceDTO> converter) {
        this.converter = converter;
    }
}
