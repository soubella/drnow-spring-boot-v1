package be.drnow.app.converter.doctor.form;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.form.DoctorJobPriceForm;
import be.drnow.app.model.doctor.JobPriceModel;
import be.drnow.app.service.doctor.DoctorService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Objects;

@RequiredArgsConstructor
@Component
public class DoctorJobPriceFormToModel implements Converter<DoctorJobPriceForm, JobPriceModel> {

    private final DoctorService doctorService;

    @Override
    public void convert(DoctorJobPriceForm source, JobPriceModel target) {
        Objects.requireNonNull(source, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + source.getClass().getName());

        if (source.getPk() != null) {
            target.setPk(source.getPk());
        }

        target.setName(source.getName());
        target.setDescription(source.getDescription());
        target.setPrice(source.getPrice());
        target.setDoctor(doctorService.findByPk(source.getDoctorPk()));
    }
}
