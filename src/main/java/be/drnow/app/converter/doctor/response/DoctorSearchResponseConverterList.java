package be.drnow.app.converter.doctor.response;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.response.DoctorSearchResponseDTO;
import be.drnow.app.model.doctor.DoctorModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Set;

/**
 * @author : medalaoui
 * @since : 24/05/2020, dim.
 **/
@Component("DoctorSearchResponseConverterList")
public class DoctorSearchResponseConverterList implements Converter<Set<DoctorModel>, Set<DoctorSearchResponseDTO>> {


    Converter<DoctorModel, DoctorSearchResponseDTO> converter;

    @Override
    public void convert(Set<DoctorModel> sources, Set<DoctorSearchResponseDTO> targets) {
        Objects.requireNonNull(sources, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + sources.getClass().getName());

        sources.forEach(source -> {
            DoctorSearchResponseDTO target = new DoctorSearchResponseDTO();
            converter.convert(source, target);
            targets.add(target);
        });
    }

    @Autowired
    public void setConverter(Converter<DoctorModel, DoctorSearchResponseDTO> converter) {
        this.converter = converter;
    }
}
