package be.drnow.app.converter.doctor;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.WorkingAgendaDTO;
import be.drnow.app.model.doctor.WorkingAgendaModel;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author : medalaoui
 * @since : 23/05/2020, sam.
 **/
@Component
public class DoctorWorkingAgendaModelToDTO implements Converter<WorkingAgendaModel, WorkingAgendaDTO> {

    @Override
    public void convert(WorkingAgendaModel source, WorkingAgendaDTO target) {
        Objects.requireNonNull(source.getPk(), DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + source.getClass().getName());

        target.setPk(source.getPk());
        target.setAvailability(source.isAvailability());
        target.setFromDate(source.getFromDate());
        target.setToDate(source.getToDate());
        target.setFromTime(source.getFromTime());
        target.setToTime(source.getToTime());
        //target.setDoctorPk(source.getDoctorModel().getPk());
    }
}
