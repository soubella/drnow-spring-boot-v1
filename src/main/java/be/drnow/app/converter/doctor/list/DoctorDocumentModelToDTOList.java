package be.drnow.app.converter.doctor.list;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.DoctorDocumentDTO;
import be.drnow.app.model.doctor.DoctorDocumentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Set;

/**
 * @author : medalaoui
 * @since : 23/05/2020, sam.
 **/
@Component(value = "DoctorDocumentModelToDTOList")
public class DoctorDocumentModelToDTOList implements Converter<Set<DoctorDocumentModel> , Set<DoctorDocumentDTO>> {

    Converter<DoctorDocumentModel, DoctorDocumentDTO> converter;

    @Override
    public void convert(Set<DoctorDocumentModel> sources, Set<DoctorDocumentDTO> targets) {
        Objects.requireNonNull(sources, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + sources.getClass().getName());

        sources.forEach(source -> {
            DoctorDocumentDTO target = new DoctorDocumentDTO();
            converter.convert(source, target);
            targets.add(target);
        });
    }

    @Autowired
    public void setConverter(Converter<DoctorDocumentModel, DoctorDocumentDTO> converter) {
        this.converter = converter;
    }
}
