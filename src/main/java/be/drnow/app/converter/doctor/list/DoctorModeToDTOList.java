package be.drnow.app.converter.doctor.list;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.DoctorDTO;
import be.drnow.app.model.doctor.DoctorModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Set;

/**
 * @author : medalaoui
 * @since : 24/05/2020, dim.
 **/
@Component("DoctorModeToDTOList")
public class DoctorModeToDTOList implements Converter<Set<DoctorModel>, Set<DoctorDTO>> {


    Converter<DoctorModel, DoctorDTO> converter;

    @Override
    public void convert(Set<DoctorModel> sources, Set<DoctorDTO> targets) {
        Objects.requireNonNull(sources, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL);

        sources.forEach(source -> {
            DoctorDTO target = new DoctorDTO();
            converter.convert(source, target);
            targets.add(target);
        });
    }

    @Autowired
    public void setConverter(Converter<DoctorModel, DoctorDTO> converter) {
        this.converter = converter;
    }
}
