package be.drnow.app.converter.doctor;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.*;
import be.drnow.app.dto.user.AddressDTO;
import be.drnow.app.model.doctor.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author : medalaoui
 * @since : 23/05/2020, sam.
 **/
@RequiredArgsConstructor
@Component("doctorModelToDTO")
public class DoctorModelToDTO implements Converter<DoctorModel, DoctorDTO> {

    private final Converter<Set<WorkingAgendaModel>, Set<WorkingAgendaDTO>> workingAgendaConverterAll;
    private final Converter<Set<DoctorDocumentModel>, Set<DoctorDocumentDTO>> doctorDocumentConvertAll;
    private final Converter<Set<LanguageModel>, Set<LanguageDTO>> languageConvertAll;
    private final Converter<Set<DiplomaModel>, Set<DiplomaDTO>> diplomaConvertAll;
    private final Converter<Set<ExperienceModel>, Set<ExperienceDTO>> experienceConvertAll;
    private final Converter<SpecialityModel, DoctorSpecialityDTO> specialityConverter;
    private final Converter<ProfessionModel, ProfessionDTO> professionConverter;
    private final Converter<Set<JobPriceModel>, Set<JobPriceDTO>> jobPriceConvertAll;

    @Override
    public void convert(DoctorModel source, DoctorDTO target) {
        Objects.requireNonNull(source.getPk(), DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + source.getClass().getName());

        target.setPk(source.getPk());
        target.setFirstName(source.getFirstName());
        target.setLastName(source.getLastName());
        target.setEmail(source.getEmail());
        target.setPhoneNumber(source.getPhoneNumber());
        target.setBirthday(source.getBirthday());
        target.setGender(source.getGender());
        target.setEnabled(source.isEnabled());
        target.setCanceled(source.isCanceled());
        target.setQualificationCode(source.getQualificationCode());
        target.setINAMI(source.getINAMI());
        target.setBio(source.getBio());
        target.setImage(source.getImage());
        target.setApproved(source.isApproved());
        target.setAgreementStatus(source.getAgreementStatus());
        target.setTeleconsultationPrice(source.getTeleconsultationPrice());
        target.setRepayable(source.isRepayable());

        populateAddress(source, target);

        if (!CollectionUtils.isEmpty(source.getWorkingAgendas())) {
            Set<WorkingAgendaDTO> workingAgendaDTOSet = new HashSet<>();
            workingAgendaConverterAll.convert(new HashSet<>(source.getWorkingAgendas()), workingAgendaDTOSet);
            target.setWorkingAgendas(workingAgendaDTOSet);
        }
        if (!CollectionUtils.isEmpty(source.getDoctorDocuments())) {
            Set<DoctorDocumentDTO> documentDTOSet = new HashSet<>();
            doctorDocumentConvertAll.convert(source.getDoctorDocuments(), documentDTOSet);
            target.setDoctorDocuments(documentDTOSet);
        }

        if (!CollectionUtils.isEmpty(source.getLanguages())) {
            Set<LanguageDTO> languageDTOSet = new HashSet<>();
            languageConvertAll.convert(source.getLanguages(), languageDTOSet);
            target.setLanguages(languageDTOSet);
        }

        if (!CollectionUtils.isEmpty(source.getDiplomas())) {
            Set<DiplomaDTO> diplomaDTOSet = new HashSet<>();
            diplomaConvertAll.convert(source.getDiplomas(), diplomaDTOSet);
            target.setDiplomas(diplomaDTOSet);
        }

        if (!CollectionUtils.isEmpty(source.getExperiences())) {
            Set<ExperienceDTO> experienceDTOSet = new HashSet<>();
            experienceConvertAll.convert(source.getExperiences(), experienceDTOSet);
            target.setExperiences(experienceDTOSet);
        }

        if (Objects.nonNull(source.getSpeciality())) {
            DoctorSpecialityDTO doctorSpecialityDTOSet = new DoctorSpecialityDTO();
            specialityConverter.convert(source.getSpeciality(), doctorSpecialityDTOSet);
            target.setSpeciality(doctorSpecialityDTOSet);
        }

        if (Objects.nonNull(source.getProfession())) {
            ProfessionDTO professionDTO = new ProfessionDTO();
            professionConverter.convert(source.getProfession(), professionDTO);
            target.setProfession(professionDTO);
        }

        if (!CollectionUtils.isEmpty(source.getJobPrices())) {
            Set<JobPriceDTO> jobPriceDTOS = new HashSet<>();
            jobPriceConvertAll.convert(source.getJobPrices(), jobPriceDTOS);
            target.setJobPrices(jobPriceDTOS);
        }
    }

    private void populateAddress(DoctorModel source, DoctorDTO target) {
        if (Objects.nonNull(source.getAddress())) {
            AddressDTO addressDTO = new AddressDTO();
            addressDTO.setCity(source.getAddress().getCity());
            addressDTO.setPostalCode(source.getAddress().getPostalCode());
            target.setAddress(addressDTO);
        }
    }

}
