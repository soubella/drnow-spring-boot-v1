package be.drnow.app.converter.doctor;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.ExperienceDTO;
import be.drnow.app.model.doctor.ExperienceModel;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author : medalaoui
 * @since : 24/05/2020, dim.
 **/
@Component("DoctorExperienceModelToDTO")
public class DoctorExperienceModelToDTO implements Converter<ExperienceModel , ExperienceDTO> {


    @Override
    public void convert(ExperienceModel source, ExperienceDTO target) {
        Objects.requireNonNull(source, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL);

        target.setPk(source.getPk());
        target.setTitle(source.getTitle());
        target.setDescription(source.getDescription());
        target.setStartAt(source.getStartAt());
        target.setEndAt(source.getEndAt());
    }
}
