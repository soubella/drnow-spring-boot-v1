package be.drnow.app.converter.doctor.list;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.WorkingAgendaDTO;
import be.drnow.app.model.doctor.WorkingAgendaModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Set;

/**
 * @author : medalaoui
 * @since : 23/05/2020, sam.
 **/
@Component("DoctorWorkingModelToDTOList")
public class DoctorWorkingModelToDTOList implements Converter<Set<WorkingAgendaModel>, Set<WorkingAgendaDTO>> {

    Converter<WorkingAgendaModel, WorkingAgendaDTO> converter;

    @Override
    public void convert(Set<WorkingAgendaModel> sources, Set<WorkingAgendaDTO> targets) {
        Objects.requireNonNull(sources, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + sources.getClass().getName());

        sources.forEach(source -> {
            WorkingAgendaDTO workingAgendaDTO = new WorkingAgendaDTO();
            converter.convert(source, workingAgendaDTO);
            targets.add(workingAgendaDTO);
        });
    }

    @Autowired
    public void setConverter(Converter<WorkingAgendaModel, WorkingAgendaDTO> converter) {
        this.converter = converter;
    }
}
