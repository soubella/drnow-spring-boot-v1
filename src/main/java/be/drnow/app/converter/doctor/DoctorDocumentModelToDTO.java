package be.drnow.app.converter.doctor;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.DoctorDocumentDTO;
import be.drnow.app.model.doctor.DoctorDocumentModel;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author : medalaoui
 * @since : 23/05/2020, sam.
 **/
@Component("DoctorDocumentModelToDTO")
public class DoctorDocumentModelToDTO implements Converter<DoctorDocumentModel, DoctorDocumentDTO> {

    @Override
    public void convert(DoctorDocumentModel source, DoctorDocumentDTO target) {
        Objects.requireNonNull(source, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + target);

        target.setName(source.getName());
        target.setUrl(source.getUrl());
        target.setPk(source.getPk());
    }
}
