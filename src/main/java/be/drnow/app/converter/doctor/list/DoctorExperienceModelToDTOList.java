package be.drnow.app.converter.doctor.list;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.ExperienceDTO;
import be.drnow.app.model.doctor.ExperienceModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Set;

/**
 * @author : medalaoui
 * @since : 24/05/2020, dim.
 **/
@Component("DoctorExperienceModelToDTOList")
public class DoctorExperienceModelToDTOList implements Converter<Set<ExperienceModel>, Set<ExperienceDTO>> {


    Converter<ExperienceModel, ExperienceDTO> converter;

    @Override
    public void convert(Set<ExperienceModel> sources, Set<ExperienceDTO> targets) {
        Objects.requireNonNull(sources, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + sources.getClass().getName());

        sources.forEach(source -> {
            ExperienceDTO target = new ExperienceDTO();
            converter.convert(source, target);
            targets.add(target);
        });
    }

    @Autowired
    public void setConverter(Converter<ExperienceModel, ExperienceDTO> converter) {
        this.converter = converter;
    }
}
