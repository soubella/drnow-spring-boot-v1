package be.drnow.app.converter.doctor;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.JobPriceDTO;
import be.drnow.app.model.doctor.JobPriceModel;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author : medalaoui
 * @since : 24/05/2020, dim.
 **/
@Component("DoctorJobPriceModelToDTO")
public class DoctorJobPriceModelToDTO implements Converter<JobPriceModel, JobPriceDTO> {

    @Override
    public void convert(JobPriceModel source, JobPriceDTO target) {
        Objects.requireNonNull(source, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + source.getClass().getName());

        target.setPk(source.getPk());
        target.setName(source.getName());
        target.setPrice(source.getPrice());
        target.setDescription(source.getDescription());
    }
}
