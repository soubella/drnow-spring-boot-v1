package be.drnow.app.converter.doctor;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.ProfessionDTO;
import be.drnow.app.model.doctor.ProfessionModel;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author : medalaoui
 * @since : 24/05/2020, dim.
 **/
@Component("DoctorProfessionModelToDTO")
public class DoctorProfessionModelToDTO implements Converter<ProfessionModel, ProfessionDTO> {

    @Override
    public void convert(ProfessionModel source, ProfessionDTO target) {
        Objects.requireNonNull(source, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + source.getClass().getName());

        target.setTitle(source.getTitle());
        target.setPk(source.getPk());
    }
}
