package be.drnow.app.converter.doctor.list;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.LanguageDTO;
import be.drnow.app.model.doctor.LanguageModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Set;

/**
 * @author : medalaoui
 * @since : 23/05/2020, sam.
 **/
@RequiredArgsConstructor
@Component("DoctorLanguagesModelToDTOList")
public class DoctorLanguagesModelToDTOList implements Converter<Set<LanguageModel>, Set<LanguageDTO>> {

    private final Converter<LanguageModel, LanguageDTO> converter;

    @Override
    public void convert(Set<LanguageModel> sources, Set<LanguageDTO> targets) {
        Objects.requireNonNull(sources, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + sources.getClass().getName());

        sources.forEach(source -> {
            LanguageDTO target = new LanguageDTO();
            converter.convert(source, target);
            targets.add(target);
        });
    }

}
