package be.drnow.app.converter.doctor;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.DoctorDTO;
import be.drnow.app.exception.RessourceNotFoundException;
import be.drnow.app.model.doctor.DoctorModel;
import be.drnow.app.model.doctor.ExperienceModel;
import be.drnow.app.repository.doctor.LanguageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Objects;

@RequiredArgsConstructor
@Component("DoctorDtoToModel")
public class DoctorDtoToModel implements Converter<DoctorDTO, DoctorModel> {

    private final LanguageRepository languageRepository;

    @Override
    public void convert(DoctorDTO source, DoctorModel target) {
        Objects.requireNonNull(source.getPk(), DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + source.getClass().getName());
        convertPersonalInfo(source, target);
        convertProfessionalInfo(source, target);
    }

    private void convertPersonalInfo(DoctorDTO source, DoctorModel target) {
        target.setFirstName(source.getFirstName());
        target.setLastName(source.getLastName());
        target.setBio(source.getBio());
        target.setPhoneNumber(source.getPhoneNumber());
        target.setGender(source.getGender());
        if (Objects.nonNull(source.getAddress())) {
            target.getAddress().setPostalCode(source.getAddress().getPostalCode());
            target.getAddress().setCity(source.getAddress().getCity());
        }
    }

    private void convertProfessionalInfo(DoctorDTO source, DoctorModel target) {
        if (Objects.nonNull(source.getQualificationCode())) {
            target.setQualificationCode(source.getQualificationCode());
        }
        if (Objects.nonNull(source.getINAMI())) {
            target.setINAMI(source.getINAMI());
        }
        if (Objects.nonNull(source.getAgreementStatus())) {
            target.setAgreementStatus(source.getAgreementStatus());
        }
        if (Objects.nonNull(source.getTeleconsultationPrice())) {
            target.setTeleconsultationPrice(source.getTeleconsultationPrice());
        }
        if (!CollectionUtils.isEmpty(source.getLanguages())) {
            source.getLanguages().forEach(
                    languageDTO -> target.getLanguages().add(
                            languageRepository.findById(languageDTO.getPk()).orElseThrow(() -> new RessourceNotFoundException("Language")
                            )));
        }
        if (!CollectionUtils.isEmpty(source.getExperiences())) {
            source.getExperiences().forEach(experienceDTO -> target.getExperiences().add(new ExperienceModel(experienceDTO.getTitle(), experienceDTO.getDescription(), experienceDTO.getStartAt(), experienceDTO.getEndAt())));
        }
    }
}
