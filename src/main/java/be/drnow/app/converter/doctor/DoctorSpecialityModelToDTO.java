package be.drnow.app.converter.doctor;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.DoctorSpecialityDTO;
import be.drnow.app.model.doctor.SpecialityModel;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author : medalaoui
 * @since : 24/05/2020, dim.
 **/
@Component("DoctorSpecialityModelToDTO")
public class DoctorSpecialityModelToDTO implements Converter<SpecialityModel, DoctorSpecialityDTO > {


    @Override
    public void convert(SpecialityModel source, DoctorSpecialityDTO target) {
        Objects.requireNonNull(source, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL);

        target.setPk(source.getPk());
        target.setName(source.getName());
    }
}
