package be.drnow.app.converter.doctor;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.WorkingAgendaDTO;
import be.drnow.app.exception.RessourceNotFoundException;
import be.drnow.app.model.doctor.WorkingAgendaModel;
import be.drnow.app.repository.doctor.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class DoctorWorkingAgendaDtoToModel implements Converter<WorkingAgendaDTO, WorkingAgendaModel> {

    private DoctorRepository doctorRepository;

    @Override
    public void convert(WorkingAgendaDTO source, WorkingAgendaModel target) {
        Objects.requireNonNull(source.getDoctorPk(), DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + source.getClass().getName());

        target.setPk(source.getPk());
        target.setAvailability(source.isAvailability());
        target.setFromDate(source.getFromDate());
        target.setToDate(source.getToDate());
        target.setFromTime(source.getFromTime());
        target.setToTime(source.getToTime());
        target.setDoctorModel(doctorRepository.findById(source.getDoctorPk()).orElseThrow(() -> new RessourceNotFoundException("Doctor")));
    }

    @Autowired
    public void setDoctorRepository(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }
}
