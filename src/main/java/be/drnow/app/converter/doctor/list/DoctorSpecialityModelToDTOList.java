package be.drnow.app.converter.doctor.list;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.DoctorSpecialityDTO;
import be.drnow.app.model.doctor.SpecialityModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Set;

/**
 * @author : medalaoui
 * @since : 24/05/2020, dim.
 **/
@Component("DoctorSpecialityModelToDTOList")
public class DoctorSpecialityModelToDTOList implements Converter<Set<SpecialityModel>, Set<DoctorSpecialityDTO>> {

    Converter<SpecialityModel, DoctorSpecialityDTO> converter;

    @Override
    public void convert(Set<SpecialityModel> sources, Set<DoctorSpecialityDTO> targets) {
        Objects.requireNonNull(sources, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + sources.getClass().getName());

        sources.forEach(source -> {
            DoctorSpecialityDTO target = new DoctorSpecialityDTO();
            converter.convert(source, target);
            targets.add(target);
        });

    }

    @Autowired
    public void setConverter(Converter<SpecialityModel, DoctorSpecialityDTO> converter) {
        this.converter = converter;
    }
}
