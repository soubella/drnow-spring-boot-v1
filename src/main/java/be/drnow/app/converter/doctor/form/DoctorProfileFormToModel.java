package be.drnow.app.converter.doctor.form;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.form.DoctorProfileForm;
import be.drnow.app.exception.RessourceNotFoundException;
import be.drnow.app.model.doctor.DoctorModel;
import be.drnow.app.repository.doctor.LanguageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.Objects;

@RequiredArgsConstructor
@Component("doctorProfileFormToModel")
public class DoctorProfileFormToModel implements Converter<DoctorProfileForm, DoctorModel> {

    private final LanguageRepository languageRepository;

    @Override
    public void convert(DoctorProfileForm source, DoctorModel target) {
        Objects.requireNonNull(source.getPk(), DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + source.getClass().getName());
        convertPersonalInfo(source, target);
        convertProfessionalInfo(source, target);
    }

    private void convertPersonalInfo(DoctorProfileForm source, DoctorModel target) {
        target.setFirstName(source.getFirstName());
        target.setLastName(source.getLastName());
        target.setBio(source.getBio());
        target.setPhoneNumber(source.getPhoneNumber());
        target.setGender(source.getGender());
        target.getAddress().setPostalCode(source.getPostalCode());
        target.getAddress().setCity(source.getCity());
        target.setTeleconsultationPrice(source.getTeleconsultationPrice());
    }

    private void convertProfessionalInfo(DoctorProfileForm source, DoctorModel target) {
        if (!CollectionUtils.isEmpty(source.getLanguages())) {
            target.setLanguages(new HashSet<>());
            source.getLanguages()
                    .forEach(languageDTO -> target.getLanguages()
                            .add(languageRepository.findById(languageDTO.getPk()).orElseThrow(() -> new RessourceNotFoundException("Language"))));
        }
        target.setAgreementStatus(source.getAgreementStatus());
    }
}
