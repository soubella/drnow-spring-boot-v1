package be.drnow.app.converter.doctor;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.DiplomaDTO;
import be.drnow.app.model.doctor.DiplomaModel;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author : medalaoui
 * @since : 24/05/2020, dim.
 **/
@Component("DoctorDiplomasModelToDTO")
public class DoctorDiplomasModelToDTO implements Converter<DiplomaModel, DiplomaDTO> {

    @Override
    public void convert(DiplomaModel source, DiplomaDTO target) {
        Objects.requireNonNull(source, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + source.getClass().getName());

        target.setPk(source.getPk());
        target.setTitle(source.getTitle());
        target.setDescription(source.getDescription());
        target.setDateDiploma(source.getDateDiploma());
        target.setUniversity(source.getUniversity());
    }
}
