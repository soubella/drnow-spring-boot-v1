package be.drnow.app.converter.user;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.user.CountryDTO;
import be.drnow.app.model.user.CountryModel;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * @author : medalaoui
 * @since : 13/05/2020, mer.
 **/
@Component
public class CountryConverter implements Converter<CountryModel, CountryDTO> {

    @Override
    public void convert(CountryModel countryModel, CountryDTO countryDTO) {
        Assert.notNull(countryModel, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL);

        countryDTO.setPk(countryModel.getPk());
        countryDTO.setCode(countryModel.getCode());
        countryDTO.setName(countryModel.getName());
    }

}
