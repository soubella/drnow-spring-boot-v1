package be.drnow.app.converter.user;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.user.AddressDTO;
import be.drnow.app.dto.user.CountryDTO;
import be.drnow.app.model.user.AddressModel;
import be.drnow.app.model.user.CountryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * @author : medalaoui
 * @since : 13/05/2020, mer.
 **/
@Component
public class AddressConverter implements Converter<AddressModel, AddressDTO> {

    private static final String SOURCE = AddressModel.class.getName();

    private Converter<CountryModel, CountryDTO> countryConverter;

    @Override
    public void convert(AddressModel addressModel, AddressDTO addressDTO) {
        Assert.notNull(addressModel, DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + SOURCE);

        addressDTO.setPk(addressModel.getPk());
        addressDTO.setLine1(addressModel.getLine1());
        addressDTO.setLine2(addressModel.getLine2());
        addressDTO.setCity(addressModel.getCity());

        addressDTO.setCountry(getCounterConvert(addressModel));
    }

    private CountryDTO getCounterConvert(AddressModel addressModel) {
        CountryDTO countryDTO = new CountryDTO();
        Assert.notNull(addressModel.getCountry(), DoctorylMessagesConstants.Error.OBJECT_NOT_NULL + addressModel.getClass().getName());
        countryConverter.convert(addressModel.getCountry(), countryDTO);
        return countryDTO;
    }

    @Autowired
    public void setCountryConverter(Converter<CountryModel, CountryDTO> countryConverter) {
        this.countryConverter = countryConverter;
    }
}
