package be.drnow.app.converter.user;

import be.drnow.app.converter.Converter;
import be.drnow.app.dto.user.NotificationDTO;
import be.drnow.app.model.user.NotificationModel;
import org.springframework.stereotype.Component;

@Component
public class NotificationDTOToModel implements Converter<NotificationDTO, NotificationModel> {

    @Override
    public void convert(NotificationDTO source, NotificationModel target) {

    }
}
