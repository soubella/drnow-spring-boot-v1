package be.drnow.app.controller;

import be.drnow.app.core.annotation.DoctorylController;
import be.drnow.app.dto.user.MessageResponseDTO;
import be.drnow.app.dto.user.NotificationDTO;
import be.drnow.app.facade.user.NotificationFacade;
import be.drnow.app.model.user.NotificationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@DoctorylController("/notifications")
public class NotificationController {


    private NotificationFacade notificationFacade;

    @GetMapping("/user/{userPk}")
    public ResponseEntity<List<NotificationModel>> getUserNotifications(@PathVariable("userPk") UUID userPk) {

        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
    }

    @PutMapping("/{pk}/user/{userPk}")
    public ResponseEntity<MessageResponseDTO> updateNotification(@PathVariable("pk") UUID pk, @PathVariable("userPk") UUID userPk) {

        return new ResponseEntity<>(new MessageResponseDTO(""), HttpStatus.OK);
    }

    @PutMapping("/save")
    public ResponseEntity<MessageResponseDTO> addNotification(@Validated @RequestBody NotificationDTO notificationDTO) {
        return new ResponseEntity<>(new MessageResponseDTO(""), HttpStatus.OK);
    }

    @Autowired
    public void setNotificationFacade(NotificationFacade notificationFacade) {
        this.notificationFacade = notificationFacade;
    }
}
