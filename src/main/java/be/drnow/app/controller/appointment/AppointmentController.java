package be.drnow.app.controller.appointment;

import be.drnow.app.core.annotation.DoctorylController;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.appointment.AgendaAppointmentForm;
import be.drnow.app.dto.appointment.AppointmentFormDTO;
import be.drnow.app.dto.appointment.CancelAppointmentForm;
import be.drnow.app.dto.user.MessageResponseDTO;
import be.drnow.app.exception.UnauthorizedException;
import be.drnow.app.facade.appointment.AppointmentFacade;
import be.drnow.app.facade.user.UserFacade;
import be.drnow.app.model.appointment.AppointmentModel;
import be.drnow.app.model.appointment.AppointmentStatusEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@DoctorylController("/appointment")
public class AppointmentController {

    private final Logger logger = LoggerFactory.getLogger(AppointmentController.class);

    private AppointmentFacade appointmentFacade;
    private UserFacade userFacade;

    private final List<AppointmentStatusEnum> appointmentStatusEnums = Arrays.asList(AppointmentStatusEnum.CANCELED_BY_DOCTOR, AppointmentStatusEnum.CANCELED_BY_PATIENT);

    private static final String APPOINTMENT_HAS_BEEN_TAKEN_SUCCESSFULLY = DoctorylMessagesConstants.Succes.APPOINTMENT_HAS_BEEN_TAKEN_SUCCESSFULLY;
    private static final String APPOINTMENT_HAS_BEEN_UPDATED_SUCCESSFULLY = String.format(DoctorylMessagesConstants.Succes.OBJECT_HAS_BEEN_UPDATED_SUCCESSFULLY, "Appointment");
    private static final String APPOINTMENT_HAS_BEEN_CANCELED_SUCCESSFULLY = String.format(DoctorylMessagesConstants.Succes.OBJECT_HAS_BEEN_CANCELED_SUCCESSFULLY, "Appointment");

    @PostMapping("/add")
    public ResponseEntity<MessageResponseDTO> takeAppointment(@Validated @RequestBody AppointmentFormDTO appointmentFormDTO) {
        try {
            appointmentFacade.takeAppointment(appointmentFormDTO);
        } catch (MessagingException e) {
            logger.warn(e.getMessage());
        }
        return new ResponseEntity<>(new MessageResponseDTO(APPOINTMENT_HAS_BEEN_TAKEN_SUCCESSFULLY), HttpStatus.OK);
    }

    @Secured({"ROLE_PATIENT", "ROLE_DOCTOR"})
    @PostMapping("/add-for-patient")
    public ResponseEntity<MessageResponseDTO> takeAppointmentForPatientByDoctor(@Validated @RequestBody AgendaAppointmentForm agendaAppointmentForm) {
        agendaAppointmentForm.setDoctorPk(userFacade.getCurrentUserPk());
        try {
            appointmentFacade.takeAppointmentForPatient(agendaAppointmentForm);
        } catch (MessagingException e) {
            logger.warn(e.getMessage());
        }
        return new ResponseEntity<>(new MessageResponseDTO(APPOINTMENT_HAS_BEEN_TAKEN_SUCCESSFULLY), HttpStatus.OK);
    }

    @Secured({"ROLE_PATIENT", "ROLE_DOCTOR"})
    @PutMapping("/{pk}/update")
    public ResponseEntity<MessageResponseDTO> updateAppointment(@Validated @RequestBody AgendaAppointmentForm agendaAppointmentForm, @PathVariable("pk") UUID pk) {
        checkAppointmentOwner(pk);
        try {
            appointmentFacade.updateAppointmentStartDate(agendaAppointmentForm, pk);
        } catch (MessagingException e) {
            logger.warn(e.getMessage());
        }
        logger.info(APPOINTMENT_HAS_BEEN_UPDATED_SUCCESSFULLY);
        return new ResponseEntity<>(new MessageResponseDTO(APPOINTMENT_HAS_BEEN_UPDATED_SUCCESSFULLY), HttpStatus.OK);
    }

    @Secured({"ROLE_PATIENT", "ROLE_DOCTOR"})
    @PutMapping("/{pk}/cancel")
    public ResponseEntity<MessageResponseDTO> cancelAppointment(@Validated @RequestBody CancelAppointmentForm cancelAppointmentForm, @PathVariable("pk") UUID pk) {
        checkAppointmentOwner(pk);
        try {
            appointmentFacade.cancelAppointment(pk, cancelAppointmentForm.getCancellationReason(), cancelAppointmentForm.getAppointmentStatusEnum());
            logger.info(APPOINTMENT_HAS_BEEN_CANCELED_SUCCESSFULLY);
        } catch (MessagingException e) {
            logger.warn(e.getMessage());
        }

        return new ResponseEntity<>(new MessageResponseDTO(APPOINTMENT_HAS_BEEN_CANCELED_SUCCESSFULLY), HttpStatus.OK);
    }

    @Secured("ROLE_DOCTOR")
    @GetMapping("/doctor/{doctorPk}/{filter}")
    public ResponseEntity<Set<AppointmentModel>> getAllAppointmentsByDoctorPk(@PathVariable("doctorPk") UUID doctorPk, @PathVariable("filter") boolean filter) {
        if (!userFacade.getCurrentUserPk().equals(doctorPk)) {
            throw new UnauthorizedException("You don't have permission to get this data");
        }

        if (filter)
            return new ResponseEntity<>(appointmentFacade.findAppointmentsByDoctorPk(doctorPk, appointmentStatusEnums), HttpStatus.OK);

        return new ResponseEntity<>(appointmentFacade.findAppointmentsByDoctorPk(doctorPk, null), HttpStatus.OK);
    }

    @GetMapping("/doctor/{doctorPk}/availability/{time}/check")
    public ResponseEntity<MessageResponseDTO> checkAvailability(@PathVariable("doctorPk") UUID doctorPk, @PathVariable("time") String time) {
        MessageResponseDTO messageResponseDTO;

        LocalDateTime localDateTime = LocalDateTime.parse(time, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));

        if (appointmentFacade.isAvailable(doctorPk, localDateTime))
            messageResponseDTO = new MessageResponseDTO("Yes");
        else
            messageResponseDTO = new MessageResponseDTO("No");

        return new ResponseEntity<>(messageResponseDTO, HttpStatus.OK);
    }

    @Secured("ROLE_PATIENT")
    @GetMapping("/patient/{patientPk}")
    public ResponseEntity<Set<AppointmentModel>> getAllAppointmentsByPatientPk(@PathVariable("patientPk") UUID patientPk) {
        if (!userFacade.getCurrentUserPk().equals(patientPk)) {
            throw new UnauthorizedException("You don't have permission to get this data");
        }

        return new ResponseEntity<>(appointmentFacade.findAppointmentsByPatientPk(patientPk), HttpStatus.OK);
    }

    private void checkAppointmentOwner(UUID appointmentPk) {
        if (!appointmentFacade.isAppointmentOwner(userFacade.getCurrentUserPk(), appointmentPk)) {
            throw new UnauthorizedException("You don't have permission to edit this appointment");
        }
    }

    @Autowired
    public void setUserFacade(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @Autowired
    public void setAppointmentFacade(AppointmentFacade appointmentFacade) {
        this.appointmentFacade = appointmentFacade;
    }

}
