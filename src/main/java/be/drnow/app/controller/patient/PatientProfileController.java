package be.drnow.app.controller.patient;

import be.drnow.app.core.annotation.DoctorylController;
import be.drnow.app.dto.patient.PatientDTO;
import be.drnow.app.dto.patient.form.PatientRegisterForm;
import be.drnow.app.dto.user.MessageResponseDTO;
import be.drnow.app.exception.UnauthorizedException;
import be.drnow.app.facade.patient.PatientFacade;
import be.drnow.app.facade.user.UserFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.UUID;

@Secured("ROLE_PATIENT")
@DoctorylController("/patient")
public class PatientProfileController {

    private final Logger logger = LoggerFactory.getLogger(PatientProfileController.class);

    public static final String USER_UPDATED_SUCCESSFULLY = "user updated successfully";

    private PatientFacade patientFacade;
    private UserFacade userFacade;

    @PatchMapping("/{pk}/update")
    public ResponseEntity<MessageResponseDTO> updatePatientProfile(@PathVariable UUID pk, @RequestBody PatientRegisterForm patientRegisterForm) {
        patientFacade.updatePatientProfile(pk, patientRegisterForm);
        logger.info(USER_UPDATED_SUCCESSFULLY);
        return new ResponseEntity<>(new MessageResponseDTO(USER_UPDATED_SUCCESSFULLY), HttpStatus.OK);
    }

    @GetMapping("/{pk}")
    public ResponseEntity<PatientDTO> findPatientByPk(@PathVariable UUID pk) {
        if (!userFacade.getCurrentUserPk().equals(pk)) {
            throw new UnauthorizedException("You don't have permission to get this data");
        }
        return new ResponseEntity<>(patientFacade.findPatientByPk(pk), HttpStatus.OK);
    }

    @Autowired
    public void setPatientFacade(PatientFacade patientFacade) {
        this.patientFacade = patientFacade;
    }

    @Autowired
    public void setUserFacade(UserFacade userFacade) {
        this.userFacade = userFacade;
    }
}
