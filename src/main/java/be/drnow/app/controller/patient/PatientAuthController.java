package be.drnow.app.controller.patient;

import be.drnow.app.core.annotation.DoctorylController;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.patient.form.PatientRegisterForm;
import be.drnow.app.dto.shared.MailDTO;
import be.drnow.app.dto.user.JwtResponseDTO;
import be.drnow.app.dto.user.LoginRequestDTO;
import be.drnow.app.dto.user.MessageResponseDTO;
import be.drnow.app.facade.patient.PatientFacade;
import be.drnow.app.facade.user.UserFacade;
import be.drnow.app.model.patient.PatientModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.mail.MessagingException;
import java.util.HashMap;
import java.util.Map;

@DoctorylController("/patient/auth")
public class PatientAuthController {

    private final Logger logger = LoggerFactory.getLogger(PatientAuthController.class);

    private static final String USER_REGISTERED_SUCCESSFULLY = DoctorylMessagesConstants.Succes.USER_REGISTERED_SUCCESSFULLY;
    private static final String SUBJECT_FOR_REGISTRATION_VERIFICATION_MAIL = DoctorylMessagesConstants.Email.SUBJECT_FOR_REGISTRATION_VERIFICATION_MAIL;
    private static final String HEADER_EMAIL_VERIFICATION = DoctorylMessagesConstants.Email.HEADER_EMAIL_VERIFICATION;
    private static final String BODY_EMAIL_VERIFICATION = DoctorylMessagesConstants.Email.BODY_EMAIL_VERIFICATION;

    private PatientFacade patientFacade;
    private UserFacade userFacade;

    @PostMapping("/register")
    public ResponseEntity<MessageResponseDTO> registerPatient(@Validated @RequestBody PatientRegisterForm patientRegisterForm) {

        MessageResponseDTO messageResponse;
        try {
            // Save doctor profile
            PatientModel patientModel = patientFacade.registerPatient(patientRegisterForm);
            messageResponse = new MessageResponseDTO(USER_REGISTERED_SUCCESSFULLY);
            messageResponse.setCode(patientModel.getPk().toString());
            logger.info(USER_REGISTERED_SUCCESSFULLY);
            // Send register email
            // MailDTO mail = buildRegisterEmail(patientRegisterForm);

            return new ResponseEntity<>(messageResponse, HttpStatus.CREATED);
        } catch (MessagingException e) {
            logger.info(e.getMessage());
            return new ResponseEntity<>(new MessageResponseDTO(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<JwtResponseDTO> authenticateUser(@Validated @RequestBody LoginRequestDTO loginRequest) {
        JwtResponseDTO jwtResponse = userFacade.authenticateUser(loginRequest);
        return new ResponseEntity<>(jwtResponse, HttpStatus.OK);
    }

    @PostMapping("/beneficiary/add")
    public ResponseEntity<MessageResponseDTO> addBeneficiary(@Validated @RequestBody PatientRegisterForm patientRegisterForm) {
        PatientModel patientModel = patientFacade.addBeneficiary(patientRegisterForm);
        MessageResponseDTO messageResponse = new MessageResponseDTO(USER_REGISTERED_SUCCESSFULLY);
        messageResponse.setCode(patientModel.getPk().toString());
        return new ResponseEntity<>(messageResponse, HttpStatus.CREATED);
    }

    private MailDTO buildRegisterEmail(PatientRegisterForm patientRegisterForm) {
        MailDTO mail = new MailDTO();
        mail.setMailTo(patientRegisterForm.getEmail());
        mail.setSubject(SUBJECT_FOR_REGISTRATION_VERIFICATION_MAIL);
        Map<String, Object> model = new HashMap<>();
        model.put("header", String.format(HEADER_EMAIL_VERIFICATION, patientRegisterForm.getFirstName()));
        model.put("body", BODY_EMAIL_VERIFICATION);
        mail.setProps(model);
        return mail;
    }

    @Autowired
    public void setPatientFacade(PatientFacade patientFacade) {
        this.patientFacade = patientFacade;
    }

    @Autowired
    public void setUserFacade(UserFacade userFacade) {
        this.userFacade = userFacade;
    }
}
