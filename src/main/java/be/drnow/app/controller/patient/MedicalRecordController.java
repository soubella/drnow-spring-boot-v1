package be.drnow.app.controller.patient;

import be.drnow.app.core.annotation.DoctorylController;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.patient.form.MedicalRecordForm;
import be.drnow.app.dto.user.MessageResponseDTO;
import be.drnow.app.facade.patient.PatientFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Secured("ROLE_PATIENT")
@DoctorylController("/patient/medical-record")
public class MedicalRecordController {

    private static final String MEDICAL_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY = DoctorylMessagesConstants.Succes.MEDICAL_RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY;

    private PatientFacade patientFacade;

    @PostMapping("/add")
    public ResponseEntity<MessageResponseDTO> addMedicalRecordToPatient(@Validated @RequestBody MedicalRecordForm medicalRecordForm) {
        patientFacade.addOrUpdateMedicalRecord(medicalRecordForm);
        return new ResponseEntity<>(new MessageResponseDTO(MEDICAL_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY), HttpStatus.CREATED);
    }

    @Autowired
    public void setPatientFacade(PatientFacade patientFacade) {
        this.patientFacade = patientFacade;
    }
}
