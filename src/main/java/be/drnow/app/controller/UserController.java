package be.drnow.app.controller;

import be.drnow.app.core.annotation.DoctorylController;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.shared.MailDTO;
import be.drnow.app.dto.user.*;
import be.drnow.app.dto.user.form.UserSocialNumberForm;
import be.drnow.app.facade.user.UserFacade;
import be.drnow.app.helper.LanguageHelper;
import be.drnow.app.helper.MailHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.mail.MessagingException;
import java.util.HashMap;
import java.util.Map;

@DoctorylController("/user/auth")
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    private static final String SUBJECT_FOR_CHANGE_EMAIL_VERIFICATION_MAIL = DoctorylMessagesConstants.Email.SUBJECT_FOR_CHANGE_EMAIL_VERIFICATION_MAIL;
    private static final String BODY_EMAIL_VERIFICATION = DoctorylMessagesConstants.Email.BODY_EMAIL_VERIFICATION;
    private static final String YOUR_EMAIL_HAS_BEEN_CHANGED_SUCCESSFULLY = DoctorylMessagesConstants.Succes.YOUR_EMAIL_HAS_BEEN_CHANGED_SUCCESSFULLY;
    private static final String USER_EXIST = "User exist with this email";
    private static final String USER_NOT_EXIST = "User not exist with this email";

    private UserFacade userFacade;

    private MailHelper mailHelper;

    @PostMapping("/check")
    public ResponseEntity<MessageResponseDTO> checkIfUserExist(@Validated @RequestBody LoginRequestDTO loginRequest) {
        if (userFacade.checkIfUserExistByEmail(loginRequest.getEmail())) {
            logger.info(USER_EXIST);
            return new ResponseEntity<>(new MessageResponseDTO(USER_EXIST), HttpStatus.OK);
        }
        logger.info(USER_NOT_EXIST);
        return new ResponseEntity<>(new MessageResponseDTO(USER_NOT_EXIST), HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/account/password/recovery")
    public ResponseEntity<MessageResponseDTO> requestPasswordRecoveryCode(@RequestHeader(value = "Accept-Language") String userLanguage, @Validated @RequestBody EmailRequestDTO emailRequestDTO) {
        try {
            MailDTO mailDTO = mailHelper.buildPasswordRecoveryEmail(emailRequestDTO.getEmail(), LanguageHelper.getUserLocalFromHeader(userLanguage));
            userFacade.sendPasswordRecoveryCode(mailDTO);
            userFacade.sendPasswordRecoveryCodeEmail(mailDTO);
        } catch (MessagingException e) {
            logger.error(e.getMessage());
        }
        MessageResponseDTO messageResponse = new MessageResponseDTO(DoctorylMessagesConstants.Succes.CONFIRMATION_CODE_HAS_BEEN_SENT_TO_YOUR_EMAIL);
        return new ResponseEntity<>(messageResponse, HttpStatus.CREATED);
    }

    @PostMapping("/account/confirmation-code/check")
    public ResponseEntity<MessageResponseDTO> checkConfirmationCode(@Validated @RequestBody ConfirmationCodeRequestDTO confirmationCodeRequestDTO) {
        userFacade.checkConfirmationCode(confirmationCodeRequestDTO);
        MessageResponseDTO messageResponse = new MessageResponseDTO(DoctorylMessagesConstants.Succes.CONFIRMATION_OK);
        return new ResponseEntity<>(messageResponse, HttpStatus.OK);
    }

    @PostMapping("/account/password/reset")
    public ResponseEntity<MessageResponseDTO> resetPassword(@Validated @RequestBody NewPasswordRequestDTO newPasswordRequestDTO) {
        userFacade.resetPassword(newPasswordRequestDTO);
        MessageResponseDTO messageResponse = new MessageResponseDTO(DoctorylMessagesConstants.Succes.PASSWORD_HAS_BEEN_CHANGED);
        return new ResponseEntity<>(messageResponse, HttpStatus.OK);
    }

    @PostMapping("/account/password/change")
    public ResponseEntity<MessageResponseDTO> changePassword(@Validated @RequestBody NewPasswordRequestDTO newPasswordRequestDTO) {
        userFacade.changePassword(newPasswordRequestDTO);
        MessageResponseDTO messageResponse = new MessageResponseDTO(DoctorylMessagesConstants.Succes.PASSWORD_HAS_BEEN_CHANGED);
        return new ResponseEntity<>(messageResponse, HttpStatus.OK);
    }

    @PostMapping("/account/email/recovery")
    public ResponseEntity<MessageResponseDTO> requestEmailRecoveryCode(@Validated @RequestBody ChangeEmailDTO changeEmailDTO) {
        try {
            MailDTO mailDTO = buildRecoveryEmail(changeEmailDTO.getNewEmail(), SUBJECT_FOR_CHANGE_EMAIL_VERIFICATION_MAIL, BODY_EMAIL_VERIFICATION);
            userFacade.sendEmailRecoveryCode(mailDTO, changeEmailDTO.getOldEmail());
        } catch (MessagingException e) {
            logger.error(e.getMessage());
        }
        MessageResponseDTO messageResponse = new MessageResponseDTO(DoctorylMessagesConstants.Succes.CONFIRMATION_CODE_HAS_BEEN_SENT_TO_YOUR_EMAIL);
        return new ResponseEntity<>(messageResponse, HttpStatus.CREATED);
    }

    @PostMapping("/account/email/reset")
    public ResponseEntity<MessageResponseDTO> resetEmail(@Validated @RequestBody ChangeEmailDTO changeEmailDTO) {
        userFacade.resetEmail(changeEmailDTO);
        MessageResponseDTO messageResponse = new MessageResponseDTO(YOUR_EMAIL_HAS_BEEN_CHANGED_SUCCESSFULLY);
        return new ResponseEntity<>(messageResponse, HttpStatus.OK);
    }

    @PostMapping("/security-number/add")
    public ResponseEntity<MessageResponseDTO> addSecurityNumber(@Validated @RequestBody UserSocialNumberForm userSocialNumberForm) {
        MessageResponseDTO messageResponse = new MessageResponseDTO(DoctorylMessagesConstants.Succes.YOUR_SECURITY_NUMBER_HAS_BEEN_ADDED_SUCCESSFULLY);
        userFacade.addSecurityNumberToUser(userSocialNumberForm);
        return new ResponseEntity<>(messageResponse, HttpStatus.CREATED);
    }

    private MailDTO buildRecoveryEmail(String email, String subject, String body) {
        MailDTO mail = new MailDTO();
        mail.setSubject(subject);
        mail.setMailTo(email);
        Map<String, Object> model = new HashMap<>();
        model.put("body", body);
        mail.setProps(model);
        return mail;
    }

    @Autowired
    public void setUserFacade(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @Autowired
    public void setMailHelper(MailHelper mailHelper) {
        this.mailHelper = mailHelper;
    }
}
