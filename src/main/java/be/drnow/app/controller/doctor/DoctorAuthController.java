package be.drnow.app.controller.doctor;

import be.drnow.app.core.annotation.DoctorylController;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.form.DoctorRegisterForm;
import be.drnow.app.dto.user.ConfirmationCodeRequestDTO;
import be.drnow.app.dto.user.JwtResponseDTO;
import be.drnow.app.dto.user.LoginRequestDTO;
import be.drnow.app.dto.user.MessageResponseDTO;
import be.drnow.app.facade.doctor.DoctorFacade;
import be.drnow.app.facade.user.UserFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.mail.MessagingException;

@DoctorylController("/doctor/auth")
public class DoctorAuthController {

    private final Logger logger = LoggerFactory.getLogger(DoctorAuthController.class);

    public static final String USER_REGISTERED_SUCCESSFULLY = DoctorylMessagesConstants.Succes.USER_REGISTERED_SUCCESSFULLY;
    public static final String ACCOUNT_CONFIRMED_SUCCESSFULLY = DoctorylMessagesConstants.Succes.ACCOUNT_CONFIRMED_SUCCESSFULLY;
    public static final String ACCOUNT_HAS_BEEN_VERIFIED_SUCCESSFULLY = DoctorylMessagesConstants.Succes.ACCOUNT_HAS_BEEN_VERIFIED_SUCCESSFULLY;

    private DoctorFacade doctorFacade;
    private UserFacade userFacade;

    @PostMapping("/register")
    public ResponseEntity<MessageResponseDTO> registerUser(@RequestHeader(value = "Accept-Language") String userLanguage, @Validated @RequestBody DoctorRegisterForm doctorRegisterForm) {

        MessageResponseDTO messageResponse;
        try {
            // Save doctor profile
            doctorFacade.registerDoctor(doctorRegisterForm);
            messageResponse = new MessageResponseDTO(USER_REGISTERED_SUCCESSFULLY);
            logger.info(USER_REGISTERED_SUCCESSFULLY);
            // Send confirmation email
            //doctorFacade.sendRegisterVerificationEmail(doctorRegisterForm, userLanguage);
            // Send welcome email
            doctorFacade.sendWelcomeEmail(doctorRegisterForm, userLanguage);

            return new ResponseEntity<>(messageResponse, HttpStatus.CREATED);
        } catch (MessagingException e) {
            logger.warn(e.getMessage());
            return new ResponseEntity<>(new MessageResponseDTO(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/account/confirm")
    public ResponseEntity<MessageResponseDTO> confirmAccount(@RequestBody ConfirmationCodeRequestDTO confirmationCodeRequestDTO) {
        doctorFacade.confirmAccount(confirmationCodeRequestDTO);
        MessageResponseDTO messageResponse = new MessageResponseDTO(ACCOUNT_CONFIRMED_SUCCESSFULLY);
        logger.info(ACCOUNT_HAS_BEEN_VERIFIED_SUCCESSFULLY);
        return new ResponseEntity<>(messageResponse, HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<JwtResponseDTO> authenticateUser(@RequestHeader(value = "Accept-Language") String userLanguage, @Validated @RequestBody LoginRequestDTO loginRequest) {
        logger.info(userLanguage);
        JwtResponseDTO jwtResponse = userFacade.authenticateUser(loginRequest);
        return new ResponseEntity<>(jwtResponse, HttpStatus.OK);
    }

    @Autowired
    public void setDoctorFacade(DoctorFacade doctorFacade) {
        this.doctorFacade = doctorFacade;
    }

    @Autowired
    public void setUserFacade(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

}
