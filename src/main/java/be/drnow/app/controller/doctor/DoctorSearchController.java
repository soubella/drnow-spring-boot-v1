package be.drnow.app.controller.doctor;

import be.drnow.app.core.annotation.DoctorylController;
import be.drnow.app.dto.doctor.DoctorDTO;
import be.drnow.app.dto.doctor.DoctorFullNameDTO;
import be.drnow.app.dto.doctor.DoctorSearchResultDTO;
import be.drnow.app.dto.doctor.response.DoctorSearchResponseDTO;
import be.drnow.app.exception.InputNotValidException;
import be.drnow.app.facade.doctor.DoctorFacade;
import be.drnow.app.repository.doctor.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@DoctorylController
public class DoctorSearchController {

    private DoctorFacade doctorFacade;

    private DoctorRepository doctorRepository;

    @GetMapping("/search")
    public ResponseEntity<Set<DoctorSearchResponseDTO>> doctorSearcher(@RequestParam(value = "doctor", required = false, defaultValue = "") final String value
            , @RequestParam(value = "address", required = false, defaultValue = "") final String address) {
        return new ResponseEntity<>(doctorFacade.searchDoctor(value, address), HttpStatus.OK);
    }

    @GetMapping("/doctor/{pkParam}")
    public ResponseEntity<DoctorDTO> findByPk(@PathVariable Optional<String> pkParam) {
        if (!pkParam.isPresent()) throw new InputNotValidException("Doctor pk");
        UUID pk = UUID.fromString(pkParam.get());
        return new ResponseEntity<>(doctorFacade.findByPk(pk), HttpStatus.OK);
    }

    @GetMapping("/doctor/{pkParam}/profile")
    public ResponseEntity<DoctorSearchResultDTO> findDoctorProfileByPk(@PathVariable Optional<String> pkParam) {
        if (!pkParam.isPresent()) throw new InputNotValidException("Doctor pk");
        UUID pk = UUID.fromString(pkParam.get());
        return new ResponseEntity<>(doctorFacade.findDoctorProfileByPk(pk), HttpStatus.OK);
    }

    @GetMapping("/doctor/name/{keyword}")
    public ResponseEntity<List<DoctorFullNameDTO>> findDoctorsByFullName(@PathVariable Optional<String> keyword) {
        if (!keyword.isPresent()) throw new InputNotValidException("keyword");
        return new ResponseEntity<>(doctorRepository.findDoctorByFullName(keyword.get()), HttpStatus.OK);
    }

    @GetMapping("/doctors/search/{keyword}/{page}/{size}")
    public ResponseEntity<List<DoctorSearchResultDTO>> searchForDoctors(@PathVariable Optional<String> keyword, @PathVariable int page, @PathVariable int size) {
        if (!keyword.isPresent()) throw new InputNotValidException("keyword");
        return new ResponseEntity<>(doctorFacade.searchForDoctors(keyword.get(), PageRequest.of(page, size)), HttpStatus.OK);
    }

    @GetMapping("/doctors/all/{page}/{size}")
    public ResponseEntity<List<DoctorSearchResultDTO>> findAllDoctors(@PathVariable int page, @PathVariable int size) {
        Sort sort = Sort.by(Sort.Order.asc("speciality.rank"), Sort.Order.asc("profession.rank"), Sort.Order.desc("workingAgendas.size"));
        return new ResponseEntity<>(doctorFacade.findAllDoctors(PageRequest.of(page, size, sort)), HttpStatus.OK);
    }

    @Autowired
    public void setDoctorFacade(DoctorFacade doctorFacade) {
        this.doctorFacade = doctorFacade;
    }

    @Autowired
    public void setDoctorRepository(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }
}
