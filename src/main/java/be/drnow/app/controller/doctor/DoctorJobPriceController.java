package be.drnow.app.controller.doctor;

import be.drnow.app.core.annotation.DoctorylController;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.form.DoctorJobPriceForm;
import be.drnow.app.dto.user.MessageResponseDTO;
import be.drnow.app.exception.InputNotValidException;
import be.drnow.app.facade.doctor.JobPriceFacade;
import be.drnow.app.model.doctor.JobPriceModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Secured("ROLE_DOCTOR")
@DoctorylController
public class DoctorJobPriceController {

    private JobPriceFacade jobPriceFacade;

    @PostMapping("/doctor/jobs/save")
    public ResponseEntity<MessageResponseDTO> addOrUpdateJobPrice(@Validated @RequestBody DoctorJobPriceForm doctorJobPriceForm) {

        MessageResponseDTO messageResponseDTO;
        jobPriceFacade.addOrUpdateJobPrice(doctorJobPriceForm);
        if (doctorJobPriceForm.getPk() != null) {
            messageResponseDTO = new MessageResponseDTO(DoctorylMessagesConstants.Succes.JOB_HAS_BEEN_UPDATED_SUCCESSFULLY);
            return new ResponseEntity<>(messageResponseDTO, HttpStatus.OK);
        } else {
            messageResponseDTO = new MessageResponseDTO(DoctorylMessagesConstants.Succes.JOB_HAS_BEEN_ADDED_SUCCESSFULLY);
            return new ResponseEntity<>(messageResponseDTO, HttpStatus.CREATED);
        }

    }

    @DeleteMapping("/doctor/jobs/{pk}/delete")
    public ResponseEntity<MessageResponseDTO> deleteJobPriceByPkO(@PathVariable UUID pk) {
        jobPriceFacade.deleteJobPriceByPk(pk);
        MessageResponseDTO messageResponseDTO = new MessageResponseDTO(DoctorylMessagesConstants.Succes.JOB_HAS_BEEN_DELETED_SUCCESSFULLY);
        return new ResponseEntity<>(messageResponseDTO, HttpStatus.OK);
    }

    @GetMapping("/doctor/{doctorPk}/jobs")
    public ResponseEntity<List<JobPriceModel>> getAllJobsByDoctorPk(@PathVariable Optional<String> doctorPk) {
        if (doctorPk.isEmpty()) throw new InputNotValidException("Doctor pk");
        UUID pk = UUID.fromString(doctorPk.get());
        return new ResponseEntity<>(jobPriceFacade.getAllJobPricesByDoctorPk(pk), HttpStatus.OK);
    }

    @Autowired
    public void setJobPriceFacade(JobPriceFacade jobPriceFacade) {
        this.jobPriceFacade = jobPriceFacade;
    }
}
