package be.drnow.app.controller.doctor;

import be.drnow.app.core.annotation.DoctorylController;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.WorkingAgendaDTO;
import be.drnow.app.dto.doctor.form.UpdateDurationForm;
import be.drnow.app.dto.user.MessageResponseDTO;
import be.drnow.app.facade.doctor.WorkingAgendaFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.UUID;

/**
 * @author : soubella
 * @since : 24/01/2021, dim.
 **/
@Secured("ROLE_DOCTOR")
@DoctorylController
public class DoctorAgendaController {

    private final Logger logger = LoggerFactory.getLogger(DoctorAgendaController.class);

    private final String ADD_WORKING_AGENDA = String.format(DoctorylMessagesConstants.Succes.OBJECT_HAS_BEEN_ADDED_SUCCESSFULLY, "Working Agenda");
    private final String UPDATE_APPOINTMENT_DURATION = String.format(DoctorylMessagesConstants.Succes.OBJECT_HAS_BEEN_UPDATED_SUCCESSFULLY, "Appointment duration");
    private final String DELETE_WORKING_AGENDA = String.format(DoctorylMessagesConstants.Succes.OBJECT_HAS_BEEN_DELETED_SUCCESSFULLY, "Working Agenda");

    private WorkingAgendaFacade workingAgendaFacade;

    @PostMapping("/working-agendas/save")
    public ResponseEntity<MessageResponseDTO> addOrUpdateWorkingAgenda(@Validated @RequestBody WorkingAgendaDTO workingAgendaDTO) {
        workingAgendaFacade.addOrUpdateWorkingAgenda(workingAgendaDTO);
        MessageResponseDTO messageResponseDTO = new MessageResponseDTO(ADD_WORKING_AGENDA);
        logger.info(ADD_WORKING_AGENDA);
        return new ResponseEntity<>(messageResponseDTO, HttpStatus.CREATED);
    }

    @PostMapping("/doctor/appointment-duration/save")
    public ResponseEntity<MessageResponseDTO> updateAppointmentDuration(@Validated @RequestBody UpdateDurationForm updateDurationForm) {
        workingAgendaFacade.updateAppointmentDuration(updateDurationForm.getDuration(), updateDurationForm.getDoctorPk());
        MessageResponseDTO messageResponseDTO = new MessageResponseDTO(UPDATE_APPOINTMENT_DURATION);
        logger.info(UPDATE_APPOINTMENT_DURATION);
        return new ResponseEntity<>(messageResponseDTO, HttpStatus.CREATED);
    }

    @DeleteMapping("/working-agenda/{pk}/delete")
    public ResponseEntity<MessageResponseDTO> deleteWorkingAgenda(@PathVariable UUID pk) {
        workingAgendaFacade.deleteWorkingAgendaByPk(pk);
        MessageResponseDTO messageResponseDTO = new MessageResponseDTO(DELETE_WORKING_AGENDA);
        logger.info(DELETE_WORKING_AGENDA);
        return new ResponseEntity<>(messageResponseDTO, HttpStatus.OK);
    }

    @GetMapping("/doctor/{pk}/appointment-duration")
    public ResponseEntity<?> findAppointmentDuration(@PathVariable UUID pk) {
        return new ResponseEntity<>(workingAgendaFacade.findAppointmentDurationByDoctorPk(pk), HttpStatus.OK);
    }

    @GetMapping("/doctor/{pk}/working-agendas/{isAvailable}")
    public ResponseEntity<Set<WorkingAgendaDTO>> findAvailableWorkingAgendas(@PathVariable UUID pk, @PathVariable boolean isAvailable) {
        Set<WorkingAgendaDTO> workingAgendaDTOs;

        if (isAvailable)
            workingAgendaDTOs = workingAgendaFacade.findAvailableWorkingAgendas(pk);
        else
            workingAgendaDTOs = workingAgendaFacade.findUnAvailableWorkingAgendas(pk);

        return new ResponseEntity<>(workingAgendaDTOs, HttpStatus.OK);
    }

    @Autowired
    public void setWorkingAgendaFacade(WorkingAgendaFacade workingAgendaFacade) {
        this.workingAgendaFacade = workingAgendaFacade;
    }
}
