package be.drnow.app.controller.doctor;

import be.drnow.app.core.annotation.DoctorylController;
import be.drnow.app.dto.doctor.ImageDTO;
import be.drnow.app.dto.doctor.form.DoctorProfileForm;
import be.drnow.app.dto.user.MessageResponseDTO;
import be.drnow.app.exception.UnauthorizedException;
import be.drnow.app.facade.doctor.DoctorFacade;
import be.drnow.app.facade.patient.PatientFacade;
import be.drnow.app.facade.user.UserFacade;
import be.drnow.app.model.patient.PatientModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@DoctorylController("/doctor")
public class DoctorProfileController {

    public static final String USER_UPDATED_SUCCESSFULLY = "user updated successfully";
    public static final String USER_IMAGE_UPLOADED_SUCCESSFULLY = "user image uploaded successfully";

    private final Logger logger = LoggerFactory.getLogger(DoctorProfileController.class);

    private DoctorFacade doctorFacade;
    private PatientFacade patientFacade;
    private UserFacade userFacade;

    @Secured("ROLE_DOCTOR")
    @PatchMapping("/{pk}/update")
    public ResponseEntity<MessageResponseDTO> updateDoctorProfile(@PathVariable UUID pk, @RequestBody DoctorProfileForm doctorProfileForm) {
        doctorFacade.updateDoctorProfile(pk, doctorProfileForm);
        logger.info(USER_UPDATED_SUCCESSFULLY);
        return new ResponseEntity<>(new MessageResponseDTO(USER_UPDATED_SUCCESSFULLY), HttpStatus.OK);
    }

    @Secured("ROLE_DOCTOR")
    @PostMapping("/{pk}/image/upload")
    public ResponseEntity<MessageResponseDTO> updateImageDoctor(@RequestParam("file") MultipartFile file, @PathVariable UUID pk) {
        doctorFacade.uploadImageDoctor(pk, file);
        logger.info(USER_IMAGE_UPLOADED_SUCCESSFULLY);
        return new ResponseEntity<>(new MessageResponseDTO(USER_IMAGE_UPLOADED_SUCCESSFULLY), HttpStatus.OK);
    }

    @GetMapping("/{pk}/image")
    public ResponseEntity<ImageDTO> downloadImageDoctor(@PathVariable UUID pk) {
        return new ResponseEntity<>(doctorFacade.downloadImageDoctor(pk), HttpStatus.OK);
    }

    @Secured("ROLE_DOCTOR")
    @GetMapping("/{pk}/patients")
    public ResponseEntity<List<PatientModel>> getDoctorPatients(@PathVariable UUID pk) {
        if (!userFacade.getCurrentUserPk().equals(pk)) {
            throw new UnauthorizedException("You don't have permission to get this data");
        }
        
        return new ResponseEntity<>(patientFacade.findPatientsByDoctorPk(pk), HttpStatus.OK);
    }

    @Autowired
    public void setUserFacade(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @Autowired
    public void setPatientFacade(PatientFacade patientFacade) {
        this.patientFacade = patientFacade;
    }

    @Autowired
    public void setDoctorFacade(DoctorFacade doctorFacade) {
        this.doctorFacade = doctorFacade;
    }
}
