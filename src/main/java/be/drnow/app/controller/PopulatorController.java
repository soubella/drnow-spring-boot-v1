package be.drnow.app.controller;

import be.drnow.app.core.annotation.DoctorylController;
import be.drnow.app.model.doctor.LanguageModel;
import be.drnow.app.model.doctor.ProfessionModel;
import be.drnow.app.model.doctor.SpecialityModel;
import be.drnow.app.repository.doctor.LanguageRepository;
import be.drnow.app.repository.doctor.ProfessionRepository;
import be.drnow.app.repository.doctor.SpecialityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@DoctorylController
public class PopulatorController {

    private ProfessionRepository professionRepository;
    private SpecialityRepository specialityRepository;
    private LanguageRepository languageRepository;

    @GetMapping("/professions")
    public List<ProfessionModel> getAllProfessions() {
        return professionRepository.findAll();
    }

    @GetMapping("/specialities")
    public List<SpecialityModel> getAllSpecialities() {
        return specialityRepository.findAll();
    }

    @GetMapping("/languages")
    public List<LanguageModel> getAllLanguages() {
        return languageRepository.findAll();
    }

    @Autowired
    public void setProfessionRepository(ProfessionRepository professionRepository) {
        this.professionRepository = professionRepository;
    }

    @Autowired
    public void setSpecialityRepository(SpecialityRepository specialityRepository) {
        this.specialityRepository = specialityRepository;
    }

    @Autowired
    public void setLanguageRepository(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }
}
