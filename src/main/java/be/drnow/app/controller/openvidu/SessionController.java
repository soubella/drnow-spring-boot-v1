package be.drnow.app.controller.openvidu;

import be.drnow.app.core.annotation.DoctorylController;
import be.drnow.app.dto.shared.TokenDTO;
import be.drnow.app.dto.user.MessageResponseDTO;
import be.drnow.app.facade.user.UserFacade;
import io.openvidu.java.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Secured({"ROLE_PATIENT", "ROLE_DOCTOR"})
@DoctorylController("/api-sessions")
public class SessionController {

    private final Logger logger = LoggerFactory.getLogger(SessionController.class);

    private UserFacade userFacade;

    // OpenVidu object as entrypoint of the SDK
    private OpenVidu openVidu;

    // Collection to pair session names and OpenVidu Session objects
    private Map<String, Session> mapSessions = new ConcurrentHashMap<>();
    // Collection to pair session names and tokens (the inner Map pairs tokens and
    // role associated)
    private Map<String, Map<String, OpenViduRole>> mapSessionNamesTokens = new ConcurrentHashMap<>();

    // URL where our OpenVidu server is listening
    private String OPENVIDU_URL;
    // Secret shared with our OpenVidu server
    private String SECRET;

    public SessionController(@Value("${openvidu.secret}") String secret, @Value("${openvidu.url}") String openviduUrl) {
        this.SECRET = secret;
        this.OPENVIDU_URL = openviduUrl;
        this.openVidu = new OpenVidu(OPENVIDU_URL, SECRET);
    }

    @PostMapping("/get-token")
    public ResponseEntity<?> getToken(@Validated @RequestBody TokenDTO appointmentTokenDTO) {

        logger.info("Getting a token from OpenVidu Server | {Appointment token}=" + appointmentTokenDTO.getToken());

        TokenDTO sessionToken = new TokenDTO();

        // Role associated to this user
        OpenViduRole role = getCurrentUserRole();

        // Optional data to be passed to other users when this user connects to the
        // video-call. In this case, a JSON with the value we stored in the HttpSession
        // object on login
        String serverData = "{\"serverData\": \"" + null + "\"}";

        // Build connectionProperties object with the serverData and the role
        ConnectionProperties connectionProperties = new ConnectionProperties.Builder().type(ConnectionType.WEBRTC).data(serverData).role(role).build();

        if (this.mapSessions.get(appointmentTokenDTO.getToken()) != null) {
            // Session already exists
            logger.info("Existing session " + appointmentTokenDTO.getToken());
            try {

                // Generate a new Connection with the recently created connectionProperties
                String token = this.mapSessions.get(appointmentTokenDTO.getToken()).createConnection(connectionProperties).getToken();

                // Update our collection storing the new token
                this.mapSessionNamesTokens.get(appointmentTokenDTO.getToken()).put(token, role);

                // Prepare the response with the token
                sessionToken.setToken(token);

                // Return the response to the client
                return new ResponseEntity<>(sessionToken, HttpStatus.OK);
            } catch (OpenViduJavaClientException e1) {
                // If internal error generate an error message and return it to client
                return new ResponseEntity<>(new MessageResponseDTO(e1.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (OpenViduHttpException e2) {
                if (404 == e2.getStatus()) {
                    // Invalid sessionId (user left unexpectedly). Session object is not valid
                    // anymore. Clean collections and continue as new session
                    this.mapSessions.remove(appointmentTokenDTO.getToken());
                    this.mapSessionNamesTokens.remove(appointmentTokenDTO.getToken());
                }
            }
        }

        // New session
        logger.info("New video call session " + appointmentTokenDTO.getToken());
        try {

            // Create a new OpenVidu Session
            Session session = this.openVidu.createSession();
            // Generate a new Connection with the recently created connectionProperties
            String token = session.createConnection(connectionProperties).getToken();

            // Store the session and the token in our collections
            this.mapSessions.put(appointmentTokenDTO.getToken(), session);
            this.mapSessionNamesTokens.put(appointmentTokenDTO.getToken(), new ConcurrentHashMap<>());
            this.mapSessionNamesTokens.get(appointmentTokenDTO.getToken()).put(token, role);

            // Prepare the response with the token
            sessionToken.setToken(token);

            // Return the response to the client
            return new ResponseEntity<>(sessionToken, HttpStatus.OK);

        } catch (Exception e) {
            // If error generate an error message and return it to client
            return new ResponseEntity<>(new MessageResponseDTO(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    private OpenViduRole getCurrentUserRole() {

        if (userFacade.getCurrentUser() == null) {
            // throw exception and logout
            return OpenViduRole.SUBSCRIBER; // this just for test
        }

        if (userFacade.getCurrentUser().getRole().getName().equals("ROLE_DOCTOR")) {
            return OpenViduRole.MODERATOR;
        }
        return OpenViduRole.PUBLISHER;
    }

    @Autowired
    public void setUserFacade(UserFacade userFacade) {
        this.userFacade = userFacade;
    }
}
