package be.drnow.app.helper;

import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.form.DoctorRegisterForm;
import be.drnow.app.dto.shared.MailDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Component
public class MailHelper {

    private static final String DOCTOR_CONSULTATION_URL = "https://doctoryl.be/teleconsultation/doctor/dashboard/consultations";
    private static final String DOCTOR_WELCOME_URL = "https://www.doctoryl.be/praticien/nous-rejoindre";
    private static final String PATIENT_CONSULTATION_URL = "https://doctoryl.be/teleconsultation/patient/dashboard/my-consultations";
    private static final String SUBJECT_FOR_REGISTRATION_VERIFICATION_MAIL = DoctorylMessagesConstants.Email.SUBJECT_FOR_REGISTRATION_VERIFICATION_MAIL;

    private MessageSource messageSource;

    public MailDTO buildDoctorRegisterEmail(DoctorRegisterForm doctorRegisterForm, Locale userLanguage) {
        MailDTO mail = new MailDTO();
        mail.setMailTo(doctorRegisterForm.getEmail());
        mail.setSubject(SUBJECT_FOR_REGISTRATION_VERIFICATION_MAIL);
        Map<String, Object> model = new HashMap<>();
        model.put("header", String.format(messageSource.getMessage("header", null, userLanguage), doctorRegisterForm.getFirstName()));
        model.put("body", messageSource.getMessage("body", null, userLanguage));
        model.put("footer", messageSource.getMessage("footer", null, userLanguage));
        mail.setProps(model);
        return mail;
    }

    public MailDTO buildDoctorWelcomeEmail(DoctorRegisterForm doctorRegisterForm, Locale userLanguage) {
        MailDTO mail = new MailDTO();
        mail.setMailTo(doctorRegisterForm.getEmail());
        mail.setSubject("Doctoryl");
        Map<String, Object> model = new HashMap<>();
        model.put("hello_doctor", messageSource.getMessage("hello_doctor", null, userLanguage) + " " + doctorRegisterForm.getFirstName());
        model.put("thank_you", messageSource.getMessage("thank_you", null, userLanguage));
        model.put("benefits", messageSource.getMessage("benefits", null, userLanguage));
        model.put("benefit1", messageSource.getMessage("benefit1", null, userLanguage));
        model.put("benefit2", messageSource.getMessage("benefit2", null, userLanguage));
        model.put("benefit3", messageSource.getMessage("benefit3", null, userLanguage));
        model.put("benefit4", messageSource.getMessage("benefit4", null, userLanguage));
        model.put("see_more", messageSource.getMessage("see_more", null, userLanguage));
        model.put("url", DOCTOR_WELCOME_URL);
        model.put("footer", messageSource.getMessage("footer", null, userLanguage));
        mail.setProps(model);
        return mail;
    }

    public MailDTO buildDoctorAppointmentNotificationEmail(String email, LocalDateTime dateTime, Locale userLanguage) {
        MailDTO mail = new MailDTO();
        mail.setMailTo(email);
        mail.setSubject(messageSource.getMessage("new_appointment", null, userLanguage));
        Map<String, Object> model = new HashMap<>();
        model.put("message", String.format(messageSource.getMessage("new_appointment_message", null, userLanguage), dateTime));
        model.put("see_more", messageSource.getMessage("see_more", null, userLanguage));
        model.put("url", DOCTOR_CONSULTATION_URL);
        model.put("footer", messageSource.getMessage("footer", null, userLanguage));
        mail.setProps(model);
        return mail;
    }

    public MailDTO buildPatientAppointmentConfirmationEmail(String email, String doctor, Locale userLanguage) {
        MailDTO mail = new MailDTO();
        mail.setMailTo(email);
        mail.setSubject(messageSource.getMessage("new_appointment_confirmation", null, userLanguage));
        Map<String, Object> model = new HashMap<>();
        model.put("message",
                String.format(messageSource.getMessage("new_appointment_confirmation_message", null, userLanguage), doctor));
        model.put("see_more", messageSource.getMessage("see_more", null, userLanguage));
        model.put("url", PATIENT_CONSULTATION_URL);
        model.put("footer", messageSource.getMessage("footer", null, userLanguage));
        mail.setProps(model);
        return mail;
    }

    public MailDTO buildPatientAppointmentNotificationEmail(String email, String doctor, LocalDateTime dateTime, Locale userLanguage) {
        MailDTO mail = new MailDTO();
        mail.setMailTo(email);
        mail.setSubject(messageSource.getMessage("new_appointment", null, userLanguage));
        Map<String, Object> model = new HashMap<>();
        model.put("message",
                String.format(messageSource.getMessage("new_appointment_by_doctor_confirmation_message", null, userLanguage), doctor, dateTime));
        model.put("see_more", messageSource.getMessage("see_more", null, userLanguage));
        model.put("url", PATIENT_CONSULTATION_URL);
        model.put("footer", messageSource.getMessage("footer", null, userLanguage));
        mail.setProps(model);
        return mail;
    }

    public MailDTO buildPatientAppointmentUpdateEmail(String email, String doctor, Locale userLanguage) {
        MailDTO mail = new MailDTO();
        mail.setMailTo(email);
        mail.setSubject(messageSource.getMessage("appointment_update", null, userLanguage));
        Map<String, Object> model = new HashMap<>();
        model.put("message",
                String.format(messageSource.getMessage("appointment_update_message", null, userLanguage), doctor));
        model.put("see_more", messageSource.getMessage("see_more", null, userLanguage));
        model.put("footer", messageSource.getMessage("footer", null, userLanguage));
        model.put("url", PATIENT_CONSULTATION_URL);
        mail.setProps(model);
        return mail;
    }

    public MailDTO buildPatientAppointmentCancellationEmail(String email, String doctor, LocalDateTime dateTime, Locale userLanguage) {
        MailDTO mail = new MailDTO();
        mail.setMailTo(email);
        mail.setSubject(messageSource.getMessage("appointment_cancellation", null, userLanguage));
        Map<String, Object> model = new HashMap<>();
        model.put("message",
                String.format(messageSource.getMessage("appointment_cancellation_message", null, userLanguage), dateTime, doctor));
        model.put("see_more", messageSource.getMessage("see_more", null, userLanguage));
        model.put("footer", messageSource.getMessage("footer", null, userLanguage));
        model.put("url", PATIENT_CONSULTATION_URL);
        mail.setProps(model);
        return mail;
    }

    public MailDTO buildPasswordRecoveryEmail(String email, Locale userLanguage) {
        MailDTO mail = new MailDTO();
        mail.setMailTo(email);
        mail.setSubject(messageSource.getMessage("reset_your_password", null, userLanguage));
        Map<String, Object> model = new HashMap<>();
        model.put("body", messageSource.getMessage("reset_your_password_message", null, userLanguage));
        model.put("see_more", messageSource.getMessage("see_more", null, userLanguage));
        model.put("footer", messageSource.getMessage("footer", null, userLanguage));
        mail.setProps(model);
        return mail;
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
}
