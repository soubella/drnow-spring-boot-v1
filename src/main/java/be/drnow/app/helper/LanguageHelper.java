package be.drnow.app.helper;

import java.util.Locale;

public class LanguageHelper {

    public static Locale getUserLocalFromHeader(String userLanguage) {
        if (userLanguage == null || userLanguage.isBlank() || userLanguage.isEmpty())
            return Locale.FRENCH;

        String[] languages = userLanguage.split(",");
        String languageCode;

        if (languages.length <= 0)
            languageCode = userLanguage;
        else
            languageCode = languages[0];

        if (languageCode.contains("fr"))
            return Locale.FRENCH;
        else if (languageCode.contains("en"))
            return Locale.ENGLISH;
        else if (languageCode.contains("de"))
            return Locale.GERMAN;
        else
            return Locale.FRENCH;
    }
}
