package be.drnow.app.helper;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public class CodeGeneratorHelper {

    private static Random rnd;

    private CodeGeneratorHelper() {
    }

    static {
        try {
            rnd = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static String generateCode() {
        int number = rnd.nextInt(999999);
        return String.format("%06d", number);
    }
}
