package be.drnow.app.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class ImageHelper {

    public static final String IMAGE_HELPER_CLASS = "image helper class";
    private static final Logger logger = LoggerFactory.getLogger(ImageHelper.class);

    private ImageHelper() throws IOException {
        throw new IOException(IMAGE_HELPER_CLASS);
    }

    /*
     *compress the image bytes before storing it in the database
     *@param data
     */
    public static byte[] compressBytes(byte[] data) {
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        logger.info(String.format("Compressed Image Byte Size - %s", outputStream.toByteArray().length));
        return outputStream.toByteArray();
    }

    /*
     *uncompress the image bytes before returning it to the angular application
     * @param data
     */
    public static byte[] decompressBytes(byte[] data) {
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } catch (IOException | DataFormatException ioe) {
            logger.error(ioe.getMessage());
        }
        logger.info("Decompressing Image OK !");
        return outputStream.toByteArray();
    }

}
