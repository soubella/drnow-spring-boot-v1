package be.drnow.app.facade.patient.impl;

import be.drnow.app.converter.Converter;
import be.drnow.app.dto.patient.PatientDTO;
import be.drnow.app.dto.patient.form.MedicalRecordForm;
import be.drnow.app.dto.patient.form.PatientRegisterForm;
import be.drnow.app.facade.patient.PatientFacade;
import be.drnow.app.model.patient.MedicalRecordModel;
import be.drnow.app.model.patient.PatientModel;
import be.drnow.app.service.patient.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class DefaultPatientFacade implements PatientFacade {

    private PatientService patientService;
    private Converter<PatientRegisterForm, PatientModel> patientRegisterFormToModel;
    private Converter<MedicalRecordForm, MedicalRecordModel> medicalRecordFormToModel;
    private Converter<PatientModel, PatientDTO> patientModelToDTO;

    @Override
    public PatientModel registerPatient(PatientRegisterForm patientRegisterForm) {
        PatientModel patientModel = new PatientModel();
        patientRegisterFormToModel.convert(patientRegisterForm, patientModel);
        patientService.registerPatient(patientModel);
        return patientModel;
    }

    @Override
    public void addOrUpdateMedicalRecord(MedicalRecordForm medicalRecordForm) {
        MedicalRecordModel medicalRecordModel = new MedicalRecordModel();
        PatientModel patientModel = patientService.findPatientByPk(medicalRecordForm.getPatientPk());
        medicalRecordFormToModel.convert(medicalRecordForm, medicalRecordModel);
        patientService.addMedicalRecordToPatient(medicalRecordModel, patientModel);
    }

    @Override
    public PatientModel addBeneficiary(PatientRegisterForm patientBeneficiaryForm) {
        PatientModel patientModel = patientService.findPatientByEmail(patientBeneficiaryForm.getEmail());
        PatientModel beneficiaryModel = new PatientModel();
        patientRegisterFormToModel.convert(patientBeneficiaryForm, beneficiaryModel);
        beneficiaryModel.setEmail(UUID.randomUUID().toString().replace("-", "") + "@mail.com");
        return patientService.addBeneficiaryToPatient(patientModel, beneficiaryModel);
    }

    @Override
    public void updatePatientProfile(UUID pk, PatientRegisterForm patientRegisterForm) {
        PatientModel patientModel = patientService.findPatientByPk(pk);
        patientRegisterFormToModel.convert(patientRegisterForm, patientModel);
        patientService.updatePatientProfile(patientModel);
    }

    @Override
    public PatientDTO findPatientByPk(UUID pk) {
        PatientDTO patientDTO = new PatientDTO();

        PatientModel patientModel = patientService.findPatientByPk(pk);
        patientModelToDTO.convert(patientModel, patientDTO);

        patientDTO.setFamilyMembers(new ArrayList<>());

        patientModel.getFamilyMembers().forEach(familyMember -> {
            PatientDTO patientDTO1 = new PatientDTO();
            patientModelToDTO.convert(familyMember, patientDTO1);
            patientDTO.getFamilyMembers().add(patientDTO1);
        });

        return patientDTO;
    }

    @Override
    public List<PatientModel> findPatientsByDoctorPk(UUID pk) {
        return patientService.findPatientsByDoctorPk(pk);
    }

    @Autowired
    public void setPatientService(PatientService patientService) {
        this.patientService = patientService;
    }

    @Autowired
    public void setPatientModelToDTO(Converter<PatientModel, PatientDTO> patientModelToDTO) {
        this.patientModelToDTO = patientModelToDTO;
    }

    @Autowired
    @Qualifier("patientRegisterFormToModel")
    public void setPatientRegisterFormToModel(Converter<PatientRegisterForm, PatientModel> patientRegisterFormToModel) {
        this.patientRegisterFormToModel = patientRegisterFormToModel;
    }

    @Autowired
    @Qualifier("medicalRecordFormToModel")
    public void setMedicalRecordFormToModel(Converter<MedicalRecordForm, MedicalRecordModel> medicalRecordFormToModel) {
        this.medicalRecordFormToModel = medicalRecordFormToModel;
    }

}
