package be.drnow.app.facade.patient;

import be.drnow.app.dto.patient.PatientDTO;
import be.drnow.app.dto.patient.form.MedicalRecordForm;
import be.drnow.app.dto.patient.form.PatientRegisterForm;
import be.drnow.app.model.patient.PatientModel;

import javax.mail.MessagingException;
import java.util.List;
import java.util.UUID;

public interface PatientFacade {
    PatientModel registerPatient(PatientRegisterForm patientRegisterForm) throws MessagingException;

    void addOrUpdateMedicalRecord(MedicalRecordForm medicalRecordForm);

    PatientModel addBeneficiary(PatientRegisterForm patientBeneficiaryForm);

    void updatePatientProfile(UUID pk, PatientRegisterForm patientRegisterForm);

    List<PatientModel> findPatientsByDoctorPk(UUID pk);

    PatientDTO findPatientByPk(UUID pk);
}
