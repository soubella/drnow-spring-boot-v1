package be.drnow.app.facade.appointment.impl;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.appointment.AgendaAppointmentForm;
import be.drnow.app.dto.appointment.AppointmentFormDTO;
import be.drnow.app.dto.shared.MailDTO;
import be.drnow.app.facade.appointment.AppointmentFacade;
import be.drnow.app.helper.LanguageHelper;
import be.drnow.app.helper.MailHelper;
import be.drnow.app.model.appointment.AppointmentModel;
import be.drnow.app.model.appointment.AppointmentStatusEnum;
import be.drnow.app.model.doctor.DoctorModel;
import be.drnow.app.model.patient.PatientModel;
import be.drnow.app.model.patient.PatientRelationshipEnum;
import be.drnow.app.model.user.UserModel;
import be.drnow.app.service.appointment.AppointmentService;
import be.drnow.app.service.doctor.DoctorService;
import be.drnow.app.service.doctor.WorkingAgendaService;
import be.drnow.app.service.patient.PatientService;
import be.drnow.app.service.shared.EmailService;
import be.drnow.app.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Component
public class DefaultAppointmentFacade implements AppointmentFacade {

    private final Logger logger = LoggerFactory.getLogger(DefaultAppointmentFacade.class);

    private final static String NOTIFICATION_MAIL_TEMPLATE = DoctorylMessagesConstants.Email.NOTIFICATION_MAIL_TEMPLATE;

    private AppointmentService appointmentService;
    private UserService userService;
    private PatientService patientService;
    private DoctorService doctorService;
    private WorkingAgendaService workingAgendaService;
    private EmailService emailService;

    private MailHelper mailHelper;

    private Converter<AppointmentFormDTO, AppointmentModel> appointmentFormToModel;

    @Override
    public void takeAppointment(AppointmentFormDTO appointmentFormDTO) throws MessagingException {

        // save appointment
        AppointmentModel appointmentModel = new AppointmentModel();
        appointmentFormToModel.convert(appointmentFormDTO, appointmentModel);
        appointmentService.takeOrUpdateAppointment(appointmentModel);

        // send email to doctor
        UserModel doctorModel = userService.findUserByPk(appointmentFormDTO.getDoctorPk());
        MailDTO doctorMailDTO = mailHelper.buildDoctorAppointmentNotificationEmail(
                doctorModel.getEmail(), appointmentFormDTO.getStartAt(), LanguageHelper.getUserLocalFromHeader(doctorModel.getDeviceLanguage()));
        emailService.sendEmail(doctorMailDTO, NOTIFICATION_MAIL_TEMPLATE);

        // send email to patient
        UserModel patientModel = userService.findUserByPk(appointmentFormDTO.getPatientPk());
        MailDTO patientMailDTO = mailHelper.buildPatientAppointmentConfirmationEmail(
                patientModel.getEmail(), doctorModel.getFullName(), LanguageHelper.getUserLocalFromHeader(patientModel.getDeviceLanguage()));
        emailService.sendEmail(patientMailDTO, NOTIFICATION_MAIL_TEMPLATE);

        // TODO send SMS to patient and doctor
        // TODO push notification to patient and doctor
    }

    @Override
    public void takeAppointmentForPatient(AgendaAppointmentForm agendaAppointmentForm) throws MessagingException {
        PatientModel patientModel;
        DoctorModel doctorModel = doctorService.findByPk(agendaAppointmentForm.getDoctorPk());
        AppointmentModel appointmentModel = new AppointmentModel();

        appointmentModel.setPaid(false);
        appointmentModel.setPrice(doctorModel.getTeleconsultationPrice());
        appointmentModel.setRepayable(doctorModel.isRepayable());
        appointmentModel.setAppointmentStatusEnum(AppointmentStatusEnum.NOT_STARTED_YET);
        appointmentModel.setStartAt(agendaAppointmentForm.getStartAt());
        appointmentModel.setDuration(doctorModel.getAppointmentDuration());
        appointmentModel.setReason(agendaAppointmentForm.getReason());
        appointmentModel.setDoctor(doctorModel);

        if (userService.checkUserByEmail(agendaAppointmentForm.getEmail())) {
            logger.info("Patient found..take appointment");
            patientModel = patientService.findPatientByEmail(agendaAppointmentForm.getEmail());
            appointmentModel.setPatient(patientModel);
            appointmentModel.setBeneficiary(patientModel);
        } else {
            logger.info("Patient not found..register and take appointment");
            patientModel = new PatientModel();
            patientModel.setPatientRelationshipEnum(PatientRelationshipEnum.MAIN_ACCOUNT);
            patientModel.setEmail(agendaAppointmentForm.getEmail());
            patientModel.setPhoneNumber(agendaAppointmentForm.getPhoneNumber());
            patientModel.setFirstName(agendaAppointmentForm.getFirstName());
            patientModel.setLastName(agendaAppointmentForm.getLastName());
            PatientModel insertedPatient = patientService.registerPatient(patientModel);

            appointmentModel.setPatient(insertedPatient);
            appointmentModel.setBeneficiary(insertedPatient);
        }
        appointmentService.takeOrUpdateAppointment(appointmentModel);
        // send email to patient
        MailDTO patientMailDTO = mailHelper.buildPatientAppointmentNotificationEmail(
                patientModel.getEmail(), doctorModel.getFullName(), agendaAppointmentForm.getStartAt(), LanguageHelper.getUserLocalFromHeader(patientModel.getDeviceLanguage()));
        emailService.sendEmail(patientMailDTO, NOTIFICATION_MAIL_TEMPLATE);
        // TODO send SMS to patient
        // TODO push notification to patient
    }

    @Override
    public void updateAppointmentStartDate(AgendaAppointmentForm agendaAppointmentForm, UUID pk) throws MessagingException {
        AppointmentModel appointmentModel = appointmentService.findAppointmentByPk(pk);
        appointmentModel.setStartAt(agendaAppointmentForm.getStartAt());
        appointmentService.takeOrUpdateAppointment(appointmentModel);

        // send email to patient
        MailDTO patientMailDTO = mailHelper.buildPatientAppointmentUpdateEmail(
                appointmentModel.getPatient().getEmail(), appointmentModel.getDoctor().getFullName(), LanguageHelper.getUserLocalFromHeader(appointmentModel.getPatient().getDeviceLanguage()));
        emailService.sendEmail(patientMailDTO, NOTIFICATION_MAIL_TEMPLATE);
        // TODO send SMS to patient
        // TODO push notification to patient
    }

    @Override
    public void cancelAppointment(UUID appointmentPk, String cancellationReason, AppointmentStatusEnum appointmentStatusEnum) throws MessagingException {
        AppointmentModel appointmentModel = appointmentService.findAppointmentByPk(appointmentPk);
        appointmentModel.setCancellationReason(cancellationReason);
        appointmentModel.setAppointmentStatusEnum(appointmentStatusEnum);
        if (appointmentStatusEnum.equals(AppointmentStatusEnum.CANCELED_BY_DOCTOR)) {
            // send email to patient
            MailDTO patientMailDTO = mailHelper.buildPatientAppointmentCancellationEmail(appointmentModel.getPatient().getEmail(),
                    appointmentModel.getDoctor().getFullName(), appointmentModel.getStartAt(), LanguageHelper.getUserLocalFromHeader(appointmentModel.getPatient().getDeviceLanguage()));
            emailService.sendEmail(patientMailDTO, NOTIFICATION_MAIL_TEMPLATE);
            // TODO send SMS to patient
            // TODO push notification to patient
        } else if (appointmentStatusEnum.equals(AppointmentStatusEnum.CANCELED_BY_PATIENT)) {
            // TODO send email to doctor
            // TODO send SMS to doctor
            // TODO push notification to doctor
        }
        appointmentService.takeOrUpdateAppointment(appointmentModel);
    }

    @Override
    public boolean isAppointmentOwner(UUID userPk, UUID appointmentPk) {
        AppointmentModel appointmentModel = appointmentService.findAppointmentByPk(appointmentPk);
        return appointmentModel.getDoctor().getPk().equals(userPk) || appointmentModel.getPatient().getPk().equals(userPk);
    }

    @Override
    public Set<AppointmentModel> findAppointmentsByDoctorPk(UUID doctorPk, List<AppointmentStatusEnum> appointmentStatusEnums) {
        return appointmentService.findAppointmentsByDoctorPk(doctorPk, appointmentStatusEnums);
    }

    @Override
    public boolean isAvailable(UUID doctorPk, LocalDateTime time) {
        return appointmentService.isAvailable(doctorPk, time) && workingAgendaService.isAvailable(time, doctorPk);
    }

    @Override
    public Set<AppointmentModel> findAppointmentsByPatientPk(UUID patientPk) {
        return appointmentService.findAppointmentsByPatientPk(patientPk);
    }

    @Autowired
    public void setPatientService(PatientService patientService) {
        this.patientService = patientService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setDoctorService(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @Autowired
    public void setAppointmentService(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @Autowired
    @Qualifier("appointmentFormToModel")
    public void setAppointmentFormToModel(Converter<AppointmentFormDTO, AppointmentModel> appointmentFormToModel) {
        this.appointmentFormToModel = appointmentFormToModel;
    }

    @Autowired
    public void setWorkingAgendaService(WorkingAgendaService workingAgendaService) {
        this.workingAgendaService = workingAgendaService;
    }

    @Autowired
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    @Autowired
    public void setMailHelper(MailHelper mailHelper) {
        this.mailHelper = mailHelper;
    }
}
