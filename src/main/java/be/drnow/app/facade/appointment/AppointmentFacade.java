package be.drnow.app.facade.appointment;

import be.drnow.app.dto.appointment.AgendaAppointmentForm;
import be.drnow.app.dto.appointment.AppointmentFormDTO;
import be.drnow.app.model.appointment.AppointmentModel;
import be.drnow.app.model.appointment.AppointmentStatusEnum;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface AppointmentFacade {
    void takeAppointment(AppointmentFormDTO appointmentFormDTO) throws MessagingException;

    void takeAppointmentForPatient(AgendaAppointmentForm agendaAppointmentForm) throws MessagingException;

    void updateAppointmentStartDate(AgendaAppointmentForm agendaAppointmentForm, UUID pk) throws MessagingException;

    Set<AppointmentModel> findAppointmentsByDoctorPk(UUID doctorPk, List<AppointmentStatusEnum> appointmentStatusEnums);

    void cancelAppointment(UUID appointmentPk, String cancellationReason, AppointmentStatusEnum appointmentStatusEnum) throws MessagingException;

    boolean isAppointmentOwner(UUID userPk, UUID appointmentPk);

    boolean isAvailable(UUID doctorPk, LocalDateTime time);

    Set<AppointmentModel> findAppointmentsByPatientPk(UUID patientPk);
}
