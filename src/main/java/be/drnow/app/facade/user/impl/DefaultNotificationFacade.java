package be.drnow.app.facade.user.impl;

import be.drnow.app.dto.user.NotificationDTO;
import be.drnow.app.facade.user.NotificationFacade;
import be.drnow.app.model.user.NotificationModel;
import be.drnow.app.service.user.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class DefaultNotificationFacade implements NotificationFacade {

    private NotificationService notificationService;

    @Override
    public List<NotificationModel> findUserNotifications(UUID userPk) {
        return notificationService.findNotificationsByUserPk(userPk);
    }

    @Override
    public void updateNotification(NotificationDTO notificationDTO) {
        NotificationModel notificationModel = notificationService.findNotificationByPk(notificationDTO.getPk());
        notificationModel.setSeen(true);
        notificationService.addOrUpdateNotification(notificationModel);
    }

    @Autowired
    public void setNotificationService(NotificationService notificationService) {
        this.notificationService = notificationService;
    }
}
