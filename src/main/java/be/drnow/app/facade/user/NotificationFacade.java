package be.drnow.app.facade.user;

import be.drnow.app.dto.user.NotificationDTO;
import be.drnow.app.model.user.NotificationModel;

import java.util.List;
import java.util.UUID;

public interface NotificationFacade {

    List<NotificationModel> findUserNotifications(UUID userPk);

    void updateNotification(NotificationDTO notificationDTO);

}
