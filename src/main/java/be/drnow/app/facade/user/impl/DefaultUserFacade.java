package be.drnow.app.facade.user.impl;

import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.shared.MailDTO;
import be.drnow.app.dto.user.*;
import be.drnow.app.dto.user.form.UserSocialNumberForm;
import be.drnow.app.exception.RessourceNotFoundException;
import be.drnow.app.facade.user.UserFacade;
import be.drnow.app.model.user.TokenTypeEnum;
import be.drnow.app.model.user.UserModel;
import be.drnow.app.model.user.VerificationCodeModel;
import be.drnow.app.service.shared.EmailService;
import be.drnow.app.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.util.UUID;

@Component
public class DefaultUserFacade implements UserFacade {



    private static final String FORGET_PASSWORD_VERIFICATION_MAIL_TEMPLATE = DoctorylMessagesConstants.Email.REGISTRATION_VERIFICATION_MAIL_TEMPLATE;
    private static final String CHANGE_EMAIL_VERIFICATION_MAIL_TEMPLATE = DoctorylMessagesConstants.Email.REGISTRATION_VERIFICATION_MAIL_TEMPLATE;

    private UserService userService;
    private EmailService emailService;

    @Override
    public JwtResponseDTO authenticateUser(LoginRequestDTO loginRequest) {
        return userService.authenticateUser(loginRequest);
    }

    @Override
    public void sendPasswordRecoveryCode(MailDTO mail)  {
        UserModel userModel = userService.findUserByEmail(mail.getMailTo());

        VerificationCodeModel verificationCode = new VerificationCodeModel(userModel);
        mail.getProps().put("confirmationCode", verificationCode.getVerificationCode());

        userService.saveVerificationCode(verificationCode, TokenTypeEnum.FOR_EMAIL_FORGET_PASSWORD);
    }

    @Async
    @Override
    public void sendPasswordRecoveryCodeEmail(MailDTO mail) throws MessagingException{
        emailService.sendEmail(mail, FORGET_PASSWORD_VERIFICATION_MAIL_TEMPLATE);
    }

    @Override
    public boolean checkIfUserExistByEmail(String email) {
        return userService.checkUserByEmail(email);
    }

    @Override
    public void resetEmail(ChangeEmailDTO changeEmailDTO) {
        userService.resetEmail(changeEmailDTO);
    }

    @Override
    public void sendEmailRecoveryCode(MailDTO mail, String oldEmail) throws MessagingException {
        UserModel userModel = userService.findUserByEmail(oldEmail);
        if (userModel == null) {
            throw new RessourceNotFoundException("Email account");
        } else {
            VerificationCodeModel verificationCode = new VerificationCodeModel(userModel);
            mail.getProps().put("confirmationCode", verificationCode.getVerificationCode());
            userService.saveVerificationCode(verificationCode, TokenTypeEnum.FOR_EMAIL_CHANGE_EMAIL);
            emailService.sendEmail(mail, CHANGE_EMAIL_VERIFICATION_MAIL_TEMPLATE);
        }
    }

    @Override
    public void addSecurityNumberToUser(UserSocialNumberForm userSocialNumberForm) {
        UserModel userModel = userService.findUserByPk(userSocialNumberForm.getUserPk());
        userModel.setSocialNumber(userSocialNumberForm.getSocialNumber());
        userService.updateUser(userModel);
    }

    @Override
    public void checkConfirmationCode(ConfirmationCodeRequestDTO confirmationCodeRequestDTO) {
        userService.checkVerificationCode(confirmationCodeRequestDTO);
    }

    @Override
    public void resetPassword(NewPasswordRequestDTO newPasswordRequestDTO) {
        userService.resetPassword(newPasswordRequestDTO);
    }

    @Override
    public void changePassword(NewPasswordRequestDTO newPasswordRequestDTO) {
        userService.changePassword(newPasswordRequestDTO);
    }

    @Override
    public UserModel getCurrentUser() {
        return userService.getCurrentUser();
    }

    @Override
    public UUID getCurrentUserPk() {
        return userService.getCurrentUserPk();
    }

    @Autowired
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
