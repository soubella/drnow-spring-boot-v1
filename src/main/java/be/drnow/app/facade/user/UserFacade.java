package be.drnow.app.facade.user;

import be.drnow.app.dto.shared.MailDTO;
import be.drnow.app.dto.user.*;
import be.drnow.app.dto.user.form.UserSocialNumberForm;
import be.drnow.app.model.user.UserModel;

import javax.mail.MessagingException;
import java.util.UUID;

public interface UserFacade {
    JwtResponseDTO authenticateUser(LoginRequestDTO loginRequest);

    void sendPasswordRecoveryCode(MailDTO mail);

    void sendPasswordRecoveryCodeEmail(MailDTO mail) throws MessagingException;

    void checkConfirmationCode(ConfirmationCodeRequestDTO confirmationCodeRequestDTO);

    void resetPassword(NewPasswordRequestDTO newPasswordRequestDTO);

    boolean checkIfUserExistByEmail(String email);

    void resetEmail(ChangeEmailDTO changeEmailDTO);

    void sendEmailRecoveryCode(MailDTO mail, String oldEmail) throws MessagingException;

    void addSecurityNumberToUser(UserSocialNumberForm userSocialNumberForm);

    UserModel getCurrentUser();

    UUID getCurrentUserPk();

    void changePassword(NewPasswordRequestDTO newPasswordRequestDTO);
}
