package be.drnow.app.facade.doctor.impl;

import be.drnow.app.converter.Converter;
import be.drnow.app.dto.doctor.WorkingAgendaDTO;
import be.drnow.app.facade.doctor.WorkingAgendaFacade;
import be.drnow.app.model.doctor.WorkingAgendaModel;
import be.drnow.app.repository.doctor.DoctorRepository;
import be.drnow.app.service.doctor.WorkingAgendaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Component
public class DefaultWorkingAgendaFacade implements WorkingAgendaFacade {

    private WorkingAgendaService workingAgendaService;

    private DoctorRepository doctorRepository;

    private Converter<WorkingAgendaDTO, WorkingAgendaModel> doctorWorkingAgendaDtoToModel;
    private Converter<Set<WorkingAgendaModel>, Set<WorkingAgendaDTO>> workingAgendaConverterAll;

    @Override
    public void addOrUpdateWorkingAgenda(WorkingAgendaDTO workingAgendaDTO) {
        WorkingAgendaModel workingAgendaModel = new WorkingAgendaModel();
        doctorWorkingAgendaDtoToModel.convert(workingAgendaDTO, workingAgendaModel);
        workingAgendaService.addOrUpdateWorkingAgenda(workingAgendaModel);
    }

    @Override
    public void deleteWorkingAgendaByPk(UUID pk) {
        workingAgendaService.deleteWorkingAgendaByPk(pk);
    }

    @Override
    public Set<WorkingAgendaDTO> findAvailableWorkingAgendas(UUID doctorPk) {
        Set<WorkingAgendaDTO> workingAgendaDTOs = new HashSet<>();
        workingAgendaConverterAll.convert(workingAgendaService.findAvailableWorkingAgendas(doctorPk), workingAgendaDTOs);
        return workingAgendaDTOs;
    }

    @Override
    public Set<WorkingAgendaDTO> findUnAvailableWorkingAgendas(UUID doctorPk) {
        Set<WorkingAgendaDTO> workingAgendaDTOs = new HashSet<>();
        workingAgendaConverterAll.convert(workingAgendaService.findUnAvailableWorkingAgendas(doctorPk), workingAgendaDTOs);
        return workingAgendaDTOs;
    }

    @Override
    public void updateAppointmentDuration(int duration, UUID doctorPk) {
        doctorRepository.updateAppointmentDuration(duration, doctorPk);
    }

    @Override
    public int findAppointmentDurationByDoctorPk(UUID doctorPk) {
        return doctorRepository.findAppointmentDurationByDoctorPk(doctorPk);
    }

    @Autowired
    public void setWorkingAgendaService(WorkingAgendaService workingAgendaService) {
        this.workingAgendaService = workingAgendaService;
    }

    @Autowired
    public void setDoctorWorkingAgendaDtoToModel(Converter<WorkingAgendaDTO, WorkingAgendaModel> doctorWorkingAgendaDtoToModel) {
        this.doctorWorkingAgendaDtoToModel = doctorWorkingAgendaDtoToModel;
    }

    @Autowired
    public void setWorkingAgendaConverterAll(@Qualifier("DoctorWorkingModelToDTOList") Converter<Set<WorkingAgendaModel>,
            Set<WorkingAgendaDTO>> workingAgendaConverterAll) {
        this.workingAgendaConverterAll = workingAgendaConverterAll;
    }

    @Autowired
    public void setDoctorRepository(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }
}
