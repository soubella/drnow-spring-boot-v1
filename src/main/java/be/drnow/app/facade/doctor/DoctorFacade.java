package be.drnow.app.facade.doctor;

import be.drnow.app.dto.doctor.DoctorAutoCompleteDTO;
import be.drnow.app.dto.doctor.DoctorDTO;
import be.drnow.app.dto.doctor.DoctorSearchResultDTO;
import be.drnow.app.dto.doctor.ImageDTO;
import be.drnow.app.dto.doctor.form.DoctorProfileForm;
import be.drnow.app.dto.doctor.form.DoctorRegisterForm;
import be.drnow.app.dto.doctor.response.DoctorSearchResponseDTO;
import be.drnow.app.dto.shared.MailDTO;
import be.drnow.app.dto.user.ConfirmationCodeRequestDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface DoctorFacade {

    void registerDoctor(DoctorRegisterForm doctorRegisterForm) throws MessagingException;

    void confirmAccount(ConfirmationCodeRequestDTO confirmationCodeRequestDTO);

    Set<DoctorSearchResponseDTO> searchDoctor(String value, String address);

    void sendRegisterVerificationEmail(DoctorRegisterForm doctorRegisterForm, String userLanguage) throws MessagingException;

    void sendWelcomeEmail(DoctorRegisterForm doctorRegisterForm, String userLanguage) throws MessagingException;

    DoctorDTO findByPk(UUID pk);

    DoctorSearchResultDTO findDoctorProfileByPk(UUID pk);

    void updateDoctorProfile(UUID pk, DoctorProfileForm doctorProfileForm);

    void uploadImageDoctor(UUID pk, MultipartFile file);

    ImageDTO downloadImageDoctor(UUID pk);

    List<DoctorAutoCompleteDTO> findDoctorsByFullName(String s);

    List<DoctorSearchResultDTO> searchForDoctors(String keyword, Pageable pageable);

    List<DoctorSearchResultDTO> findAllDoctors(Pageable pageable);
}
