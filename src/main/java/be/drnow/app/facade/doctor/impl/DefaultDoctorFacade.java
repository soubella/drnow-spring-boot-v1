package be.drnow.app.facade.doctor.impl;

import be.drnow.app.converter.Converter;
import be.drnow.app.core.constant.DoctorylMessagesConstants;
import be.drnow.app.dto.doctor.DoctorAutoCompleteDTO;
import be.drnow.app.dto.doctor.DoctorDTO;
import be.drnow.app.dto.doctor.DoctorSearchResultDTO;
import be.drnow.app.dto.doctor.ImageDTO;
import be.drnow.app.dto.doctor.form.DoctorProfileForm;
import be.drnow.app.dto.doctor.form.DoctorRegisterForm;
import be.drnow.app.dto.doctor.response.DoctorSearchResponseDTO;
import be.drnow.app.dto.shared.MailDTO;
import be.drnow.app.dto.user.ConfirmationCodeRequestDTO;
import be.drnow.app.facade.doctor.DoctorFacade;
import be.drnow.app.helper.LanguageHelper;
import be.drnow.app.helper.MailHelper;
import be.drnow.app.model.doctor.DoctorModel;
import be.drnow.app.model.user.AccountStatusEnum;
import be.drnow.app.model.user.TokenTypeEnum;
import be.drnow.app.model.user.UserModel;
import be.drnow.app.model.user.VerificationCodeModel;
import be.drnow.app.service.doctor.DoctorService;
import be.drnow.app.service.shared.EmailService;
import be.drnow.app.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.util.*;

@Component
public class DefaultDoctorFacade implements DoctorFacade {

    private static final String REGISTRATION_VERIFICATION_MAIL_TEMPLATE = DoctorylMessagesConstants.Email.REGISTRATION_VERIFICATION_MAIL_TEMPLATE;
    private static final String DOCTOR_WELCOME_MAIL_TEMPLATE = DoctorylMessagesConstants.Email.DOCTOR_WELCOME_MAIL_TEMPLATE;

    private DoctorService doctorService;
    private EmailService emailService;
    private UserService userService;
    private Converter<DoctorModel, DoctorDTO> doctorModelToDTO;
    private Converter<DoctorProfileForm, DoctorModel> doctorProfileFormToModel;
    private Converter<Set<DoctorModel>, Set<DoctorSearchResultDTO>> doctorModelToSearchDTOList;
    private Converter<DoctorModel, DoctorSearchResultDTO> doctorModelToSearchDTO;

    private MailHelper mailHelper;

    @Override
    public void registerDoctor(DoctorRegisterForm doctorRegisterForm) {
        doctorService.register(doctorRegisterForm);
    }

    @Override
    public void confirmAccount(ConfirmationCodeRequestDTO confirmationCodeRequestDTO) {
        VerificationCodeModel verificationCodeModel = userService.checkVerificationCode(confirmationCodeRequestDTO);
        doctorService.confirmAccount(verificationCodeModel);
    }

    @Override
    public Set<DoctorSearchResponseDTO> searchDoctor(final String value, final String address) {
        return doctorService.search(value, address);
    }

    @Async
    @Override
    public void sendRegisterVerificationEmail(DoctorRegisterForm doctorRegisterForm, String userLanguage) throws MessagingException {
        UserModel userModel = userService.findUserByEmail(doctorRegisterForm.getEmail());
        if (userModel != null) {
            MailDTO mail = mailHelper.buildDoctorRegisterEmail(doctorRegisterForm, LanguageHelper.getUserLocalFromHeader(userLanguage));
            VerificationCodeModel verificationCode = new VerificationCodeModel(userModel);
            mail.getProps().put("confirmationCode", verificationCode.getVerificationCode());
            userService.saveVerificationCode(verificationCode, TokenTypeEnum.FOR_EMAIL_REGISTRATION);
            emailService.sendEmail(mail, REGISTRATION_VERIFICATION_MAIL_TEMPLATE);
        }
    }

    @Async
    @Override
    public void sendWelcomeEmail(DoctorRegisterForm doctorRegisterForm, String userLanguage) throws MessagingException {
        UserModel userModel = userService.findUserByEmail(doctorRegisterForm.getEmail());
        if (userModel != null) {
            MailDTO mail = mailHelper.buildDoctorWelcomeEmail(doctorRegisterForm, LanguageHelper.getUserLocalFromHeader(userLanguage));
            emailService.sendEmail(mail, DOCTOR_WELCOME_MAIL_TEMPLATE);
        }
    }

    @Override
    public DoctorDTO findByPk(UUID pk) {
        DoctorDTO doctorDTO = new DoctorDTO();
        doctorModelToDTO.convert(doctorService.findByPk(pk), doctorDTO);
        return doctorDTO;
    }

    @Override
    public DoctorSearchResultDTO findDoctorProfileByPk(UUID pk) {
        DoctorSearchResultDTO doctorSearchResultDTO = new DoctorSearchResultDTO();
        doctorModelToSearchDTO.convert(doctorService.findByPk(pk), doctorSearchResultDTO);
        return doctorSearchResultDTO;
    }

    public void updateDoctorProfile(UUID pk, DoctorProfileForm doctorProfileForm) {
        DoctorModel doctorModel = doctorService.findByPk(pk);
        doctorModel.setAccountStatus(AccountStatusEnum.ACCOUNT_OKAY);
        doctorProfileFormToModel.convert(doctorProfileForm, doctorModel);
        doctorService.updateProfile(doctorModel);
    }

    @Override
    public void uploadImageDoctor(UUID pk, MultipartFile file) {
        doctorService.updateImage(pk, file);
    }

    @Override
    public ImageDTO downloadImageDoctor(UUID pk) {
        return new ImageDTO(doctorService.downloadImage(pk));
    }

    @Override
    public List<DoctorAutoCompleteDTO> findDoctorsByFullName(String s) {
        return null;
    }

    @Override
    public List<DoctorSearchResultDTO> searchForDoctors(String keyword, Pageable pageable) {
        Set<DoctorSearchResultDTO> results = new HashSet<>();
        doctorModelToSearchDTOList.convert(new HashSet<>(doctorService.searchForDoctors(keyword, pageable)), results);
        return new ArrayList<>(results);
    }

    @Override
    public List<DoctorSearchResultDTO> findAllDoctors(Pageable pageable) {
        Set<DoctorSearchResultDTO> results = new LinkedHashSet<>();
        doctorModelToSearchDTOList.convert(new LinkedHashSet<>(doctorService.findAllDoctors(pageable)), results);
        return new ArrayList<>(results);
    }

    @Autowired
    public void setDoctorService(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @Autowired
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setDoctorModelToDTO(Converter<DoctorModel, DoctorDTO> doctorModelToDTO) {
        this.doctorModelToDTO = doctorModelToDTO;
    }

    @Autowired
    public void setDoctorProfileFormToModel(Converter<DoctorProfileForm, DoctorModel> doctorProfileFormToModel) {
        this.doctorProfileFormToModel = doctorProfileFormToModel;
    }

    @Autowired
    public void setDoctorModelToSearchDTOList(Converter<Set<DoctorModel>, Set<DoctorSearchResultDTO>> doctorModelToSearchDTOList) {
        this.doctorModelToSearchDTOList = doctorModelToSearchDTOList;
    }

    @Autowired
    public void setDoctorModelToSearchDTO(Converter<DoctorModel, DoctorSearchResultDTO> doctorModelToSearchDTO) {
        this.doctorModelToSearchDTO = doctorModelToSearchDTO;
    }

    @Autowired
    public void setMailHelper(MailHelper mailHelper) {
        this.mailHelper = mailHelper;
    }
}
