package be.drnow.app.facade.doctor;

import be.drnow.app.dto.doctor.WorkingAgendaDTO;

import java.util.Set;
import java.util.UUID;

public interface WorkingAgendaFacade {

    void addOrUpdateWorkingAgenda(WorkingAgendaDTO workingAgendaDTO);

    void deleteWorkingAgendaByPk(UUID pk);

    Set<WorkingAgendaDTO> findAvailableWorkingAgendas(UUID doctorPk);

    Set<WorkingAgendaDTO> findUnAvailableWorkingAgendas(UUID doctorPk);

    void updateAppointmentDuration(int duration, UUID doctorPk);

    int findAppointmentDurationByDoctorPk(UUID doctorPk);

}
