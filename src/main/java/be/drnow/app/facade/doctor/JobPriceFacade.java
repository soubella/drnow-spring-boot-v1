package be.drnow.app.facade.doctor;

import be.drnow.app.dto.doctor.form.DoctorJobPriceForm;
import be.drnow.app.model.doctor.JobPriceModel;

import java.util.List;
import java.util.UUID;

public interface JobPriceFacade {
    void addOrUpdateJobPrice(DoctorJobPriceForm doctorJobPriceForm);

    List<JobPriceModel> getAllJobPricesByDoctorPk(UUID doctorPk);

    void deleteJobPriceByPk(UUID jobPricePk);
}
