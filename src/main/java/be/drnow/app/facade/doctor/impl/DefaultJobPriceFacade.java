package be.drnow.app.facade.doctor.impl;

import be.drnow.app.converter.Converter;
import be.drnow.app.dto.doctor.form.DoctorJobPriceForm;
import be.drnow.app.facade.doctor.JobPriceFacade;
import be.drnow.app.model.doctor.JobPriceModel;
import be.drnow.app.service.doctor.JobPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class DefaultJobPriceFacade implements JobPriceFacade {

    private JobPriceService jobPriceService;

    private Converter<DoctorJobPriceForm, JobPriceModel> doctorJobPriceFormToModel;

    @Override
    public void addOrUpdateJobPrice(DoctorJobPriceForm doctorJobPriceForm) {
        JobPriceModel jobPriceModel = new JobPriceModel();
        doctorJobPriceFormToModel.convert(doctorJobPriceForm, jobPriceModel);
        jobPriceService.addOrUpdateJobPrice(jobPriceModel);
    }

    @Override
    public void deleteJobPriceByPk(UUID jobPricePk) {
        jobPriceService.deleteJobPriceByPk(jobPricePk);
    }

    @Override
    public List<JobPriceModel> getAllJobPricesByDoctorPk(UUID doctorPk) {
        return jobPriceService.getAllJobPricesByDoctorPk(doctorPk);
    }

    @Autowired
    public void setJobPriceService(JobPriceService jobPriceService) {
        this.jobPriceService = jobPriceService;
    }

    @Autowired
    public void setDoctorJobPriceFormToModel(Converter<DoctorJobPriceForm, JobPriceModel> doctorJobPriceFormToModel) {
        this.doctorJobPriceFormToModel = doctorJobPriceFormToModel;
    }
}
