package be.drnow.app.exception.doctor;

/**
 * @author : medalaoui
 * @since : 29/05/2020, ven.
 **/
public class INAMIFormatNotCorrectException extends RuntimeException {

    public INAMIFormatNotCorrectException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    @Override
    public String getLocalizedMessage() {
        return super.getLocalizedMessage();
    }

    @Override
    public synchronized Throwable getCause() {
        return super.getCause();
    }

    @Override
    public void printStackTrace() {
        super.printStackTrace();
    }
}
