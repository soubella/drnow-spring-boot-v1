package be.drnow.app.exception.doctor;

import be.drnow.app.core.constant.DoctorylMessagesConstants;

public class DoctorNotApprovedException extends RuntimeException {
    public DoctorNotApprovedException() {
        super(DoctorylMessagesConstants.Exception.ACCOUNT_NOT_APPROVED);
    }
}
