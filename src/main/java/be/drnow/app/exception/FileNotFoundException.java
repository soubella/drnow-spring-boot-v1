package be.drnow.app.exception;

import be.drnow.app.core.constant.DoctorylMessagesConstants;

public class FileNotFoundException extends RuntimeException {
    public FileNotFoundException(String fileType) {
        super(String.format(DoctorylMessagesConstants.Exception.RESSOURCE_NOT_FOUND, fileType));
    }
}
