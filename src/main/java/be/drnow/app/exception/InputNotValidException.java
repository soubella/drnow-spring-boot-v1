package be.drnow.app.exception;

import be.drnow.app.core.constant.DoctorylMessagesConstants;

public class InputNotValidException extends RuntimeException {
    public InputNotValidException(String input) {
        super(String.format(DoctorylMessagesConstants.Exception.INPUT_IS_NOT_VALID, input));
    }
}
