package be.drnow.app.exception;

import java.io.IOException;

/**
 * @author : medalaoui
 * @since : 29/05/2020, ven.
 **/

public class WSException extends IOException {

    public WSException(String message) {
        super(message);
    }

}
