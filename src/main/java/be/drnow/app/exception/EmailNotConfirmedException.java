package be.drnow.app.exception;

import be.drnow.app.core.constant.DoctorylMessagesConstants;

public class EmailNotConfirmedException extends RuntimeException {
    public EmailNotConfirmedException() {
        super(DoctorylMessagesConstants.Exception.EMAIL_NOT_CONFIRMED);
    }
}
