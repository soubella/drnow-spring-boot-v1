package be.drnow.app.exception;

import be.drnow.app.core.constant.DoctorylMessagesConstants;

public class VerificationCodeExpiredException extends RuntimeException {
    public VerificationCodeExpiredException() {
        super(DoctorylMessagesConstants.Exception.VERIFICATION_CODE_EXPIRED);
    }
}
