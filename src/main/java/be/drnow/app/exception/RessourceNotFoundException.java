package be.drnow.app.exception;

import be.drnow.app.core.constant.DoctorylMessagesConstants;

public class RessourceNotFoundException extends RuntimeException {
    public RessourceNotFoundException(String resource) {
        super(String.format(DoctorylMessagesConstants.Exception.RESSOURCE_NOT_FOUND, resource));
    }
}
