package be.drnow.app.exception;

import be.drnow.app.core.constant.DoctorylMessagesConstants;

public class ResourceAlreadyInUseException extends RuntimeException {
    public ResourceAlreadyInUseException(String resource) {
        super(String.format(DoctorylMessagesConstants.Exception.RESOURCE_IS_ALREADY_IN_USE, resource));
    }
}
