package be.drnow.app.exception;

public class UnauthorizedException extends RuntimeException {
    public UnauthorizedException(String input) {
        super(input);
    }
}
