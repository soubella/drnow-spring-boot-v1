package be.drnow.app.exception;

public class EmailVerificationCodeException extends RuntimeException {

    public EmailVerificationCodeException(String message) {
        super(message);
    }
}
