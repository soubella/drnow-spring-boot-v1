export const environment = {
  production: true,
  apiUrl:'https://doctoryl.com/api',
  defaultLanguage:'fr-FR',
  supportedLanguages:['fr-FR','nl-BE','en-US'],
  apiVersion:'1'
};
