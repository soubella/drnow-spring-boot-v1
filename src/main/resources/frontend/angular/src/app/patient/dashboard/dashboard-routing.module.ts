import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchForPraticienComponent } from '../../public/search-for-praticien/search-for-praticien.component';
import { MedicalRecordComponent } from './medical-record/medical-record.component';
import { MyConsultationsComponent } from './my-consultations/my-consultations.component';
import { MyDocumentsComponent } from './my-documents/my-documents.component';
import { PraticienProfileComponent } from '../../public/praticien-profile/praticien-profile.component';
import {OpenviduComponent} from "./openvidu/openvidu.component";

const routes: Routes = [
  { path: '', component: SearchForPraticienComponent },
  { path: 'medical-record', component: MedicalRecordComponent },
  { path: 'my-consultations', component: MyConsultationsComponent },
  { path: 'my-documents', component: MyDocumentsComponent },
  { path: 'praticien', component: PraticienProfileComponent },
  { path: 'video-call', component: OpenviduComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
