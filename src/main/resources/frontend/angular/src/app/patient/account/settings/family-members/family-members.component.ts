import { Component, OnInit } from '@angular/core';
import { PatientService } from '../../../../core/services/patient/patient.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-family-members',
  templateUrl: './family-members.component.html',
  styleUrls: ['./family-members.component.scss'],
  providers: [DatePipe]
})
export class FamilyMembersComponent implements OnInit {

  familyMembers = [];

  familyMemberForm: FormGroup;
  submitted: boolean;
  isEdit: boolean;

  selectedMember;

  constructor(private patientService: PatientService, private toastr: ToastrService,
              private translateService: TranslateService, public modalService: NgbModal,
              private formBuilder: FormBuilder, private datePipe: DatePipe) {
  }

  ngOnInit() {
    this.loadFamilyMembers();
    this.familyMemberForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      patientRelationshipEnum: ['', [Validators.required]],
      genderEnum: ['', [Validators.required]],
      birthday: ['', [Validators.required]],
      email: ['']
    });
  }

  onSubmit() {
    if (this.isEdit) {
      this.onSubmitUpdate();
    } else {
      this.onSubmitAdd();
    }
  }

  onSubmitAdd() {

    this.submitted = true;

    if (this.familyMemberForm.invalid) {
      return;
    }

    this.patientService.addBeneficiary(this.familyMemberForm.value).subscribe(
      success => {
        this.submitted = false;
        this.closeModal();
        this.loadFamilyMembers();
      });
  }

  onSubmitUpdate() {
    this.submitted = true;

    if (this.familyMemberForm.invalid) {
      return;
    }

    this.patientService.updatePatientProfile(this.familyMemberForm.value, this.selectedMember.pk).subscribe(
      value => {
        this.loadFamilyMembers();
        this.closeModal();
        this.submitted = false;
      });
  }

  loadFamilyMembers() {
    this.patientService.findPatientByPatientPk().subscribe(data => {
      this.familyMembers = data.body.familyMembers;
      console.log(this.familyMembers)
    });
  }

  get f() {
    return this.familyMemberForm.controls;
  }

  editPerson(content, person: any) {
    this.selectedMember = person;
    this.isEdit = true;

    console.log(person);

    this.familyMemberForm.controls['firstName'].setValue(person.firstName);
    this.familyMemberForm.controls['lastName'].setValue(person.lastName);
    this.familyMemberForm.controls['genderEnum'].setValue(person.gender);
    this.familyMemberForm.controls['patientRelationshipEnum'].setValue(person.relationType);
    this.familyMemberForm.controls['birthday'].setValue(this.datePipe.transform(person.birthday, 'dd/MM/yyyy'));

    this.modalService.open(content, { centered: true });
  }

  delete(person: any) {

  }

  closeModal() {
    this.modalService.dismissAll();
    this.familyMemberForm.reset();
    this.isEdit = false;
  }
}
