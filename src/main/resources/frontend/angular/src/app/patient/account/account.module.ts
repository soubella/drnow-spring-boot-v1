import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { PatientLayoutModule } from '../../layouts/patient-layout/patient-layout.module';
import { AuthModule } from '../../doctor/account/auth/auth.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AccountRoutingModule,
    AuthModule,
    PatientLayoutModule
  ]
})
export class AccountModule {
}
