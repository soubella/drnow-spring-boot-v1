import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../../../core/services/auth.service';
import {ToastrService} from 'ngx-toastr';
import {PasswordStrengthBarComponent} from "../../../../shared/ui/password-strength-bar/password-strength-bar.component";

@Component({
  selector: 'app-login-password',
  templateUrl: './login-password.component.html',
  styleUrls: ['./login-password.component.scss']
})
export class LoginPasswordComponent implements OnInit {

  @ViewChild(PasswordStrengthBarComponent, {static: false}) passwordComponent;

  loginForm: FormGroup;
  submitted: boolean;

  constructor(private formBuilder: FormBuilder, private authService: AuthenticationService, private toastr: ToastrService) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      currentPassword: ['', [Validators.required]],
      newPassword: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
      email: ['']
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  updateLogin() {
    this.submitted = true;

    if(!this.passwordComponent.isPasswordOkay){
      this.loginForm.controls['newPassword'].setErrors({'incorrect': true});
      this.loginForm.controls['confirmPassword'].setErrors({'incorrect': true});
      return;
    }

    if (this.loginForm.invalid || this.loginForm.value.newPassword !== this.loginForm.value.confirmPassword) {
      return;
    }

    this.authService.changePassword(this.loginForm.value).subscribe(success => {
      this.submitted = false;
      this.toastr.success(success.body.message, '', {timeOut: 6000, closeButton: true});
    }, error => {
      this.toastr.warning(error, '', {timeOut: 6000, closeButton: true});
    });
  }
}
