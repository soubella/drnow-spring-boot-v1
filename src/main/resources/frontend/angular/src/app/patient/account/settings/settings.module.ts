import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { PaymentInfoComponent } from './payment-info/payment-info.component';
import { LoginPasswordComponent } from './login-password/login-password.component';
import { FamilyMembersComponent } from './family-members/family-members.component';
import { NgbModalModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { UIModule } from '../../../shared/ui/ui.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from '@iplab/ngx-file-upload';
import { NgSelectModule } from '@ng-select/ng-select';
import { PersonalInfoComponent } from './personal-info/personal-info.component';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [ProfileComponent, PaymentInfoComponent, LoginPasswordComponent, FamilyMembersComponent, PersonalInfoComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    UIModule,
    NgbTabsetModule,
    TranslateModule,
    ReactiveFormsModule,
    FileUploadModule,
    NgbModalModule,
    NgSelectModule,
    FormsModule,
    NgxMaskModule.forRoot()
  ]
})
export class SettingsModule { }
