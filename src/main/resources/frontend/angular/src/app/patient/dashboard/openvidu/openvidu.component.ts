import {Component, HostListener, OnInit} from '@angular/core';
import {OpenVidu, Publisher, Session, StreamEvent, StreamManager, Subscriber} from "openvidu-browser";
import {OpenViduService} from "../../../core/services/openvidu.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-openvidu',
  templateUrl: './openvidu.component.html',
  styleUrls: ['./openvidu.component.scss']
})
export class OpenviduComponent implements OnInit {

  // OpenVidu objects
  OV: OpenVidu;
  session: Session;
  publisher: StreamManager; // Local
  subscribers: StreamManager[] = []; // Remotes

  options = {
    audioSource: undefined, // The source of audio. If undefined default microphone
    videoSource: undefined, // The source of video. If undefined default webcam
    publishAudio: true,     // Whether you want to start publishing with your audio unmuted or not
    publishVideo: true,     // Whether you want to start publishing with your video enabled or not
    resolution: '640x480',//window.screen.availWidth+ 'x' + window.screen.availHeight,  // The resolution of your video
    frameRate: 30,          // The frame rate of your video
    insertMode: 'APPEND',   // How the video is inserted in the target element 'video-container'
    mirror: false           // Whether to mirror your local video or not
  }

  username = 'Patient X';

  // Main video of the page, will be 'publisher' or one of the 'subscribers',
  // updated by click event in UserVideoComponent children
  mainStreamManager: StreamManager;

  appointmentToken;
  appointmentData;

  constructor(private openViduService: OpenViduService, private router: Router, private route: ActivatedRoute) {
    // if (this.router.getCurrentNavigation().extras.state) {
    //   this.appointmentData = this.router.getCurrentNavigation().extras.state;
    //   this.joinSession();
    // } else {
    //   this.router.navigateByUrl('/patient/dashboard/consultations');
    // }

    if (!this.route.snapshot.paramMap.has('room')) {
      this.router.navigate(['/patient/dashboard/consultations']);
      this.appointmentData = this.router.getCurrentNavigation().extras.state;
    } else {
      this.appointmentToken = this.route.snapshot.paramMap.get('room');
      this.joinSession();
    }

  }

  ngOnInit() {
  }

  joinSession() {
    // --- 1) Get an OpenVidu object ---
    this.OV = new OpenVidu();

    // --- 2) Init a session ---
    this.session = this.OV.initSession();

    // --- 3) Specify the actions when events take place in the session ---
    // On every new Stream received...
    this.session.on('streamCreated', (event: StreamEvent) => {
      // Subscribe to the Stream to receive it. Second parameter is undefined
      // so OpenVidu doesn't create an HTML video by its own
      let subscriber: Subscriber = this.session.subscribe(event.stream, undefined);
      this.subscribers.push(subscriber);
    });

    // On every Stream destroyed...
    this.session.on('streamDestroyed', (event: StreamEvent) => {
      // Remove the stream from 'subscribers' array
      this.deleteSubscriber(event.stream.streamManager);
    });

    // --- 4) Connect to the session with a valid user token ---
    this.openViduService.getOpenViduToken(this.appointmentToken).subscribe(
      data => {
        const token = data.token;
        this.sessionConnect(token);
      });

  }

  sessionConnect(token) {
    // First param is the token got from OpenVidu Server. Second param can be retrieved by every user on event
    // 'streamCreated' (property Stream.connection.data), and will be appended to DOM as the user's nickname
    this.session.connect(token, {clientData: this.username})
      .then(() => {

        // --- 5) Get your own camera stream ---

        // Init a publisher passing undefined as targetElement (we don't want OpenVidu to insert a video
        // element: we will manage it on our own) and with the desired properties
        let publisher: Publisher = this.OV.initPublisher(undefined, this.options);

        // --- 6) Publish your stream ---

        this.session.publish(publisher);

        // Set the main video in the page to display our webcam and store our Publisher
        this.mainStreamManager = publisher;
        this.publisher = publisher;
      })
      .catch(error => {
        console.log('There was an error connecting to the session:', error.code, error.message);
      });
  }

  updateMainStreamManager(streamManager: StreamManager) {
    this.mainStreamManager = streamManager;
  }

  @HostListener('window:beforeunload')
  beforeunloadHandler() {
    // On window closed leave session
    this.leaveSession();
  }

  ngOnDestroy() {
    // On component destroyed leave session
    this.leaveSession();
  }

  leaveSession() {
    // --- 7) Leave the session by calling 'disconnect' method over the Session object ---

    if (this.session) {
      this.session.disconnect();
    }

    // Empty all properties...
    this.subscribers = [];
    delete this.publisher;
    delete this.session;
    delete this.OV;
    this.router.navigateByUrl('/doctor/dashboard/consultations');
  }

  private deleteSubscriber(streamManager: StreamManager): void {
    let index = this.subscribers.indexOf(streamManager, 0);
    if (index > -1) {
      this.subscribers.splice(index, 1);
    }
  }

  goToDoctorProfile(doctor: any) {
    this.router.navigate(['/praticien',
      { doctor: doctor.pk }]
    );
  }
}
