import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NormalSignupComponent } from './normal-signup/normal-signup.component';
import { AppointmentSignupComponent } from './appointment-signup/appointment-signup.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: NormalSignupComponent },
  { path: 'reservation', component: AppointmentSignupComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
