import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientLayoutComponent as PatientLayoutComponent } from '../layouts/patient-layout/patient-layout.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { Role } from '../core/models/role.enums';

const routes: Routes = [
  {
    path: 'dashboard', component: PatientLayoutComponent,
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate: [AuthGuard],
    data: { roles: [Role.Patient] }
  },
  {
    path: 'account', loadChildren: () => import('./account/account.module').then(m => m.AccountModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientRoutingModule {
}
