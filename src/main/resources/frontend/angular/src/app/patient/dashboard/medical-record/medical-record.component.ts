import { Component, OnInit } from '@angular/core';
import { MedicalRecord } from '../../../core/models/patient/medical-record.model';
import { PatientService } from '../../../core/services/patient/patient.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-medical-record',
  templateUrl: './medical-record.component.html',
  styleUrls: ['./medical-record.component.scss'],
  providers: [DatePipe]
})
export class MedicalRecordComponent implements OnInit {

  medicalRecord = new MedicalRecord();
  selectedVaccine = [];
  vaccineData = ['BCG', 'Coronavirus', 'Diphtérie - Poliomyélite', 'Tetanos', 'Coqueluche',
    'Haemophilius', 'Hépatite B', 'Pneumocoque', 'Méningite C', 'Rougeole, Oreillons, Rubéole', 'HPV', 'Grippe', 'Zona'];
  cvDiseasesRiskData = ['HTA', 'Dyslipidémie', 'Diabète'];
  selectedCvDiseasesRisk = [];

  currentPatientData;

  patientFamily = [];

  selectedPatientPk = '';

  constructor(private patientService: PatientService, private toastr: ToastrService,
              public modalService: NgbModal,
              private formBuilder: FormBuilder, private datePipe: DatePipe) {
  }

  ngOnInit() {
    this.loadPatientData();

    this.familyMemberForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      patientRelationshipEnum: ['', [Validators.required]],
      genderEnum: ['', [Validators.required]],
      birthday: ['', [Validators.required]],
      email: ['']
    });

  }


  loadPatientData() {
    this.patientService.findPatientByPatientPk().subscribe(
      data => {
        this.currentPatientData = data.body;
        this.patientFamily = this.currentPatientData.familyMembers;
        this.patientFamily.push(this.currentPatientData);
        if (this.selectedPatientPk == '') {
          this.selectedPatientPk = this.currentPatientData.pk;
        }
        this.selectPatient(this.currentPatientData.pk);
      });
  }

  selectPatient(pk) {
    this.selectedPatientPk = pk;
    const medicalRecord = this.patientFamily.find(value => value.pk == pk).medicalRecord;
    if (medicalRecord != null) {
      this.medicalRecord = medicalRecord;
      if (this.medicalRecord.vaccineStatus && this.medicalRecord.vaccineStatus !== '') {
        this.selectedVaccine = this.medicalRecord.vaccineStatus.split(',');
      }
      if (this.medicalRecord.cvDiseasesRisk && this.medicalRecord.cvDiseasesRisk !== '') {
        this.selectedCvDiseasesRisk = this.medicalRecord.cvDiseasesRisk.split(',');
      }
      this.medicalRecord.patientPk = pk;
    } else {
      this.medicalRecord = new MedicalRecord();
      this.selectedVaccine = [];
      this.selectedCvDiseasesRisk = [];
    }
    console.log(pk);
    console.log(this.medicalRecord);
  }

  updateMedicalRecord() {
    this.medicalRecord.vaccineStatus = this.selectedVaccine.toString();
    this.medicalRecord.cvDiseasesRisk = this.selectedCvDiseasesRisk.toString();
    this.medicalRecord.patientPk = this.selectedPatientPk;
    console.log(this.medicalRecord);
    this.patientService.addMedicalRecord(this.medicalRecord).subscribe(response => {
      this.loadPatientData();
      this.toastr.success(response.body.message, '', { timeOut: 6000, closeButton: true });
    });
  }


  // add family member

  familyMemberForm: FormGroup;
  submitted: boolean;

  onSubmit() {

    this.submitted = true;

    if (this.familyMemberForm.invalid) {
      return;
    }

    this.patientService.addBeneficiary(this.familyMemberForm.value).subscribe(
      success => {
        this.loadPatientData();
        this.submitted = false;
        this.closeModal();
      });
  }

  closeModal() {
    this.modalService.dismissAll();
    this.familyMemberForm.reset();
  }

  get f() {
    return this.familyMemberForm.controls;
  }


}
