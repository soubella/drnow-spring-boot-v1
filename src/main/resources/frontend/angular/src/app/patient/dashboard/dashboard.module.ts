import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { PublicModule } from '../../public/public.module';
import { MedicalRecordComponent } from './medical-record/medical-record.component';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { MyConsultationsComponent } from './my-consultations/my-consultations.component';
import { MyDocumentsComponent } from './my-documents/my-documents.component';
import { NgbTabsetModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { OpenviduComponent } from './openvidu/openvidu.component';
import {UIModule} from "../../shared/ui/ui.module";
import {NgxMaskModule} from "ngx-mask";

@NgModule({
  declarations: [MedicalRecordComponent, MyConsultationsComponent, MyDocumentsComponent, OpenviduComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    PublicModule,
    UIModule,
    AccordionModule.forRoot(),
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgbTabsetModule,
    NgbTypeaheadModule,
    NgxMaskModule.forRoot()
  ]
})
export class DashboardModule { }
