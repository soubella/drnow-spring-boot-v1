import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../../core/services/auth.service';
import { WizardComponent as BaseWizardComponent } from 'angular-archwizard';
import { PatientService } from '../../../../core/services/patient/patient.service';
import {PasswordStrengthBarComponent} from "../../../../shared/ui/password-strength-bar/password-strength-bar.component";

@Component({
  selector: 'app-normal-signup',
  templateUrl: './normal-signup.component.html',
  styleUrls: ['./normal-signup.component.scss']
})
export class NormalSignupComponent implements OnInit {

  @ViewChild(PasswordStrengthBarComponent, {static: false}) passwordComp;

  emailForm: FormGroup;
  accountForm: FormGroup;
  passwordForm: FormGroup;
  @ViewChild('wizardForm', { static: false }) wizard: BaseWizardComponent;
  submitted = false;
  submitted2 = false;
  submitted3 = false;
  error = '';
  success = '';
  loading = false;
  password;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router,
              private authService: AuthenticationService, private patientService: PatientService) {
  }

  ngOnInit() {
    this.emailForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });

    this.accountForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required]],
      password: ['', []],
      email: ['', []],
      patientRelationshipEnum: ['', []],
      genderEnum: ['', [Validators.required]],
      birthday: ['', [Validators.required]]
    });

    this.passwordForm = this.formBuilder.group({
      password: ['', [Validators.required]]
    });
  }

  ngAfterViewInit() {
    document.body.classList.add('authentication-bg');
    document.body.classList.add('authentication-bg-pattern');
  }

  get f() {
    return this.emailForm.controls;
  }

  get f2() {
    return this.accountForm.controls;
  }

  get f3() {
    return this.passwordForm.controls;
  }

  onCheckEmail() {

    this.success = '';
    this.submitted = true;

    // stop here if form is invalid
    if (this.emailForm.invalid) {
      return;
    }

    this.loading = true;

    this.authService.checkAccountByEmail(this.emailForm.value)
      .subscribe(success => {
        this.router.navigate(['/patient/account/auth/login', { email: this.emailForm.value.email }]);
      });
    this.loading = false;
  }

  setPersonalInfo() {
    // check birthday
    const day = this.accountForm.value.birthday.substr(0, 2);
    const month = this.accountForm.value.birthday.substr(2, 2);
    const year = this.accountForm.value.birthday.substr(4, 4);
    if(parseInt(day) > 31 || parseInt(month) > 12
      || parseInt(year) > new Date().getFullYear() || parseInt(year) < 1900){
      this.accountForm.controls['birthday'].setErrors({'incorrect': true});
      return;
    }

    if (this.accountForm.valid) {
      this.wizard.navigation.goToNextStep();
    }
    this.submitted2 = true;
  }

  onSubmit() {

    this.success = '';
    this.submitted3 = true;

    if(!this.passwordComp.isPasswordOkay){
      this.passwordForm.controls['password'].setErrors({'incorrect': true});
      return;
    }

    // stop here if form is invalid
    if (this.accountForm.invalid) {
      return;
    }

    this.accountForm.controls['email'].setValue(this.emailForm.value.email);
    this.accountForm.controls['password'].setValue(this.passwordForm.value.password);
    this.accountForm.controls['patientRelationshipEnum'].setValue('MAIN_ACCOUNT');

    this.patientService.register(this.accountForm.value).subscribe(value => {
      this.router.navigate(['/patient/account/auth/login', { email: this.emailForm.value.email }]);
    });

    this.loading = true;

  }

}
