import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PatientService } from '../../../../core/services/patient/patient.service';
import { DatePipe } from '@angular/common';
import UserUtils from '../../../../core/utils/user.utils';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss'],
  providers:[DatePipe]
})
export class PersonalInfoComponent implements OnInit {

  profileForm: FormGroup;
  submitted = false;

  patientData: any;

  constructor(private formBuilder: FormBuilder, private patientService: PatientService, private datePipe: DatePipe) {
  }

  ngOnInit() {

    this.profileForm = this.formBuilder.group({
      // Personal Info
      pk: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      birthday: ['', Validators.required],
      city: ['', Validators.required],
      postalCode: ['', Validators.required],
      genderEnum: ['', Validators.required],
    });

    this.loadProfileData();
  }

  updateProfile() {
    this.submitted = true;

    if(this.profileForm.invalid){
      return;
    }

    this.patientService.updatePatientProfile(this.profileForm.value, UserUtils.getCurrentUserPk()).subscribe(
      value => {
      this.submitted = false;
      this.loadProfileData();
    });

  }

  get f() {
    return this.profileForm.controls;
  }

  loadProfileData() {
    this.patientService.findPatientByPatientPk().subscribe(
      data => {
        this.patientData = data.body;
        this.populateForm();
      });
  }

  populateForm(){
    this.profileForm.controls['firstName'].setValue(this.patientData.firstName);
    this.profileForm.controls['lastName'].setValue(this.patientData.lastName);
    this.profileForm.controls['phoneNumber'].setValue(this.patientData.phoneNumber);
    this.profileForm.controls['genderEnum'].setValue(this.patientData.gender);
    this.profileForm.controls['birthday'].setValue(this.datePipe.transform(this.patientData.birthday, 'dd/MM/yyyy'));
    if(this.patientData.address){
      this.profileForm.controls['city'].setValue(this.patientData.address.city);
      this.profileForm.controls['postalCode'].setValue(this.patientData.address.postalCode);
    }
  }

}
