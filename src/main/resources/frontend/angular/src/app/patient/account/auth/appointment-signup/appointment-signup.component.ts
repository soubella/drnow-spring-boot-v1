import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {WizardComponent} from 'angular-archwizard';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../../../core/services/auth.service';
import {Doctor} from '../../../../core/models/doctor/doctor.model';
import {DoctorService} from '../../../../core/services/doctor/doctor.service';
import {PatientService} from '../../../../core/services/patient/patient.service';
import {User} from '../../../../core/models/auth.model';
import {AgendaService} from '../../../../core/services/doctor/agenda.service';

@Component({
  selector: 'app-appointment-signup',
  templateUrl: './appointment-signup.component.html',
  styleUrls: ['./appointment-signup.component.scss']
})
export class AppointmentSignupComponent implements OnInit {

  emailForm: FormGroup;
  accountForm: FormGroup;
  passwordForm: FormGroup;
  reservationForm: FormGroup;
  beneficiaryForm: FormGroup;
  medicalRecordForm: FormGroup;
  socialNumberForm: FormGroup;
  paymentForm: FormGroup;
  urlBack: string = '/';
  isLogged: boolean;
  @ViewChild('wizard', {static: false}) wizard: WizardComponent;

  submitted = false;
  submitted2 = false;
  submitted3 = false;
  submitted4 = false;
  submitted5 = false;
  submitted6 = false;
  submitted7 = false;
  error = '';
  success = '';
  loading = false;

  reservationTimestamp;
  doctorPk;
  doctorData: Doctor;
  profilePic: string = 'assets/images/doctor/no-pic.png';
  currentUserFullName: string = '';
  currentPatientPk;
  currentBeneficiaryPk;
  afterMedicalRecordStepId: string = 'step10';

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private authService: AuthenticationService,
              private doctorService: DoctorService, private patientService: PatientService, private agendaService: AgendaService) {
  }

  ngOnInit() {
    if (!this.route.snapshot.paramMap.has('doctor') || !this.route.snapshot.paramMap.has('time')) {
      this.router.navigate(['/']);
    } else {
      this.doctorPk = this.route.snapshot.paramMap.get('doctor');
      this.reservationTimestamp = new Date(parseInt(this.route.snapshot.paramMap.get('time')));
      this.checkAvailability();
      this.loadDoctorData();
      this.downloadProfileImage();
    }


    this.emailForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
    this.accountForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', []],
      phoneNumber: ['', [Validators.required]],
      password: ['', []],
      patientRelationshipEnum: ['', []],
      genderEnum: ['', [Validators.required]],
      birthday: ['', [Validators.required]]
    });
    this.passwordForm = this.formBuilder.group({
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]]
    });
    this.reservationForm = this.formBuilder.group({
      startAt: ['', []],
      reason: ['', [Validators.required]],
      price: ['', []],
      repayable: ['', []],
      doctorPk: ['', []],
      patientPk: ['', []],
      appointmentStatusEnum: ['', []],
      beneficiaryPk: ['', []]
    });
    this.beneficiaryForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      patientRelationshipEnum: ['', [Validators.required]],
      genderEnum: ['', [Validators.required]],
      birthday: ['', [Validators.required]],
      email: ['']
    });
    this.medicalRecordForm = this.formBuilder.group({
      weight: ['', [Validators.required]],
      height: ['', [Validators.required]],
      patientPk: ['', []]
    });
    this.socialNumberForm = this.formBuilder.group({
      socialNumber: ['', [Validators.required]],
      userPk: ['', []]
    });
    this.paymentForm = this.formBuilder.group({});

    this.currentUser = this.authService.currentUser();

    if (this.currentUser && this.currentUser.role == 'ROLE_DOCTOR') {
      this.authService.logout();
    }

    if (this.currentUser && this.currentUser.role == 'ROLE_PATIENT') {
      this.currentPatientPk = this.currentUser.pk;
      this.currentBeneficiaryPk = this.currentUser.pk;
      this.isLogged = true;
      this.patientService.findPatientByPatientPk().subscribe(data => {
        this.currentUserFullName = data.body.firstName + ' ' + data.body.lastName;
        this.familyMembers = data.body.familyMembers;

      });
    }

  }

  loadDoctorData() {
    this.doctorService.getProfileByPk(this.doctorPk).subscribe(value => {
      this.doctorData = value.body;
      if (value.body.repayable == true) {
        this.doctorData.repayable = 'Oui';
        this.afterMedicalRecordStepId = 'step9';
      } else {
        this.doctorData.repayable = 'Non';
      }
    });
  }

  downloadProfileImage() {
    this.doctorService.getDoctorImageByPk(this.doctorPk).subscribe(response => {
      if (response.status == 200 && response.body.message != ' Image not found') {
        this.profilePic = 'data:image/jpeg;base64,' + response.body.imageBytes;
      }
    });
  }

  // Step 1
  onCheckEmail() {
    this.success = '';
    this.submitted = true;
    // stop here if form is invalid
    if (this.emailForm.invalid) {
      return;
    }
    this.loading = true;
    this.authService.checkAccountByEmail(this.emailForm.value)
      .subscribe(success => {
        this.router.navigate(['/patient/account/auth/login', {
          email: this.emailForm.value.email,
          doctor: this.doctorPk,
          time: this.reservationTimestamp.getTime()
        }]);
      }, error1 => {
        this.urlBack = window.location.pathname;
        this.wizard.navigation.goToNextStep();
        console.log(error1);
      });
    this.loading = false;
  }

  // Step 2
  setPersonalInfo() {
    if (this.accountForm.valid) {
      this.wizard.navigation.goToNextStep();
    }
    this.currentUserFullName = this.accountForm.value.firstName + ' ' + this.accountForm.value.lastName;
    this.submitted2 = true;
  }

  // Step 3
  onSubmit() {
    this.accountForm.controls['email'].setValue(this.emailForm.value.email);
    this.accountForm.controls['password'].setValue(this.passwordForm.value.password);
    this.accountForm.controls['patientRelationshipEnum'].setValue('MAIN_ACCOUNT');
    this.success = '';
    this.submitted3 = true;

    console.log(this.accountForm.value);

    // stop here if form is invalid
    if (this.accountForm.invalid || this.passwordForm.value.password !== this.passwordForm.value.confirmPassword) {
      return;
    }

    this.patientService.register(this.accountForm.value).subscribe(value => {
      this.currentPatientPk = value.body.code;
      this.currentBeneficiaryPk = value.body.code;
      this.wizard.navigation.goToNextStep();
    });

    this.loading = true;
  }

  // Step 4
  showPolicyDiv() {
    this.wizard.navigation.goToNextStep();
    // document.getElementById('div1').classList.remove('col-xl-4');
    // document.getElementById('div1').classList.add('col-xl-6');
    // document.getElementById('div1').classList.remove('offset-2');
    // document.getElementById('div1').classList.add('offset-1');
  }

  changeDivSize() {
    this.wizard.navigation.goToNextStep();
    // document.getElementById('div1').classList.remove('col-xl-6');
    // document.getElementById('div1').classList.add('col-xl-4');
    // document.getElementById('div1').classList.remove('offset-1');
    // document.getElementById('div1').classList.add('offset-2');
  }

  // Step 5
  setReservationMotif(beneficiaryPk: string) {
    this.currentBeneficiaryPk = beneficiaryPk;
    this.submitted4 = true;
  }

  // Step 6
  addBeneficiary() {
    this.submitted5 = true;
    if (this.beneficiaryForm.invalid) {
      return;
    }
    if (this.currentUser && this.currentUser.role == 'ROLE_PATIENT') {
      this.beneficiaryForm.controls['email'].setValue(this.currentUser.email);
    } else {
      this.beneficiaryForm.controls['email'].setValue(this.emailForm.value.email);
    }

    this.patientService.addBeneficiary(this.beneficiaryForm.value).subscribe(value => {
      this.currentBeneficiaryPk = value.body.code;
      this.wizard.navigation.goToNextStep();
    }, error => {
      console.log(error);
    });
  }

  // Step 7
  addMedicalRecord() {
    this.submitted6 = true;
    if (this.medicalRecordForm.invalid) {
      return;
    }
    this.medicalRecordForm.controls['patientPk'].setValue(this.currentBeneficiaryPk);
    console.log(this.medicalRecordForm.value);
    this.patientService.addMedicalRecord(this.medicalRecordForm.value).subscribe(value => {
      console.log(value);
    });
  }

  // Step 8
  addSocialNumber() {
    this.submitted7 = true;
    if (this.socialNumberForm.invalid) {
      return;
    }
    this.socialNumberForm.controls['userPk'].setValue(this.currentBeneficiaryPk);
    this.patientService.addSecurityNumberToPatient(this.socialNumberForm.value).subscribe(value => {
      console.log(value);
    });
  }

  // Step 9
  pay() {
    this.takeReservation();
  }

  takeReservation() {
    this.reservationForm.controls['startAt'].setValue(this.reservationTimestamp);
    this.reservationForm.controls['price'].setValue(this.doctorData.teleconsultationPrice);
    this.reservationForm.controls['repayable'].setValue(this.doctorData.repayable == 'Oui');
    this.reservationForm.controls['doctorPk'].setValue(this.doctorData.pk);
    this.reservationForm.controls['patientPk'].setValue(this.currentPatientPk);
    this.reservationForm.controls['beneficiaryPk'].setValue(this.currentBeneficiaryPk);
    this.reservationForm.controls['appointmentStatusEnum'].setValue('NOT_STARTED_YET');
    this.submitted7 = true;
    this.patientService.makeReservation(this.reservationForm.value).subscribe(value => {
      console.log(value);
    });
  }

  currentUser: User;
  familyMembers = [];

  ngAfterViewInit() {
    document.body.classList.add('authentication-bg');
    document.body.classList.add('authentication-bg-pattern');
    document.body.classList.add('pb-0');

    if (this.currentUser && this.currentUser.role == 'ROLE_PATIENT') {
      this.wizard.navigation.goToStep(5);
    }
    //this.wizard.navigation.goToStep(4)
    // console.log(this.wizard.navigation.canGoToStep(2))
  }

  goBack() {

    if (!this.isAppointmentAvailable) {
      this.router.navigate(['/']).then();
      return;
    }

    console.log(this.wizard.model.currentStep.stepId);

    const stepsToGoBack = ['step1', 'step6', 'step11', 'step4'];
    const stepsToGoPrevious = ['step2', 'step3', 'step7', 'step8', 'step5'];

    if (stepsToGoBack.includes(this.wizard.model.currentStep.stepId)) {
      this.router.navigate(['/']).then();
    } else if (stepsToGoPrevious.includes(this.wizard.model.currentStep.stepId)) {
      this.wizard.navigation.goToPreviousStep();
    }else if(this.wizard.model.currentStep.stepId == 'step10'){
      this.wizard.navigation.goToStep(5);
    }
  }

  get f() {
    return this.emailForm.controls;
  }

  get f2() {
    return this.accountForm.controls;
  }

  get f3() {
    return this.passwordForm.controls;
  }

  get f4() {
    return this.reservationForm.controls;
  }

  get f5() {
    return this.beneficiaryForm.controls;
  }

  get f6() {
    return this.medicalRecordForm.controls;
  }

  get f7() {
    return this.socialNumberForm.controls;
  }

  barLabel: string = 'LoginPage.PasswordStrength';

  onSelectChildPatient(pk: string) {
    this.submitted4 = true;
    this.currentBeneficiaryPk = pk;
    this.wizard.navigation.goToStep(9);
  }

  isAppointmentAvailable: boolean = true;

  checkAvailability() {
    // TODO check vs current datetime
    this.agendaService.checkAvailability(this.doctorPk, this.reservationTimestamp).subscribe(
      data => {
        this.isAppointmentAvailable = (data.body.message == 'Yes');
      });
  }

}
