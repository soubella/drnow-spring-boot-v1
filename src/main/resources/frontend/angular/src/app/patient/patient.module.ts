import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientRoutingModule } from './patient-routing.module';
import { PatientLayoutModule } from '../layouts/patient-layout/patient-layout.module';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PatientRoutingModule,
    PatientLayoutModule,
    NgbModalModule,
  ]
})
export class PatientModule { }
