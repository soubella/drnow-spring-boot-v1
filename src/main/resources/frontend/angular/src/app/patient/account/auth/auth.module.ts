import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { AuthFooterComponent } from './auth-footer/auth-footer.component';
import { NormalSignupComponent } from './normal-signup/normal-signup.component';
import { UIModule } from '../../../shared/ui/ui.module';
import { ArchwizardModule } from 'angular-archwizard';
import { NgxMaskModule } from 'ngx-mask';
import { AppointmentSignupComponent } from './appointment-signup/appointment-signup.component';

@NgModule({
  declarations: [LoginComponent, AuthFooterComponent, NormalSignupComponent, AppointmentSignupComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    NgbAlertModule,
    ReactiveFormsModule,
    TranslateModule,
    UIModule,
    ArchwizardModule,
    NgxMaskModule.forRoot()
  ]
})
export class AuthModule { }
