import { Component, OnInit } from '@angular/core';
import { AppointmentService } from '../../../core/services/appointment/appointment.service';
import { Router } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr);

@Component({
  selector: 'app-my-consultations',
  templateUrl: './my-consultations.component.html',
  styleUrls: ['./my-consultations.component.scss']
})
export class MyConsultationsComponent implements OnInit {

  disabledStatus = ['ENDED', 'CANCELED_BY_PATIENT', 'CANCELED_BY_DOCTOR']

  constructor(private appointmentService: AppointmentService, private router: Router) { }

  upComingAppointment = [];
  historyAppointment = [];

  ngOnInit() {
    this.loadAppointments();
  }

  loadAppointments(){
    this.appointmentService.getPatientAppointment().subscribe(
      data => {
          this.historyAppointment = data.filter(elem => elem.startAt.getTime() < new Date().getTime());
          this.upComingAppointment = data.filter(elem => elem.startAt.getTime() >= new Date().getTime());
      });
  }

  getAppointmentColor(status: string): string {
    let color;
    if (status === 'NOT_STARTED_YET' || status === 'ENDED') {
      color = 'badge bg-soft-success text-success';
    } else if (status === 'CANCELED_BY_PATIENT' || status === 'CANCELED_BY_DOCTOR') {
      color = 'badge bg-soft-danger text-danger';
    }
    return color;
  }

  goToDoctorProfile(pk: any) {
    this.router.navigate(['patient/dashboard/praticien',
      { doctor: pk }]
    );
  }

  joinAppointment(appointment: any) {
    this.router.navigateByUrl('/patient/dashboard/video-call;room='+ appointment.sessionToken, { state: appointment });

    // check if is time to start appointment

    // this.router.navigate(['/patient/dashboard/video-call',
    //   { room: appointment.sessionToken }]
    // );
  }
}
