export default class DateUtils {

  static getDiffInDays(date2: Date, date1: Date) {
    let diff = Math.abs(date2.getTime() - new Date(date1).getTime());
    return Math.ceil(diff / (1000 * 3600 * 24));
  }

  static addDays(date: Date, days: number): Date {
    let result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }

  static dateRangeOverlaps(a_start, a_end, b_start, b_end): boolean {
    if (a_start <= b_start && b_start <= a_end) {
      return true;
    } // b starts in a
    if (a_start <= b_end && b_end <= a_end) {
      return true;
    } // b ends in a
    if (b_start < a_start && a_end < b_end) {
      return true;
    } // a in b
    return false;
  }

  static dateRangeAinDateRangeB(a_start, a_end, b_start, b_end): boolean {
    return b_start <= a_start && a_end <= b_end; // a in b
  }

  static timeRangeAinTimeRangeB(a_start, a_end, b_start, b_end): boolean {
    return b_start <= a_start && a_end <= b_end; // a in b
  }

  static timeRangeOverlaps(a_start: Date, a_end: Date, b_start: Date, b_end: Date): boolean {
    return (a_end.getHours() * 60 + a_end.getMinutes() > b_start.getHours() * 60 + b_start.getMinutes()) &&
      (a_start.getHours() * 60 + a_start.getMinutes() < b_end.getHours() * 60 + b_end.getMinutes());
  }

  static timeRangeOverlapsMinutes(a_start, a_end, b_start, b_end): boolean {
    return a_end > b_start && a_start < b_end;
  }

  static convertToLocalDateFromUTC(date: Date): Date {
    return new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
  }

}
