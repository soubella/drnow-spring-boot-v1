import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import UserUtils from '../../utils/user.utils';

@Injectable({
  providedIn: 'root'
})
export class AgendaService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  addAgenda(agendaForm) {
    agendaForm.doctorPk = UserUtils.getCurrentUserPk();
    // let tempDate = new Date();
    // tempDate.setHours(parseInt(agendaForm.fromTime.split(':')[0]) + new Date().getTimezoneOffset() / 60);
    // agendaForm.fromTime = tempDate.getHours() + ':' + agendaForm.fromTime.split(':')[1];
    // tempDate.setHours(parseInt(agendaForm.toTime.split(':')[0]) + new Date().getTimezoneOffset() / 60);
    // agendaForm.toTime = tempDate.getHours() + ':' + agendaForm.toTime.split(':')[1];
    return this.http.post<any>(this.baseUrl + '/working-agendas/save', agendaForm, { observe: 'response' });
  }

  getAgendaByAvailability(availability): Observable<any> {
    return this.http.get<any>(this.baseUrl + '/doctor/' + UserUtils.getCurrentUserPk() + '/working-agendas/' + availability, { observe: 'response' });
  }

  getAppointments(filter) {
    return this.http.get<any>(this.baseUrl + '/appointment/doctor/' + UserUtils.getCurrentUserPk() + '/' + filter, { observe: 'response' });
  }

  getAppointmentDuration() {
    return this.http.get<any>(this.baseUrl + '/doctor/' + UserUtils.getCurrentUserPk() + '/appointment-duration', { observe: 'response' });
  }

  updateAppointmentDuration(durationForm) {
    durationForm.doctorPk = UserUtils.getCurrentUserPk();
    return this.http.post<any>(this.baseUrl + '/doctor/appointment-duration/save', durationForm, { observe: 'response' });
  }

  takeAppointmentForPatient(appointmentForm) {
    appointmentForm.doctorPk = UserUtils.getCurrentUserPk();
    return this.http.post<any>(this.baseUrl + '/appointment/add-for-patient', appointmentForm, { observe: 'response' });
  }

  updateAppointmentStartDate(appointmentForm, appointmentPk) {
    appointmentForm.doctorPk = UserUtils.getCurrentUserPk();
    return this.http.put<any>(this.baseUrl + '/appointment/' + appointmentPk + '/update', appointmentForm, { observe: 'response' });
  }

  cancelAppointment(cancelAppointmentForm, appointmentPk) {
    return this.http.put<any>(this.baseUrl + '/appointment/' + appointmentPk + '/cancel', cancelAppointmentForm, { observe: 'response' });
  }

  deleteWorkingAgendaByPk(pk) {
    return this.http.delete<any>(this.baseUrl + '/working-agenda/' + pk + '/delete', { observe: 'response' });
  }

  checkAvailability(doctorPk, date: Date) {
    const datetime = date.toISOString().slice(0, 19);
    return this.http.get<any>(this.baseUrl + '/appointment/doctor/' + doctorPk + '/availability/' + datetime + '/check', { observe: 'response' });
  }

}
