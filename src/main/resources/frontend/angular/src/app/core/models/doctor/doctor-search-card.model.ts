import { Speciality } from './speciality.model';
import { WorkingAgenda } from './working-agenda.model';
import { Profession } from './profession.model';
import { Language } from '../language.model';
import { Address } from '../address.model';

export interface IDoctorSearchCard {
  pk: string;
  firstName: string;
  lastName: string;
  bio: string;
  gender: string;
  teleconsultationPrice: number;
  appointmentDuration: number;
  repayable: string;
  speciality: Speciality;
  profession : Profession;
  workingAgendas: WorkingAgenda[];
  languages: Language[];
  address: Address;
  unAvailableTimes: any[];
  agreementStatus: string;
  image: string;
}
