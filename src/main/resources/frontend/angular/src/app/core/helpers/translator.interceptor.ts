import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AuthenticationService } from '../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class TranslatorInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService,
              private translateService: TranslateService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(tap(evt => {

      if (evt instanceof HttpResponse) {
        if (evt.body && evt.body.message) {
          this.translateService.get(evt.body.message).subscribe((text: string) => evt.body.message = text);
        }
      }

    }));
  }
}
