export class Address {
  pk: string;
  postalCode: string;
  city: string;
}
