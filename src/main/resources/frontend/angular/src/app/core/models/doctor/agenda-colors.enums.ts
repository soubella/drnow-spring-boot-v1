export enum AgendaColors {
  AvailablePeriod = '#1abc9c',
  UnAvailablePeriod = '#2b2f30',
  Appointment_Not_Started_Yet = '#5eba7d',
}
