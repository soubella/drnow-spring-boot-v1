export class Language {
  pk: string;
  code: string;
  name: string;
}
