import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthenticationService } from '../services/auth.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.authenticationService.currentUser();
    if (currentUser) {
      // check if route is restricted by role
      if (route.data.roles && route.data.roles.indexOf(currentUser.role) === -1) {
        // role not authorised so redirect to home page
        this.router.navigate(['/']);
        return false;
      }

      // authorised so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    if (state.url.indexOf('admin') > -1) {
      this.router.navigate(['/admin/account/auth/login'], { queryParams: { returnUrl: state.url } });
    } else if (state.url.indexOf('doctor') > -1) {
      this.router.navigate(['/doctor/account/login'], { queryParams: { returnUrl: state.url } });
    } else if (state.url.indexOf('patient') > -1) {
      this.router.navigate(['/patient/account/auth/login'], { queryParams: { returnUrl: state.url } });
    }else {
      this.router.navigate(['/'], { queryParams: { returnUrl: state.url } });
    }

    return false;
  }

}
