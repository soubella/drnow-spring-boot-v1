export class WorkingAgenda {
  pk: string;
  fromDate: Date;
  toDate: Date;
  fromTime: string;
  toTime: string;
  availability: boolean;
  doctorPk: string;
}
