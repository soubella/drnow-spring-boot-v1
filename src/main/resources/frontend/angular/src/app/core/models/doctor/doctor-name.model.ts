export class DoctorName {
  pk: string;
  fullName: string;
  autoComplete: string = this.fullName;
  groupedBy: string = 'Doctor';
}
