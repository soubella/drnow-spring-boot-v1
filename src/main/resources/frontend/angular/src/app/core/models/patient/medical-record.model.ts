export class MedicalRecord {
  patientPk: string;
  height: number;
  weight: number;
  treatingDoctorFirstName: string;
  treatingDoctorLastName: string;
  treatingDoctorPhone: string;
  treatingDoctorEmail: string;
  cvDiseases: string; // Avez-vous une ou plusieurs maladies cardiovasculaires (infarctus, AVC) ?
  // Avez-vous des antécédents cardiovasculaires familiaux (infarctus, AVC) ?
  cvDiseasesFamily: string;
  cvDiseasesRisk: string; // Avez-vous un ou plusieurs facteur(s) de risque personnel(s) cardio-vasculaire(s) parmi les suivants ?
  // Êtes-vous asthmatique ?
  asthma: string;
  stress: string; // Vous sentez-vous stressé(e) ?
  moral: string; // Votre moral est-il bon ?
  otherPathology: string; // Informez-nous de toute autre pathologie dont vous êtes, ou avez été porteur.
  surgical: string; // Antécédents chirurgicaux : Veuillez mentionner vos éventuelles interventions chirurgicales
  longTermTreatment: string; // Traitement de longue durée : Si oui, laquelle ou lesquelles ?
  drugAllergies: string; // Allergies et intolérances : Si oui, laquelle ou lesquelles ?
  vaccineStatus: string; // Statut vaccinal (BCG;Coronavirus;...)
  insuranceProvider: string; // Organisme assureur ou mutuelle

  constructor() {
  }
}
