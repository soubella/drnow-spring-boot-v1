import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import UserUtils from '../../utils/user.utils';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getPatientAppointment(): Observable<any> {
    return this.http.get(this.baseUrl + '/appointment/patient/' + UserUtils.getCurrentUserPk());
  }

}
