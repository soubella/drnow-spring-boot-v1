export class Patient {
  pk: string;
  firstName: string;
  lastName: string;
  email: string;
}
