import { Role } from './role.enums';

export class User {
    pk: string;
    jwt?: string;
    email: string;
    role: Role
}
