export default class UserUtils {
  static getCurrentUserPk() {
    if(JSON.parse(localStorage.getItem('currentUser')))
      return JSON.parse(localStorage.getItem('currentUser')).pk;
    return null;
  }

  static getCurrentUserEmail() {
    if(JSON.parse(localStorage.getItem('currentUser')))
      return JSON.parse(localStorage.getItem('currentUser')).email;
    return null;
  }
}
