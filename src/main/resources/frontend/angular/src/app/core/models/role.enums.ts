export enum Role {
  Admin = 'ROLE_ADMIN',
  Doctor = 'ROLE_DOCTOR',
  Patient = 'ROLE_PATIENT',
}
