import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import UserUtils from '../../utils/user.utils';

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  register(registerForm) {
    return this.http.post<any>(this.baseUrl + '/patient/auth/register', registerForm, { observe: 'response' });
  }

  addBeneficiary(beneficiaryForm) {
    beneficiaryForm.email = UserUtils.getCurrentUserEmail();
    return this.http.post<any>(this.baseUrl + '/patient/auth/beneficiary/add', beneficiaryForm, { observe: 'response' });
  }

  addMedicalRecord(medicalRecordForm) {
    return this.http.post<any>(this.baseUrl + '/patient/medical-record/add', medicalRecordForm, { observe: 'response' });
  }

  addSecurityNumberToPatient(socialNumberForm) {
    return this.http.post<any>(this.baseUrl + '/user/auth/security-number/add', socialNumberForm, { observe: 'response' });
  }

  makeReservation(reservationForm) {
    return this.http.post<any>(this.baseUrl + '/appointment/add', reservationForm, { observe: 'response' });
  }

  findPatientsByDoctorPk() {
    return this.http.get<any>(this.baseUrl + '/doctor/' + UserUtils.getCurrentUserPk() + '/patients', { observe: 'response' });
  }

  findPatientByPatientPk() {
    return this.http.get<any>(this.baseUrl + '/patient/' + UserUtils.getCurrentUserPk(), { observe: 'response' });
  }

  findPatientByPatientPkParam(pk) {
    return this.http.get<any>(this.baseUrl + '/patient/' + pk, { observe: 'response' });
  }

  updatePatientProfile(profileForm, pk) {
    return this.http.patch<any>(this.baseUrl + '/patient/' + pk + '/update', profileForm, { observe: 'response' });
  }

}
