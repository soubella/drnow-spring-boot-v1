import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { CookieService } from './cookie.service';
import { User } from '../models/auth.model';
import { environment } from '../../../environments/environment';
import UserUtils from '../utils/user.utils';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  user: User;
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient, private cookieService: CookieService) {
  }

  /**
   * Returns the current user
   */
  public currentUser(): User {
    if (!this.user) {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
    }
    return this.user;
  }

  /**
   * Performs the auth
   * @param email email of user
   * @param password password of user
   * @param actor
   */
  login(email: string, password: string, actor: string) {
    return this.http.post<any>(`${this.baseUrl}/${actor}/auth/login`, { email, password })
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.jwt) {
          this.user = user;
          // store user details and jwt in localstorage
          localStorage.setItem('currentUser', JSON.stringify(user));
          // store user details and jwt in cookie
          // this.cookieService.setCookie('currentUser', JSON.stringify(user), 1);
        }
        return user;
      }));
  }

  /**
   * Logout the user
   */
  logout() {
    this.user = null;
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    // remove user from local storage to log user out
    this.cookieService.deleteCookie('currentUser');
  }

  requestResetPasswordVerificationCode(email) {
    return this.http.post<any>(this.baseUrl + '/user/auth/account/password/recovery', email, { observe: 'response' });
  }

  /**
   * Send verification code
   * @param email of user
   * @param reason of the code
   */
  sendVerificationCode(email, reason) {
    const request = { email: email, reason: reason };
    return this.http.post<any>(this.baseUrl + '/user/auth/reset-password', request, { observe: 'response' });
  }

  /**
   * check verification code
   * @param email of user
   * @param confirmationCode
   * @param reason of the code
   */
  checkVerificationCode(email, confirmationCode, reason) {
    const request = { email: email, confirmationCode: confirmationCode, tokenType: reason };
    return this.http.post<any>(this.baseUrl + '/user/auth/account/confirmation-code/check', request, { observe: 'response' });
  }


  changeForgottenPassword(email, confirmationCode, newPassword) {
    const request = { email: email, confirmationCode: confirmationCode, newPassword: newPassword};
    return this.http.post<any>(this.baseUrl + '/user/auth/account/password/reset', request, { observe: 'response' });
  }

  checkAccountByEmail(email) {
    return this.http.post<any>(this.baseUrl + '/user/auth/check', email, { observe: 'response' });
  }

  changePassword(changePasswordForm){
    changePasswordForm.email = UserUtils.getCurrentUserEmail();
    return this.http.post<any>(this.baseUrl + '/user/auth/account/password/change', changePasswordForm, { observe: 'response' });
  }

}

