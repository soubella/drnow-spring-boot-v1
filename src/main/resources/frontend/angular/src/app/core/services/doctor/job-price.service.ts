import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JobPriceService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getAllJobPrices(): Observable<any> {
    return this.http.get<any>(this.baseUrl + '/doctor/' + this.getCurrentDoctorPk() + '/jobs', { observe: 'response' });
  }

  saveOrUpdateJobPrice(jobPriceForm) {
    return this.http.post<any>(this.baseUrl + '/doctor/jobs/save', jobPriceForm, { observe: 'response' });
  }

  deleteJobPrice(servicePk) {
    return this.http.delete<any>(this.baseUrl + '/doctor/jobs/' + servicePk + '/delete', { observe: 'response' });
  }

  getCurrentDoctorPk() {
    return JSON.parse(localStorage.getItem('currentUser')).pk;
  }

}
