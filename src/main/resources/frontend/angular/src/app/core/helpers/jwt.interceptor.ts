import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthenticationService } from '../services/auth.service';
import { environment } from '../../../environments/environment';
import DateUtils from '../utils/date.utils';
import { map } from 'rxjs/operators';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  private apiVersion = environment.apiVersion;
  private _isoDateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/;

  constructor(private authenticationService: AuthenticationService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    const currentUser = this.authenticationService.currentUser();
    if (currentUser && currentUser.jwt) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${currentUser.jwt}`
        }
      });
    }
    request = request.clone({
      setHeaders: {
        'DOCTORYL-API-VERSION': this.apiVersion,
        // 'Content-Type': 'application/json'
      }
    });

    // convert all dates to local timezone
    return next.handle(request).pipe(map( (val: HttpEvent<any>) => {
      if (val instanceof HttpResponse){
        const body = val.body;
        this.convertToLocalDate(body);
      }
      return val;
    }));
  }

  isIsoDateObject(value: any): boolean {
    if (value === null || value === undefined) {
      return false;
    }
    if (typeof value === 'string'){
      return this._isoDateFormat.test(value);
    }
    return false;
  }

  convertToLocalDate(body: any) {
    if (body === null || body === undefined) {
      return body;
    }
    if (typeof body !== 'object') {
      return body;
    }
    for (const key of Object.keys(body)) {
      const value = body[key];
      if (this.isIsoDateObject(value)) {
        body[key] = DateUtils.convertToLocalDateFromUTC(new Date(value));
      } else if (typeof value === 'object') {
        this.convertToLocalDate(value);
      }
    }
  }

}
