import { Address } from '../address.model';
import { Profession } from './profession.model';
import { Speciality } from './speciality.model';

export class Doctor {
  pk: string;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  qualificationCode: string;
  inami: string;
  bio: string;
  teleconsultationPrice:number;
  repayable:string;
  address: Address;
  profession: Profession;
  speciality: Speciality;
  agreementStatus: string;
}
