import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PopulatorService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getProfessionsList(): Observable<any> {
    return this.http.get(this.baseUrl + '/professions');
  }

  getSpecialitiesList(): Observable<any> {
    return this.http.get(this.baseUrl + '/specialities');
  }

  getLanguagesList(): Observable<any> {
    return this.http.get(this.baseUrl + '/languages');
  }

  findDoctorsByFullName(keyword): Observable<any> {
    return this.http.get(this.baseUrl + '/doctor/name/' + keyword);
  }
}
