import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OpenViduService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getOpenViduToken(appointmentToken): Observable<any> {
    return this.http.post(this.baseUrl + '/api-sessions/get-token', {token: appointmentToken});
  }

}
