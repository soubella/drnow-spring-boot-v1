import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import UserUtils from '../../utils/user.utils';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  register(registerForm) {
    return this.http.post<any>(this.baseUrl + '/doctor/auth/register', registerForm, { observe: 'response' });
  }

  confirmEmail(confirmationCode, email, tokenType) {
    return this.http.post<any>(this.baseUrl + '/doctor/auth/account/confirm', {
      email,
      confirmationCode,
      tokenType
    }, { observe: 'response' });
  }

  getProfile(doctorPk) {
    return this.http.get<any>(this.baseUrl + '/doctor/' + doctorPk, { observe: 'response' });
  }

  getProfileByPk(doctorPk) {
    return this.http.get<any>(this.baseUrl + '/doctor/' + doctorPk, { observe: 'response' });
  }

  getDoctorProfileByPk(doctorPk) {
    return this.http.get<any>(this.baseUrl + '/doctor/' + doctorPk + '/profile', { observe: 'response' });
  }

  updateProfile(profileForm) {
    return this.http.patch<any>(this.baseUrl + '/doctor/' + UserUtils.getCurrentUserPk() + '/update', profileForm, { observe: 'response' });
  }

  uploadProfileImage(uploadImageData) {
    return this.http.post<any>(this.baseUrl + '/doctor/' + UserUtils.getCurrentUserPk() + '/image/upload', uploadImageData, { observe: 'response' });
  }

  getProfileImage(pk) {
    return this.http.get<any>(this.baseUrl + '/doctor/' + pk + '/image', { observe: 'response' });
  }

  getDoctorImageByPk(doctorPk) {
    return this.http.get<any>(this.baseUrl + '/doctor/' + doctorPk + '/image', { observe: 'response' });
  }

  searchForDoctors(keyword, page, size) {
    return this.http.get<any>(this.baseUrl + '/doctors/search/' + keyword + '/' + page + '/' + size, { observe: 'response' });
  }

  loadAllDoctors(page, size) {
    return this.http.get<any>(this.baseUrl + '/doctors/all/' + page + '/' + size, { observe: 'response' });
  }

}
