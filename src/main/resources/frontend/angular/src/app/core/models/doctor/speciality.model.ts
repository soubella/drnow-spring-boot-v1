import { Profession } from './profession.model';

export class Speciality {
  pk: string;
  name: string;
  rank: number;
  autoComplete: string;
  professionModel: Profession;
  groupedBy: string = 'Speciality';
}
