import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoctorAgendaComponent } from './doctor-agenda/doctor-agenda.component';
import { MyConsultationsComponent } from './my-consultations/my-consultations.component';
import { MyPatientsComponent } from './my-patients/my-patients.component';
import { PatientInfoComponent } from './patient-info/patient-info.component';
import {OpenviduComponent} from "./openvidu/openvidu.component";


const routes: Routes = [
  {
    path: 'agenda',
    component: DoctorAgendaComponent
  },
  {
    path: 'consultations',
    component: MyConsultationsComponent
  },
  {
    path: 'patients',
    component: MyPatientsComponent
  },
  {
    path: 'patient',
    component: PatientInfoComponent
  },  {
    path: 'video-call',
    component: OpenviduComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
