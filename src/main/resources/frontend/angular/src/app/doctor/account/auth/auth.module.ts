import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { UIModule } from '../../../shared/ui/ui.module';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { AuthRoutingModule } from './auth-routing';
import { ConfirmComponent } from './confirm/confirm.component';
import { PasswordresetComponent } from './passwordreset/passwordreset.component';
import { TranslateModule } from '@ngx-translate/core';
import { AuthFooterComponent } from './auth-footer/auth-footer.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ConfirmresetpasswordComponent } from './confirmresetpassword/confirmresetpassword.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { AccountActivationComponent } from './account-activation/account-activation.component';
import {NgxMaskModule} from "ngx-mask";

@NgModule({
  declarations: [LoginComponent, SignupComponent, ConfirmComponent, PasswordresetComponent, AuthFooterComponent, ConfirmresetpasswordComponent, ChangepasswordComponent, AccountActivationComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgbAlertModule,
        UIModule,
        AuthRoutingModule,
        TranslateModule,
        NgSelectModule,
        NgxMaskModule.forRoot()
    ]
})
export class AuthModule { }
