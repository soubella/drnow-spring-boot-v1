import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { AuthModule } from './auth/auth.module';
import { DoctorLayoutsModule } from '../../layouts/doctor-layout/doctor-layouts.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AccountRoutingModule,
    AuthModule,
    DoctorLayoutsModule
  ]
})
export class AccountModule { }
