import { Component, OnInit } from '@angular/core';
import { PatientService } from '../../../core/services/patient/patient.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-patients',
  templateUrl: './my-patients.component.html',
  styleUrls: ['./my-patients.component.scss'],
})
export class MyPatientsComponent implements OnInit {
  patients$: any = [];
  patientsResult$: any = [];
  searchTerm: any;
  total: any;
  selectedStatus: any;

  constructor(private patientService: PatientService, private router:Router) {

  }

  ngOnInit() {
    this.loadAllPatients();
  }

  loadAllPatients(){
    this.patientService.findPatientsByDoctorPk().subscribe(
      value => {
        this.patients$ = value.body;
        this.patientsResult$ = value.body;
        this.total = this.patientsResult$.length;
      });
  }

  search(value) {
    if(!value || value.length <= 1){
      this.patientsResult$ = this.patients$;
      this.total = this.patients$.length;
    }else {
      this.patientsResult$ = this.patients$.filter(elem =>
        elem.firstName.toLowerCase().includes(value) ||
        elem.lastName.toLowerCase().includes(value) ||
        elem.email.toLowerCase().includes(value) ||
        elem.phoneNumber.toLowerCase().includes(value)
      );
      this.total = this.patientsResult$.length;
    }
  }

  onStatusChange(event: Event) {
    if(this.selectedStatus == 'firstName')
      this.patientsResult$.sort((a,b) => a.firstName.localeCompare(b.firstName));
    if(this.selectedStatus == 'lastName')
      this.patientsResult$.sort((a,b) => a.lastName.localeCompare(b.lastName));
    if(this.selectedStatus == 'email')
      this.patientsResult$.sort((a,b) => a.email.localeCompare(b.email));
    if(this.selectedStatus == 'phoneNumber')
      this.patientsResult$.sort((a,b) => a.phoneNumber.localeCompare(b.phoneNumber));
  }

  goToMedicalRecord(patient: any) {
    this.router.navigateByUrl('/doctor/dashboard/patient', { state: patient });
  }
}
