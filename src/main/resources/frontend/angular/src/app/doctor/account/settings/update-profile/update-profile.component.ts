import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Language } from '../../../../core/models/language.model';
import { PopulatorService } from '../../../../core/services/populator.service';
import { DoctorService } from '../../../../core/services/doctor/doctor.service';
import { Doctor } from '../../../../core/models/doctor/doctor.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FileUploadValidators } from '@iplab/ngx-file-upload';
import { NgSelectComponent } from '@ng-select/ng-select';
import { JobPriceService } from '../../../../core/services/doctor/job-price.service';
import UserUtils from '../../../../core/utils/user.utils';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.scss']
})
export class UpdateProfileComponent implements OnInit {

  // Forms
  profileForm: FormGroup;
  imageForm: FormGroup;
  jobPriceForm: FormGroup;
  filesControl = new FormControl(null, FileUploadValidators.filesLimit(1));
  // UI
  submitted = false;
  submitted2 = false;
  error = '';
  loading = false;
  breadCrumbItems: Array<{}>; // bread crumb items
  animation: boolean = false;
  multiple: boolean = false;
  jobPriceModalTitle:string='Add new service';
  @ViewChild(NgSelectComponent, { static: false })
  ngSelect: NgSelectComponent;
  // Data
  languagesData: Language[];
  jobPricesData: any[];
  doctorData: Doctor;
  profilePic: string = 'assets/images/doctor/no-pic.png';

  constructor(private formBuilder: FormBuilder, private populatorService: PopulatorService, private jobPriceService: JobPriceService,
              private doctorService: DoctorService, private toastr: ToastrService, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.breadCrumbItems = [{ label: 'Doctoryl', path: '/' }, { label: 'Settings', path: '/' }, {
      label: 'Profile',
      path: '/',
      active: true
    }];

    // init profile form
    this.profileForm = this.formBuilder.group({
      // Personal Info
      pk: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      city: ['', Validators.required],
      postalCode: ['', Validators.required],
      gender: ['', Validators.required],
      bio: ['', Validators.required],
      // Professional Info
      languages: ['', Validators.required],
      agreementStatus: ['', Validators.required],
      teleconsultationPrice: ['', Validators.required]
    });
    // init job price form
    this.jobPriceForm = this.formBuilder.group({
      pk: [''],
      name: ['', Validators.required],
      description: ['', Validators.required],
      price: ['', Validators.required],
      doctorPk: ['']
    });
    // init image upload form
    this.imageForm = new FormGroup({
      files: this.filesControl
    });

    this._fetchData();
  }

  // profile form getter
  get f() {
    return this.profileForm.controls;
  }

  private _fetchData() {
    // load languages combobox
    this.populatorService.getLanguagesList().subscribe(data => {
      this.languagesData = data;
    });
    // load profile info
    this.doctorService.getProfile(UserUtils.getCurrentUserPk()).subscribe(data => {
      this._setDataToForm(data.body);
    }, error => {
      console.log(error);
    });
    // download profile image
    this.downloadProfileImage();
    // load Job Prices
    this.loadAllJobPrice();
  }

  private _setDataToForm(doctor) {
    // Doctor data
    this.doctorData = doctor;
    // Personal Info
    this.profileForm.controls['pk'].setValue(doctor.pk);
    this.profileForm.controls['firstName'].setValue(doctor.firstName);
    this.profileForm.controls['lastName'].setValue(doctor.lastName);
    this.profileForm.controls['phoneNumber'].setValue(doctor.phoneNumber);
    this.profileForm.controls['city'].setValue(doctor.address.city);
    this.profileForm.controls['postalCode'].setValue(doctor.address.postalCode);
    if (doctor.gender != undefined) {
      this.profileForm.controls['gender'].setValue(doctor.gender);
    }
    this.profileForm.controls['bio'].setValue(doctor.bio);
    // Professional Info
    if (doctor.languages != undefined) {
      doctor.languages.forEach((language => {
        this.ngSelect.select(this.ngSelect.itemsList.findByLabel(language.name));
      }));
    }
    if (doctor.agreementStatus != undefined) {
      this.profileForm.controls['agreementStatus'].setValue(doctor.agreementStatus);
    }
    if (doctor.teleconsultationPrice != undefined) {
      this.profileForm.controls['teleconsultationPrice'].setValue(doctor.teleconsultationPrice);
    }
  }

  updateProfile() {
    this.submitted=true;
    // stop here if form is invalid
    if (this.profileForm.invalid) {
      return;
    }
    this.doctorService.updateProfile(this.profileForm.value)
      .subscribe(response => {
        this.toastr.success(response.body.message, '', { timeOut: 6000, closeButton: true });
      }, error => {
        console.log(error);
      });
  }

  /** Image Upload **/
  openEditImageModal(content: any) {
    this.modalService.open(content, { centered: true });
  }

  onUpload() {
    if (this.imageForm.invalid) {
      return;
    }

    if(!this.checkExtension(this.imageForm.value.files[0])){
      this.toastr.warning('Only image format accepted !', '', { timeOut: 6000, closeButton: true });
    }

    const uploadImageData = new FormData();
    uploadImageData.append('file', this.imageForm.value.files[0]);
    this.doctorService.uploadProfileImage(uploadImageData)
      .subscribe(response => {
        if (response.status == 200) {
          this.modalService.dismissAll();
          this.downloadProfileImage(); // refresh image
          this.filesControl.setValue([]); // clear input
          this.toastr.success(response.body.message, '', { timeOut: 6000, closeButton: true });
        } else {
          console.log('Image not uploaded successfully');
        }
      });
  }

  private checkExtension(file: any) {
    let valToLower = file.name.toLowerCase();
    let regex = new RegExp("(.*?)\.(jpg|png|jpeg)$"); //add or remove required extensions here
    return regex.test(valToLower);
  }

  downloadProfileImage() {
    this.doctorService.getProfileImage(UserUtils.getCurrentUserPk()).subscribe(response => {
      if (response.status == 200 && response.body.imageBytes) {
        this.profilePic = 'data:image/jpeg;base64,' + response.body.imageBytes;
      }
    }, error => {
      console.log(error);
    });
  }

  /** JobPrice CRUD **/
  openJobPriceModal(content: any) {
    this.modalService.open(content, { centered: true });
  }

  // service form getter
  get fs() {
    return this.jobPriceForm.controls;
  }

  saveJobPrice() {
    this.submitted2=true;
    // stop here if form is invalid
    if (this.jobPriceForm.invalid) {
      return;
    }
    this.jobPriceForm.controls['doctorPk'].setValue(this.jobPriceService.getCurrentDoctorPk());
    this.jobPriceService.saveOrUpdateJobPrice(this.jobPriceForm.value)
      .subscribe(response => {
        this.modalService.dismissAll();
        this.loadAllJobPrice();
        this.toastr.success(response.body.message, '', { timeOut: 6000, closeButton: true });
      }, error => {
        console.log(error);
      });
  }

  setJobDataToUpdateModal(job, content: any) {
    this.jobPriceModalTitle='Update Service';
    this.jobPriceForm.controls['doctorPk'].setValue(this.jobPriceService.getCurrentDoctorPk());
    this.jobPriceForm.controls['pk'].setValue(job.pk);
    this.jobPriceForm.controls['name'].setValue(job.name);
    this.jobPriceForm.controls['description'].setValue(job.description);
    this.jobPriceForm.controls['price'].setValue(job.price);
    this.openJobPriceModal(content);
  }

  deleteJob(jobPk) {
    this.jobPriceService.deleteJobPrice(jobPk).subscribe(response => {
      this.toastr.success(response.body.message, '', { timeOut: 6000, closeButton: true });
      this.loadAllJobPrice();
    });
  }

  loadAllJobPrice() {
    this.jobPriceService.getAllJobPrices().subscribe(data => {
      this.jobPricesData = data.body;
    });
  }
}
