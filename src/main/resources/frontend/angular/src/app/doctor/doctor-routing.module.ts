import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoctorLayoutComponent as DoctorLayoutComponent } from '../layouts/doctor-layout/doctor-layout.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { Role } from '../core/models/role.enums';


const routes: Routes = [
  {
    path: 'dashboard',
    component: DoctorLayoutComponent,
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate: [AuthGuard],
    data: { roles: [Role.Doctor] }
  },
  { path: 'account', loadChildren: () => import('./account/account.module').then(m => m.AccountModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorRoutingModule {
}
