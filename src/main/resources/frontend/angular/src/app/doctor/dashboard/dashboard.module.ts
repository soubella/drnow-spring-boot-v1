import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DashboardRoutingModule} from './dashboard-routing.module';
import {DoctorAgendaComponent} from './doctor-agenda/doctor-agenda.component';
import {TranslateModule} from '@ngx-translate/core';
import {FullCalendarModule} from '@fullcalendar/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  NgbDatepickerModule,
  NgbModalModule,
  NgbPaginationModule,
  NgbTimepickerModule,
  NgbTypeaheadModule
} from '@ng-bootstrap/ng-bootstrap';
import {NgbdDatepickerI18n} from '../../core/helpers/datepicker-i18n';
import {MyPatientsComponent} from './my-patients/my-patients.component';
import {MyConsultationsComponent} from './my-consultations/my-consultations.component';
import {PatientInfoComponent} from './patient-info/patient-info.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {OpenviduComponent} from './openvidu/openvidu.component';
import {UIModule} from "../../shared/ui/ui.module";
import {NgxMaskModule} from "ngx-mask";

@NgModule({
  declarations: [DoctorAgendaComponent, NgbdDatepickerI18n, MyPatientsComponent, MyConsultationsComponent, PatientInfoComponent, OpenviduComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    TranslateModule,
    FullCalendarModule,
    ReactiveFormsModule,
    NgbModalModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    NgbPaginationModule,
    NgbTypeaheadModule,
    FormsModule,
    UIModule,
    NgSelectModule,
    NgxMaskModule.forRoot()
  ]
})
export class DashboardModule {
}
