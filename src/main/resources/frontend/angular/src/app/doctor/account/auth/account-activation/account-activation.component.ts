import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DoctorService } from '../../../../core/services/doctor/doctor.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-account-activation',
  templateUrl: './account-activation.component.html',
  styleUrls: ['./account-activation.component.scss']
})
export class AccountActivationComponent implements OnInit {

  confirmationCodeForm: FormGroup;
  submitted = false;
  error = '';
  success = '';
  loading = false;
  email;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute,
              private router: Router,private toastr: ToastrService, private doctorService:DoctorService) { }

  ngOnInit() {
    if(this.route.snapshot.paramMap.has('email')){
      this.email=this.route.snapshot.paramMap.get('email');
    }else {
      this.router.navigate(['/doctor/account/login']);
    }

    this.confirmationCodeForm = this.formBuilder.group({
      confirmationCode: ['', [Validators.required]],
    });
  }

  ngAfterViewInit() {
    document.body.classList.add('authentication-bg');
    document.body.classList.add('authentication-bg-pattern');
  }

  // convenience getter for easy access to form fields
  get f() { return this.confirmationCodeForm.controls; }

  /**
   * On submit form
   */
  onSubmit() {
    this.success = '';
    this.submitted = true;

    // stop here if form is invalid
    if (this.confirmationCodeForm.invalid) {
      return;
    }

    this.loading = true;

    this.doctorService.confirmEmail(this.confirmationCodeForm.value.confirmationCode,this.email,'FOR_EMAIL_REGISTRATION').subscribe(
      (success) => {
        if(success.status==200){
          this.toastr.success(success.body.message,'',{ timeOut: 6000, closeButton:true });
          this.router.navigate(['/doctor/account/login']);
        }
      },
      error => {
        this.loading = false;
        this.toastr.warning(error,'',{ timeOut: 6000, closeButton:true });
      },
      () => {
        this.loading = false;
        // The POST observable is now completed.
        console.log("The POST observable is now completed.");
      }
    );
  }
}
