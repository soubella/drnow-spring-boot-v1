import {Component, OnInit, ViewChild} from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import bootstrapPlugin from '@fullcalendar/bootstrap';
import {EventInput} from '@fullcalendar/core/structs/event';
import frLocale from '@fullcalendar/core/locales/fr';
import deLocale from '@fullcalendar/core/locales/de';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbModal, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import {AgendaService} from '../../../core/services/doctor/agenda.service';
import {WorkingAgenda} from '../../../core/models/doctor/working-agenda.model';
import {ToastrService} from 'ngx-toastr';
import DateUtils from '../../../core/utils/date.utils';
import {AgendaColors} from '../../../core/models/doctor/agenda-colors.enums';
import Swal from 'sweetalert2';
import {FullCalendarComponent} from '@fullcalendar/angular';
import {PatientService} from '../../../core/services/patient/patient.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-doctor-agenda',
  templateUrl: './doctor-agenda.component.html',
  styleUrls: ['./doctor-agenda.component.scss']
})
export class DoctorAgendaComponent implements OnInit {

  // Forms
  availabilityForm: FormGroup;
  unAvailabilityForm: FormGroup;
  appointmentForm: FormGroup;
  appointmentDurationForm: FormGroup;
  cancelAppointmentForm: FormGroup;
  submitted: boolean;
  isEditAppointment: boolean;

  // appointment duration default value
  appointmentDuration = 15;

  // calendar plugin
  calendarPlugins = [dayGridPlugin, bootstrapPlugin, timeGridPlugin, interactionPlugin];
  calendarWeekends: any;

  // show events
  avCalendarEvents: EventInput[] = [];
  unAvCalendarEvents: EventInput[] = [];
  appointmentsEvents: EventInput[] = [];
  calendarEvents: EventInput[] = [];
  appointmentData = [];
  selectedAppointment;

  // data
  availableWorkingAgendas: WorkingAgenda[] = [];
  unAvailableWorkingAgendas: WorkingAgenda[] = [];

  // locales
  locales = [frLocale, deLocale];

  @ViewChild('fullcalendar', {static: false}) calendarComponent: FullCalendarComponent;

  constructor(public translateService: TranslateService, public modalService: NgbModal, private formBuilder: FormBuilder,
              private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private agendaService: AgendaService,
              private toastr: ToastrService, private router: Router) {
    this.fromDate = calendar.getToday();
    this.toDate = calendar.getToday();
    this.minDate = calendar.getToday();
    this.maxDate = calendar.getNext(calendar.getToday(), 'm', 6);
  }

  ngOnInit() {
    this.availabilityForm = this.formBuilder.group({
      fromTime: ['', Validators.required],
      toTime: ['', Validators.required],
      fromDate: [''],
      toDate: [''],
      availability: [true],
      doctorPk: ['']
    });

    this.unAvailabilityForm = this.formBuilder.group({
      fromTime: ['', Validators.required],
      toTime: ['', Validators.required],
      fromDate: [''],
      toDate: [''],
      availability: [false],
      doctorPk: ['']
    });

    this.appointmentForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      startAt: ['', Validators.required],
      startTime: ['', Validators.required],
      reason: ['', Validators.required],
      doctorPk: ['']
    });

    this.appointmentDurationForm = this.formBuilder.group({
      duration: ['', Validators.required],
      doctorPk: ['']
    });

    this.cancelAppointmentForm = this.formBuilder.group({
      cancellationReason: [''],
      appointmentStatusEnum: ['CANCELED_BY_DOCTOR']
    });

    this.refreshAgenda();
    this.loadAppointmentDuration();
  }

  ngAfterViewInit() {

    this.calendarComponent.scrollTime = '18:00';
  }

  openModal(content: any, $event: any) {
    this.modalService.open(content);
    this.appointmentDate = new NgbDate($event.date.getFullYear(), $event.date.getMonth() + 1, $event.date.getDate());
    let prefix1 = '';
    let prefix2 = '';
    if ($event.date.getHours() < 10) {
      prefix1 = '0';
    }
    if ($event.date.getMinutes() == 0) {
      prefix2 = '0';
    }
    this.appointmentForm.controls['startTime'].setValue(prefix1 + $event.date.getHours() + ':' + prefix2 + $event.date.getMinutes());
    console.log(this.appointmentForm.value.startTime);
  }

  showAppointmentDetails(editContent: any, event: EventInput) {
    if(event.event.rendering == 'background') return;
    this.isEditAppointment = true;
    this.selectedAppointment = this.appointmentData.find(appointment => appointment.pk == event.event.id);
    const hours = ('0' + this.selectedAppointment.startAt.getHours()).slice(-2);
    const minutes = ('0' + this.selectedAppointment.startAt.getMinutes()).slice(-2);
    const time = hours + ':' + minutes;
    this.appointmentForm.controls['startTime'].setValue(time);
    this.appointmentForm.controls['firstName'].setValue(this.selectedAppointment.beneficiary.firstName);
    this.appointmentForm.controls.firstName.disable();
    this.appointmentForm.controls['lastName'].setValue(this.selectedAppointment.beneficiary.lastName);
    this.appointmentForm.controls.lastName.disable();
    this.appointmentForm.controls['email'].setValue(this.selectedAppointment.beneficiary.email);
    this.appointmentForm.controls.email.disable();
    this.appointmentForm.controls['phoneNumber'].setValue(this.selectedAppointment.beneficiary.phoneNumber);
    this.appointmentForm.controls.phoneNumber.disable();
    this.appointmentForm.controls['reason'].setValue(this.selectedAppointment.reason);
    this.modalService.open(editContent, {centered: true});
    this.appointmentDate = new NgbDate(this.selectedAppointment.startAt.getFullYear(), this.selectedAppointment.startAt.getMonth() + 1, this.selectedAppointment.startAt.getDate());
  }

  openAddAppointmentModal(content: any) {
    this.modalService.open(content, {centered: true});
    this.isEditAppointment = false;
    this.appointmentForm.reset();
    this.appointmentDate = this.calendar.getToday();
  }

  loadAvailabilities() {
    this.calendarEvents = [];
    this.agendaService.getAgendaByAvailability(true).subscribe(
      data => {
        this.availableWorkingAgendas = data.body;
        this.drawBackground(data.body, AgendaColors.AvailablePeriod, true);
      });
  }

  loadUnAvailabilities() {
    this.calendarEvents = [];
    this.agendaService.getAgendaByAvailability(false).subscribe(
      data => {
        this.unAvailableWorkingAgendas = data.body;
        this.drawBackground(data.body, AgendaColors.UnAvailablePeriod, false);
      });
  }

  loadAppointmentDuration() {
    this.agendaService.getAppointmentDuration().subscribe(value => {
      this.appointmentDuration = parseInt(value.body);
      this.appointmentDurationForm.controls['duration'].setValue(this.appointmentDuration);
    });
  }

  drawBackground(workingAgendasParam, color, isAvailable) {
    let workingAgendas: WorkingAgenda[] = workingAgendasParam;
    this.avCalendarEvents = [];
    this.unAvCalendarEvents = [];
    this.appointmentsEvents = [];
    let randomId = 1;
    for (let elem of workingAgendas) {
      // let fromHours = parseInt(elem.fromTime.split(':')[0]);
      // let fromMinutes = parseInt(elem.fromTime.split(':')[1]);
      // let toHours = parseInt(elem.toTime.split(':')[0]);
      // let toMinutes = parseInt(elem.toTime.split(':')[1]);
      //
      // let fromDate = new Date(elem.fromDate);
      // fromDate.setHours(fromHours);
      // fromDate.setMinutes(fromMinutes);
      // let toDate = new Date(elem.fromDate);
      // toDate.setHours(toHours);
      // toDate.setMinutes(toMinutes);
      //
      let diffDays = DateUtils.getDiffInDays(elem.toDate, new Date(elem.fromDate));
      let j = 0;

      if (isAvailable && elem.toDate.getDate() == elem.fromDate.getDate()) {
        this.avCalendarEvents.push({
          id: randomId,
          start: elem.fromDate,
          end: elem.toDate,
          color: color,
          rendering: 'background',
          className: 'available'
        });
      } else if (elem.toDate.getDate() == elem.fromDate.getDate()) {
        this.unAvCalendarEvents.push({
          id: randomId,
          start: elem.fromDate,
          end: elem.toDate,
          color: color,
          className: 'unAvailable',
          rendering: 'background',
          title: 'Indisponible'
        });
      }

      while (diffDays != j && elem.toDate.getDate() != elem.fromDate.getDate()) {
        let calculatedFromDate = DateUtils.addDays(elem.fromDate, j);
        let calculatedToDate = DateUtils.addDays(elem.fromDate, j);
        calculatedFromDate.setHours(elem.fromDate.getHours());
        calculatedFromDate.setMinutes(elem.fromDate.getMinutes());
        calculatedToDate.setHours(elem.toDate.getHours());
        calculatedToDate.setMinutes(elem.toDate.getMinutes());
        if (isAvailable) {
          this.avCalendarEvents.push({
            id: randomId++,
            start: calculatedFromDate,
            end: calculatedToDate,
            color: color,
            rendering: 'background',
            className: 'available'
          });
        } else {
          this.unAvCalendarEvents.push({
            id: randomId++,
            start: calculatedFromDate,
            end: calculatedToDate,
            color: color,
            rendering: 'background',
            className: 'unAvailable',
            title: 'Indisponible'
          });
        }
        j++;
      }
      randomId++;
    }
    if (isAvailable) {
      this.calendarEvents = this.calendarEvents.concat(this.avCalendarEvents);
    } else {
      this.calendarEvents = this.calendarEvents.concat(this.unAvCalendarEvents);
    }
  }

  loadAppointments() {
    this.calendarEvents = [];
    this.agendaService.getAppointments(true).subscribe(
      data => {
        this.appointmentData = data.body;
        data.body.forEach(elem => {
          let startAt = new Date(elem.startAt);
          let endAt = new Date(startAt.getTime() + elem.duration * 60000);
          this.appointmentsEvents.push({
            id: elem.pk,
            start: startAt,
            end: endAt,
            color: AgendaColors.Appointment_Not_Started_Yet,
            className: 'agenda-event',
            title: elem.beneficiary.firstName + ' ' + elem.beneficiary.lastName
          });
        });
        this.calendarEvents = this.calendarEvents.concat(this.appointmentsEvents);
      });
  }

  updateAppointmentDuration() {
    this.submitted = true;

    if (this.appointmentDurationForm.invalid || this.appointmentDurationForm.value.duration == 0 || this.appointmentDurationForm.value.duration % 5 != 0) {
      return;
    }

    this.agendaService.updateAppointmentDuration(this.appointmentDurationForm.value).subscribe(
      success => {
        this.toastr.success('Votre durée pour un RDV a été modifiée avec succès', '', {
          timeOut: 6000,
          closeButton: true
        });
        this.modalService.dismissAll();
        this.loadAppointmentDuration();
        this.submitted = false;
      });
  }

  addAvailability() {
    this.submitted = true;

    if (this.availabilityForm.invalid) {
      return;
    }

    const startHours = parseInt(this.availabilityForm.value.fromTime.split(':')[0]);
    const startMinutes = parseInt(this.availabilityForm.value.fromTime.split(':')[1]);
    const endHours = parseInt(this.availabilityForm.value.toTime.split(':')[0]);
    const endMinutes = parseInt(this.availabilityForm.value.toTime.split(':')[1]);

    this.availabilityForm.controls['fromDate'].setValue(new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day, startHours, startMinutes));
    this.availabilityForm.controls['availability'].setValue(true);

    if (!this.toDate) {
      this.availabilityForm.controls['toDate'].setValue(new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day, endHours, endMinutes));
    } else {
      this.availabilityForm.controls['toDate'].setValue(new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day, endHours, endMinutes));
    }

    this.checkIfThereIsUnAvailableExactPeriod(this.availabilityForm.value);

    if (this.checkIfAvailablePeriodIsAlreadyExist(this.availabilityForm.value)) {
      this.toastr.warning('Cette période de disponibilité existe déjà', '', {timeOut: 6000, closeButton: true});
      return;
    }

    this.agendaService.addAgenda(this.availabilityForm.value).subscribe(
      success => {
        this.toastr.success('Votre disponibilité a été ajoutée avec succès', '', {timeOut: 6000, closeButton: true});
        this.modalService.dismissAll();
        this.refreshAgenda();
        this.availabilityForm.reset();
        this.submitted = false;
      });
  }

  addUnAvailability() {
    this.submitted = true;

    if (this.unAvailabilityForm.invalid) {
      return;
    }

    const startHours = parseInt(this.unAvailabilityForm.value.fromTime.split(':')[0]);
    const startMinutes = parseInt(this.unAvailabilityForm.value.fromTime.split(':')[1]);
    const endHours = parseInt(this.unAvailabilityForm.value.toTime.split(':')[0]);
    const endMinutes = parseInt(this.unAvailabilityForm.value.toTime.split(':')[1]);

    if (this.unAvailabilityForm.value.fromTime > this.unAvailabilityForm.value.toTime) {
      this.toastr.warning('L\'heure de début doit être inférieure à l\'heure de fin', '', {
        timeOut: 6000,
        closeButton: true
      });
      return;
    }

    this.unAvailabilityForm.controls['fromDate'].setValue(new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day, startHours, startMinutes));

    if (!this.toDate) {
      this.unAvailabilityForm.controls['toDate'].setValue(new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day, endHours, endMinutes));
    } else {
      this.unAvailabilityForm.controls['toDate'].setValue(new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day, endHours, endMinutes));
    }

    if (this.checkIfItsInsideAvailablePeriod(this.unAvailabilityForm.value) && !this.checkIfUnAvailablePeriodIsAlreadyExist(this.unAvailabilityForm.value)) {
      // okay you can cancel an available period
    } else {
      // no the unavailable period is already exist
      this.toastr.warning('Cette période de indisponibilité existe déjà', '', {timeOut: 6000, closeButton: true});
      return;
    }

    this.agendaService.addAgenda(this.unAvailabilityForm.value).subscribe(
      success => {
        this.toastr.success('Votre disponibilité a été annulée avec succès', '', {timeOut: 6000, closeButton: true});
        this.modalService.dismissAll();
        this.refreshAgenda();
        this.unAvailabilityForm.reset();
        this.submitted = false;
      });
  }

  addAppointment() {
    this.submitted = true;

    if (this.appointmentForm.invalid) {
      return;
    }

    this.appointmentForm.controls['startAt'].setValue(new Date(this.appointmentDate.year, this.appointmentDate.month - 1, this.appointmentDate.day, 0, 0, 0, 0));
    let hours = parseInt(this.appointmentForm.value.startTime.split(':')[0]);
    let minutes = parseInt(this.appointmentForm.value.startTime.split(':')[1]);
    let date = new Date(this.appointmentForm.value.startAt);
    date.setHours(hours);
    date.setMinutes(minutes);
    this.appointmentForm.controls['startAt'].setValue(date);

    this.agendaService.takeAppointmentForPatient(this.appointmentForm.value).subscribe(
      value => {
        this.toastr.success('Votre RDV a été ajoutée avec succès', '', {timeOut: 6000, closeButton: true});
        this.modalService.dismissAll();
        this.refreshAgenda();
        this.appointmentForm.reset();
        this.submitted = false;
      });
  }

  updateAppointmentStartDate() {
    this.submitted = true;

    if (!this.appointmentForm.value.startTime) {
      return;
    }

    this.appointmentForm.controls['startAt'].setValue(new Date(this.appointmentDate.year, this.appointmentDate.month - 1, this.appointmentDate.day));
    let hours = parseInt(this.appointmentForm.value.startTime.split(':')[0]);
    let minutes = parseInt(this.appointmentForm.value.startTime.split(':')[1]);
    this.appointmentForm.value.startAt.setHours(hours);
    this.appointmentForm.value.startAt.setMinutes(minutes);

    this.agendaService.updateAppointmentStartDate(this.appointmentForm.value, this.selectedAppointment.pk).subscribe(
      value => {
        this.toastr.success('Votre RDV a été modifiée avec succès', '', {timeOut: 6000, closeButton: true});
        this.modalService.dismissAll();
        this.refreshAgenda();
        this.appointmentForm.reset();
        this.submitted = false;
      });
  }

  editAppointment() {
    if (this.isEditAppointment) {
      this.updateAppointmentStartDate();
    } else {
      this.addAppointment();
    }
  }

  cancelAppointment() {
    let title, textModal, yes, no, successMessage, successTitle;
    this.translateService.get('Agenda.AreYouSureToCancel').subscribe((text: string) => title = text);
    this.translateService.get('Agenda.AreYouSureToCancelText').subscribe((text: string) => textModal = text);
    this.translateService.get('Action.Yes').subscribe((text: string) => yes = text);
    this.translateService.get('Action.No').subscribe((text: string) => no = text);
    this.translateService.get('Agenda.AppointmentHasBeenCanceled').subscribe((text: string) => successMessage = text);
    this.translateService.get('Agenda.Canceled').subscribe((text: string) => successTitle = text);
    Swal.fire({
      title: title,
      text: textModal,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: yes,
      cancelButtonText: no,
      confirmButtonClass: 'btn btn-success mt-2 btn-lg',
      cancelButtonClass: 'btn btn-danger ml-2 mt-2',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.agendaService.cancelAppointment(this.cancelAppointmentForm.value, this.selectedAppointment.pk).subscribe(
          value => {
            Swal.fire(successTitle, successMessage, 'success');
            this.refreshAgenda();
            this.modalService.dismissAll();
          });
      }
    });
  }

  // RG : availabilities
  checkIfAvailablePeriodIsAlreadyExist(workingAgendaParam: WorkingAgenda) {
    return this.availableWorkingAgendas.filter(
      workingAgenda => {
        let fromMinutes1 = workingAgendaParam.fromDate.getHours() * 60 + workingAgendaParam.fromDate.getMinutes();
        let toMinutes1 = workingAgendaParam.toDate.getHours() * 60 + workingAgendaParam.toDate.getMinutes();
        let fromMinutes2 = workingAgenda.fromDate.getHours() * 60 + workingAgenda.fromDate.getMinutes();
        let toMinutes2 = workingAgenda.toDate.getHours() * 60 + workingAgenda.toDate.getMinutes();

        let dateRangeOverlaps = DateUtils.dateRangeOverlaps(workingAgendaParam.fromDate, workingAgendaParam.toDate, new Date(workingAgenda.fromDate), new Date(workingAgenda.toDate));
        let timeRangeOverlapsMinutes = DateUtils.timeRangeOverlapsMinutes(fromMinutes1, toMinutes1, fromMinutes2, toMinutes2);

        return dateRangeOverlaps && timeRangeOverlapsMinutes;
      }
    ).length > 0;
  }

  checkIfThereIsUnAvailableExactPeriod(workingAgendaParam: WorkingAgenda) {
    let toDelete: WorkingAgenda[] = this.unAvailableWorkingAgendas.filter(workingAgenda => {
        let tempDate1 = new Date(workingAgenda.fromDate);
        tempDate1.setHours(0);
        tempDate1.setMinutes(0);
        let tempDate2 = new Date(workingAgenda.toDate);
        tempDate2.setHours(0);
        tempDate2.setMinutes(0);
        return workingAgenda.fromDate.getTime() == workingAgendaParam.fromDate.getTime() && workingAgenda.toDate.getTime() == workingAgendaParam.toDate.getTime();
      }
    );
    toDelete.forEach(elem => {
      this.agendaService.deleteWorkingAgendaByPk(elem.pk).subscribe(value => {
        console.log('unAvailable exact period has been deleted');
      });
    });
    if (toDelete.length > 0) {
      this.refreshAgenda();
    }
  }

  checkIfThereIsUnAvailableNotExactPeriod() {

  }

  // RG : unAvailabilities
  checkIfUnAvailablePeriodIsAlreadyExist(workingAgendaParam: WorkingAgenda) {
    return this.unAvailableWorkingAgendas.filter(
      workingAgenda => {
        let fromMinutes1 = workingAgendaParam.fromDate.getHours() * 60 + workingAgendaParam.fromDate.getMinutes();
        let toMinutes1 = workingAgendaParam.toDate.getHours() * 60 + workingAgendaParam.toDate.getMinutes();
        let fromMinutes2 = workingAgenda.fromDate.getHours() * 60 + workingAgenda.fromDate.getMinutes();
        let toMinutes2 = workingAgenda.toDate.getHours() * 60 + workingAgenda.toDate.getMinutes();

        let dateRangeOverlaps = DateUtils.dateRangeOverlaps(workingAgendaParam.fromDate, workingAgendaParam.toDate, new Date(workingAgenda.fromDate), new Date(workingAgenda.toDate));
        let timeRangeOverlapsMinutes = DateUtils.timeRangeOverlapsMinutes(fromMinutes1, toMinutes1, fromMinutes2, toMinutes2);

        return dateRangeOverlaps && timeRangeOverlapsMinutes;
      }
    ).length > 0;
  }

  checkIfItsInsideAvailablePeriod(workingAgendaParam: WorkingAgenda) {
    return this.availableWorkingAgendas.filter(
      workingAgenda => {
        let fromMinutes1 = workingAgendaParam.fromDate.getHours() * 60 + workingAgendaParam.fromDate.getMinutes();
        let toMinutes1 = workingAgendaParam.toDate.getHours() * 60 + workingAgendaParam.toDate.getMinutes();
        let fromMinutes2 = workingAgenda.fromDate.getHours() * 60 + workingAgenda.fromDate.getMinutes();
        let toMinutes2 = workingAgenda.toDate.getHours() * 60 + workingAgenda.toDate.getMinutes();

        let dateRangeOverlaps = DateUtils.dateRangeAinDateRangeB(workingAgendaParam.fromDate, workingAgendaParam.toDate, new Date(workingAgenda.fromDate), new Date(workingAgenda.toDate));
        let timeRangeOverlapsMinutes = DateUtils.timeRangeAinTimeRangeB(fromMinutes1, toMinutes1, fromMinutes2, toMinutes2);

        return dateRangeOverlaps && timeRangeOverlapsMinutes;
      }
    ).length > 0;
  }

  refreshAgenda() {
    this.loadAvailabilities();
    this.loadUnAvailabilities();
    this.loadAppointments();
  }

  // DatePicker Range
  hoveredDate: NgbDate | null = null;
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  time: { hour: 0, minute: 0 };

  time1: NgbTimeStruct;
  time2: NgbTimeStruct;

  minDate: NgbDate;
  maxDate: NgbDate;

  appointmentDate: NgbDate;

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  // get forms

  get avForm() {
    return this.availabilityForm.controls;
  }

  get unAvForm() {
    return this.unAvailabilityForm.controls;
  }

  get durationForm() {
    return this.appointmentDurationForm.controls;
  }

  get appForm() {
    return this.appointmentForm.controls;
  }

  goToMedicalRecord() {
    this.modalService.dismissAll();
    this.router.navigateByUrl('/doctor/dashboard/patient', {state: this.selectedAppointment.beneficiary});
  }

}
