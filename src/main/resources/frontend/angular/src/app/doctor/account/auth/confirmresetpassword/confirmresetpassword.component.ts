import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../../core/services/auth.service';

@Component({
  selector: 'app-confirmresetpassword',
  templateUrl: './confirmresetpassword.component.html',
  styleUrls: ['./confirmresetpassword.component.scss']
})
export class ConfirmresetpasswordComponent implements OnInit {

  resetForm: FormGroup;
  submitted = false;
  error = '';
  success = '';
  loading = false;
  email;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private authService: AuthenticationService) {
  }

  ngOnInit() {

    if (this.route.snapshot.paramMap.has('email')) {
      this.email = this.route.snapshot.paramMap.get('email');
    } else {
      this.router.navigate(['/doctor/account/login']);
    }

    this.resetForm = this.formBuilder.group({
      confirmationCode: ['', [Validators.required]]
    });
  }

  ngAfterViewInit() {
    document.body.classList.add('authentication-bg');
    document.body.classList.add('authentication-bg-pattern');
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.resetForm.controls;
  }

  /**
   * On submit form
   */
  onSubmit() {
    this.success = '';
    this.submitted = true;

    // stop here if form is invalid
    if (this.resetForm.invalid) {
      return;
    }

    this.loading = true;
    this.authService.checkVerificationCode(this.email, this.resetForm.value.confirmationCode, 'FOR_EMAIL_FORGET_PASSWORD')
      .subscribe(value => {
        this.router.navigate(['/doctor/account/change-password', {
          confirmationCode: this.resetForm.value.confirmationCode,
          email: this.email
        }]);
      });
  }
}
