import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../../../core/services/auth.service';
import {PasswordStrengthBarComponent} from "../../../../shared/ui/password-strength-bar/password-strength-bar.component";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss']
})
export class ChangepasswordComponent implements OnInit {

  @ViewChild(PasswordStrengthBarComponent, {static: false}) passwordComponent;

  changePasswordForm: FormGroup;
  submitted = false;
  error = '';
  success = '';
  loading = false;
  email;
  confirmationCode;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute,
              private router: Router, private authService: AuthenticationService, private translateService: TranslateService) {
  }

  ngOnInit() {

    if (this.route.snapshot.paramMap.has('email') && this.route.snapshot.paramMap.has('confirmationCode')) {
      this.email = this.route.snapshot.paramMap.get('email');
      this.confirmationCode = this.route.snapshot.paramMap.get('confirmationCode');
    } else {
      this.router.navigate(['/doctor/account/login']);
    }

    this.changePasswordForm = this.formBuilder.group({
      newPassword: ['', [Validators.required]],
      confirmNewPassword: ['', [Validators.required]]
    });
  }

  ngAfterViewInit() {
    document.body.classList.add('authentication-bg');
    document.body.classList.add('authentication-bg-pattern');
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.changePasswordForm.controls;
  }

  /**
   * On submit form
   */
  onSubmit() {
    this.success = '';
    this.submitted = true;

    if (!this.passwordComponent.isPasswordOkay) {
      this.changePasswordForm.controls['newPassword'].setErrors({'incorrect': true});
      this.changePasswordForm.controls['confirmNewPassword'].setErrors({'incorrect': true});
      return;
    }

    // stop here if form is invalid
    if (this.changePasswordForm.invalid) {
      return;
    }

    if (this.changePasswordForm.value.newPassword != this.changePasswordForm.value.confirmNewPassword) {
      //this.error = 'Check your confirmation password';
      return;
    }

    this.loading = true;
    this.authService.changeForgottenPassword(this.email, this.confirmationCode, this.changePasswordForm.value.newPassword)
      .subscribe(success => {
        this.loading = false;
        this.router.navigate(['/doctor/account/login']);
      }, error => {
        this.loading = false;
        this.translateService.get('invalid confimation code').subscribe((text: string) => this.error = text);
      });
  }

}
