import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { PasswordresetComponent } from './passwordreset/passwordreset.component';
import { ConfirmresetpasswordComponent } from './confirmresetpassword/confirmresetpassword.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { AccountActivationComponent } from './account-activation/account-activation.component';

const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'register', component: SignupComponent },
    { path: 'confirm-account', component: ConfirmComponent },
    { path: 'forget-password', component: PasswordresetComponent },
    { path: 'confirm-reset-password', component: ConfirmresetpasswordComponent },
    { path: 'reset-password', component: PasswordresetComponent },
    { path: 'change-password', component: ChangepasswordComponent },
    { path: 'account-activation', component: AccountActivationComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule { }
