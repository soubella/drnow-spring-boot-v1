import { Component, OnInit } from '@angular/core';
import { AgendaService } from '../../../core/services/doctor/agenda.service';
import { Router } from '@angular/router';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup} from "@angular/forms";
import Swal from "sweetalert2";
import {TranslateService} from "@ngx-translate/core";
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr);

@Component({
  selector: 'app-my-consultations',
  templateUrl: './my-consultations.component.html',
  styleUrls: ['./my-consultations.component.scss']
})
export class MyConsultationsComponent implements OnInit {

  appointments$: any = [];
  appointmentsResult$: any = [];
  searchTerm: any;
  total: any;
  selectedStatus: string;
  cancelAppointmentForm: FormGroup;

  constructor(private agendaService: AgendaService, private router:Router,
              private formBuilder: FormBuilder, public translateService: TranslateService, public modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.loadAppointments();
  }

  loadAppointments(){
    this.agendaService.getAppointments(false).subscribe(
      value => {
        this.appointments$ = value.body;
        this.appointmentsResult$ = value.body;
        this.total = this.appointmentsResult$.length;
      });
  }

  search(value) {
    if (!value || value.length <= 1) {
      this.appointmentsResult$ = this.appointments$;
      this.total = this.appointments$.length;
    } else {
      this.appointmentsResult$ = this.appointments$.filter(elem =>
        elem.beneficiary.firstName.toLowerCase().includes(value) ||
        elem.beneficiary.lastName.toLowerCase().includes(value) ||
        //elem.reason.toLowerCase().includes(value) ||
        elem.startAt.toString().includes(value)
      );
      this.total = this.appointmentsResult$.length;
    }
  }

  getAppointmentColor(status: string): string {
    let color;

    if (status === 'NOT_STARTED_YET' || status === 'ENDED') {
      color = 'badge bg-soft-success text-success';
    } else if (status === 'CANCELED_BY_PATIENT' || status === 'CANCELED_BY_DOCTOR') {
      color = 'badge bg-soft-danger text-danger';
    }

    return color;
  }

  onStatusChange(event: Event) {
    if(this.selectedStatus == 'firstName')
      this.appointmentsResult$.sort((a,b) => a.beneficiary.firstName.localeCompare(b.beneficiary.firstName));
    if(this.selectedStatus == 'lastName')
      this.appointmentsResult$.sort((a,b) => a.beneficiary.lastName.localeCompare(b.beneficiary.lastName));
    if(this.selectedStatus == 'status')
      this.appointmentsResult$.sort((a,b) => a.appointmentStatusEnum.localeCompare(b.appointmentStatusEnum));
    if(this.selectedStatus == 'startAt')
      this.appointmentsResult$.sort((a,b) => b.startAt.getTime() - a.startAt.getTime());
  }

  goToMedicalRecord(patient: any) {
   this.router.navigateByUrl('/doctor/dashboard/patient', { state: patient });
  }

  startAppointment(appointment: any) {
    this.router.navigateByUrl('/doctor/dashboard/video-call;room='+ appointment.sessionToken, { state: appointment });
    // this.router.navigate(['/doctor/dashboard/video-call',
    //   { room: appointment.sessionToken }]
    // );
  }

  cancelAppointment(appointment: any) {
    this.cancelAppointmentForm = this.formBuilder.group({
      cancellationReason: [''],
      appointmentStatusEnum: ['CANCELED_BY_DOCTOR']
    });
    let title, textModal, yes, no, successMessage, successTitle;
    this.translateService.get('Agenda.AreYouSureToCancel').subscribe((text: string) => title = text);
    this.translateService.get('Agenda.AreYouSureToCancelText').subscribe((text: string) => textModal = text);
    this.translateService.get('Action.Yes').subscribe((text: string) => yes = text);
    this.translateService.get('Action.No').subscribe((text: string) => no = text);
    this.translateService.get('Agenda.AppointmentHasBeenCanceled').subscribe((text: string) => successMessage = text);
    this.translateService.get('Agenda.Canceled').subscribe((text: string) => successTitle = text);
    Swal.fire({
      title: title,
      text: textModal,
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: yes,
      cancelButtonText: no,
      confirmButtonClass: 'btn btn-success mt-2 btn-lg',
      cancelButtonClass: 'btn btn-danger ml-2 mt-2',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.agendaService.cancelAppointment(this.cancelAppointmentForm.value, appointment.pk).subscribe(
          value => {
            Swal.fire(successTitle, successMessage, 'success');
            this.loadAppointments();
            this.modalService.dismissAll();
          });
      }
    });
  }
}
