import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DoctorRoutingModule } from './doctor-routing.module';
import { DoctorLayoutsModule } from '../layouts/doctor-layout/doctor-layouts.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DoctorRoutingModule,
    DoctorLayoutsModule,
  ]
})
export class DoctorModule { }
