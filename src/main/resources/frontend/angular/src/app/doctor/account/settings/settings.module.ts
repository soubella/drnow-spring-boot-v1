import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { UIModule } from '../../../shared/ui/ui.module';
import { NgbModalModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { FileUploadModule } from '@iplab/ngx-file-upload';
import {NgxMaskModule} from "ngx-mask";

@NgModule({
  declarations: [ChangePasswordComponent, UpdateProfileComponent],
    imports: [
        CommonModule,
        SettingsRoutingModule,
        UIModule,
        NgbTabsetModule,
        TranslateModule,
        ReactiveFormsModule,
        FileUploadModule,
        NgbModalModule,
        NgSelectModule,
        FormsModule,
        NgxMaskModule.forRoot()
    ]
})
export class SettingsModule { }
