import {Component, OnInit, AfterViewInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {PopulatorService} from '../../../../core/services/populator.service';
import {Profession} from '../../../../core/models/doctor/profession.model';
import {Speciality} from '../../../../core/models/doctor/speciality.model';
import {DoctorService} from '../../../../core/services/doctor/doctor.service';
import {ToastrService} from 'ngx-toastr';
import {PasswordStrengthBarComponent} from "../../../../shared/ui/password-strength-bar/password-strength-bar.component";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, AfterViewInit {

  @ViewChild(PasswordStrengthBarComponent, {static: false}) passwordComp;

  public barLabel: string = 'Password strength:';
  password;

  signupForm: FormGroup;
  submitted = false;
  error = '';
  loading = false;
  professions: Profession[];
  specialities: Speciality[];
  selectedSpecialities: Speciality[];
  hideSpecialities: boolean = true;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private toastr: ToastrService,
              private router: Router, private populatorService: PopulatorService, private doctorService: DoctorService) {
  }

  ngOnInit() {
    this._fetchData();
    this.signupForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: [''],
      qualificationCode: [''],
      inami: [''],
      phoneNumber: ['', Validators.required],
      city: ['', Validators.required],
      profession: ['', Validators.required],
      speciality: [''],
      role: ['ROLE_DOCTOR']
    });
  }

  ngAfterViewInit() {
    document.body.classList.add('authentication-bg');
    document.body.classList.add('authentication-bg-pattern');
  }

  // convenience getter for easy access to form fields
  isPasswordOkay: boolean;

  get f() {
    return this.signupForm.controls;
  }

  /**
   * On submit form
   */
  onSubmit() {
    this.submitted = true;

    if(!this.passwordComp.isPasswordOkay){
      this.signupForm.controls['password'].setErrors({'incorrect': true});
      return;
    }

    // stop here if form is invalid
    if (this.signupForm.invalid) {
      return;
    }

    if (!this.signupForm.value.speciality)
      this.signupForm.value.speciality = this.signupForm.value.profession;

    this.doctorService.register(this.signupForm.value).subscribe(
      (success) => {
        if (success.status == 201) {
          this.toastr.success(success.body.message, '', {timeOut: 6000, closeButton: true});
          //this.router.navigate(['/doctor/account/account-activation', {email: this.signupForm.value.email}]);
          this.router.navigate(['/doctor/account/login', {email: this.signupForm.value.email}]);
        }
      },
      error => {
        this.toastr.warning(error, '', {timeOut: 6000, closeButton: true});
      },
      () => {
        // The POST observable is now completed.
      }
    );
  }

  professionOnChange($event) {
    this.selectedSpecialities = [];
    this.specialities.forEach((value, index) => {
      if ($event.pk == value.professionModel.pk && value.name.length > 0 && $event.title != value.name) {
        this.selectedSpecialities.push(value);
      }
    });
    this.hideSpecialities = this.selectedSpecialities.length <= 0;
  }

  specialityOnChange($event) {
    if ($event.professionModel.title != this.signupForm.value.profession) {
      this.signupForm.controls.profession.reset();
    }
  }

  private _fetchData() {
    this.populatorService.getProfessionsList()
      .subscribe(data => {
        this.professions = data;
      });

    this.populatorService.getSpecialitiesList()
      .subscribe(data => {
        this.specialities = data;
      });
  }
}
