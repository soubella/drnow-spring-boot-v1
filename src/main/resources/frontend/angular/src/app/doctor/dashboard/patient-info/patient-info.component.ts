import {Component, OnInit} from '@angular/core';
import {PatientService} from '../../../core/services/patient/patient.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MedicalRecord} from '../../../core/models/patient/medical-record.model';
import {Location} from '@angular/common';

@Component({
  selector: 'app-patient-info',
  templateUrl: './patient-info.component.html',
  styleUrls: ['./patient-info.component.scss']
})
export class PatientInfoComponent implements OnInit {

  medicalRecord = new MedicalRecord();

  selectedVaccine = [];
  selectedCvDiseasesRisk = [];

  patientFullName: string;

  hideGoBackBtn: boolean = false;

  constructor(private patientService: PatientService,
              private router: Router,
              private _location: Location,
              private route: ActivatedRoute) {

    if (this.route.snapshot.paramMap.has('patient')) {
      this.hideGoBackBtn = true;
      const patientPk = this.route.snapshot.paramMap.get('patient');
      this.patientService.findPatientByPatientPkParam(patientPk).subscribe(
        data => {
          if (data.body.medicalRecord)
            this.medicalRecord = data.body.medicalRecord
          else
            this.medicalRecord = new MedicalRecord();
        });

    } else if (this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.medicalRecord) {
      this.medicalRecord = this.router.getCurrentNavigation().extras.state.medicalRecord;
      this.populatePatientFullName();
    } else if (!this.router.getCurrentNavigation().extras.state) {
      this.medicalRecord = new MedicalRecord();
      this.router.navigateByUrl('/doctor/dashboard/patients');
    } else {
      this.medicalRecord = new MedicalRecord();
      this.populatePatientFullName();
    }
  }

  populatePatientFullName() {
    this.patientFullName = this.router.getCurrentNavigation().extras.state.firstName + ' ' + this.router.getCurrentNavigation().extras.state.lastName;
  }

  ngOnInit() {

  }

  ngAfterContentInit() {
    if (this.medicalRecord && this.medicalRecord.vaccineStatus && this.medicalRecord.vaccineStatus !== '') {
      this.selectedVaccine = this.medicalRecord.vaccineStatus.split(',');
    }
  }

  goBack() {
    this._location.back();
  }
}
