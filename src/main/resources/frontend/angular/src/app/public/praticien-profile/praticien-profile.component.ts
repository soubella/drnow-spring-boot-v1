import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { DoctorService } from '../../core/services/doctor/doctor.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IDoctorSearchCard } from '../../core/models/doctor/doctor-search-card.model';

@Component({
  selector: 'app-praticien-profile',
  templateUrl: './praticien-profile.component.html',
  styleUrls: ['./praticien-profile.component.scss']
})
export class PraticienProfileComponent implements OnInit {

  doctor: IDoctorSearchCard;

  @ViewChild('target', { static: false, read: ViewContainerRef }) entry: ViewContainerRef;

  constructor(private doctorService: DoctorService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.loadDoctorData();
  }

  loadDoctorData() {
    if (!this.route.snapshot.paramMap.has('doctor')) {
      this.router.navigate(['/']);
    } else {
      const doctorPk = this.route.snapshot.paramMap.get('doctor');

      this.doctorService.getDoctorProfileByPk(doctorPk).subscribe(
        data => {
          this.doctor = data.body;
        });
    }
  }

}
