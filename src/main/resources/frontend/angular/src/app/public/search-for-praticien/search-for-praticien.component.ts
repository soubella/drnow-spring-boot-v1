import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../core/services/auth.service';
import { Role } from '../../core/models/role.enums';
import { Speciality } from '../../core/models/doctor/speciality.model';
import { PopulatorService } from '../../core/services/populator.service';
import { DoctorName } from '../../core/models/doctor/doctor-name.model';
import { SearchElem } from '../../core/models/doctor/search-elem.model';
import { DoctorService } from '../../core/services/doctor/doctor.service';
import { IDoctorSearchCard } from '../../core/models/doctor/doctor-search-card.model';

@Component({
  selector: 'app-search-for-praticien',
  templateUrl: './search-for-praticien.component.html',
  styleUrls: ['./search-for-praticien.component.scss']
})
export class SearchForPraticienComponent implements OnInit {

  selectedElem;
  specialities: Speciality[] = [];
  doctorsNames: DoctorName[] = [];
  searchElements: SearchElem[] = [];
  searchInput: string = '';

  // search param
  page: number = 0;
  size: number = 10;
  stopScrollLoading: boolean;

  // search result
  doctors: IDoctorSearchCard[] = [];

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private populatorService: PopulatorService, private doctorService: DoctorService
  ) {
  }

  ngOnInit() {
    const currentUser = this.authenticationService.currentUser();
    if (currentUser && currentUser.role == Role.Patient) {
      //this.router.navigate(['/patient/dashboard']);
    }

    this.populateSpecialities();
    this.loadAllDoctors();
  }

  populateSpecialities() {
    this.populatorService.getSpecialitiesList()
      .subscribe(data => {
        this.specialities = data;
        this.specialities = this.specialities.map(elem => {
          if (elem.professionModel.title == elem.name) {
            elem.autoComplete = elem.name;
          } else {
            elem.autoComplete = elem.professionModel.title + ' ' + elem.name;
          }

          elem.groupedBy = 'Speciality';
          return elem;
        });
        this.searchElements = this.specialities.map(elem => {
          return { 'name': elem.autoComplete, 'groupedBy': elem.groupedBy };
        });
      });
  }

  findDoctorsByFullName(value) {
    this.searchInput = value.term;
    if (value.term.length >= 2) {
      this.populatorService.findDoctorsByFullName(value.term)
        .subscribe(data => {
          this.doctorsNames = data;
          this.doctorsNames = this.doctorsNames.map(elem => {
            elem.autoComplete = elem.fullName;
            elem.groupedBy = 'Doctor';
            return elem;
          });
          this.searchElements = [];
          this.searchElements = this.specialities.map(elem => {
            return { 'name': elem.autoComplete, 'groupedBy': elem.groupedBy };
          });
          this.searchElements = this.searchElements.concat(this.doctorsNames.map(elem => {
            return { 'name': elem.fullName, 'groupedBy': elem.groupedBy };
          }));
        });
    }
  }

  search() {
    if (this.searchInput.length > 0) {
      this.page = 0;
      this.doctorService.searchForDoctors(this.searchInput, this.page, this.size).subscribe(
        data => {
          this.doctors = data.body;
          console.log(data.body);
        });
    }
  }

  loadAllDoctors() {
    this.doctorService.loadAllDoctors(this.page, this.size).subscribe(
      data => {
        this.doctors = data.body;
        console.log(data.body);
      });
  }

  change(event) {
    if (!event) {
      this.loadAllDoctors();
    } else {
      this.searchInput = event.name;
    }
  }

  keypress(event) {
    if (event.key == 'Enter') {
      this.search();
    }
  }

  @HostListener('window:scroll', [])
  onScroll(): void {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - 20) {
      // you're at the bottom of the page
      console.log('you have reached the bottom');
      if (!this.stopScrollLoading) {
        console.log('loading more data');
        this.page++;
        this.doctorService.loadAllDoctors(this.page, this.size).subscribe(
          data => {
            if (data.body.length == 0) {
              this.stopScrollLoading = true;
            } else {
              this.doctors = this.doctors.concat(data.body);
            }
            console.log(data.body);
          });
      } else {
        console.log('no more data');
      }
    }
  }

}
