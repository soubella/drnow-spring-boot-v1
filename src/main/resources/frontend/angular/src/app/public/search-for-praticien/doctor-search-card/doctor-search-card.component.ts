import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { IDoctorSearchCard } from '../../../core/models/doctor/doctor-search-card.model';
import DateUtils from '../../../core/utils/date.utils';
import { TranslateService } from '@ngx-translate/core';
import { WorkingAgenda } from '../../../core/models/doctor/working-agenda.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-doctor-search-card',
  templateUrl: './doctor-search-card.component.html',
  styleUrls: ['./doctor-search-card.component.scss']
})
export class DoctorSearchCardComponent implements OnInit {

  @Input() doctor: IDoctorSearchCard;
  @Input() isFullPage: boolean;

  doctorTitle: string;

  constructor(private translateService: TranslateService, private router: Router) {
    this.fromDate = new Date();
    this.today = new Date();
    this.toDate = DateUtils.addDays(new Date(), 4);
    this.today.setHours(0, 0, 0, 0);
    this.fromDate.setHours(0, 0, 0, 0);
    this.toDate.setHours(0, 0, 0, 0);
    this.disablePreviousBtn = this.today.getTime() === this.fromDate.getTime();

    // init columns
    this.updateColumns();
  }

  ngOnInit() {
    // init periods
    this.availablePeriods = this.doctor.workingAgendas.filter(elem => elem.availability);
    this.unAvailablePeriods = this.doctor.workingAgendas.filter(elem => !elem.availability);
    this.updateColumnsData();
    this.maxDate = this.availablePeriods.reduce((a, b) => (a.toDate > b.toDate) ? a : b, this.colDate3).toDate;
    this.disableNextBtn = this.maxDate == null;
    this.noAvailableAgenda = this.maxDate != null && this.maxDate < this.today;
    this.disableNextBtn = this.noAvailableAgenda;
    this.disablePreviousBtn = this.noAvailableAgenda;
  }

  ngAfterContentInit() {
    this.setTitle();
    this.updateColumnsData();
    console.log(this.doctor.languages)
    if (this.isFullPage) {
      this.onShowMore();
    }
  }

  setTitle() {
    if (this.doctor.profession.title == 'Médecin' || this.doctor.speciality.professionModel.title == 'Dentiste') {
      this.doctorTitle = 'Dr';
    } else if (this.doctor.gender == 'MALE') {
      this.doctorTitle = 'Mr';
    } else if (this.doctor.gender == 'FEMALE') {
      this.doctorTitle = 'Mme';
    }
  }

  noAvailableAgenda: boolean;
  fromDate;
  toDate;
  today;
  i = 4;
  formatterMonth = new Intl.DateTimeFormat(this.translateService.currentLang, { month: 'short' });
  formatterDay = new Intl.DateTimeFormat(this.translateService.currentLang, { weekday: 'short' });
  disablePreviousBtn: boolean;
  disableNextBtn: boolean;

  // doctors periods
  availablePeriods: WorkingAgenda[] = [];
  unAvailablePeriods: WorkingAgenda[] = [];

  // columns
  colDate1;
  colDate2;
  colDate3;
  colDates = [];
  maxDate;
  buttonsLimits = 3;

  // map buttons
  buttonsMap = new Map();

  showMore: boolean;

  previousPeriod() {
    this.disablePreviousBtn = this.today.getTime() === this.fromDate.getTime();
    if (!this.disablePreviousBtn) {
      this.fromDate = DateUtils.addDays(this.fromDate, -this.i);
      this.toDate = DateUtils.addDays(this.toDate, -this.i);

      // columns
      this.updateColumns();
      this.updateColumnsData();

      this.disableNextBtn = this.colDates.find(value => value.getTime() == this.maxDate.getTime()) != null;
      this.disablePreviousBtn = this.today.getTime() === this.fromDate.getTime();
    }
  }

  nextPeriod() {
    this.disableNextBtn = this.colDates.find(value => value.getTime() == this.maxDate.getTime()) != null;
    if (!this.disableNextBtn) {
      this.fromDate = DateUtils.addDays(this.fromDate, this.i);
      this.toDate = DateUtils.addDays(this.toDate, this.i);

      // columns
      this.updateColumns();
      this.updateColumnsData();

      this.disableNextBtn = this.colDates.find(value => value.getTime() == this.maxDate.getTime()) != null;
      this.disablePreviousBtn = this.today.getTime() === this.fromDate.getTime();
    }
  }

  updateColumns() {
    this.colDate1 = DateUtils.addDays(this.fromDate, 1);
    this.colDate2 = DateUtils.addDays(this.fromDate, 2);
    this.colDate3 = DateUtils.addDays(this.fromDate, 3);
    this.colDates[0] = this.fromDate;
    this.colDates[1] = DateUtils.addDays(this.fromDate, 1);
    this.colDates[2] = DateUtils.addDays(this.fromDate, 2);
    this.colDates[3] = DateUtils.addDays(this.fromDate, 3);
  }

  updateColumnsData() {
    this.buttonsMap.set(1, this.getAvailableTimes(this.fromDate));
    this.buttonsMap.set(2, this.getAvailableTimes(this.colDate1));
    this.buttonsMap.set(3, this.getAvailableTimes(this.colDate2));
    this.buttonsMap.set(4, this.getAvailableTimes(this.colDate3));
  }

  getAvailableTimes(dayParam) {

    let day = new Date(dayParam);
    const local = this.translateService.currentLang;
    let availablePeriods: WorkingAgenda[] = this.availablePeriods.filter(elem => {
        let fromDate = new Date(elem.fromDate);
        fromDate.setHours(0);
        fromDate.setMinutes(0);
        let toDate = new Date(elem.toDate);
        toDate.setHours(0);
        toDate.setMinutes(0);
        return day >= fromDate && day <= toDate;
      }
    );
    let unAvailablePeriods: WorkingAgenda[] = this.unAvailablePeriods.filter(elem => {
      let fromDate = new Date(elem.fromDate);
      fromDate.setHours(0);
      fromDate.setMinutes(0);
      let toDate = new Date(elem.toDate);
      toDate.setHours(0);
      toDate.setMinutes(0);
      return day >= fromDate && day <= toDate;
    });

    const availableTimes = [];
    availablePeriods.forEach(availablePeriod => {
      const hours = availablePeriod.fromDate.getHours();
      const minutes = availablePeriod.fromDate.getMinutes();
      const hoursEnd = availablePeriod.toDate.getHours();
      const minutesEnd = availablePeriod.toDate.getMinutes();

      let dt = new Date(day);
      dt.setHours(hours);
      dt.setMinutes(minutes);

      if (day.getTime() == this.today.getTime() && new Date().getTime() > dt.getTime()) {
        dt = new Date(new Date().getTime() + (new Date().getMinutes() + this.adjustMinutes() * 60000));
        dt.setSeconds(0);
        dt.setMilliseconds(0);
      }

      let dtEnd = new Date(day);
      dtEnd.setHours(hoursEnd);
      dtEnd.setMinutes(minutesEnd);
      dtEnd = new Date(dtEnd.getTime() - this.doctor.appointmentDuration * 60000);
      let i = 0;


      while (dt.getTime() <= dtEnd.getTime() && i <= this.buttonsLimits) {
        if (!this.checkIfExistInUnAvailabilities(unAvailablePeriods, availablePeriod, dt) &&
          !this.checkIfExistInAppointment(dt)) {
          availableTimes.push(dt.toLocaleTimeString(local, { hour: '2-digit', minute: '2-digit' }));
          i++;
        }
        dt.setMinutes(dt.getMinutes() + this.doctor.appointmentDuration);
      }
    });
    return availableTimes;
  }

  checkIfExistInUnAvailabilities(unAvailablePeriods: WorkingAgenda[], availablePeriod: WorkingAgenda, time) {
    return unAvailablePeriods.find(elem => elem.fromDate <= time && elem.toDate >= time);
  }

  checkIfExistInAppointment(dateTime: Date) {
    return this.doctor.unAvailableTimes.find(elem => elem.startAt.getTime() == dateTime.getTime());
  }

  adjustMinutes() {
    const minutesParam = new Date().getMinutes();
    if (minutesParam >= 0 && minutesParam <= 15) {
      return 15 - minutesParam + 15;
    } else if (minutesParam > 15 && minutesParam <= 30) {
      return 30 - minutesParam + 30;
    } else if (minutesParam > 30 && minutesParam <= 45) {
      return 45 - minutesParam + 30;
    } else if (minutesParam > 45 && minutesParam < 60) {
      return 60 - minutesParam + 30;
    }
  }

  takeAppointment(date: Date, time: string) {
    const hours = parseInt(time.split(':')[0]);
    const minutes = parseInt(time.split(':')[1]);
    let appointmentDate = date;
    appointmentDate.setHours(hours);
    appointmentDate.setMinutes(minutes);
    this.router.navigate(['/patient/account/auth/reservation',
      { doctor: this.doctor.pk, time: appointmentDate.getTime() }]
    );
  }

  goToProfile() {
    // this.isFullPage = true;
    // this.onShowMore();
    this.router.navigate(['/praticien',
      { doctor: this.doctor.pk }]
    );
  }

  onShowMore() {
    this.showMore = true;
    this.buttonsLimits = 144;
    this.updateColumnsData();
  }

  onShowLess() {
    this.showMore = false;
    this.buttonsLimits = 3;
  }

}
