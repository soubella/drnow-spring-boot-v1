import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { SearchForPraticienComponent } from './search-for-praticien/search-for-praticien.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { DoctorSearchCardComponent } from './search-for-praticien/doctor-search-card/doctor-search-card.component';
import { TranslateModule } from '@ngx-translate/core';
import { PraticienProfileComponent } from './praticien-profile/praticien-profile.component';


@NgModule({
  declarations: [SearchForPraticienComponent, DoctorSearchCardComponent, PraticienProfileComponent],
  imports: [
    CommonModule,
    PublicRoutingModule,
    NgSelectModule,
    FormsModule,
    TranslateModule
  ]
})
export class PublicModule { }
