import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchForPraticienComponent } from './search-for-praticien/search-for-praticien.component';
import { PraticienProfileComponent } from './praticien-profile/praticien-profile.component';


const routes: Routes = [
  { path: '', component: SearchForPraticienComponent },
  { path: 'praticien', component: PraticienProfileComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule {
}
