import { Component, OnChanges, Input, SimpleChange } from '@angular/core';


@Component({
  selector: 'app-password-strength-bar',
  templateUrl: './password-strength-bar.component.html',
  styleUrls: ['./password-strength-bar.component.scss']
})

export class PasswordStrengthBarComponent implements OnChanges {

  @Input() passwordToCheck: string;
  @Input() barLabel: string;
  isPasswordOkay = false;
  bar0: string;
  bar1: string;
  bar2: string;
  bar3: string;
  bar4: string;

  private colors = ['#F00', '#F90', '#FF0', '#9F0', '#0F0'];

  checkPassword = {
    digits: false,
    lower: false,
    upper: false,
    nonWords: false,
    length: false
  };

  private measureStrength(pass: string) {
    let score = 0;

    // award every unique letter until 5 repetitions
    let letters = {};

    for (let i = 0; i < pass.length; i++) {
      letters[pass[i]] = (letters[pass[i]] || 0) + 1;
      score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    let variations = {
      digits: /\d/.test(pass),
      lower: /[a-z]/.test(pass),
      upper: /[A-Z]/.test(pass),
      nonWords: /\W/.test(pass),
      length: pass.length >= 6
    };

    let variationCount = 0;

    for (let check in variations) {
      variationCount += (variations[check]) ? 1 : 0;
      this.checkPassword[check] = variations[check];
    }

    score += (variationCount - 1) * 10;

    return Math.trunc(score);
  }

  private getColor(score: number) {

    let idx = 0;
    if (this.checkPassword.upper) {
      idx = 4;
    } else if (this.checkPassword.digits) {
      idx = 3;
    } else if (this.checkPassword.nonWords) {
      idx = 2;
    } else if (this.checkPassword.lower) {
      idx = 1;
    }

    return {
      idx: idx + 1,
      col: this.colors[idx]
    };
  }

  ngOnChanges(changes: { [propName: string]: SimpleChange }): void {
    let password = changes['passwordToCheck'].currentValue;
    // this.setBarColors(5, '#DDD');
    // if (password) {
    //   let c = this.getColor(this.measureStrength(password));
    //   this.setBarColors(c.idx, c.col);
    // }
  if(password != undefined)
    this.validatePassword(password);

  }

  private setBarColors(count, col) {
    for (let _n = 0; _n < count; _n++) {
      this['bar' + _n] = col;
    }
  }

  validatePassword(pass){
    // bonus points for mixing it up
    let variations = {
      digits: /\d/.test(pass),
      lower: /[a-z]/.test(pass),
      upper: /[A-Z]/.test(pass),
      nonWords: /\W/.test(pass),
      length: pass.length >= 6
    };

    for (let check in variations) {
      this.checkPassword[check] = variations[check];
    }

    this.isPasswordOkay = this.checkPassword.lower && this.checkPassword.upper &&
      this.checkPassword.nonWords && this.checkPassword.lower && this.checkPassword.digits && this.checkPassword.length

  }

}

