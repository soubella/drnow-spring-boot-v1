import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgbCollapseModule, NgbDatepickerModule, NgbTimepickerModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { ClickOutsideModule } from 'ng-click-outside';

import { CountToDirective } from './count-to.directive';
import { PreloaderComponent } from './preloader/preloader.component';
import { PagetitleComponent } from './pagetitle/pagetitle.component';
import { PortletComponent } from './portlet/portlet.component';
import { EmaillistComponent } from './emaillist/emaillist.component';
import { WidgetComponent } from './widget/widget.component';
import { PasswordStrengthBarComponent } from './password-strength-bar/password-strength-bar.component';
import {OpenViduVideoComponent} from "./openvidu/ov-video.component";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [OpenViduVideoComponent, CountToDirective, PreloaderComponent, PagetitleComponent, PortletComponent, EmaillistComponent, WidgetComponent, PasswordStrengthBarComponent],
  imports: [
    CommonModule,
    FormsModule,
    ClickOutsideModule,
    NgbCollapseModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    NgbDropdownModule,
    TranslateModule
  ],
  // tslint:disable-next-line: max-line-length
  exports: [OpenViduVideoComponent, CountToDirective, PreloaderComponent, PagetitleComponent, PortletComponent, EmaillistComponent, WidgetComponent, PasswordStrengthBarComponent]
})
export class UIModule { }
