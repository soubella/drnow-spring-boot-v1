import {AfterViewInit, Component, ElementRef, Input, ViewChild} from '@angular/core';
import {StreamManager} from 'openvidu-browser';

@Component({
  selector: 'ov-video',
  template: '<video [ngClass]="getStyleClass()" #videoElement></video>',
  styles: [`
    .video-small {
      height: 35%;
      width: 100%;
      z-index: 999;
      border-radius: 2%;
      border: calc(1px + 0.1vw) solid transparent;
      background-origin: border-box;
      background-clip: content-box, border-box;
      background-size: cover;
      box-sizing: border-box;
      box-shadow: 0 0 1px 1px rgba(0, 0, 0, 0.5);
    }
  `,`
    .video-large {
      width: 80%;
      height: 100%;
      border-radius: 1%;
      border: calc(1px + 0.1vw) solid transparent;
      background-origin: border-box;
      background-clip: content-box, border-box;
      background-size: cover;
      box-sizing: border-box;
      box-shadow: 0 0 1px 1px rgba(0, 0, 0, 0.5);
    }
  `
  ]
})
export class OpenViduVideoComponent implements AfterViewInit {

  @Input()
  size: string;

  @ViewChild('videoElement', {static: false}) elementRef: ElementRef;

  _streamManager: StreamManager;

  ngAfterViewInit() {
    this._streamManager.addVideoElement(this.elementRef.nativeElement);
  }

  @Input()
  set streamManager(streamManager: StreamManager) {
    this._streamManager = streamManager;
    if (!!this.elementRef) {
      this._streamManager.addVideoElement(this.elementRef.nativeElement);
    }
  }

  getStyleClass() {
    return this.size === 'small' ? 'video-small' : 'video-large';
  }

}
