import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbDropdownModule, NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { ClickOutsideModule } from 'ng-click-outside';

import { UIModule } from '../../shared/ui/ui.module';
import { DoctorLayoutComponent } from './doctor-layout.component';
import { TopbarComponent } from './topbar/topbar.component';
import { FooterComponent } from './footer/footer.component';
import { RightsidebarComponent } from './rightsidebar/rightsidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [DoctorLayoutComponent, TopbarComponent, FooterComponent, RightsidebarComponent, NavbarComponent],
  exports: [
    DoctorLayoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgbDropdownModule,
    NgbCollapseModule,
    ClickOutsideModule,
    UIModule,
    TranslateModule
  ]
})
export class DoctorLayoutsModule { }
