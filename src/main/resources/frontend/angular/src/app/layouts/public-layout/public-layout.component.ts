import { AfterViewInit, Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../../core/services/auth.service";

@Component({
  selector: 'app-public-layout',
  templateUrl: './public-layout.component.html',
  styleUrls: ['./public-layout.component.scss']
})
export class PublicLayoutComponent implements OnInit, AfterViewInit {

  showMobileMenu = false;
  currentUser;

  constructor(private authService: AuthenticationService) {
    this.currentUser = authService.currentUser();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // if (!location.href.includes('register') && !location.href.includes('login')) {
    //   document.body.classList.remove('authentication-bg');
    //   document.body.classList.remove('authentication-bg-pattern');
    // }
      document.body.classList.remove('authentication-bg');
      document.body.classList.remove('authentication-bg-pattern');
  }

  /**
   * on settings button clicked from topbar
   */
  onSettingsButtonClicked() {
    document.body.classList.toggle('right-bar-enabled');
  }

  /**
   * On mobile toggle button clicked
   */
  onToggleMobileMenu() {
    this.showMobileMenu = !this.showMobileMenu;
  }
}
