import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbCollapseModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { ClickOutsideModule } from 'ng-click-outside';
import { UIModule } from '../../shared/ui/ui.module';
import { PublicLayoutComponent } from './public-layout.component';
import { TopbarComponent } from './topbar/topbar.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RightsidebarComponent } from './rightsidebar/rightsidebar.component';
import { TranslateModule } from '@ngx-translate/core';
import {DoctorLayoutsModule} from "../doctor-layout/doctor-layouts.module";
import {PatientLayoutModule} from "../patient-layout/patient-layout.module";

@NgModule({
  declarations: [PublicLayoutComponent, TopbarComponent, FooterComponent, RightsidebarComponent, NavbarComponent],
  imports: [
    CommonModule,
    RouterModule,
    NgbDropdownModule,
    NgbCollapseModule,
    ClickOutsideModule,
    UIModule,
    TranslateModule,
    DoctorLayoutsModule,
    PatientLayoutModule
  ]
})
export class PublicLayoutModule { }
