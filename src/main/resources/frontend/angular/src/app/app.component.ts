import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '../environments/environment';
import defaultLanguage from './../assets/i18n/fr-FR.json';

@Component({
  selector: 'app-drnow',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private translateService: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translateService.setTranslation('fr-FR', defaultLanguage);
    translateService.setDefaultLang(environment.defaultLanguage);
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translateService.use(environment.defaultLanguage);
  }
}
