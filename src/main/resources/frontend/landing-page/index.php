<?php  include 'config.php'?>
<?php include SITE_ROOT.'/languages/inc.language.php'?>
<!doctype html>
<html lang="<?php print $language ?>">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge" /><![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1"><!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" href="favicon.ico">
    <title>Doctoryl</title>
    <?php  include SITE_ROOT.'/blocks/ressources.php'?>
</head>
<body>
        <?php  include SITE_ROOT.'/blocks/navbar.php'?>
    <main>
        <?php  include SITE_ROOT.'/blocks/header.php'?>

        <?php  include SITE_ROOT.'/blocks/how-it-works.php'?>

        <?php  include SITE_ROOT.'/blocks/benefits.php'?>

        <?php  include SITE_ROOT.'/blocks/single-testimonial.php'?>

        <?php  include SITE_ROOT.'/blocks/counters.php'?>

        <?php  include SITE_ROOT.'/blocks/cta.php'?>

        <?php  include SITE_ROOT.'/blocks/footer.php'?>
        <div class="alert alert-info" role="alert" style="position: sticky !important; bottom: 0; font-weight: 600; z-index: 999; text-align: center;">
            Ce site est encore en maintenance <i class="fas fa-code"></i>
        </div>
    </main>
        <?php  include SITE_ROOT.'/blocks/js-ressources.php'?>
</body>
</html>
