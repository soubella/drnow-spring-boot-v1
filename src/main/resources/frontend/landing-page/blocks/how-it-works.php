<section id="howitworks" class="section how-it-works">
    <div class="container">
        <div class="section-heading text-center">
            <h2 class="bold"><?php print t('HowItWorks')?></h2>
<!--            <p class="lead text-secondary">...</p>-->
        </div>
        <div class="row gap-y text-center text-md-left">
            <div class="col-md-4 py-4 text-center">
                <div class="shapes-figure shapes-container">
                    <div class="shape shape-circle center-x"></div>
                </div>
                <figure class="mockup mb-4"><img src="img/patient/how/register.png" class="mb-3 image-responsive"></figure>
                <h5 class="bold"><?php print t('HowTitle1')?></h5>
                <p class="text-muted"><?php print t('HowContent1')?></p>
            </div>
            <div class="col-md-4 py-4 text-center border-left border-right border-dark">
                <div class="shapes-figure shapes-container">
                    <div class="shape shape-circle center-x"></div>
                </div>
                <figure class="mockup mb-4"><img src="img/patient/how/doctor.png" class="mb-3 image-responsive"></figure>
                <h5 class="bold"><?php print t('HowTitle2')?></h5>
                <p class="text-muted"><?php print t('HowContent2')?></p>
            </div>
            <div class="col-md-4 py-4 text-center">
                <div class="shapes-figure shapes-container">
                    <div class="shape shape-circle center-x"></div>
                </div>
                <figure class="mockup mb-4"><img src="img/patient/how/doctor-in-laptop.png" class="mb-3 image-responsive"></figure>
                <h5 class="bold"><?php print t('HowTitle3')?></h5>
                <p class="text-muted"><?php print t('HowContent3')?></p>
            </div>
        </div>
    </div>
</section>