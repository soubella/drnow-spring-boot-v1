<section class="section b-b bg-light">
    <div class="container pt-5">
        <div class="d-flex align-items-center flex-column flex-md-row">
            <div class="text-center text-md-left">
                <p class="light mb-0 text-primary lead"><?php print t('Cta1')?></p>
                <h2 class="mt-0"><?php print t('Cta2')?></h2>
            </div><a href="<?php print PATIENT_SEARCH?>" class="btn btn-primary btn-rounded btn-lg mt-3 mt-md-0 ml-md-auto"><?php print t('Cta4')?></a>
        </div>
    </div>
</section>