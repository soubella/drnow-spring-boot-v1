<section id="faqs" class="section bg-light">
    <div class="container">
        <div class="row ">
            <div class="col-lg-12 b-md-l ">
                <h3 class="light"><?php print t('CommonQuestions')?></h3>
                <hr class="mb-4">
                <div class="accordion accordion-clean" id="faqs-accordion">
                    <div class="card mb-3">
                        <div class="card-header"><a href="#" class="card-title btn" data-toggle="collapse" data-target="#v1-q1"><i class="fas fa-angle-down angle"></i><?php print t('PraticienQ1')?></a></div>
                        <div id="v1-q1" class="collapse" data-parent="#faqs-accordion">
                            <div class="card-body"><?php print t('PraticienR1')?></div>
                        </div>
                    </div>
                    <div class="card mb-3">
                        <div class="card-header"><a href="#" class="card-title btn" data-toggle="collapse" data-target="#v1-q2"><i class="fas fa-angle-down angle"></i><?php print t('PraticienQ2')?></a></div>
                        <div id="v1-q2" class="collapse" data-parent="#faqs-accordion">
                            <div class="card-body"><?php print t('PraticienR2')?></div>
                        </div>
                    </div>
                    <div class="card mb-3">
                        <div class="card-header"><a href="#" class="card-title btn" data-toggle="collapse" data-target="#v1-q3"><i class="fas fa-angle-down angle"></i><?php print t('PraticienQ3')?></a></div>
                        <div id="v1-q3" class="collapse" data-parent="#faqs-accordion">
                            <div class="card-body"><?php print t('PraticienR3')?></div>
                        </div>
                    </div>
                    <div class="card mb-3">
                        <div class="card-header"><a href="#" class="card-title btn" data-toggle="collapse" data-target="#v1-q4"><i class="fas fa-angle-down angle"></i><?php print t('PraticienQ4')?></a></div>
                        <div id="v1-q4" class="collapse" data-parent="#faqs-accordion">
                            <div class="card-body"><?php print t('PraticienR4')?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>