<footer class="site-footer section">
    <div class="bg-darker">
        <div class="container text-center py-4">
            <nav class="nav justify-content-center my-4"><a href="https://www.facebook.com/doctoryl.official"
                                                            class="mr-4 font-regular text-contrast">
                    <i class="fab fa-facebook"></i>
                    <span>/doctoryl.offical</span></a> <a href="https://www.instagram.com/doctoryl.be/"
                                                          class="mr-4 font-regular text-contrast"><i
                            class="fab fa-twitter-square"></i><span>/doctoryl.be</span></a> <a
                        href="https://www.linkedin.com/company/doctoryl"
                        class="mr-4 font-regular text-contrast"><i
                            class="fab fa-linkedin"></i><span>/company/doctoryl</span></a></nav>
        </div>
    </div>
    <div class="container pb-3">
        <div class="row gap-y text-center text-md-left">
            <div class="col-md-4 mr-auto"><img src="../img/logo-dark.png" alt="" class="logo">
                <p><?php print t('Footer1') ?></p>
            </div>
            <div class="col-md-2" style="display: none;">
                <h6 class="py-2 bold">Company</h6>
                <nav class="nav flex-column"><a class="nav-item py-2" href="about.html">About</a> <a
                            class="nav-item py-2" href="#">Services</a> <a class="nav-item py-2"
                                                                           href="blog/blog-grid.html">Blog</a></nav>
            </div>
            <div class="col-md-2" style="display: none;">
                <h6 class="py-2 bold">Product</h6>
                <nav class="nav flex-column"><a class="nav-item py-2" href="#">Features</a> <a class="nav-item py-2"
                                                                                               href="#">API</a> <a
                            class="nav-item py-2" href="#">Customers</a></nav>
            </div>
            <div class="col-md-2" style="display: none;">
                <h6 class="py-2 bold">Channels</h6>
                <nav class="nav flex-column"><a class="nav-item py-2" href="#">Careers</a> <a class="nav-item py-2"
                                                                                              href="#">Contact</a> <a
                            class="nav-item py-2" href="#">Search</a></nav>
            </div>
        </div>
        <hr class="mt-5">
        <div class="row small align-items-center">
            <div class="col-md-4">
                <p class="mt-2 mb-md-0 text-secondary text-center text-md-left">© 2021 Doctoryl. All Rights Reserved</p>
            </div>
            <div class="col-md-8">
                <nav class="nav justify-content-center justify-content-md-end"><a href="https://www.facebook.com/doctoryl.official" target="_blank"
                                                                                  class="btn btn-circle btn-sm btn-secondary mr-3 op-4"><i
                                class="fab fa-facebook"></i></a> <a href="#"
                                                                    class="btn btn-circle btn-sm btn-secondary mr-3 op-4" target="_blank"><i
                                class="fab fa-twitter"></i></a> <a href="https://www.instagram.com/doctoryl.be/" target="_blank"
                                                                   class="btn btn-circle btn-sm btn-secondary op-4"><i
                                class="fab fa-instagram"></i></a></nav>
            </div>
        </div>
    </div>
</footer>