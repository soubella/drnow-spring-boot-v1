<!-- Making stripe menu -->
<nav class="st-nav navbar main-nav navigation fixed-top" id="main-nav">
    <div class="container">
        <ul class="st-nav-menu nav navbar-nav">
            <li class="st-nav-section nav-item">
                <a href="../" class="navbar-brand">
                    <img src="../img/logo-dark.png" alt="Doctoryl" class="logo logo-sticky d-block d-md-none">
                    <img src="../img/logo-light.png" alt="Doctoryl" class="logo d-none d-md-block">
                </a>
            </li>
            <li class="st-nav-section st-nav-primary nav-item">
                <a class="st-root-link nav-link nav-link2" href="../"><?php print t('Home')?></a>
                <a class="st-root-link item-products nav-link nav-link2" href="#advantages"><?php print t('Advantage')?></a>
                <a class="st-root-link item-products nav-link nav-link2" href="#pricing"><?php print t('Pricing')?></a>
                <a class="st-root-link item-company nav-link nav-link2" href="#faqs"><?php print t('Faqs')?></a>
            </li>
            <li class="st-nav-section st-nav-secondary nav-item">
                <a class="mr-3">
                    <select id="lang-selector" class="form-control bold btn-solid btn-outline custom-selector">
                        <?php
                        foreach ($supportedLanguages as $value) {
                            if($value==$language){
                                echo '<option class="custom-selector-item" selected>'.strtoupper($value).'</option>';
                            }else
                                echo '<option class="custom-selector-item">'.strtoupper($value).'</option>';
                        }
                        ?>
                    </select>
                </a>
                <a class="btn btn-rounded btn-outline mr-3 px-3" href="<?php print DOCTOR_LOGIN_URL?>">
                    <i class="fas fa-sign-in-alt d-none d-md-inline mr-md-0 mr-lg-2"></i>
                    <span class="d-md-none d-lg-inline"><?php print strtoupper(t('Login'))?></span>
                </a>
                <a class="btn btn-rounded btn-solid btn-outline px-3" href="<?php print DOCTOR_SIGNUP_URL?>">
                    <i class="fas fa-sign-in-alt d-none d-md-inline mr-md-0 mr-lg-2"></i>
                    <span class="d-md-none d-lg-inline"><?php print strtoupper(t('SignUp'))?></span>
                </a>
            </li><!-- Mobile Navigation -->
            <li class="st-nav-section st-nav-mobile nav-item">
                <button class="st-root-link navbar-toggler" type="button">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="st-popup">
                    <div class="st-popup-container">
                        <a class="st-popup-close-button">Close</a>
                        <div class="st-dropdown-content-group">
                            <a class="regular text-info" href="../"><i class="far fa fa-home mr-2"></i> <?php print t('Home')?></a>
                            <a class="regular text-primary" href="#advantages"><i class="fa fa-check mr-2"></i> <?php print t('Advantage')?></a>
                            <a class="regular text-success" href="#pricing"><i class="fas fa-hand-holding-usd mr-2"></i> <?php print t('Pricing')?> </a>
                            <a class="regular text-warning" href="#faqs"><i class="far fa-question-circle mr-2"></i> <?php print t('Faqs')?> </a>
                        </div>
                        <div class="st-dropdown-content-group bg-light b-t">
                            <a href="<?php print DOCTOR_LOGIN_URL?>"><?php print strtoupper(t('Login'))?> <i class="fas fa-sign-in-alt"></i></a>
                            <a href="<?php print DOCTOR_SIGNUP_URL?>"><?php print strtoupper(t('SignUp'))?> <i class="fa fa-user-plus"></i></a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="st-dropdown-root">
        <div class="st-dropdown-bg">
            <div class="st-alt-bg"></div>
        </div>
        <div class="st-dropdown-container">
        </div>
    </div>
</nav>
