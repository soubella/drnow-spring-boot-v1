<header id="header-bg-doctor" class="header section parallax image-background overlay alpha-8 text-contrast" style="background-image: url('../img/doctor/doc-bg.jpeg')">
    <div class="container overflow-hidden">
        <div class="row">
            <div class="col-md-8">
                <p class="lead bold"><?php print t('Welcome')?></p>
                <h1 class="display-4 text-contrast light"><?php print t('WelcomeMessage1')?></h1>
                <h1 class="text-contrast">
                    <span class="typed bold display-4 display-md-3" data-strings='["<?php print t('Doctor')?>", "<?php print t('Therapist')?>", "<?php print t('MedicalExpert')?>"]'></span>
                </h1>
                <p class="lead bold"><?php print t('WelcomeMessage3')?></p>
                <nav class="nav mt-5"><a href="<?php print DOCTOR_SIGNUP_URL?>" class="nav-link btn btn btn-rounded btn-contrast btn-lg px-5 bold"><?php print strtoupper(t('WelcomeBtn'))?></a></nav>
            </div>
        </div>
    </div>
</header>
