<section id="features" class="section edge features-carousel" style="z-index: 1">
    <div class="container pb-5">
        <div class="section-heading mb-6 text-center">
            <h2 class="mt-3"><?php print t('FeatureTitle0')?></h2>
        </div>
        <div class="row gap-y text-center text-md-left">
            <div class="col-md-4 py-4 rounded shadow-hover"><svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock text-info mb-2"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>
                <h5 class="bold"><?php print t('FeatureTitle1')?></h5>
                <p class="text-secondary"><?php print t('FeatureContent1')?></p>
            </div>
            <div class="col-md-4 py-4 rounded shadow-hover"><svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-wind text-info mb-2"><path d="M9.59 4.59A2 2 0 1 1 11 8H2m10.59 11.41A2 2 0 1 0 14 16H2m15.73-8.27A2.5 2.5 0 1 1 19.5 12H2"></path></svg>
                <h5 class="bold"><?php print t('FeatureTitle2')?></h5>
                <p class="text-secondary"><?php print t('FeatureContent2')?></p>
            </div>
            <div class="col-md-4 py-4 rounded shadow-hover"><svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-box text-info mb-2"><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line></svg>
                <h5 class="bold"><?php print t('FeatureTitle3')?></h5>
                <p class="text-secondary"><?php print t('FeatureContent3')?></p>
            </div>
            <div class="col-md-4 py-4 rounded shadow-hover"><svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders text-info mb-2"><line x1="4" y1="21" x2="4" y2="14"></line><line x1="4" y1="10" x2="4" y2="3"></line><line x1="12" y1="21" x2="12" y2="12"></line><line x1="12" y1="8" x2="12" y2="3"></line><line x1="20" y1="21" x2="20" y2="16"></line><line x1="20" y1="12" x2="20" y2="3"></line><line x1="1" y1="14" x2="7" y2="14"></line><line x1="9" y1="8" x2="15" y2="8"></line><line x1="17" y1="16" x2="23" y2="16"></line></svg>
                <h5 class="bold"><?php print t('FeatureTitle4')?></h5>
                <p class="text-secondary"><?php print t('FeatureContent4')?></p>
            </div>
            <div class="col-md-4 py-4 rounded shadow-hover"><svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-target text-info mb-2"><circle cx="12" cy="12" r="10"></circle><circle cx="12" cy="12" r="6"></circle><circle cx="12" cy="12" r="2"></circle></svg>
                <h5 class="bold"><?php print t('FeatureTitle5')?></h5>
                <p class="text-secondary"><?php print t('FeatureContent5')?></p>
            </div>
            <div class="col-md-4 py-4 rounded shadow-hover"><svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bar-chart text-info mb-2"><line x1="12" y1="20" x2="12" y2="10"></line><line x1="18" y1="20" x2="18" y2="4"></line><line x1="6" y1="20" x2="6" y2="16"></line></svg>
                <h5 class="bold"><?php print t('FeatureTitle6')?></h5>
                <p class="text-secondary"><?php print t('FeatureContent6')?></p>
            </div>
        </div>
    </div>
</section>