

<section class="section">
    <div class="shape-wrapper">
        <div class="shape shape-background shape-right"></div>
        <div class="shape shape-background top shape-left bg-info op-1"></div>
    </div>
    <div class="container">
        <div class="row align-items-center text-center text-lg-left">
            <div class="col-12 col-md-7 col-lg-6 mr-lg-auto text-center text-md-left">
                <h2 class="bold"><?php print t('Counter1') ?></h2>
                <p class="text-secondary"><?php print t('Counter2') ?></p>
            </div>
            <div class="col-12 col-md-5 col-lg-5">
                <div class="row no-gutters">
                    <div class="col-6 mb-3 pr-3">
                        <div class="rounded shadow-box p-2 p-sm-3 d-flex align-items-center flex-wrap bg-contrast">
                            <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user mr-4 stroke-secondary">
                                <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                <circle cx="12" cy="7" r="4"></circle>
                            </svg>
                            <div class="text-left">
                                <p class="counter font-md bold m-0 text-info" style="visibility: visible;"><?php print t('Counter31') ?></p>
                                <p class="m-0"><?php print t('Counter3') ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 mb-3">
                        <div class="rounded shadow-box p-2 p-sm-3 d-flex align-items-center flex-wrap bg-contrast">
                            <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-download-cloud mr-4 stroke-secondary">
                                <polyline points="8 17 12 21 16 17"></polyline>
                                <line x1="12" y1="12" x2="12" y2="21"></line>
                                <path d="M20.88 18.09A5 5 0 0 0 18 9h-1.26A8 8 0 1 0 3 16.29"></path>
                            </svg>
                            <div class="text-left">
                                <p class="counter font-md bold m-0 text-info" style="visibility: visible;"><?php print t('Counter41') ?></p>
                                <p class="m-0"><?php print t('Counter4') ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 pr-3">
                        <div class="rounded shadow-box p-2 p-sm-3 d-flex align-items-center flex-wrap bg-contrast">
                            <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-box mr-4 stroke-secondary">
                                <path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path>
                                <polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline>
                                <line x1="12" y1="22.08" x2="12" y2="12"></line>
                            </svg>
                            <div class="text-left">
                                <p class="counter font-md bold m-0 text-info" style="visibility: visible;"><?php print t('Counter51') ?></p>
                                <p class="m-0"><?php print t('Counter5') ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

