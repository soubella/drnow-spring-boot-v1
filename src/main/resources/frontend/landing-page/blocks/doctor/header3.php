<header class="section header smart-business-header" style="font-family: Arial,serif">
    <div class="shape-wrapper">
        <img src="../img/doctor/bg.svg" class="main-background img-responsive" alt="">
        <div class="shape shape-background shape-top center-xy"></div>
        <div class="shape shape-background shape-right"></div>
    </div>
    <div class="container overflow-hidden">
        <div class="row gap-y">
            <div class="col-md-7">
                <h1 class="extra-bold display-md-3 font-md"><span
                            class="d-block light"><?php print t('HeadLine1') ?></span></h1>
                <p class="lead text-black"><?php print t('HeadLine2') ?></p>
                <nav class="nav mt-5"><a href="<?php print DOCTOR_SIGNUP_URL?>" class="nav-link btn btn-rounded btn-primary btn-lg bw-2"><?php print t('HeadLine3') ?></a></nav>
            </div>
        </div>
    </div>
    <div class="main-shape-wrapper">
        <div data-aos="fade-left" data-aos-delay="300" class="aos-init"><img src="../img/doctor/sanae.png"
                                                                             class="img-responsive main-shape" alt="">
            <img src="img/v6/header/anim-1.svg" class="anim anim-1 floating" alt=""> <img src="img/v6/header/anim-1.svg"
                                                                                          class="anim anim-2 floating"
                                                                                          alt=""> <img
                    src="img/v6/header/anim-1.svg" class="anim anim-3 floating" alt=""></div>
    </div>
</header>