<section id="pricing" class="edge">
    <div class="container bring-to-front pb-0 pt-3">
        <div class="section-heading">
            <div class="row justify-content-center">
                <div class="col-md-10 col-lg-8 text-center">
                    <h2><?php print t('Pricing')?></h2>
                </div>
            </div>
            <div class="text-center mt-4">
                <div class="btn-group btn-group-toggle pricing-table-basis" data-toggle="buttons" hidden>
                    <label class="btn btn-info active">
                        <input type="radio" name="pricing-value" value="monthly" autocomplete="off" checked="checked"> <?php print t('MonthlyPricing')?>
                    </label>
                    <label class="btn btn-info">
                        <input type="radio" name="pricing-value" value="yearly" autocomplete="off"> <?php print t('AnnualPricing')?>
                    </label>
                </div>
<!--                <p>(--><?php //print t('PricingSave')?><!--)</p>-->
            </div>
        </div>
        <div class="row align-items-center no-gutters justify-content-md-center">
            <div class="col-auto" style="z-index: 1">
                <div class="card border-0 rounded-lg shadow-lg mb-4 mb-md-0 aos-init aos-animate" data-aos="fade-up">
                    <div class="card-body py-4">
                        <div class="row">
                            <div class="col-xl-9 mx-auto">
                                <div class="pricing text-center mb-"5"">
                                    <h5 class="bold text-uppercase text-primary"><?php print t('PricingHeadLine2')?></h5>
                                    <hr class="my-4">
                                    <p><?php print t('PricingHeadLine')?></p>
                                    <div class="pricing-value"><span class="price display-lg-4 semibold odometer text-dark" data-monthly-price="<?php print t('PricingMonthlyPrice')?>" data-yearly-price="0.78"><?php print t('PricingMonthlyPrice')?></span></div>

                                </div>
                                <ul class="list-unstyled">
                                    <li>
                                        <div class="media align-items-center mb-3">
                                            <div class="icon-md bg-success p-2 rounded-circle center-flex mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-box stroke-contrast"><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line></svg></div>
                                            <div class="media-body"><?php print t('PricingFeature1')?></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media align-items-center mb-3">
                                            <div class="icon-md bg-success p-2 rounded-circle center-flex mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-airplay stroke-contrast"><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg></div>
                                            <div class="media-body"><?php print t('PricingFeature2')?></div>
                                        </div>
                                    </li>
<!--                                    <li>-->
<!--                                        <div class="media align-items-center mb-3">-->
<!--                                            <div class="icon-md bg-success p-2 rounded-circle center-flex mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock stroke-contrast"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg></div>-->
<!--                                            <div class="media-body">--><?php //print t('PricingFeature3')?><!--</div>-->
<!--                                        </div>-->
<!--                                    </li>-->
                                    <li>
                                        <div class="media align-items-center mb-3">
                                            <div class="icon-md bg-success p-2 rounded-circle center-flex mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-repeat stroke-contrast"><polyline points="17 1 21 5 17 9"></polyline><path d="M3 11V9a4 4 0 0 1 4-4h14"></path><polyline points="7 23 3 19 7 15"></polyline><path d="M21 13v2a4 4 0 0 1-4 4H3"></path></svg></div>
                                            <div class="media-body"><?php print t('PricingFeature4')?></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div><a href="<?php print DOCTOR_SIGNUP_URL?>" class="btn btn-primary btn-lg btn-block rounded-top-0 rounded-bottom py-4"><?php print t('GetItNow')?><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right ml-3"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg></a>
                </div>
            </div>
        </div>
    </div>
</section>