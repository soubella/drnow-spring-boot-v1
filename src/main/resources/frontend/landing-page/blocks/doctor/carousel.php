<section class="section features-carousel b-b" id="advantages">
    <div class="container pt-0">
        <div class="swiper-container swiper-container-initialized swiper-container-horizontal" data-sw-show-items="4" data-sw-autoplay="3500" data-sw-loop="true" data-sw-nav-arrows=".features-nav" data-sw-breakpoints="{&quot;1024&quot;: {&quot;slidesPerView&quot;: 4,&quot;spaceBetween&quot;: 40},&quot;992&quot;: {&quot;slidesPerView&quot;: 4,&quot;spaceBetween&quot;: 15},&quot;768&quot;: {&quot;slidesPerView&quot;: 4.5,&quot;spaceBetween&quot;: 15},&quot;576&quot;: {&quot;slidesPerView&quot;: 1.5,&quot;spaceBetween&quot;: 10}}">
            <div class="swiper-wrapper px-1" style="transition-duration: 0ms; transform: translate3d(-2031.25px, 0px, 0px);">
                <div class="swiper-slide px-2 px-sm-1 swiper-slide-duplicate swiper-slide-duplicate-prev" data-swiper-slide-index="1" style="width: 292.5px; margin-right: 20px;">
                    <div class="card border-0 shadow">
                        <div class="card-body">
                            <div class="rounded-circle bg-light p-3 d-flex align-items-center justify-content-center shadow icon-xl"><img src="../img/v6/icons/user.svg" class="img-responsive" alt=""></div>
                            <h4 class="mt-4"><span class="bold"><?php print t('Title1') ?></span></h4>
                            <p><?php print t('Benefit1') ?></p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide px-2 px-sm-1 swiper-slide-duplicate-active" data-swiper-slide-index="2" style="width: 292.5px; margin-right: 20px;">
                    <div class="card border-0 shadow">
                        <div class="card-body">
                            <div class="rounded-circle bg-light p-3 d-flex align-items-center justify-content-center shadow icon-xl"><img src="../img/v6/icons/worldwide.svg" class="img-responsive" alt=""></div>
                            <h4 class="mt-4"><span class="bold"><?php print t('Title2') ?></span></h4>
                            <p><?php print t('Benefit2') ?></p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide px-2 px-sm-1" data-swiper-slide-index="3" style="width: 292.5px; margin-right: 20px;">
                    <div class="card border-0 shadow">
                        <div class="card-body">
                            <div class="rounded-circle bg-light p-3 d-flex align-items-center justify-content-center shadow icon-xl"><img src="../img/v6/icons/like.svg" class="img-responsive" alt=""></div>
                            <h4 class="mt-4"><span class="bold"><?php print t('Title3') ?></span></h4>
                            <p><?php print t('Benefit3') ?></p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide px-2 px-sm-1" data-swiper-slide-index="4" style="width: 292.5px; margin-right: 20px;">
                    <div class="card border-0 shadow">
                        <div class="card-body">
                            <div class="rounded-circle bg-light p-3 d-flex align-items-center justify-content-center shadow icon-xl"><img src="../img/v6/icons/graph.svg" class="img-responsive" alt=""></div>
                            <h4 class="mt-4"><span class="bold"><?php print t('Title4') ?></span></h4>
                            <p><?php print t('Benefit4') ?></p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide px-2 px-sm-1" data-swiper-slide-index="5" style="width: 292.5px; margin-right: 20px;">
                    <div class="card border-0 shadow">
                        <div class="card-body">
                            <div class="rounded-circle bg-light p-3 d-flex align-items-center justify-content-center shadow icon-xl"><img src="../img/v6/icons/chat.svg" class="img-responsive" alt=""></div>
                            <h4 class="mt-4"><span class="bold"><?php print t('Title5') ?></span></h4>
                            <p><?php print t('Benefit5') ?></p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide px-2 px-sm-1" data-swiper-slide-index="6" style="width: 292.5px; margin-right: 20px;">
                    <div class="card border-0 shadow">
                        <div class="card-body">
                            <div class="rounded-circle bg-light p-3 d-flex align-items-center justify-content-center shadow icon-xl"><img src="../img/v6/icons/strategy.svg" class="img-responsive" alt=""></div>
                            <h4 class="mt-4"><span class="bold"><?php print t('Title6') ?></span></h4>
                            <p><?php print t('Benefit6') ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Add Arrows -->
            <div class="text-primary features-nav features-nav-next" tabindex="0" role="button" aria-label="Next slide"><span class="text-uppercase small">Next</span> <i class="features-nav-icon fas fa-long-arrow-alt-right"></i></div>
            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
        </div>
    </div>
</section>