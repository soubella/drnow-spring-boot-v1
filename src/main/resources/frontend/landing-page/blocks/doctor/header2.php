<header class="header online-payment-header section text-contrast">
    <div id="stripes"><span></span><span></span><span></span><span></span><span></span></div>
    <div class="pb-md-0 pl-md-6 pr-md-6 pt-md-6 container-fluid overflow-hidden bring-to-front">
        <div class="row">
            <div class="col-md-12 col-xl-6">
                <h2 class="text-contrast bold"><?php print t('Header1')?> <h3 class="d-block light" style="color: white;"><?php print t('Header2')?></h3></h2>
                <ul class="lead">
                    <li><?php print t('Header3')?></li>
                    <li><?php print t('Header4')?></li>
                    <li><?php print t('Header5')?><strong><?php print t('Header5-1')?></strong></li>
                </ul>
                <nav class="nav mt-5">
                    <a href="#" class="nav-link mr-3 btn btn btn-rounded btn-contrast">
                        <i class="fas fa-tag mr-3"></i> Plans &amp; <?php print t('Pricing')?> </a>
                    <a href="#" class="nav-link btn btn-rounded btn-success"><?php print t('HeaderCta')?>
                    </a>
                </nav>
            </div>
            <div class="col-md-12 col-xl-6 pt-md-6"><img src="../img/doctor/doctors.png" class="pt-4 pt-md-0 img-hero img-responsive"  alt=""></div>
        </div>
    </div>
</header>