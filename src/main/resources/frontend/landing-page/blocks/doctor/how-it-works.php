<section id="howitworks" class="section why-us overflow-hidden">
    <div class="shape-wrapper">
        <div class="absolute shape-background top right" style="background: linear-gradient(280deg, #05d5ff 10%, #a6ffcb 94%);"></div>
    </div>
    <div class="container">
        <div class="section-heading text-center">
            <h2 class="bold"><?php print t('HowItWorks')?></h2>
            <p class="lead text-secondary">Our mission is to provide you with an all-in-one template so you don't have to look aside in order to get what you need</p>
        </div>
        <div class="row gap-y">
            <div class="col-md-5">
                <ul class="list-unstyled why-icon-list">
                    <li class="list-item">
                        <div class="media">
                            <div class="rounded-circle bg-info shadow-info text-contrast p-3 icon-xl d-flex align-items-center justify-content-center mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-cloud cloud"><path d="M18 10h-1.26A8 8 0 1 0 9 20h9a5 5 0 0 0 0-10z"></path></svg></div>
                            <div class="media-body">
                                <h5 class="bold"><?php print t('DocHowTitle1')?></h5>
                                <p class="my-0"><?php print t('DocHowContent1')?></p>
                            </div>
                        </div>
                    </li>
                    <li class="list-item">
                        <div class="media">
                            <div class="rounded-circle bg-success shadow-success text-contrast p-3 icon-xl d-flex align-items-center justify-content-center mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg></div>
                            <div class="media-body">
                                <h5 class="bold"><?php print t('DocHowTitle2')?></h5>
                                <p class="my-0"><?php print t('DocHowContent2')?></p>
                            </div>
                        </div>
                    </li>
                    <li class="list-item">
                        <div class="media">
                            <div class="rounded-circle bg-alternate shadow-alternate text-contrast p-3 icon-xl d-flex align-items-center justify-content-center mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up thumbs-up"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></div>
                            <div class="media-body">
                                <h5 class="bold"><?php print t('DocHowTitle3')?></h5>
                                <p class="my-0"><?php print t('DocHowContent3')?></p>
                            </div>
                        </div>
                    </li>
                    <li class="list-item">
                        <div class="media">
                            <div class="rounded-circle bg-danger shadow-danger text-contrast p-3 icon-xl d-flex align-items-center justify-content-center mr-3"><svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-pie-chart pie-chart"><path d="M21.21 15.89A10 10 0 1 1 8 2.83"></path><path d="M22 12A10 10 0 0 0 12 2v10z"></path></svg></div>
                            <div class="media-body">
                                <h5 class="bold"><?php print t('DocHowTitle4')?></h5>
                                <p class="my-0"><?php print t('DocHowContent4')?></p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-7">
                <figure data-aos="fade-left" class="aos-init aos-animate"><img src="../img/automate-social/build.svg" class="img-responsive" alt=""></figure>
            </div>
        </div>
        <div class="mt-5 text-center">
            <a href="javascript:;" class="btn btn-primary btn-rounded"><?php print t('HeaderCta')?></a>
        </div>
    </div>
</section>