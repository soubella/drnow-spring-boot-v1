<header id="header-bg" class="header section parallax image-background overlay alpha-8 text-contrast">
    <div id="header-mobile-bg" class="row">
<!--        <img class="img-responsive" src="img/patient/mobile-bg.jpg" alt="">-->
    </div>
    <div id="header-mobile-text" class="container overflow-hidden" style="font-family: 'Verdana',sans-serif">
        <div class="row">
            <div class="col-md-12">
                <p class="lead bold text-black" style="display: none"><?php print t('Welcome')?></p>
                <h1 class="text-black bold display-md-4"><?php print t('WelcomeMessage1')?></h1>
                <div id="headline">
                    <h1 class="text-black bold  display-md-3">
                        <span id="headline-text" class="typed bold display-4 display-md-3" data-strings='["<?php print t('Doctor')?>", "<?php print t('Therapist')?>", "<?php print t('MedicalExpert')?>"]'></span>
                    </h1>
                </div>
                <h4 class=" text-black"><?php print t('WelcomeMessage3')?></h4>
                <nav class="nav mt-3     mt-lg-5 mt-md-5"><a href="teleconsultation" class="nav-link btn btn btn-rounded btn-contrast btn-lg btn-cta px-5 bold" style="font-size: 1rem"><?php print strtoupper(t('WelcomeBtn'))?></a></nav>
            </div>
        </div>
    </div>
</header>
