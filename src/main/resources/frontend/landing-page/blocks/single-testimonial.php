<section class="singl-testimonial shadow">
    <div class="container-fluid py-0">
        <div class="row align-items-center gradient gradient-blue-navy text-contrast">
            <div class="col-md-3 mx-auto py-4 py-md-0">
<!--                <p class="rounded-pill py-2 px-4 text-uppercase mb-0 badge badge-contrast">Dashcore is great for</p>-->
                <p class="font-md bold mt-1 text-contrast"><?php print t('single-testimonial1')?></p>
                <hr class="my-4">
                <div class="small text-contrast"><?php print t('single-testimonial2')?></div>
            </div>
            <div class="col-12 col-md-7 p-0">
                <img class="img-responsive" src="img/patient/doctors.jpg" />
            </div>
        </div>
    </div>
</section>