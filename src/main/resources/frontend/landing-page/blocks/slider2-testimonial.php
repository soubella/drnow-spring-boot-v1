<section class="section bg-light edge bottom-right">
    <div class="container">
        <div class="section-heading text-center">
            <i class="fas fa-quote-right fa-3x text-danger mb-3"></i>
            <h2 class="bold display-4">Testimonials</h2>
        </div>
        <div class="testimonials-slider">
            <div class="swiper-container pb-5 swiper-container-initialized swiper-container-horizontal">
                <div class="swiper-wrapper text-center w-50" style="transition-duration: 0ms; transform: translate3d(-930px, 0px, 0px);">
                    <div class="swiper-slide swiper-slide-duplicate swiper-slide-prev" data-swiper-slide-index="3" style="width: 930px;">
                        <div class="d-flex flex-column align-items-center">
                            <img src="img/avatar/4.jpg" alt="" class="rounded-circle shadow mb-4">
                            <p class="w-75 lead mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus asperiores consequatur cum distinctio, dolorem earum error esse ex fugiat inventore maiores minima, non placeat praesentium quam quas ut, vero voluptatem.</p>
                            <hr class="w-50">
                            <footer><cite class="bold text-primary text-capitalize">— Jane Doe,</cite> <span class="small text-secondary mt-0">Awesome Company</span></footer>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="width: 930px;">
                        <div class="d-flex flex-column align-items-center">
                            <img src="img/avatar/1.jpg" alt="" class="rounded-circle shadow mb-4">
                            <p class="w-75 lead mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus asperiores consequatur cum distinctio, dolorem earum error esse ex fugiat inventore maiores minima, non placeat praesentium quam quas ut, vero voluptatem.</p>
                            <hr class="w-50">
                            <footer><cite class="bold text-primary text-capitalize">— Jane Doe,</cite> <span class="small text-secondary mt-0">Awesome Company</span></footer>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-next" data-swiper-slide-index="1" style="width: 930px;">
                        <div class="d-flex flex-column align-items-center">
                            <img src="img/avatar/2.jpg" alt="" class="rounded-circle shadow mb-4">
                            <p class="w-75 lead mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus asperiores consequatur cum distinctio, dolorem earum error esse ex fugiat inventore maiores minima, non placeat praesentium quam quas ut, vero voluptatem.</p>
                            <hr class="w-50">
                            <footer><cite class="bold text-primary text-capitalize">— Jane Doe,</cite> <span class="small text-secondary mt-0">Awesome Company</span></footer>
                        </div>
                    </div>
                    <div class="swiper-slide" data-swiper-slide-index="2" style="width: 930px;">
                        <div class="d-flex flex-column align-items-center">
                            <img src="img/avatar/3.jpg" alt="" class="rounded-circle shadow mb-4">
                            <p class="w-75 lead mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus asperiores consequatur cum distinctio, dolorem earum error esse ex fugiat inventore maiores minima, non placeat praesentium quam quas ut, vero voluptatem.</p>
                            <hr class="w-50">
                            <footer><cite class="bold text-primary text-capitalize">— Jane Doe,</cite> <span class="small text-secondary mt-0">Awesome Company</span></footer>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-duplicate-prev" data-swiper-slide-index="3" style="width: 930px;">
                        <div class="d-flex flex-column align-items-center">
                            <img src="img/avatar/4.jpg" alt="" class="rounded-circle shadow mb-4">
                            <p class="w-75 lead mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus asperiores consequatur cum distinctio, dolorem earum error esse ex fugiat inventore maiores minima, non placeat praesentium quam quas ut, vero voluptatem.</p>
                            <hr class="w-50">
                            <footer><cite class="bold text-primary text-capitalize">— Jane Doe,</cite> <span class="small text-secondary mt-0">Awesome Company</span></footer>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active" data-swiper-slide-index="0" style="width: 930px;">
                        <div class="d-flex flex-column align-items-center">
                            <img src="img/avatar/1.jpg" alt="" class="rounded-circle shadow mb-4">
                            <p class="w-75 lead mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus asperiores consequatur cum distinctio, dolorem earum error esse ex fugiat inventore maiores minima, non placeat praesentium quam quas ut, vero voluptatem.</p>
                            <hr class="w-50">
                            <footer><cite class="bold text-primary text-capitalize">— Jane Doe,</cite> <span class="small text-secondary mt-0">Awesome Company</span></footer>
                        </div>
                    </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 3"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 4"></span></div>
                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
            </div>
        </div>
    </div>
</section>

