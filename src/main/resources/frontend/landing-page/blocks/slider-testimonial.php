<section class="section slider-testimonials bg-light">
    <div class="container bring-to-front">
        <div class="section-heading text-center">
            <h2>Our customers have something to say</h2>
            <p class="lead text-muted">They are the best, our customer want to let you know how DashCore is helping them to achieve their goals and how simple is to use it</p>
        </div>
        <div class="card shadow-lg">
            <div class="row no-gutters">
                <div class="col-md-6">
                    <!-- Images slider, will fade -->
                    <div class="swiper-container h-100 swiper-container-fade swiper-container-initialized swiper-container-horizontal" data-sw-effect="fade" data-sw-space-between="0">
                        <div class="swiper-wrapper" style="transition-duration: 0ms;"><div class="swiper-slide swiper-slide-duplicate swiper-slide-prev" data-swiper-slide-index="3" style="width: 464px; opacity: 1; transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
                                <figure class="m-0 image-background cover" style="background-image: url(img/testimonials/6.jpg)"><img src="img/testimonials/6.jpg" alt="..." class="img--responsive invisible"></figure>
                            </div>
                            <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="width: 464px; opacity: 1; transform: translate3d(-464px, 0px, 0px); transition-duration: 0ms;">
                                <figure class="m-0 image-background cover" style="background-image: url(img/testimonials/3.jpg)"><img src="img/testimonials/3.jpg" alt="..." class="img--responsive invisible"></figure>
                            </div>
                            <div class="swiper-slide swiper-slide-next" data-swiper-slide-index="1" style="width: 464px; opacity: 0; transform: translate3d(-928px, 0px, 0px); transition-duration: 0ms;">
                                <figure class="m-0 image-background cover" style="background-image: url(img/testimonials/2.jpg)"><img src="img/testimonials/2.jpg" alt="..." class="img--responsive invisible"></figure>
                            </div>
                            <div class="swiper-slide" data-swiper-slide-index="2" style="width: 464px; opacity: 0; transform: translate3d(-1392px, 0px, 0px); transition-duration: 0ms;">
                                <figure class="m-0 image-background cover" style="background-image: url(img/testimonials/5.jpg)"><img src="img/testimonials/5.jpg" alt="..." class="img--responsive invisible"></figure>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate-prev" data-swiper-slide-index="3" style="width: 464px; opacity: 0; transform: translate3d(-1856px, 0px, 0px); transition-duration: 0ms;">
                                <figure class="m-0 image-background cover" style="background-image: url(img/testimonials/6.jpg)"><img src="img/testimonials/6.jpg" alt="..." class="img--responsive invisible"></figure>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active" data-swiper-slide-index="0" style="width: 464px; opacity: 0; transform: translate3d(-2320px, 0px, 0px); transition-duration: 0ms;">
                                <figure class="m-0 image-background cover" style="background-image: url(img/testimonials/3.jpg)"><img src="img/testimonials/3.jpg" alt="..." class="img--responsive invisible"></figure>
                            </div></div>
                        <div class="divider">
                            <div></div>
                        </div>
                        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div><!-- Prev button -->
                    <div class="swiper-button swiper-button-prev shadow" tabindex="0" role="button" aria-label="Previous slide"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg></div>
                </div>
                <div class="col-md-6">
                    <!-- Testimonials slider, will slide -->
                    <div class="swiper-container h-100 swiper-container-initialized swiper-container-horizontal">
                        <div class="swiper-wrapper" style="transform: translate3d(-464px, 0px, 0px); transition-duration: 0ms;"><div class="swiper-slide swiper-slide-duplicate swiper-slide-prev" data-swiper-slide-index="3" style="width: 464px;">
                                <div class="card-body h-100 d-flex flex-column justify-content-center">
                                    <blockquote class="blockquote text-center mb-0">
                                        <figure class="mockup mb-5"><img src="img/logos/companies/2.svg" alt="..." class="img-responsive"></figure>
                                        <p class="mb-5 mb-md-6">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo harum eaque voluptatibus est obcaecati exercitationem maxime illo nihil voluptatem.</p>
                                        <footer class="blockquote-footer"><span class="h6 text-uppercase">5studios team</span></footer>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="width: 464px;">
                                <div class="card-body h-100 d-flex flex-column justify-content-center">
                                    <blockquote class="blockquote text-center mb-0">
                                        <figure class="mockup mb-5"><img src="img/logos/companies/1.svg" alt="..." class="img-responsive"></figure>
                                        <p class="mb-5 mb-md-6">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Atque, quas sunt enim tempore minima tenetur voluptatem provident. Incidunt accusantium.</p>
                                        <footer class="blockquote-footer"><span class="h6 text-uppercase">Jane Doe</span></footer>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-next" data-swiper-slide-index="1" style="width: 464px;">
                                <div class="card-body h-100 d-flex flex-column justify-content-center">
                                    <blockquote class="blockquote text-center mb-0">
                                        <figure class="mockup mb-5"><img src="img/logos/companies/2.svg" alt="..." class="img-responsive"></figure>
                                        <p class="mb-5 mb-md-6">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo harum eaque voluptatibus est obcaecati exercitationem maxime illo nihil voluptatem.</p>
                                        <footer class="blockquote-footer"><span class="h6 text-uppercase">John Doe</span></footer>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="swiper-slide" data-swiper-slide-index="2" style="width: 464px;">
                                <div class="card-body h-100 d-flex flex-column justify-content-center">
                                    <blockquote class="blockquote text-center mb-0">
                                        <figure class="mockup mb-5"><img src="img/logos/companies/1.svg" alt="..." class="img-responsive"></figure>
                                        <p class="mb-5 mb-md-6">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Atque, quas sunt enim tempore minima tenetur voluptatem provident. Incidunt accusantium.</p>
                                        <footer class="blockquote-footer"><span class="h6 text-uppercase">Mauro</span></footer>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate-prev" data-swiper-slide-index="3" style="width: 464px;">
                                <div class="card-body h-100 d-flex flex-column justify-content-center">
                                    <blockquote class="blockquote text-center mb-0">
                                        <figure class="mockup mb-5"><img src="img/logos/companies/2.svg" alt="..." class="img-responsive"></figure>
                                        <p class="mb-5 mb-md-6">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo harum eaque voluptatibus est obcaecati exercitationem maxime illo nihil voluptatem.</p>
                                        <footer class="blockquote-footer"><span class="h6 text-uppercase">5studios team</span></footer>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active" data-swiper-slide-index="0" style="width: 464px;">
                                <div class="card-body h-100 d-flex flex-column justify-content-center">
                                    <blockquote class="blockquote text-center mb-0">
                                        <figure class="mockup mb-5"><img src="img/logos/companies/1.svg" alt="..." class="img-responsive"></figure>
                                        <p class="mb-5 mb-md-6">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Atque, quas sunt enim tempore minima tenetur voluptatem provident. Incidunt accusantium.</p>
                                        <footer class="blockquote-footer"><span class="h6 text-uppercase">Jane Doe</span></footer>
                                    </blockquote>
                                </div>
                            </div></div>
                        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div><!-- Next button -->
                    <div class="swiper-button swiper-button-next shadow" tabindex="0" role="button" aria-label="Next slide"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg></div>
                </div>
            </div>
        </div>
    </div>
</section>