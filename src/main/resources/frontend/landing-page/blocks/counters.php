<section class="section image-background contain center-bottom" style="background-image: url(img/shps/map-dots.svg)">
    <div class="container">
        <div class="text-center mb-6">
            <h2 class="bold"><?php print t('counters-0')?></h2>
        </div>
        <div class="row gap-y text-center">
            <div class="col-6 col-md-4">
                <p class="counter regular text-primary font-md display-md-4 my-0" style="visibility: visible;">96%</p>
                <p class="text-secondary regular m-0"><?php print t('counters-1')?></p>
            </div>
            <div class="col-6 col-md-4">
                <p class="counter regular text-primary font-md display-md-4 my-0" style="visibility: visible;">82%</p>
                <p class="text-secondary regular m-0"><?php print t('counters-2')?></p>
            </div>
            <div class="col-6 col-md-4">
                <p class="counter regular text-primary font-md display-md-4 my-0" style="visibility: visible;">+650 000</p>
                <p class="text-secondary regular m-0"><?php print t('counters-3')?></p>
            </div>
        </div>
    </div>
</section>