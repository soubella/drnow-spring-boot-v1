<section class="section overflow-hidden bg-light">
    <div class="container bring-to-front">
        <div class="row gap-y align-items-center">
            <div class="col-md-6 col-lg-5 mr-lg-auto">
                <div class="center-xy op-1">
                    <div class="shape shape-background rounded-circle shadow-lg bg-info aos-init aos-animate" style="width: 600px; height: 600px;" data-aos="zoom-in"></div>
                </div>
                <div class="device-twin align-items-center">
                    <div class="mockup absolute aos-init aos-animate" data-aos="fade-left">
                        <div class="screen"><img src="img/screens/app/3.png" alt="..."></div><span class="button"></span>
                    </div>
                    <div class="iphone-x front mr-0">
                        <div class="screen shadow-box"><img src="img/patient/screens/2.jpeg" alt="..."></div>
                        <div class="notch"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 text-center text-md-left">
                <div class="section-heading">
                    <h2 class="bold" style="font-size: 2.7rem !important">
<!--                        <i class="fa fa-trophy fa-2x text-danger "></i> -->
                        <?php print t('Marketing0')?></h2>
                </div>
                <div class="row gap-y">
                    <div class="col-12">
                        <div class="media flex-column flex-lg-row align-items-center align-items-md-start">
                            <img _ngcontent-hqu-c7="" class="mx-auto mr-md-3 mt-3" width="40" src="img/patient/marketing/check.png">
                            <div class="media-body mt-3 mt-md-0">
                                <h5 class="mt-3 mb-1"><?php print t('Marketing1')?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="media flex-column flex-lg-row align-items-center align-items-md-start">
                            <img _ngcontent-hqu-c7="" class="mx-auto mr-md-3 mt-3" width="40" src="img/patient/marketing/check.png">
                            <div class="media-body mt-3 mt-md-0">
                                <h5 class="mt-3 mb-1"><?php print t('Marketing2')?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="media flex-column flex-lg-row align-items-center align-items-md-start">
                            <img _ngcontent-hqu-c7="" class="mx-auto mr-md-3 mt-3" width="40" src="img/patient/marketing/check.png">
                            <div class="media-body mt-3 mt-md-0">
                                <h5 class="mt-3 mb-1"><?php print t('Marketing3')?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="media flex-column flex-lg-row align-items-center align-items-md-start">
                            <img _ngcontent-hqu-c7="" class="mx-auto mr-md-3 mt-3" width="40" src="img/patient/marketing/lock.png">
                            <div class="media-body mt-3 mt-md-0">
                                <h5 class="mt-3 mb-1"><?php print t('Marketing4')?></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>