<?php
session_start();
define('LANG_FRENCH', 'fr');
define('LANG_DUTCH', 'nl');
define('LANG_ENGLISH', 'en');
//$supportedLanguages=array('fr','nl','en');
$supportedLanguages=array('fr');

// Look for l in query string and set as language.
$language = isset($_GET['lang']) && in_array($_GET['lang'], $supportedLanguages) ? $_GET['lang'] : LANG_FRENCH;
if(isset($_SESSION['lang']) && !isset($_GET['lang'])){
    $language=$_SESSION['lang'];
}else{
    $_SESSION['lang']=$language;
}
// load associative array content.
require_once(SITE_ROOT."/languages/lang.".$language.".php");

/**
 * Translate a string
 * Use the $translate variable to define your set of language translations.
 * Note: Based off of Drupal's t() function.
 *
 * @param $string
 *  The string that is to be translated.
 * @param $args
 *  An array of placeholders that will populate the translated string.
 * @param $langcode
 *  The language you wish to translate the string to.
 */
function t($string, $args = array(), $langcode = NULL) {
    global $language, $translation;

    // Set language code.
    $langcode = isset($langcode) ? $langcode : $language;

    // Search for a translated string.
    if ( isset($translation[$langcode][$string]) ) {
        $string = $translation[$langcode][$string];
    }

    // Replace arguments if present.
    if ( empty($args) ) {
        return $string;
    } else {
        foreach ( $args as $key => $value ) {
            switch ( $key[0] ) {
                case '!':
                case '@':
                case '%':
                default: $args[$key] = $value; break;
            }
        }

        return strtr($string, $args);
    }
}