<?php

// Set our translations.
$translation['en'] = array(
    'Email' => 'Courriel',
    'Name' => 'Nom',
    'Organization' => 'Entreprise',
    'Phone Number' => 'Num&eacute;ro de t&eacute;l&eacute;phone',
    'Hello %name' => 'Bonjour %name',
);
