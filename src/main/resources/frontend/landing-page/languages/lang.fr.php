<?php

// Set our translations.
$translation['fr'] = array(
    // Patient
    "Home" => "Accueil",
    "HowItWorks" => "Comment ça marche ?",
    "Advantage" => "Avantages",
    "IamADoctor" => "Je suis Praticien",
    "Faqs" => "Faqs",
    "DownloadApp" => "Telecharger l'App",
    "Login" => "Se connecter",
    "SignUp" => "S'inscrire",
    "MyAccount" => "Mon Compte",

    "Welcome" => "Bienvenue !",
    "WelcomeMessage1" => "Consultez",
    "Doctor" => "un médecin",
    "Therapist" => "un thérapeute",
    "MedicalExpert" => "un expert médical",
    "WelcomeMessage3" => "depuis votre ordinateur ou votre smartphone.",
    "WelcomeBtn" => "Commencez Maintenant",

    "HowTitle1" => "1 - Inscrivez-vous gratuitement",
    "HowContent1" => "La création de votre compte sécurisé ne prend que quelques instants. Ensuite, vous êtes prêts pour recevoir une téléconsultation.",
    "HowTitle2" => "2 - Choisissez votre médecin",
    "HowContent2" => "Recherchez dans notre réseau de médecins certifiés et choisissez celui qui vous convient. Vous pouvez voir un médecin tout de suite ou planifier votre rendez-vous à une heure qui vous convient.",
    "HowTitle3" => "3 - Commencez votre consultation",
    "HowContent3" => "Un médecin, inscrit à l’Ordre belge, vous communique son avis, diagnostic, et ses recommandations.",

    "Marketing0" => "Consultez votre médecin où et quand vous voulez",
    "Marketing1" => "Améliorez votre prise en charge avec un suivi plus simple, plus fréquent et encore plus personnalisé",
    "Marketing2" => "Ne perdez plus votre temps dans les salles d’attente ou dans les embouteillages.",
    "Marketing3" => "Priorisez votre bien être avec un accès rapide au médecin",
    "Marketing4" => "Vos données sont sécurisées et soumises au secret médical. La confidentialité est notre priorité.",

    "single-testimonial1" => "Des soins de qualité commencent par des médecins de qualité.",
    "single-testimonial2" => "Médecins belges certifiés l   Les meilleurs en Europe  l   Formés en téléconsultation",

    "counters-0" => "Les chiffres prouvent l’efficacité de la téléconsultation",
    "counters-1" => "de patients satisfaits",
    "counters-2" => "de praticiens satisfaits",
    "counters-3" => "téléconsultations effectuées",

    // Doctor

    "HeadLine1" => "Connectez-Vous aux Patients",
    "HeadLine2" => "Fournissez des soins de qualité, optimisez votre temps, et libérez-vous de la paperasse, tout en augmentant vos revenus.",
    "HeadLine3" => "Commencer Maintenant",

    "Title1" => "Plus de liberté",
    "Benefit1" => "Travaillez où et quand vous voulez, il vous faut juste un ordinateur et une connexion internet.",

    "Title2" => "Gagnez du temps",
    "Benefit2" => "Ne perdez plus votre temps précieux dans les embouteillages et les déplacements.",

    "Title3" => "Augmentez vos revenus",
    "Benefit3" => "Rentabilisez vos heures creuses, et gagnez de l’argent depuis le confort de votre domicile.",

    "Title4" => "Enrichissez votre exercice",
    "Benefit4" => "La téléconsultation est le meilleur complément à votre pratique en cabinet ou en milieu hospitalier.",

    "Title5" => "Meilleur suivi",
    "Benefit5" => "Traitez vos patients en continu, les études ont prouvé que les consultations à distance augmentent la compliance des patients.",

    "Title6" => "Protégez l’environnement",
    "Benefit6" => "Doctoryl vous permet de réduire votre empreinte carbone, en éliminant les déplacements et l’utilisation du papier.",

    "Counter1" => "Mettez en valeur vos qualifications, votre expérience et votre temps",
    "Counter2" => "Adopter la téléconsultation vous permet de vous concentrer sur le soin de vos patients, d’augmenter vos revenus, et de libérer votre temps pour vos autres activités.",

    "Counter3" => "de patients satisfaits",
    "Counter31" => "96 % ",

    "Counter4" => "de soignants satisfaits",
    "Counter41" => "82 %",

    "Counter5" => "téléconsultations effectuées",
    "Counter51" => "+650 000",

    "Header1" => "Réalisez vos consultations en vidéo",
    "Header2" => "Pratiquez d’où et quand vous voulez",
    "Header3" => "Complétez et enrichissez votre exercice par la téléconsultation",
    "Header4" => "Restez en contact avec vos patients depuis votre smartphone, tablette ou ordinateur",
    "Header5" => "Période d’essai",
    "Header5-1" => "Gratuite.",
    "HeaderCta" => "Rejoindre Doctoryl",
    "Pricing" => "Tarifs",

    "DocHowTitle1" => "Inscription du patient",
    "DocHowContent1" => "Le patient crée son compte sur notre plateforme sécurisée.",
    "DocHowTitle2" => "Prise de Rendez-vous",
    "DocHowContent2" => "Le patient choisit le créneau qu’il lui convient parmi vos plages horaires de disponibilité, vous serez notifiés avant le début de chaque visite.",
    "DocHowTitle3" => "Dossier médical",
    "DocHowContent3" => "Le patient a la possibilité de compléter son dossier médical et ajouter le motif de consultation.",
    "DocHowTitle4" => "La consultation virtuelle",
    "DocHowContent4" => "Vous serez en communication avec votre patient via vidéo. La visite est soumise au secret médical et personne d’autre n’a accès à votre entretien.",

    "FeatureTitle0" => "Faites le bon choix en rejoignant Doctoryl",
    "FeatureTitle1" => "Confidentialité et Sécurité",
    "FeatureContent1" => "Travaillez avec les standards de sécurité et de confidentialité les plus exigeants",
    "FeatureTitle2" => "Assurez la continuité des soins",
    "FeatureContent2" => "Restez en contact plus souvent avec vos patients grâce aux consultations à distance, ceci permet d'augmenter leur compliance au traitement.",
    "FeatureTitle3" => "Gérer votre agenda comme vous voulez",
    "FeatureContent3" => "La téléconsultation permet de travailler partout et à tout moment. Gérez mieux vos horaires de travail et gagnez de précieuses heures pour votre temps libre",
    "FeatureTitle4" => "Participez à la prochaine révolution médicale!",
    "FeatureContent4" => "Comme tous les autres secteurs, la médecine évolue vers la digitalisation et l’incorporation des solutions technologiques dans la pratique de tous les jours.",
    "FeatureTitle5" => "Une plateforme simple et intuitive",
    "FeatureContent5" => "Doctoryl s’intègre facilement dans votre pratique quotidienne.",
    "FeatureTitle6" => "Accompagnement de A à Z",
    "FeatureContent6" => "Bénéficiez du meilleur service avec notre support disponible 7j/7, nous sommes toujours là pour répondre à vos besoins.",

    "PricingTile" => "Plans de prix abordables",
    "MonthlyPricing" => "Tarif Mensuel",
    "AnnualPricing" => "Tarif Annuel",
    "PricingSave" => "Économisez jusqu'à 30% sur le plan annuel",
    "PricingHeadLine" => "Développez votre cabinet et améliorez votre qualité de vie au travail",
    "PricingHeadLine2" => "Offre de lancement",
    "PricingMonthlyPrice" => "0",
    "PricingAnnualPrice" => "0",
    "PricingFeature1" => "Consultations vidéo illimitées",
    "PricingFeature2" => "Sans engagement",
    "PricingFeature3" => "Tout compris dans votre abonnement, il n’y a aucun autre frais",
    "PricingFeature4" => "Doctoryl est offert sans aucun frais pour une période limitée",
    "GetItNow" => "Rejoindre Doctoryl",

    "CommonQuestions" => "Questions fréquemment posées",
    "PraticienQ1" => "Qui peut rejoindre notre équipe médicale ?",
    "PraticienR1" => "Tous les professionnels de la santé certifiés voulant offrir des soins optimisés à leur patientèle en intégrant la téléconsultation à leur pratique quotidienne.",
    "PraticienQ2" => "De quoi ai-je besoin pour commencer à téléconsulter ?",
    "PraticienR2" => "Il vous suffit de disposer d’un ordinateur équipé d’une caméra, d’un microphone et d’une connexion internet pour pouvoir profiter de Doctoryl. Pas d'installation requise.",
    "PraticienQ3" => "Que contient l’abonnement à Doctoryl ?",
    "PraticienR3" => "Pour cette offre de lancement, vous aurez un accès Gratuit et illimité à Doctoryl. Après l’activation de votre compte, vous pourrez compléter votre profil soignant, gérer votre agenda de disponibilité/indisponibilité, et effectuer des consultations vidéo avec votre patientèle existante ou avec de nouveaux patients.",
    "PraticienQ4" => "Pourquoi Doctoryl est le meilleur service de téléconsultation en Belgique ?",
    "PraticienR4" => "Doctoryl est la plateforme de téléconsultation offrant le plus de fonctionnalités en Belgique. Le développement de Doctoryl a été réalisé en synergie avec les médecins et les patients. Doctoryl vous permet d’être toujours plus proche de vos patients en leur offrant des soins plus simples, plus rapides et plus personnalisés.",
    "PraticienQ5" => "Combien puis-je gagner ?",

    "Cta1" => "Prêt à commencer ?",
    "Cta2" => "Priorisez votre bien-être avec Doctoryl",
    "Cta3" => "Commencer maintenant",
    "Cta4" => "Consultez un Soignant",

    "Footer1" => "Doctoryl est une startup utilisant les derniers outils technologiques pour améliorer les soins de santé, en proposant une plateforme de téléconsultation qui permet à un professionnel médical de donner une consultation à un patient à distance.",
);
