<?php  include '../config.php'?>
<?php include SITE_ROOT.'/languages/inc.language.php'?>
<!doctype html>
<html lang="<?php print $language ?>">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge" /><![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1"><!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" href="../favicon.ico">
    <title>Doctoryl | Praticien</title><!-- themeforest:css -->
    <?php  include SITE_ROOT.'/blocks/doctor/ressources.php'?>
</head>
<body>
<?php  include SITE_ROOT.'/blocks/doctor/navbar.php'?>
<main>
    <?php  include SITE_ROOT.'/blocks/doctor/header3.php'?>

    <?php  include SITE_ROOT.'/blocks/doctor/carousel.php'?>

    <?php  include SITE_ROOT.'/blocks/doctor/counter.php'?>

    <?php  include SITE_ROOT.'/blocks/doctor/features.php'?>

<!--    --><?php // include SITE_ROOT.'/blocks/doctor/revolution.php'?>
<!---->
<!--    --><?php // include SITE_ROOT.'/blocks/doctor/reviews.php'?>

    <?php  include SITE_ROOT.'/blocks/doctor/pricing.php'?>

    <?php  include SITE_ROOT.'/blocks/doctor/faqs.php'?>

    <?php  include SITE_ROOT.'/blocks/doctor/cta.php'?>

    <?php  include SITE_ROOT.'/blocks/doctor/footer.php'?>
</main>
<?php  include SITE_ROOT.'/blocks/doctor/js-ressources.php'?>
</body>
</html>

