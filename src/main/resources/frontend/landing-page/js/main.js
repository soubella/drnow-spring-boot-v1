$( document ).ready(function() {
    //var images_bg=['bg1.jpg','bg2.jpeg','bg3.jpeg','bg4.jpeg', 'bg5.jpeg'];
    //var currentImage=4;
    //$("#header-bg").css("background-image",'url(img/patient/'+images_bg[currentImage]+')');
    // setInterval(function(){
    //     currentImage++;
    //     if(currentImage==4){
    //         currentImage=0;
    //     }
    //     $("#header-bg").css("background-image",'url(img/patient/'+images_bg[currentImage]+')');
    // }, 3000);

    var previousLang;
    $("#lang-selector").focus(function(){
        previousLang=this.value.toLowerCase();
    }).change(function () {
        var oldUrl=''+window.location;
        var n = oldUrl.indexOf('#');
        oldUrl = oldUrl.substring(0, n != -1 ? n : oldUrl.length);
        var newUrl;
        if(oldUrl.includes('?lang='+previousLang)){
            newUrl=oldUrl.replace('?lang='+previousLang,'?lang='+this.value.toLowerCase());
            window.location.replace(newUrl);
        }else {
            window.location.replace(oldUrl+'?lang='+this.value.toLowerCase());
        }
    });

});