<?php
    $root=pathinfo($_SERVER['SCRIPT_FILENAME']);
    define ('BASE_FOLDER', basename($root['dirname']));
    define ('SITE_ROOT',    realpath(dirname(__FILE__)));
    define ('SITE_URL',    'http://'.$_SERVER['HTTP_HOST'].'/'.BASE_FOLDER);

    define ('DOCTOR_LOGIN_URL', '../teleconsultation/doctor/account/login');
    define ('DOCTOR_SIGNUP_URL', '../teleconsultation/doctor/account/register');

    define ('PATIENT_SIGNUP_URL', '/teleconsultation/patient/account/auth/login');
    define ('PATIENT_SEARCH', '/teleconsultation/');